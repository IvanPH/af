/* ------------------------------------------------------------------------------
 *
 *  # Datatables data sources
 *
 *  Esta será la configuración general de los datatables
 *
 *  Version: 1.0
 *  Latest update: Aug 06, 2017
 *
 * ---------------------------------------------------------------------------- */

$(function() {
    // Setting datatable defaults
    $.extend( $.fn.dataTable.defaults, {
        autoWidth: false,
        colReorder: true,
        dom: '<"datatable-header"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
        language: {
            search: '<span>Búsqueda:</span> _INPUT_',
            lengthMenu: '<span>Mostrar:</span> _MENU_',
            loadingRecords: "Cargando...",
            info:           "Mostrando _START_ a _END_ de _TOTAL_ registros",
            zeroRecords: "No se encontraron registros con ese término de búsqueda",
            infoFiltered: "(búsqueda en _MAX_ de los registros)",
            paginate: { 'first': 'Primero', 'last': 'Ultimo', 'next': '&rarr;', 'previous': '&larr;' }
        },
        drawCallback: function () {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
        },
        preDrawCallback: function() {
            $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
        }

    });
});
