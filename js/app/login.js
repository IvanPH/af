function login(){
    deshabilitarboton("btn_login", 1);
    $.ajax({
        url: "connections/exeLogin.php",
        type: "POST",
        data: $("#frm_login").serialize(),
        success: function (data) {
            try {
                var json = JSON.parse(data);
                if(json.res == 1){
                    if(json.rolex == 3 && json.id > 0){
                        openLink3(2,json.id,"indice");
                    }else{
                        window.location.assign(json.goto);
                    }

                }
                else{
                    $('#error').html(
                        custom_alert('danger', '', json.goto, 1, 1)
                    );
                    $('#frm_login')[0].reset();
                    $("#user").focus();
                    habilitaboton("btn_login");
                }
            }catch(err) {
                $("#error").html(
                    custom_alert('danger', '¡Ocurrió un inconveniente! ... ', data, 1, 1)
                );
                habilitaboton('btn_login');
            }
        }
    });
}
