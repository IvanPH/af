<?php

class IndiceManual {
    public function index() {
        require_once('business/manual/indice/index_vw.php');
    }
    public function cap1() {
        require_once('business/manual/indice/cap1_vw.php');
    }
    public function cap2() {
        require_once('business/manual/indice/cap2_vw.php');
    }
    public function cap3() {
        require_once('business/manual/indice/cap3_vw.php');
    }
    public function cap4() {
        require_once('business/manual/indice/cap4_vw.php');
    }
    public function cap5() {
        require_once('business/manual/indice/cap5_vw.php');
    }
    public function cap6() {
        require_once('business/manual/indice/cap6_vw.php');
    }
    public function cap7() {
        require_once('business/manual/indice/cap7_vw.php');
    }
    public function cap8() {
        require_once('business/manual/indice/cap8_vw.php');
    }
    public function cap9() {
        require_once('business/manual/indice/cap9_vw.php');
    }
    public function cap10() {
        require_once('business/manual/indice/cap10_vw.php');
    }
  
}
?>
