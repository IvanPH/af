<?php

class clasificador {
    public function index() {
        require_once('business/catalogos/clasificador/index_vw.php');
    }
    public function ver() {
        require_once('business/catalogos/clasificador/editar_vw.php');
    }
    public function nuevo() {
        require_once('business/catalogos/clasificador/nuevo_vw.php');
    }
    public function error() {
        require_once('business/error.php');
    }
}
?>
