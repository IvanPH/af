<?php

class conceptos {
    public function index() {
        require_once('business/catalogos/conceptos/index_vw.php');
    }
    public function ver() {
        require_once('business/catalogos/conceptos/editar_vw.php');
    }
    public function nuevo() {
        require_once('business/catalogos/conceptos/nuevo_vw.php');
    }
    public function error() {
        require_once('business/error.php');
    }
}
?>
