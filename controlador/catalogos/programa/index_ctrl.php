<?php

class programa {
    public function index() {
        require_once('business/catalogos/programa/index_vw.php');
    }
    public function ver() {
        require_once('business/catalogos/programa/editar_vw.php');
    }
    public function nuevo() {
        require_once('business/catalogos/programa/nuevo_vw.php');
    }
    public function error() {
        require_once('business/error.php');
    }
}
?>
