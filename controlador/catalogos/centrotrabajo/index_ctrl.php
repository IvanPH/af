<?php

class centrotrabajo {
    public function index() {
        require_once('business/catalogos/centrotrabajo/index_vw.php');
    }
    public function ver() {
        require_once('business/catalogos/centrotrabajo/editar_vw.php');
    }
    public function nuevo() {
        require_once('business/catalogos/centrotrabajo/nuevo_vw.php');
    }
    public function error() {
        require_once('business/error.php');
    }
}
?>
