<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Global stylesheets -->
<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
<link href="<?php echo $raiz ?>css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
<link href="<?php echo $raiz ?>css/icons/fontawesome/styles.min.css" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="<?php echo $raiz ?>css/bootstrap.css?ver=1.9" rel="stylesheet" type="text/css">
<link href="<?php echo $raiz ?>css/core.css?ver=1.6" rel="stylesheet" type="text/css">
<link href="<?php echo $raiz ?>css/components.css?ver=3.9" rel="stylesheet" type="text/css">
<link href="<?php echo $raiz ?>css/colors.css?ver=2.03" rel="stylesheet" type="text/css">
<link href="<?php echo $raiz ?>css/style.css?v=1.05" rel="stylesheet" type="text/css">
<!-- <link href="<?php echo $raiz ?>css/app.css?ver=1.8" rel="stylesheet" type="text/css"> -->
<link href="<?php echo $raiz ?>css/app.css?v=1.10" rel="stylesheet" type="text/css">
<link href="<?php echo $raiz ?>css/cards.css" rel="stylesheet" type="text/css">
<!-- /global stylesheets -->
<!-- Core JS files -->
<script type="text/javascript" src="<?php echo $raiz ?>js/plugins/loaders/pace.min.js"></script>
<script type="text/javascript" src="<?php echo $raiz ?>js/core/libraries/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo $raiz ?>js/core/libraries/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo $raiz ?>js/plugins/loaders/blockui.min.js"></script>
<!-- /core JS files -->
<!-- Theme JS files -->
<script type="text/javascript" src="<?php echo $raiz ?>js/plugins/ui/moment/moment.min.js"></script>
<script type="text/javascript" src="<?php echo $raiz ?>js/plugins/ui/nicescroll.min.js"></script>
<script type="text/javascript" src="<?php echo $raiz ?>js/core/app.js"></script>
<script type="text/javascript" src="<?php echo $raiz ?>js/pages/layout_fixed_custom.js"></script>
<script type="text/javascript" src="<?php echo $raiz ?>js/plugins/ui/ripple.min.js"></script>

<link rel="shortcut icon" href="<?php echo $raiz ?>images/favicon.ico"/>