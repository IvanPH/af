<!-- Menú principal  -->
<div id="menu" class="sidebar sidebar-main sidebar-default sidebar-fixed">
    <div class="sidebar-content">
        <!-- Menú de Usuario -->
        <div class="sidebar-user-material">
        
            <div class="category-content">
                <!--<div class="sidebar-user-material-content">-->  
                <div class="sidebar-user">
                    <a href="http://localhost/presupuesto/business">
                    <h2 id="ISF" class="blue text-center sif">HEX</h2> </a>
                    <p class="text-center sif"><strong><small class="text-center">Sistema de activos fijos</small></strong></p>
                </div>

                <div class="sidebar-user-material-menu">
                    <a href="#user-nav" data-toggle="collapse"><span>Mi Cuenta</span> <i class="caret"></i></a>
                </div>
            </div>

            <div class="navigation-wrapper collapse" id="user-nav">
                <ul class="navigation">
                    <li><a href="<?php echo $raiz?>?fmc=sys&td=acount"><i class="icon-user-plus"></i> <span>Mi Perfil</span></a></li>
                    <!--  <li><a href="#"><i class="icon-coins"></i> <span>My balance</span></a></li>-->
                    <!--<li><a href="#"><i class="icon-comment-discussion"></i> <span><span class="badge bg-teal-400 pull-right">58</span> Mensajes</span></a></li> -->
                    <li class="divider"></li>
                    <li><a href="<?php echo $raiz?>business/sys/logout.php"><i class="icon-switch2"></i> <span>Salir</span></a></li>
                </ul>
            </div>
        </div>
        <!-- /- Menú de Usuario -->
        <!-- Menú de navegación -->
        <div class="sidebar-category sidebar-category-visible">
            <div class="category-content no-padding">
                <ul class="navigation navigation-main navigation-accordion">
                    <!-- Principal -->
                   <!--  <li class="navigation-header"><span>Navegación</span> <i class="icon-menu" title="Main pages"></i></li>-->
                    <li <?php if($checkMenu == $raiz."business/index.php"){ echo "class='active'";}?>>
                        <a href="<?php echo $raiz?>business/">
                            <i class="icon-home"></i>
                            <span>Inicio</span>
                        </a>
                    </li>
                    <?php
                    $result = $cInicial->menuParents($_SESSION[id_usr]);
                    $totalRows_row = $result->rowCount();
                    $menu = "";

                    while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
                        $has_ul = "";
                        $idpadre = $row['id_menu'];
                        //Consulta los hijos 
                        $result2 = $cInicial->menuChild($idpadre, $_SESSION[id_usr]);
                        $totalRows_row2 = $result2->rowCount();
                        //$ipadre = "";
                        $padreActivo ="";
                    if ($totalRows_row2 > 0) {

                       // $ipadre = "gui-folder ";
                        $padreActivo = "";
                        $menu = "<ul>";
                        while ($row2 = $result2->fetch(PDO::FETCH_ASSOC)){

                            if($sys_id_men == $row2['id']){
                                $activo = "active";
                                $padreActivo = "active";
                                $has_ul = "has-ul legitRipple";
                            }else{
                                $activo = "";
                            }
                            //$menu.="<li class='".$activo."'><a href=\"".$raiz.$row2['link']."\"><span class='title'>".$row2['texto']."</span></a></li>";
                            $menu.="<li class='".$activo. $has_ul."'>
                                        <a href=\"".$raiz.$row2['link']."\">".$row2['texto']."</a>
                                    </li>";
                        }
                        $menu.= "</ul>";
                    }
                    ?>
                        <li class="<?php echo $padreActivo ?>">
                            <a href="javascript:void(0);" class="<?php echo $has_ul?>">
                                <i class="<?php echo $row['class'] ;?>"></i>
                                <span><?php echo $row['texto'] ;?></span>
                            </a>
                            <?php
                            if($totalRows_row2!="0") {
                                echo $menu;
                            }
                            ?>
                        </li>
                    <?php
                    }
                    ?>
                </ul>
            </div>
        </div>
        <!-- /Menú navigación -->
    </div>
</div>
<!-- /Menú principal -->