<!-- Main navbar -->
<div class="navbar navbar-inverse navbar-fixed-top bg-blue">
    <div class="navbar-header">
        <a class="navbar-brand" href="<?php echo $raiz?>">
        <img src="<?php echo $raiz?>images/hex.png" alt="">
        </a>
        <ul class="nav navbar-nav visible-xs-block">
            <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
            <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
        </ul>
    </div>

    <div class="navbar-collapse collapse" id="navbar-mobile">
        <ul class="nav navbar-nav">
            <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
            <!-- Aquí se puede agregar el menu de la parte de arriba Izquierdo en caso de que se requiera-->
        </ul>

        <div class="navbar-right">
            <p class="navbar-text">¡Bienvenido, <?php echo $_SESSION[s_nombre]?>!</p>
            <p class="navbar-text"><span class="label bg-success-400">En linea</span></p>

            <!-- Aquí se puede agregar el menu de la parte de arriba en caso de que se requiera-->
        </div>
    </div>
</div>
<!-- /main navbar -->