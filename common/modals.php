<!--////////////////////////////-->
<!-- Modal  baja-->
<div class="modal small fade" id="idConfimaModalBA" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel">Confirmación</h3>
            </div>
            <div class="modal-body">
                <p class="error-text">
                    <i class="fa fa-warning fa-2x text-warning modal-icon"></i>
                    <span id="men"> ¿Está seguro que quieres dar de baja este registro?</span>
                </p>
                <input type="hidden" name="txtIdBaja" id="idBaja">
            </div>
            <div class="modal-footer">
                <div class="col-lg-offset-6 col-md-offset-6 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <button type="button" class="btn btn-block ink-reaction btn-danger" data-dismiss="modal" aria-hidden="true">
                        <i class="fa fa-times"></i> Cancelar
                    </button>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <button type="button" id="aceptar_baja" class="btn btn-block ink-reaction btn-success" onClick="baja()">
                        <i class="fa fa-check"></i> Aceptar
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- FinModal baja  -->
<!--////////////////////////////-->
<!-- Modal Alta / baja-->
<div class="modal small fade" id="idConfimaModalBA_estatus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel">Confirmación</h3>
                <div id="respuesta_ab_modal"></div>
            </div>
            <div class="modal-body">
                <p class="error-text">
                    <i class="fa fa-warning fa-2x text-warning modal-icon"></i>
                    <span id="men"> ¿Está seguro que quieres dar de baja este registro?</span>
                </p>
                <div class="row">
                    <div class="col-md-5">
                        <label for="txtObservaciones"> Observaciones: </label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10">
                        <textarea class="form-control input-sm" name="txtObservaciones" id="txtObservaciones" cols="60" rows="5"></textarea>
                    </div>
                </div>
                <input type="hidden" name="idBaja_ce" id="idBaja_ce">
            </div>
            <div class="modal-footer">
                <div class="col-lg-offset-6 col-md-offset-6 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <button type="button" class="btn btn-block ink-reaction btn-danger" data-dismiss="modal" aria-hidden="true">
                        <i class="fa fa-times"></i> Cancelar
                    </button>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <button type="button" id="aceptar_baja_e" class="btn btn-block ink-reaction btn-success" onClick="baja_estatus()">
                        <i class="fa fa-check"></i> Aceptar
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- FinModal Alta / baja  ->
<!-- Modal Eliminar Registro -->
<div class="modal small fade" id="idConfimaModalElimina" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel">Confirmación</h3>
            </div>
            <div class="modal-body">
                <p class="error-text">
                    <i class="fa fa-warning fa-2x text-danger modal-icon"></i>
                     <span id="men"><strong> ¿Está seguro que quieres borrar este registro?</strong></span>
                    <span> (Se eliminará por completo de la base de datos)</span>
                </p>
                <input type="hidden" name="idEliminar" id="idEliminar">
            </div>
            <div class="modal-footer">
                <div class="col-lg-offset-6 col-md-offset-6 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <button type="button" class="btn btn-block ink-reaction btn-danger" data-dismiss="modal" aria-hidden="true">
                        <i class="fa fa-times"></i> Cancelar
                    </button>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <button type="button" id="aceptar_eliminar" class="btn btn-block ink-reaction btn-success"
                            onClick="eliminar()">
                        <i class="fa fa-check"></i> Aceptar</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- FinModal Eliminar Registro -->
