<?php
//Incluyendo la conexión a la base de datos
require_once $dir_fc."connections/conn_data.php";
/**
 * * Operaciones y movimientos que se realizan para el menú y otras herramientas de inciio que viene de la base de datos
 */
class cParametros extends BD
{

    private $conn;
    private $id_estatus;
    private $admin;
    private $id_responsable;

    /**
     * @return mixed
     */
    public function getAdmin()
    {
        return $this->admin;
    }

    /**
     * @param mixed $admin
     */
    public function setAdmin($admin)
    {
        $this->admin = $admin;
    }

    /**
     * @return mixed
     */
    public function getIdResponsable()
    {
        return $this->id_responsable;
    }

    /**
     * @param mixed $id_responsable
     */
    public function setIdResponsable($id_responsable)
    {
        $this->id_responsable = $id_responsable;
    }



    /**
     * @return mixed
     */
    public function getIdEstatus()
    {
        return $this->id_estatus;
    }

    /**
     * @param mixed $id_estatus
     */
    public function setIdEstatus($id_estatus)
    {
        $this->id_estatus = $id_estatus;
    }



    function __construct()
    {
        //Esta es la que llama a la base de datos
        //parent::__construct();
        $this->conn = new BD();
    }
    private $id_usuario_captura;

    public function set_id_usuario_captura($id_usuario_captura){
        $this->id_usuario_captura = $id_usuario_captura;
    }

    public function get_id_usuario_captura(){
        return $this->id_usuario_captura;
    }

    public function getTipoMovimiento($valor){
        try {
            $queryMP = "SELECT valor FROM ws_parametro WHERE activo = 1 AND id_parametro = ".$valor;
            $result = $this->conn->prepare($queryMP);
            $result->execute();
            $rw = $result->fetch(PDO::FETCH_OBJ);
            $result = $rw->valor;

            return $result;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }

    public function getValorParametro($valor){
        //echo $valor;
        try {
            $queryMP = "SELECT valor FROM ws_parametro WHERE activo = 1 AND id_parametro = ".$valor;
            $result = $this->conn->prepare($queryMP);
            $result->execute();
            $rw = $result->fetch(PDO::FETCH_OBJ);
            $result = $rw->valor;

            return $result;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }


}
?>
