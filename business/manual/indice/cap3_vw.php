<?php
/*--------------------------------------------------------------------------------------------------------*/
$dir_fc = "";
include_once $dir_fc.'connections/trop.php';
include_once $dir_fc.'connections/php_config.php';

$sys_id_men   = 72;	//Corresponde al menú
$sys_tipo     = 0;	//Indicando que es una lista (0)
$real_sis     = "contratos/serviciosp";
$title_act    = "Equipo de computo";
$sub_t_act    = "Lista";
$fmc		  = "";
$busqueda     = "";
$id_estatus   = 1;

extract($_REQUEST);

if($id_estatus == 2){
    $sys_id_men = 23;
}

$dir          = dirname($_SERVER['PHP_SELF']);
$current_file = basename($_SERVER["PHP_SELF"]);
$checkMenu    = $server_name.$dir."/".$current_file;   //con este se checa si index està activo
$param        = "?fmc=".$fmc."&td=";

/*------------------------  Incluyendo e Instanciando Clases Inicial  -------------------------------*/
include_once $dir_fc.'data/inicial.class.php';
include_once $dir_fc.'data/contratos.class.php';
include_once $dir_fc.'common/parametros.class.php';
include_once $dir_fc.'common/function.class.php';

$cInicial   = new cInicial(); //Operaciones generales (Menu de incio, etc)
$parametro  = new cParametros();
$cLista     = new cConServiciosP();
$cFn        = new cFunction();
/*---------------------------------------------------------------------------------------------------*/
include_once 'business/sys/check_session.php';
$showBusqueda = "";
$msgBusqueda  = "";

$id_filtro_mov = 0;

if(isset($_SESSION[fil_tipo_mov_ofi])){
    /*
     * Nuevo Filtro de Búsqueda con el Select. para seleccionar los tipos de movimientos
     * */
    if($_SESSION[fil_tipo_mov_ofi] != ""){
        $id_filtro_mov = $_SESSION[fil_tipo_mov_ofi];
    }else{
        $_SESSION[fil_tipo_mov_ofi] = 0;
    }
}
IF($busqueda == "do_search_please"){
	if(isset($_SESSION[fil_descripcion])){
		if($_SESSION[fil_descripcion] != ""){
			$showBusqueda.= "<br> <strong>Objeto: </strong>". $_SESSION[fil_descripcion];
		}
	}
	if(isset($_SESSION[fil_no_fua])){
		if($_SESSION[fil_no_fua] != ""){
			$showBusqueda.= "<br> <strong>No. FUA: </strong>". $_SESSION[fil_no_fua];
		}
	}
	if(isset($_SESSION[fil_id])){
		if($_SESSION[fil_id] != ""){
			$showBusqueda.= "<br> <strong>FUA: </strong>". $_SESSION[fil_id];
		}
	}

	if(isset($_SESSION[fil_fecha_ini]) && isset($_SESSION[fil_fecha_fin])){
		if($_SESSION[fil_fecha_ini] != "" && $_SESSION[fil_fecha_fin] != ""){
			$showBusqueda.= "<br> <strong>Fecha </strong> Del: ".$cFn->formatear_fecha_show($_SESSION[fil_fecha_ini]). " 
                   al ".$cFn->formatear_fecha_show($_SESSION[fil_fecha_fin]);
		}
	}
	$msgBusqueda = $cFn->custom_alert('info', 'Resultados con la busqueda:', $showBusqueda, 1, 1);
}

$rs_parametro = $parametro->getTipoMovimiento(25);


?>
<!DOCTYPE html>
<html lang="es">
<head>
	<title><?php echo c_page_title?></title>
	<meta content="Administrador Sistema GRE - (Proyecto <?php echo c_page_title?>) " name="description"/>
	<meta content="Subgerencia de Informática - By JF! ;)" name="author"/>

	<?php include($dir_fc."inc/headercommon.php"); ?>

    <link href="<?php echo $raiz ?>css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo $raiz ?>css/bootstrap-datetimepicker-standalone.css" rel="stylesheet" type="text/css"/>
	<script type="text/javascript" src="<?php echo $raiz?>js/plugins/tables/datatables/datatables.min.js"></script>
	<script type="text/javascript" src="<?php echo $raiz?>js/plugins/forms/selects/select2.min.js"></script>
	<script type="text/javascript" src="<?php echo $raiz?>js/plugins/tables/datatables/extensions/col_reorder.min.js"></script>
	<script type="text/javascript" src="<?php echo $raiz?>js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js"></script>
	<script type="text/javascript" src="<?php echo $raiz?>js/plugins/forms/tags/tagsinput.min.js"></script>
	<script type="text/javascript" src="<?php echo $raiz?>js/plugins/forms/tags/tokenfield.min.js"></script>
	<script type="text/javascript" src="<?php echo $raiz?>js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
	<script type="text/javascript" src="<?php echo $raiz?>js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js"></script>
	<script type="text/javascript" src="<?php echo $raiz?>js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js"></script>
	<script type="text/javascript" src="<?php echo $raiz?>js/plugins/tables/datatables/extensions/buttons.min.js"></script>
	<script type="text/javascript" src="<?php echo $raiz?>js/plugins/tables/datatables/extensions/buttons.min.js"></script>
	<script type="text/javascript" src="<?php echo $raiz?>js/plugins/forms/styling/uniform.min.js"></script>
	<script type="text/javascript" src="<?php echo $raiz?>js/plugins/forms/styling/switchery.min.js"></script>
	<script type="text/javascript" src="<?php echo $raiz?>js/plugins/forms/styling/switch.min.js"></script>
    <script src="<?php echo $raiz ?>js/app/moment.min.js" type="text/javascript"></script>
    <script src="<?php echo $raiz ?>js/app/bootstrap-datetimepicker.min.js" type="text/javascript"></script>

	<script src="<?php echo $raiz?>js/app/status_plugins.js?ver=1.1"></script>
</head>
<body class="navbar-top">
	<?php include ($dir_fc."inc/header.php")?>
	<!-- Page container -->
	<div class="page-container">
		<!-- Page content -->
		<div class="page-content">
			<?php include ($dir_fc."inc/menucommon.php")?>
			<!-- Main content -->
			<div class="content-wrapper">
				<!-- Page header -->
                
				<div class="page-header page-header-default">
					<div class="page-header-content">

					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="<?php echo $raiz?>business"><i class="icon-home2 position-left"></i> Inicio</a></li>
							<li class="active"><?php echo $title_act?></li>
						</ul>
					</div>
				</div>
				<!-- /page header -->
				<!-- Content area -->
				<div class="content">
					<ul class="fab-menu fab-menu-absolute fab-menu-top-left affix-top">
						<li>

						</li>
					</ul>
					<div id="respuesta_ajax">
						<?php echo $msgBusqueda?>
					</div>
					<!-- Ajax sourced data -->
					<div class="panel panel-flat">
						<div class="panel-body">
							<div class="panel-heading">
								<h4 class="panel-title">Equipo de computo</h4>
								<div class="heading-elements"></div>
							</div>
							<p>Dentro del apartado "Activos Fijos", se encuentra la única opción "Equipo de computo", donde su objetivo principal es mostrar
							una tabla que alberga los datos relacionados a los activos fijos que son insertados mediante los movimientos que se generen dentro del sistema.</p>
							<img class="manualImg" src="images/manualUsuario/activosFijos/equipoComputo.png" alt="adecuacionInicial" height="auto" width="1003">
							<img class="manualImg" src="images/manualUsuario/activosFijos/equipoComputo2.png" alt="adecuacionInicial" height="auto" width="1003">
							<img class="manualImg" src="images/manualUsuario/activosFijos/equipoComputo3.png" alt="adecuacionInicial" height="auto" width="1003">
							<p>Esta tabla cuenta con muchas herramientas que permiten manipular la tabla dependiendo de lo que se quiera buscar o hacer con ella.</p>
							<p>En primer lugar se puede observar la busqueda de activos fijos en especifico, para eso, es necesario cualquier dato relacionado con el activo fijo, dar enter para iniciar la busqueda, de esa manera, arrojara el resultado en pantalla.</p>
							<img class="manualImg" src="images/manualUsuario/activosFijos/busquedaActivos.png" alt="adecuacionesFiltro" height="auto" width="1003">
							<p>Al igual, se tiene un parametro de las filas que se quieran mostrar dentro de la tabla.</p>
							<img class="manualImg" src="images/manualUsuario/activosFijos/parametroMostrar.png" alt="adecuacionesTamano" height="auto" width="1003">
							<p>Después de lado derecho se tienen tres botones, el primero permite copiar la tabla.</p>
							<img class="manualImg" src="images/manualUsuario/activosFijos/copiarActivos.png" alt="adecuacionesCopiar" height="auto" width="1003">
							<p>El siguiente permite descargar a la tabla mediante una hoja de calculo de EXCEL.</p>
							<img class="manualImg" src="images/manualUsuario/activosFijos/excelActivos.png" alt="adecuacionesExcel" height="auto" width="1003">
							<img class="manualImg" src="images/manualUsuario/activosFijos/excelActivos2.png" alt="adecuacionesExcel2" height="auto" width="1003">
							<p>Con el tercer botón permite descargar un PDF de la tabla.</p>
							<img class="manualImg" src="images/manualUsuario/activosFijos/pdfActivos.png" alt="adecuacionesPDF" height="auto" width="1003">
							<p>Con el cual se genera el informe requerido acerca de los activos fijos asignados, número de equipos por usuario, por área de departamento, etc.</p>
							<img class="manualImg" src="images/manualUsuario/activosFijos/pdfActivos2.png" alt="adecuacionesPDF2" height="auto" width="1003">
							<p>Por ultimo, se tiene el filtro de busqueda avanzada, donde se seleccionan los elementos que se quieran o no visualizar dentro de la pantalla.</p>
							<img class="manualImg" src="images/manualUsuario/activosFijos/avanzadaActivos1.png" alt="adecuacionesAvanzada" height="auto" width="1003">
							<img class="manualImg" src="images/manualUsuario/activosFijos/avanzadaActivos2.png" alt="adecuacionesAvanzada" height="auto" width="1003">
							<img class="manualImg" src="images/manualUsuario/activosFijos/avanzadaActivos3.png" alt="adecuacionesAvanzada" height="auto" width="1003">
							
						</div>
					</div>
					<?php include ($dir_fc."inc/footer.php")?>
				</div>
				<!-- /content area -->
			</div>
			<!-- /main content -->
		</div>
		<!-- /page content -->
	</div>
	<!-- /page container -->
</body>
<!-- Remote source -->
<div id="modal_remote" class="modal fade in" data-backdrop="static">
	<div class="modal-dialog modal-lg"></div>
</html>
