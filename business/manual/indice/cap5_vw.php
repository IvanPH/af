<?php
/*--------------------------------------------------------------------------------------------------------*/
$dir_fc = "";
include_once $dir_fc.'connections/trop.php';
include_once $dir_fc.'connections/php_config.php';

$sys_id_men   = 72;	//Corresponde al menú
$sys_tipo     = 0;	//Indicando que es una lista (0)
$real_sis     = "contratos/servicios";
$title_act    = "Usuarios";
$sub_t_act    = "Lista";
$fmc		  = "";
$busqueda     = "";
$id_estatus   = 1;

extract($_REQUEST);

if($id_estatus == 2){
    $sys_id_men = 23;
}

$dir          = dirname($_SERVER['PHP_SELF']);
$current_file = basename($_SERVER["PHP_SELF"]);
$checkMenu    = $server_name.$dir."/".$current_file;   //con este se checa si index està activo
$param        = "?fmc=".$fmc."&td=";

/*------------------------  Incluyendo e Instanciando Clases Inicial  -------------------------------*/
include_once $dir_fc.'data/inicial.class.php';
include_once $dir_fc.'data/contratos.class.php';
include_once $dir_fc.'common/parametros.class.php';
include_once $dir_fc.'common/function.class.php';

$cInicial   = new cInicial(); //Operaciones generales (Menu de incio, etc)
$parametro  = new cParametros();
$cLista     = new cConServiciosP();
$cFn        = new cFunction();
/*---------------------------------------------------------------------------------------------------*/
include_once 'business/sys/check_session.php';
$showBusqueda = "";
$msgBusqueda  = "";

$id_filtro_mov = 0;

if(isset($_SESSION[fil_tipo_mov_ofi])){
    /*
     * Nuevo Filtro de Búsqueda con el Select. para seleccionar los tipos de movimientos
     * */
    if($_SESSION[fil_tipo_mov_ofi] != ""){
        $id_filtro_mov = $_SESSION[fil_tipo_mov_ofi];
    }else{
        $_SESSION[fil_tipo_mov_ofi] = 0;
    }
}
IF($busqueda == "do_search_please"){
	if(isset($_SESSION[fil_descripcion])){
		if($_SESSION[fil_descripcion] != ""){
			$showBusqueda.= "<br> <strong>Objeto: </strong>". $_SESSION[fil_descripcion];
		}
	}
	if(isset($_SESSION[fil_no_fua])){
		if($_SESSION[fil_no_fua] != ""){
			$showBusqueda.= "<br> <strong>No. FUA: </strong>". $_SESSION[fil_no_fua];
		}
	}
	if(isset($_SESSION[fil_id])){
		if($_SESSION[fil_id] != ""){
			$showBusqueda.= "<br> <strong>FUA: </strong>". $_SESSION[fil_id];
		}
	}

	if(isset($_SESSION[fil_fecha_ini]) && isset($_SESSION[fil_fecha_fin])){
		if($_SESSION[fil_fecha_ini] != "" && $_SESSION[fil_fecha_fin] != ""){
			$showBusqueda.= "<br> <strong>Fecha </strong> Del: ".$cFn->formatear_fecha_show($_SESSION[fil_fecha_ini]). " 
                   al ".$cFn->formatear_fecha_show($_SESSION[fil_fecha_fin]);
		}
	}
	$msgBusqueda = $cFn->custom_alert('info', 'Resultados con la busqueda:', $showBusqueda, 1, 1);
}

$rs_parametro = $parametro->getTipoMovimiento(25);


?>
<!DOCTYPE html>
<html lang="es">
<head>
	<title><?php echo c_page_title?></title>
	<meta content="Administrador Sistema GRE - (Proyecto <?php echo c_page_title?>) " name="description"/>
	<meta content="Subgerencia de Informática - By JF! ;)" name="author"/>

	<?php include($dir_fc."inc/headercommon.php"); ?>

    <link href="<?php echo $raiz ?>css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo $raiz ?>css/bootstrap-datetimepicker-standalone.css" rel="stylesheet" type="text/css"/>
	<script type="text/javascript" src="<?php echo $raiz?>js/plugins/tables/datatables/datatables.min.js"></script>
	<script type="text/javascript" src="<?php echo $raiz?>js/plugins/forms/selects/select2.min.js"></script>
	<script type="text/javascript" src="<?php echo $raiz?>js/plugins/tables/datatables/extensions/col_reorder.min.js"></script>
	<script type="text/javascript" src="<?php echo $raiz?>js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js"></script>
	<script type="text/javascript" src="<?php echo $raiz?>js/plugins/forms/tags/tagsinput.min.js"></script>
	<script type="text/javascript" src="<?php echo $raiz?>js/plugins/forms/tags/tokenfield.min.js"></script>
	<script type="text/javascript" src="<?php echo $raiz?>js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
	<script type="text/javascript" src="<?php echo $raiz?>js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js"></script>
	<script type="text/javascript" src="<?php echo $raiz?>js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js"></script>
	<script type="text/javascript" src="<?php echo $raiz?>js/plugins/tables/datatables/extensions/buttons.min.js"></script>
	<script type="text/javascript" src="<?php echo $raiz?>js/plugins/tables/datatables/extensions/buttons.min.js"></script>
	<script type="text/javascript" src="<?php echo $raiz?>js/plugins/forms/styling/uniform.min.js"></script>
	<script type="text/javascript" src="<?php echo $raiz?>js/plugins/forms/styling/switchery.min.js"></script>
	<script type="text/javascript" src="<?php echo $raiz?>js/plugins/forms/styling/switch.min.js"></script>
    <script src="<?php echo $raiz ?>js/app/moment.min.js" type="text/javascript"></script>
    <script src="<?php echo $raiz ?>js/app/bootstrap-datetimepicker.min.js" type="text/javascript"></script>

	<script src="<?php echo $raiz?>js/app/status_plugins.js?ver=1.1"></script>
</head>
<body class="navbar-top">
	<?php include ($dir_fc."inc/header.php")?>
	<!-- Page container -->
	<div class="page-container">
		<!-- Page content -->
		<div class="page-content">
			<?php include ($dir_fc."inc/menucommon.php")?>
			<!-- Main content -->
			<div class="content-wrapper">
				<!-- Page header -->
                
				<div class="page-header page-header-default">
					<div class="page-header-content">

					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="<?php echo $raiz?>business"><i class="icon-home2 position-left"></i> Inicio</a></li>
							<li class="active"><?php echo $title_act?></li>
						</ul>
					</div>
				</div>
				<!-- /page header -->
				<!-- Content area -->
				<div class="content">
					<ul class="fab-menu fab-menu-absolute fab-menu-top-left affix-top">
						<li>

						</li>
					</ul>
					<div id="respuesta_ajax">
						<?php echo $msgBusqueda?>
					</div>
					<!-- Ajax sourced data -->
					<div class="panel panel-flat">
						<div class="panel-body">
							<div class="panel-heading">
								<h3 class="panel-title">Usuarios</h3>
								<div class="heading-elements"></div>
							</div>
							<p class="manual">Dentro del apartado "Administrador", se encuentran 3 opciones "Usuarios", "Roles" y "Movimientos", en esta sección (Usuarios) se mostraran los tipos de usuarios 
							existentes en el sistema, que alberga los datos de dichos usuarios.</p>
							<img class="manualImg zoom" src="images/manualUsuario/Administrador/Usuarios/usuarios.png" alt="presupuestoInicial" height="auto" width="300">
							<p class="manual">En este apartado al igual que en el resto del sistema, tenemos la busqueda especifica, parametro de filas que se quieran mostrar y 
							la sección de los botones "COPIAR", "EXCEL", "PDF" y "busqueda avanzada".</p>
							<img class="manualImg zoom" src="images/manualUsuario/Administrador/Usuarios/busquedaEspecifica.png" alt="presupuestoBusqueda" height="auto" width="300">
							<img class="manualImg zoom" src="images/manualUsuario/Administrador/Usuarios/parametroMostrar.png" alt="presupuestoTamano" height="auto" width="300">
							<p class="manual">Copiar tabla.</p>
							<img class="manualImg zoom" src="images/manualUsuario/Administrador/Usuarios/copiarUsuarios.png" alt="presupuestoCopia" height="auto" width="300">
							<p class="manual">Hoja de calculo de EXCEL.</p>
							<img class="manualImg zoom" src="images/manualUsuario/Administrador/Usuarios/excelUsuarios.png" alt="presupuestoExcel" height="auto" width="300">
							<img class="manualImg zoom" src="images/manualUsuario/Administrador/Usuarios/descargarExcel.png" alt="presupuestoExcel2" height="auto" width="300">
							<p class="manual">PDF de la tabla.</p>
							<img class="manualImg zoom" src="images/manualUsuario/Administrador/Usuarios/pdfUsuarios.png" alt="presupuestoPDF" height="auto" width="300">
							<img class="manualImg zoom" src="images/manualUsuario/Administrador/Usuarios/pdfDescarga.png" alt="presupuestoPDF2" height="auto" width="300">
							<p class="manual">Por ultimo, se tiene el filtro de busqueda avanzada, donde se seleccionan los elementos que se quieran o no visualizar dentro de la pantalla.</p>
							<img class="manualImg zoom" src="images/manualUsuario/Administrador/Usuarios/avanzadaUsuarios.png" alt="presupuestoAvanzada" height="auto" width="300">
							<img class="manualImg zoom" src="images/manualUsuario/Administrador/Usuarios/avanzadaUsuarios1.png" alt="presupuestoAvanzada2" height="auto" width="300">
							<img class="manualImg zoom" src="images/manualUsuario/Administrador/Usuarios/avanzadaUsuarios2.png" alt="presupuestoAvanzada3" height="auto" width="300">
						</div>
					</div>
					<?php include ($dir_fc."inc/footer.php")?>
				</div>
				<!-- /content area -->
			</div>
			<!-- /main content -->
		</div>
		<!-- /page content -->
	</div>
	<!-- /page container -->
</body>
<!-- Remote source -->
<div id="modal_remote" class="modal fade in" data-backdrop="static">
	<div class="modal-dialog modal-lg"></div>
</html>
