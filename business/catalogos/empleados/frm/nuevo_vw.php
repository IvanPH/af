<?php
session_start();

$type = 0;
$id   = 0;

extract($_REQUEST);

/*--------------------------------------------------------------------------------------------------------*/
$dir_fc       = "../../../../";
include_once $dir_fc.'connections/trop.php';
include_once $dir_fc.'connections/php_config.php';
include_once $dir_fc.'data/empleados.class.php';
include_once $dir_fc . 'common/function.class.php';
$cFuntion = new cFunction();

$id_empleado="";
$rfc="";
$estatus="";
$noempleado="";
$plaza="";
$segsoc="";
$apepat = "";
$apemat= "";
$nombre="";
$percentpto="";
$f_nombramiento = "";
$f_inicio = "";
$f_termino = "";
$f_ingreso = "";
$sueldo_neto = "";
$estatus_laboral = "";
$AREA = "";
$ID_AREA = "";
$cve_puesto = "";


if(($_SESSION[nuev] == 1 && $type == 1) || ($_SESSION[edit] == 1 && $type == 2 && $id > 0) || $type == 3){//Si tiene permisos de agregar un nuevo emepledo
    $_SESSION[_type_] = $type;
    if($type == 2 || $type == 3){
        $cAccion = new cEmpleados();
        $cAccion->setIdUsuario($id);
        $cAccion->setIdUsuarioCaptura($_SESSION[id_usr]);
        $rsEditar = $cAccion->getRegbyid();
        $rsEditar->rowCount();

        if($rsEditar->rowCount() > 0){
            
            $arrEdi             = $rsEditar->fetch(PDO::FETCH_OBJ);
            $id_empleado        = $arrEdi->id_empleado;
            $_SESSION[_editar_] = $id_empleado; //Importante que si es ediciòn se asigne el ID que se va a editar
            $rfc                = $arrEdi->rfc;
            $estatus            = $arrEdi->estatus;
            $noempleado         = $arrEdi->noempleado;
            $plaza              = $arrEdi->plaza;
            $segsoc             = $arrEdi->segsoc;
            $apepat             = $arrEdi->apepat;
            $apemat             = $arrEdi->apemat;
            $nombre             = $arrEdi->nombre;
            $f_nombramiento     = $arrEdi->f_nombramiento;
            $f_inicio           = $arrEdi->f_inicio;
            $f_termino          = $arrEdi->f_termino;
            $f_ingreso          = $arrEdi->f_ingreso;
            $sueldo_neto        = $arrEdi->sueldo_neto;
            $estatus_laboral    = $arrEdi->estatus_laboral;
            $AREA               = $arrEdi->AREA;
            $ID_AREA            = $arrEdi->ID_AREA;
            $cve_puesto         = $arrEdi->cve_puesto;
        }

        if ($f_nombramiento == "1901-01-01" || $f_nombramiento == NULL || $f_nombramiento == "0000-00-00") {
            $f_nombramiento = "00/00/0000";
        } else {
            $f_nombramiento = $cFuntion ->formatear_fecha_show($f_nombramiento);
        }
        if ($f_inicio == "1901-01-01" || $f_inicio == NULL || $f_inicio == "0000-00-00") {
            $f_inicio = "00/00/0000";
        } else {
            $f_inicio = $cFuntion ->formatear_fecha_show($f_inicio);
        }
        if ($f_termino == "1901-01-01" || $f_termino == NULL || $f_termino == "0000-00-00") {
            $f_termino = "00/00/0000";
        } else {
            $f_termino = $cFuntion ->formatear_fecha_show($f_termino);
        }
        if ($f_ingreso == "1901-01-01" || $f_ingreso == NULL || $f_ingreso == "0000-00-00") {
            $f_ingreso = "00/00/0000";
        } else {
            $f_ingreso = $cFuntion ->formatear_fecha_show($f_ingreso);
        }
        if ($ID_AREA == "0" || $ID_AREA == NULL) {
            $ID_AREA = NULL;
        }
        if ($cve_puesto == "0" || $cve_puesto == NULL) {
            $cve_puesto = NULL;
        }
    }

    ?>
<script type="text/javascript" src="<?php echo $raiz?>js/app/floatingLabels.js"></script>
<form id="frm_guardar" class="" method="post" action="javascript: guardar()" >
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h5 class="modal-title"> <?php
                if($type == 1){
                    echo "Agregando";
                } elseif($type == 2){
                    echo "Editando";
                }elseif($type == 3){
                    echo "Visualizando";
                }
                ?> Empleado</h5>
        </div>
        <div class="modal-body">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-xs-12 col-md-12 col-sm-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h6 class="panel-title">Datos de Empleado</h6>
                            <!--<div class="heading-elements">
                                <ul class="icons-list">
                                    <li><a data-action="collapse"></a></li>
                                    <li><a data-action="reload"></a></li>
                                    <li><a data-action="close"></a></li>
                                </ul>
                            </div> -->
                        </div>

                        <div class="panel-body">
                            <fieldset>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group ">
                                            <label class="control-label">
                                                Nombre(s)<span class="text-danger">*</span>
                                            </label>
                                            <input type="text" class="form-control" name="nombre" id="nombre" autocomplete="off"
                                                   required placeholder="Nombre(s) *" value="<?php echo $nombre ?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group ">
                                            <label  class="control-label" for="apepat">
                                                Apellido Paterno <span class="text-danger">*</span>
                                            </label>
                                            <input type="text" class="form-control" name="apepat" id="apepat" autocomplete="off"
                                                   value="<?php echo $apepat ?>"
                                                   placeholder="Apellido Paterno *">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group ">
                                            <label class="control-label" for="apemat">Apellido Materno
                                                <span class="text-danger">*</span>
                                            </label>
                                            <input type="text" class="form-control" name="apemat" id="apemat" autocomplete="off"
                                                   placeholder="Apellido Materno *" value="<?php echo $apemat ?>">
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group ">
                                            <label for="rfc" class="control-label">
                                                RFC <span class="text-danger">*</span>
                                            </label>
                                            <input type="text" class="form-control" name="rfc" id="rfc" autocomplete="off"
                                                   required placeholder="RFC *"   value="<?php echo $rfc ?>"
                                                   title="RFC del empleado">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group ">
                                            <label for="estatus_laboral" class="control-label">
                                                Estatus: <span class="text-danger">*</span>
                                            </label>
                                            <select name="estatus_laboral" id="estatus_laboral" class="form-control" required>
                                                <option value="">Estatus Laborar*</option>
                                                <option value="V" <?php if($estatus_laboral == 'V'){ echo "selected";}?>>Vigente</option>
                                                <option value="L" <?php if($estatus_laboral == 'L'){ echo "selected";}?>>Liciencia</option>
                                                <option value="D" <?php if($estatus_laboral == 'D'){ echo "selected";}?>>Baja</option>
                                                <option value="T" <?php if($estatus_laboral == 'T'){ echo "selected";}?>>Temporal</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group ">
                                            <label for="noempleado" class="control-label">Matricula de empleado
                                                <span class="text-danger">*</span>
                                            </label>
                                            <input type="noempleado" class="form-control" name="noempleado" id="noempleado"
                                                autocomplete="off" placeholder="Matricula de empleado*"
                                                   value="<?php echo $noempleado?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group ">
                                            <label for="plaza" class="control-label">Número de plaza
                                               
                                            </label>
                                            <input type="plaza" class="form-control" name="plaza" id="plaza"
                                                    autocomplete="off" placeholder="No plaza "
                                                   value="<?php echo  $plaza?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group ">
                                            <label for="segsoc" class="control-label">Seguro Social
                                                
                                            </label>
                                            <input type="segsoc" class="form-control" name="segsoc" id="segsoc"
                                                    autocomplete="off" placeholder="Seguro Social *"
                                                   value="<?php echo $segsoc?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="sueldo_neto" class="control-label">Sueldo Neto
                                               
                                            </label>
                                            <input type="sueldo_neto" class="form-control" name="sueldo_neto" id="sueldo_neto"
                                                    autocomplete="off" placeholder="Seguro Social *"
                                                   value="<?php echo $sueldo_neto?>">
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label  class="control-label" for="f_nombramiento">
                                            Fecha Nombramiento
                                        </label>
                                        <input type="text" class="form-control" name="f_nombramiento" id="f_nombramiento"
                                               autocomplete="off" 
                                               value="<?php echo $f_nombramiento?>"
                                               placeholder="Fecha Nombramiento">
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label  class="control-label" for="f_inicio">
                                            Fecha Inicial
                                        </label>
                                        <input type="text" class="form-control" name="f_inicio" id="f_inicio"
                                               autocomplete="off" 
                                               value="<?php echo $f_inicio ?>"
                                               placeholder="Fecha Inicial">
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label  class="control-label" for="f_termino">
                                            Fecha Final
                                        </label>
                                        <input type="text" class="form-control" name="f_termino" id="f_termino"
                                               autocomplete="off" 
                                               value="<?php echo $f_termino?>"
                                               placeholder="Fecha Final">
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label  class="control-label" for="f_ingreso">
                                            Fecha Ingreso
                                        </label>
                                        <input type="text" class="form-control" name="f_ingreso" id="f_ingreso"
                                               autocomplete="off" 
                                               value="<?php echo $f_ingreso?>"
                                               placeholder="Fecha Ingreso">
                                    </div>
                                </div>    
                                </div>
                                <br>
                                <div class="row">
                               
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="ID_AREA" class="control-label">
                                        Área: 
                                        </label>
                                        <select name="ID_AREA" id="ID_AREA" class="form-control"
                                                 >
                                            <option value="0">SIN OTORGAR</option>
                                            <?php
                                                 $cAccion = new cEmpleados();
                                                 $rs_ben = $cAccion->getAreas();
                                        
                                            
                                            while($rw_ben = $rs_ben->fetch(PDO::FETCH_OBJ)){
                                                $selected_ts = "";
                                                if($type == 2 || $type == 3){
                                                    //Aquí serán las condiciones para poner los que van seleccionados
                                                    if($ID_AREA == $rw_ben->id_area){
                                                        $selected_ts = "selected";
                                                    }
                                                }
                                                ?>
                                                <option value="<?php echo $rw_ben->id_area?>"
                                                    <?php echo $selected_ts?>>
                                                    <?php echo $rw_ben->area?>
                                                </option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label  class="control-label" for="cve_puesto">
                                            Puesto
                                        </label>
                                        <select name="cve_puesto" id="cve_puesto" class="form-control" >
                                        <option value="0">SIN OTORGAR</option>
                                            <?php
                                                 $cAccion = new cEmpleados();
                                                 $rs_puesto = $cAccion->getPuestos();
                                        
                                            
                                            while($rw_puesto = $rs_puesto->fetch(PDO::FETCH_OBJ)){
                                                $selected_ts = "";
                                                if($type == 2 || $type == 3){
                                                    //Aquí serán las condiciones para poner los que van seleccionados
                                                    if($cve_puesto == $rw_puesto->id_puesto){
                                                        $selected_ts = "selected";
                                                    }
                                                }
                                                ?>
                                                <option value="<?php echo $rw_puesto->id_puesto?>"
                                                    <?php echo $selected_ts?>>
                                                    <?php echo $rw_puesto->desc_puesto?>
                                                </option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                     
                                    </div>
                                </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                        <!-- Caja de respuetas a la accion del usuario -->
                        <div class="row">
                            <div class="col-md-12">
                                <div id="respuesta_ajax_modal">
                                </div>
                            </div>
                        </div>                   
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">
                <i class="icon-close2"></i> Cerrar
            </button>
            <?php
          
            if($type <> 3){
                ?>
                <button type="submit" class="btn btn-primary" id="btn_guardar">
                    <i class="icon-floppy-disk"></i> Guardar Cambios
                </button>
            <?php
            }
            ?>
        </div>
    </div>
</form>
<script>
    $(function() {
      
        
        $('.child-menu').hide();
        $('.ocultar').hide();
        $('#f_nombramiento').datetimepicker({
            format: 'L',
            locale: 'es',
      
        });
        $('#f_inicio').datetimepicker({
            format: 'L',
            locale: 'es',
           
        });
        $('#f_termino').datetimepicker({
            format: 'L',
            locale: 'es',
            
        });
        $('#f_ingreso').datetimepicker({
            format: 'L',
            locale: 'es',
          
        });
  
        <?php
        
        if($type == 3){
            ?>
        $('#frm_guardar :input:not(:button)').attr({
            'disabled': 'disabled'
        });
            <?php
        }
        ?>
        
    });


</script>
<?php
}else{
    echo "<h1>No se puede acceder a la información solicitada</h1>";
}
?>