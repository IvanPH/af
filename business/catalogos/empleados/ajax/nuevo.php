<?php
session_start();
$dir_fc = "../../../../";
/*-----------------------------------      Estableciendo la Clases  --------------------------------------*/
include_once $dir_fc.'data/empleados.class.php';
include_once $dir_fc . 'common/function.class.php';
/*--------------------------------------------------------------------------------------------------------*/
include_once $dir_fc.'connections/trop.php'; //Inclueye configuración de fecha y  hora de mexico
include_once $dir_fc.'connections/php_config.php'; //Inclueye configuración de fecha y  hora de mexico

$cAccion  = new cEmpleados();
$cFuntion = new cFunction();

if(!isset($_SESSION[_type_])){
    $_SESSION[_type_] = 0;
}

$id_empleado="";
$rfc="";
$estatus="";
$noempleado="";
$plaza="";
$segsoc="";
$apepat = "";
$apemat= "";
$nombre="";
$percentpto="";
$f_nombramiento = "";
$f_inicio = "";
$f_termino = "";
$f_ingreso = "";
$sueldo_neto = "";
$estatus_laboral = "";
$AREA = "";
$ID_AREA = "";
$cve_puesto = "";

if($_SESSION[_type_] == 2){
    $cAccion->setIdUsuario($_SESSION[_editar_]);
    $clave = "- Con valor -";
}

extract($_REQUEST);
    if ($f_nombramiento == "00/00/0000" || $f_nombramiento == NULL) {
        $f_nombramiento = "1901-01-01";
    } else {
        $f_nombramiento = $cFuntion ->formatear_fecha_ins($f_nombramiento);
    }
    if ($f_inicio == "00/00/0000" || $f_inicio == NULL) {
        $f_inicio = "1901-01-01";
    } else {
        $f_inicio = $cFuntion ->formatear_fecha_ins($f_inicio);
    }
    if ($f_termino == "00/00/0000" || $f_termino == NULL) {
        $f_termino = "1901-01-01";
    } else {
        $f_termino = $cFuntion ->formatear_fecha_ins($f_termino);
    }
    if ($f_ingreso == "00/00/0000" || $f_ingreso == NULL) {
        $f_ingreso = "1901-01-01";
    } else {
        $f_ingreso = $cFuntion ->formatear_fecha_ins($f_ingreso);
    }
    if ($ID_AREA == NULL) {
        $ID_AREA = "0";
    }
    if ($cve_puesto == NULL) {
        $cve_puesto = "0";
    }
       
if($apepat == "" || $apemat == "" || $rfc == "" || $estatus_laboral ==""){ //Verficando datos vacios
    $resp = "Debes de ingresar correctamente los datos";
}
else{
    if(isset($_SESSION[admin]) && $_SESSION[admin] == 1){
        $user_admin = $admin;
    }else{
        $user_admin = 0;
    }
    //buscar si existe un usuario con el mismo nombre
    //$cAccion->setUsuario($usuario);
        //Obtenemos fecha y hora
        
        $matricula_empleado = $rfc."-".$ID_AREA."-".$cve_puesto;
        $id_empleado=$_SESSION[_editar_];
        $fecha  = date("Y-m-d");
        $cAccion->setId_empleado($id_empleado);
        $cAccion->setRfc($rfc);
        $cAccion->setNoempleado($matricula_empleado);
        $cAccion->setPlaza($plaza);
        $cAccion->setSegsoc($segsoc);
        $cAccion->setApepat($apepat);
        $cAccion->setApemat($apemat);
        $cAccion->setNombre($nombre);
        $cAccion->setPercentpto($percentpto);
        $cAccion->setF_nombramiento($f_nombramiento);
        $cAccion->setF_inicio($f_inicio);
        $cAccion->setF_termino($f_termino);
        $cAccion->setF_ingreso($f_ingreso);
        $cAccion->setSueldo_neto($sueldo_neto);
        $cAccion->setEstatus_laboral($estatus_laboral);
        $cAccion->setAREA($AREA);
        $cAccion->setID_AREA($ID_AREA);
        $cAccion->setCve_puesto($cve_puesto);
    
        //$cAccion->setIdResponsable($id_responsable);

        if($_SESSION[_type_] == 2){
            $inserted = $cAccion->updateRegCat();
        }else{
            $inserted = $cAccion->insertReg($fecha);
        }
        if ($inserted==1)
        {
            $done  = 1;
            $resp  = "Usuario agregado correctamente.";
        }
        else{
            $done  = 0;
            $resp  = "Ocurrió un incoveniente con la base de datos: -- ".$inserted;
        }
            
       
            
        
    
}
echo json_encode(array("done" => $done, "resp" => $resp));
?>
