<?php
session_start();
$dir_fc = "../../../../";
/*-----------------------------------      Estableciendo la Clases  --------------------------------------*/
include_once $dir_fc.'data/plazas.class.php';
/*--------------------------------------------------------------------------------------------------------*/
include_once $dir_fc.'connections/trop.php'; //Inclueye configuración de fecha y  hora de mexico
include_once $dir_fc.'connections/php_config.php'; //Inclueye configuración de fecha y  hora de mexico

$cAccion  = new cPlazas();

if(!isset($_SESSION[_type_])){
    $_SESSION[_type_] = 0;
}


$id_puesto="";
$desc_puesto="";
$tipo_puesto="";
$cve_alta="";
$cve_shcp="";
$cvesecodam="";
$nivel="";
$nivel_viat="";
$p_estrategico="";
$responsable="";
$t_funcionp="";

if($_SESSION[_type_] == 2){
    $cAccion->setIdUsuario($_SESSION[_editar_]);
    $clave = "- Con valor -";
}

extract($_REQUEST);
if($desc_puesto == "" || $tipo_puesto == "" || $cve_alta == "" ){ //Verficando datos vacios
    $resp = "Debes de ingresar correctamente los datos";
}
else{
    if(isset($_SESSION[admin]) && $_SESSION[admin] == 1){
        $user_admin = $admin;
    }else{
        $user_admin = 0;
    }
    //buscar si existe un usuario con el mismo nombre
    //$cAccion->setUsuario($usuario);

 
        //Obtenemos fecha y hora
       $id_puesto=$_SESSION[_editar_];
        $cAccion->setId_puesto($id_puesto);
        $cAccion->setDesc_puesto($desc_puesto);
        $cAccion->setTipo_puesto($tipo_puesto);
        $cAccion->setCve_alta($cve_alta);
        $cAccion->setCve_shcp($cve_shcp);
        $cAccion->setCvesecodam($cvesecodam);
        $cAccion->setNivel($nivel);
        $cAccion->setNivel_viat($nivel_viat);
        $cAccion->setP_estrategico($p_estrategico);
        
    
        //$cAccion->setIdResponsable($id_responsable);

        if($_SESSION[_type_] == 2){
            $inserted = $cAccion->updateReg();
        }else{
            $inserted = $cAccion->insertReg();
        }
        if ($inserted==1)
        {
            $done  = 1;
            $resp  = "Usuario agregado correctamente.";
        }
        else{
            $done  = 0;
            $resp  = "Ocurrió un incoveniente con la base de datos: -- ".$inserted;
        }
            
       
            
        
    
}
echo json_encode(array("done" => $done, "resp" => $resp));
?>
