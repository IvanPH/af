<?php
$dir_fc = "../../../../";
/*-----------------------------------      Estableciendo la Clases  --------------------------------------*/
include_once $dir_fc . 'data/users.class.php';
include_once $dir_fc . 'data/rol.class.php';
/*--------------------------------------------------------------------------------------------------------*/
include_once $dir_fc . 'connections/trop.php'; //Inclueye configuración de fecha y  hora de mexico
include_once $dir_fc . 'connections/php_config.php'; //Inclueye configuración de fecha y  hora de mexico

$cAccion = new cUsers();
$cEditar = new Rol();

$id_rol = 0;

extract($_REQUEST);

if(is_numeric($id_rol) && $id_rol <> 0){
    $cEditar->set_id($id_rol);

    $rsRol           = $cEditar->parentsMenu();
    $totalRows_rsRol = $rsRol->rowCount();
    while ($rowReg   = $rsRol->fetch(PDO::FETCH_OBJ)) {
        $chk = "";
        $cEditar->set_id_menu($rowReg->id);
        $checked_r  = $cEditar->checarRol_menu();
        $rowsc1  = $checked_r->rowCount();
        if ($rowsc1>0) {
            $chk = "checked";
        }
        ?>
        <div id="parents-menu_<?php echo $rowReg->id?>">
            <div class="form-group">
                <span>
                    <span id="mostrar_<?php echo $rowReg->id?>" class="btn-plus-ne mostrar"
                          onclick="mostrar(<?php echo $rowReg->id?>)">
                    <i class="fa fa-plus-square-o"></i>
                    </span>
                    <span id="ocultar_<?php echo $rowReg->id?>" class="btn-plus-ne ocultar"
                          onclick="ocultar(<?php echo $rowReg->id?>)">
                        <i class="fa fa-minus-square-o"></i>
                    </span>
                </span>
                <label class="checkbox-inline">
                    <input type="checkbox" checked="checked" name="menus[]"
                           id="menu_<?php echo $rowReg->id?>" value="<?php echo $rowReg->id?>" <?php echo $chk ?>
                           title="<?php echo $rowReg->texto?>" onchange="sel_des_child(<?php echo $rowReg->id?>)">
                    <?php echo $rowReg->texto ?>
                </label>

            </div>
            <input type="hidden" id="grupo_m_<?php echo $rowReg->id?>" name="grupo[<?php echo $rowReg->id?>]" value="<?php echo $rowReg->id_grupo?>">
        </div>
        <?php
        $rsRol_c  = $cEditar->childsMenu($rowReg->id);
        ?>
        <div id="child-menu_<?php echo $rowReg->id?>" class="child-menu">
            <?php
            while ($rowReg_c = $rsRol_c->fetch(PDO::FETCH_OBJ)) {
                $chk_imp     = "";
                $chk_edit    = "";
                $chk_nuevo   = "";
                $chk_elim    = "";
                $chk_exportar= "";
                $chk_2 = "";
                $cEditar->set_id_menu($rowReg_c->id);
                $checked_r_2 = $cEditar->checarRol_menu();
                $rowsc = $checked_r_2->rowCount();
                if ($rowsc > 0) {
                    $chk_2 = "checked";
                    $rw_check = $checked_r_2->fetch(PDO::FETCH_OBJ);
                    $chk_imp = $rw_check->imp;
                    $chk_edit = $rw_check->edit;
                    $chk_nuevo = $rw_check->nuevo;
                    $chk_elim = $rw_check->elim;
                    $chk_exportar = $rw_check->exportar;
                }
                ?>
                <input type="hidden" id="grupo_m_<?php echo $rowReg_c->id?>" name="grupo[<?php echo $rowReg_c->id?>]" value="<?php echo $rowReg_c->id_grupo?>">
                <div class="checkbox">
                    <label class="checkbox-inline separador-desc">
                        <input name="menus[]" id="child_<?php echo $rowReg->id?> _<?php echo $rowReg_c->id?>" type="checkbox"
                               value="<?php echo $rowReg_c->id?>" title="<?php echo $rowReg_c->texto?>"
                               onchange="checamain(<?php echo $rowReg->id?>)" <?php echo $chk_2 ?>>
                        <?php echo $rowReg_c->texto ?>
                    </label>
                    <label class="checkbox-inline separador">
                        <input type="checkbox" name="permiso_imp[<?php echo $rowReg_c->id?>]" value="1"
                               title="Editar" id="permiso_imp<?php echo $rowReg_c->id?>" <?php if($chk_imp == 1){ echo "checked";}?>>
                        Imprimir

                    </label>
                    <label class="checkbox-inline separador">
                        <input type="checkbox" name="permiso_nuevo[<?php echo $rowReg_c->id?>]" value="1"
                               title="Editar" id="permiso_nuevo<?php echo $rowReg_c->id?>" <?php if($chk_nuevo == 1){ echo "checked";}?>>
                        Nuevo
                    </label>
                    <label class="checkbox-inline  separador">
                        <input type="checkbox" name="permiso_edit[<?php echo $rowReg_c->id?>]" value="1"
                               title="Editar" id="permiso_edit<?php echo $rowReg_c->id?>" <?php if($chk_edit == 1){ echo "checked";}?>>
                        Editar
                    </label>
                    <label class="checkbox-inline separador">
                        <input type="checkbox" name="permiso_elim[<?php echo $rowReg_c->id?>]" value="1"
                               title="Editar" id="permiso_elim<?php echo $rowReg_c->id?>" <?php if($chk_elim == 1){ echo "checked";}?>>
                        Eliminar
                    </label>
                    <label class="checkbox-inline separador">
                        <input type="checkbox" name="permiso_exportar[<?php echo $rowReg_c->id?>]" value="1"
                               title="Editar" id="permiso_exportar<?php echo $rowReg_c->id?>" <?php if($chk_exportar == 1){ echo "checked";}?>>
                        Exportar
                    </label>
                </div>
                <?php
            }
            ?>
        </div>
        <?php
    }

}else{
    echo "Valores recibidos, no válidos";
}
?>