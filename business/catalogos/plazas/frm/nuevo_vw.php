<?php
session_start();

$type = 0;
$id   = 0;

extract($_REQUEST);

/*--------------------------------------------------------------------------------------------------------*/
$dir_fc       = "../../../../";
include_once $dir_fc.'connections/trop.php';
include_once $dir_fc.'connections/php_config.php';
include_once $dir_fc.'data/plazas.class.php';

$id_puesto="";
$desc_puesto="";
$tipo_puesto="";
$cve_alta="";
$cve_shcp="";
$cvesecodam="";
$nivel="";
$nivel_viat="";
$p_estrategico="";
$responsable="";
$t_funcionp="";
if(($_SESSION[nuev] == 1 && $type == 1) || ($_SESSION[edit] == 1 && $type == 2 && $id > 0) || $type == 3){//Si tiene permisos de agregar un nuevo emepledo
    $_SESSION[_type_] = $type;
    if($type == 2 || $type == 3){
        $cAccion = new cPlazas();
        $cAccion->setIdUsuario($id);
        $cAccion->setIdUsuarioCaptura($_SESSION[id_usr]);
        $rsEditar    = $cAccion->getRegbyid();
        $rsEditar->rowCount();

        if($rsEditar->rowCount() > 0){
            
            $arrEdi               = $rsEditar->fetch(PDO::FETCH_OBJ);
            $id_puesto            = $arrEdi->id_puesto;
            $_SESSION[_editar_]   = $id_puesto; //Importante que si es ediciòn se asigne el ID que se va a editar
            $desc_puesto          = $arrEdi->desc_puesto;
            $tipo_puesto          = $arrEdi->tipo_puesto;
            $cve_alta             = $arrEdi->cve_alta;
            $cve_shcp             = $arrEdi->cve_shcp;
            $cvesecodam           = $arrEdi->cvesecodam;
            $nivel                = $arrEdi->nivel;
            $nivel_viat           = $arrEdi->nivel_viat;
            $p_estrategico        = $arrEdi->p_estrategico;
            $responsable          = $arrEdi->responsable;
            $t_funcionp           = $arrEdi->t_funcionp;
       
        }
    }
   
    ?>
<script type="text/javascript" src="<?php echo $raiz?>js/app/floatingLabels.js"></script>
<form id="frm_guardar" class="" method="post" action="javascript: guardar()" >
    <div class="modal-content">
        <div class="row">
            <div class="col-md-12">
                <div id="respuesta_ajax_modal">
                </div>
            </div>
        </div>
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h5 class="modal-title"> <?php
                if($type == 1){
                    echo "Agregando";
                } elseif($type == 2){
                    echo "Editando";
                }elseif($type == 3){
                    echo "Visualizando";
                }
                ?> Plaza</h5>
        </div>
        <div class="modal-body">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-xs-12 col-md-12 col-sm-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h6 class="panel-title">Datos de Plaza</h6>
                            <!--<div class="heading-elements">
                                <ul class="icons-list">
                                    <li><a data-action="collapse"></a></li>
                                    <li><a data-action="reload"></a></li>
                                    <li><a data-action="close"></a></li>
                                </ul>
                            </div> -->
                        </div>

                        <div class="panel-body">
                            <fieldset>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group ">
                                            <label class="control-label">
                                                Puesto<span class="text-danger">*</span>
                                            </label>
                                            <input type="text" class="form-control" name="desc_puesto" id="desc_puesto" autocomplete="off"
                                                   required placeholder="Puesto *" value="<?php echo $desc_puesto ?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group ">
                                            <label for="tipo_puesto" class="control-label">
                                            Típo de Puesto: <span class="text-danger">*</span>
                                            </label>
                                            <select name="tipo_puesto" id="tipo_puesto" class="form-control" required>
                                                <option value="">Típo de Puesto*</option>
                                                <option value="S" <?php if($tipo_puesto == 'S'){ echo "selected";}?>>S</option>
                                                <option value="M" <?php if($tipo_puesto == 'M'){ echo "selected";}?>>M</option> 
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group ">
                                            <label for="cve_alta" class="control-label">
                                            Clave de alta: <span class="text-danger">*</span>
                                            </label>
                                            <select name="cve_alta" id="cve_alta" class="form-control" required>
                                                <option value="">Clave de alta*</option>
                                                <option value="1" <?php if($cve_alta == '1'){ echo "selected";}?>>1</option>
                                                <option value="2" <?php if($cve_alta == '2'){ echo "selected";}?>>2</option> 
                                                <option value="3" <?php if($cve_alta == '3'){ echo "selected";}?>>3</option> 
                                                <option value="4" <?php if($cve_alta == '4'){ echo "selected";}?>>4</option> 
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group ">
                                            <label for="rfc" class="control-label">
                                            Clave SHCP 
                                            </label>
                                            <input type="text" class="form-control" name="rfc" id="rfc" autocomplete="off"
                                                    placeholder=" Clave SHCP "   value="<?php echo $cve_shcp ?>"
                                                   title="Clave SHCP ">
                                        </div>
                                    </div>
                                 
                                    <div class="col-sm-4">
                                        <div class="form-group ">
                                            <label for="cvesecodam" class="control-label">cvesecodam
                                               
                                            </label>
                                            <input type="cvesecodam" class="form-control" name="cvesecodam" id="cvesecodam"
                                                    autocomplete="off" placeholder="cvesecodam"
                                                   value="<?php echo $cvesecodam?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group ">
                                            <label for="nivel" class="control-label">Nivel
                                               
                                            </label>
                                            <input type="nivel" class="form-control" name="nivel" id="nivel"
                                                    autocomplete="off" placeholder="Nivel "
                                                   value="<?php echo $nivel?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group ">
                                            <label for="nivel_viat" class="control-label">Nivel Viaticos
                                                
                                            </label>
                                            <input type="nivel_viat" class="form-control" name="nivel_viat" id="nivel_viat"
                                                    autocomplete="off" placeholder="Nivel Viaticos "
                                                   value="<?php echo $nivel_viat?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="p_estrategico" class="control-label">Plan Estrategico
                                               
                                            </label>
                                            <input type="sueldo_neto" class="form-control" name="p_estrategico" id="p_estrategico"
                                                    autocomplete="off" placeholder="Plan Estrategico"
                                                   value="<?php echo $p_estrategico?>">
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                
                            </fieldset>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">
                <i class="icon-close2"></i> Cerrar
            </button>
            <?php
          
            if($type <> 3){
                ?>
                <button type="submit" class="btn btn-primary" id="btn_guardar">
                    <i class="icon-floppy-disk"></i> Guardar Cambios
                </button>
            <?php
            }
            ?>
        </div>
    </div>
</form>
<script>
    $(function() {
      
        
        $('.child-menu').hide();
        $('.ocultar').hide();
       
  
        <?php
        
        if($type == 3){
            ?>
        $('#frm_guardar :input:not(:button)').attr({
            'disabled': 'disabled'
        });
            <?php
        }
        ?>
        
    });


</script>
<?php
}else{
    echo "<h1>No se puede acceder a la información solicitada</h1>";
}
?>