<?php
session_start();

$type = 0;
$id   = 0;

extract($_REQUEST);

/*--------------------------------------------------------------------------------------------------------*/
$dir_fc       = "../../../../";
include_once $dir_fc.'connections/trop.php';
include_once $dir_fc.'connections/php_config.php';
include_once $dir_fc . 'data/cat_titulo.class.php';

$id_titulo = "";
$descripcion = "";
$abreviacion = "";
$activo = "";


if(($_SESSION[nuev] == 1 && $type == 1) || ($_SESSION[edit] == 1 && $type == 2 && $id > 0) || $type == 3){
    //Si tiene permisos de agregar un nuevo documento
    $_SESSION[_type_] = $type;
    if($type == 2 || $type == 3){
        $cAccion = new cTitulos();
        $rsEditar = $cAccion->getRegbyid($id);
        $rsEditar->rowCount();

        if($rsEditar->rowCount() > 0){
            $_SESSION[_editar_] = $id; //Importante que si es ediciòn se asigne el ID que se va a editar
            $arrEdi             = $rsEditar->fetch(PDO::FETCH_OBJ);
            $descripcion        = $arrEdi->descripcion;
            $abreviacion        = $arrEdi->abreviacion;
        }
    }
?>
<script type="text/javascript" src="<?php echo $raiz?>js/app/floatingLabels.js"></script>
<form id="frm_guardar" class="" method="post" action="javascript: guardar()" >
    <div class="modal-content">
        <!-- Caja de respuetas a la accion del usuario -->
        <div class="row">
            <div class="col-md-12">
                <div id="respuesta_ajax_modal">
                </div>
            </div>
        </div>

        <!-- Encabezado de la caja de captura -->
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h5 class="modal-title"> <?php
                if($type == 1){
                    echo "Agregando";
                } elseif($type == 2){
                    echo "Editando";
                }elseif($type == 3){
                    echo "Visualizando";
                }
                ?> Titulo</h5>
        </div>

        <!-- Caja de captura -->
        <div class="modal-body">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-xs-12 col-md-12 col-sm-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h6 class="panel-title">Datos de Titulo</h6>
                        </div>

                        <div class="panel-body">
                            <fieldset>
                                <div class="row">
                                    <div class="col-xs-6 col-md-6 col-sm-6">
                                    <div class="form-group">
                                            <label class="control-label">Titulo<span class="text-danger">*</span></label>
                                            <input type="text" class="form-control" name="descripcion" id="descripcion" autocomplete="off"
                                                   required placeholder="Descripcion *" value="<?php echo  $descripcion ?>">
                                        </div>
                                    </div>
                                    <div class="col-xs-6 col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <label  class="control-label" for="abreviacion">Abreviación <span class="text-danger">*</span></label>
                                            <input type="text" class="form-control" name="abreviacion" id="abreviacion" autocomplete="off"
                                                   value="<?php echo $abreviacion ?>"
                                                   placeholder="Abreviación *">
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Pie de la caja de captura con botones de acciones -->
        <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">
                <i class="icon-close2"></i> Cerrar
            </button>
            <?php
            if($type <> 3){
                ?>
                <button type="submit" class="btn btn-primary" id="btn_guardar">
                    <i class="icon-floppy-disk"></i> Guardar Cambios
                </button>
            <?php
            }
            ?>
        </div>
    </div>
</form>
<script>
    $(function() {
        <?php
        if($type == 3){
            ?>
        $('#frm_guardar :input:not(:button)').attr({
            'disabled': 'disabled'
        });
            <?php
        }
        ?>
    });
</script>
<?php
    }else{
        echo "<h1>No se puede acceder a la información solicitada</h1>";
    }
?>