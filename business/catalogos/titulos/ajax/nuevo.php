<?php
session_start();
$dir_fc = "../../../../";
/*-----------------------------------      Estableciendo la Clase  --------------------------------------*/
include_once $dir_fc . 'data/cat_titulo.class.php';
/*--------------------------------------------------------------------------------------------------------*/
include_once $dir_fc.'connections/trop.php'; //Inclueye configuración de fecha y  hora de mexico
include_once $dir_fc.'connections/php_config.php'; //Inclueye configuración de fecha y  hora de mexico

$cDatos = new cTitulos();
$resultadoEnBd = 0;
$done = 0;
$resp = "";

if(!isset($_SESSION[_type_])){ $_SESSION[_type_] = 0; }

$descripcion = "";
$abreviacion = "";
//$activo = "";

extract($_REQUEST);

if($descripcion == "" || $abreviacion == ""){ //Verficando datos vacios
    $resp = "Debes ingresar los datos correctamente";
} else {
    $cDatos->setDescripcion($descripcion);
    $cDatos->setAbreviacion($abreviacion);
    //$cDatos->setActivo($activo);
    $claveRepetida = 0;

    if($_SESSION[_type_] == 2){
        $cDatos->setId_titulo($_SESSION[_editar_]);
        $claveRepetida = $cDatos->buscaCoincidencia($cDatos);
    }else{
        $claveRepetida = $cDatos->buscaTitulo($cDatos);
    }

    if (!is_numeric($claveRepetida)){
        $resp = $claveRepetida;
    } else {
        if ($claveRepetida>0) {
            $resp = "El Titulo capturado ya existe en la base de datos, intente con otro descripcion ó modifique el banco correspondiente: (" . $descripcion .")";
        } else {
            if($_SESSION[_type_] == 2){
                $resultadoEnBd = $cDatos->actualizaRegistro($cDatos);
            }else{
                $resultadoEnBd = $cDatos->agregarRegistro($cDatos);
            }

            if(is_numeric($resultadoEnBd) AND $resultadoEnBd>0){
                $done = 1;
                $resp = "Concepto agregado/actualizado correctamente.";
            }else{
                $resp = "Ocurrió un incoveniente con la base de datos: -- ".$resultadoEnBd;
            }
        }
    }
}
echo json_encode(array("done" => $done, "resp" => $resp));
?>
