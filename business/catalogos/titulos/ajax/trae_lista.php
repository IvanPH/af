<?php
session_start();
/*--------------------------------------------------------------------------------------------------------
Regresará un json con La lista que se pintará en la tabla
*/
$dir_fc       = "../../../../";
include_once $dir_fc.'data/cat_titulo.class.php';
/*--------------------------------------------------------------------------------------------------------*/
include_once $dir_fc.'connections/trop.php';        //Inclueye configuración de fecha y  hora de mexico
include_once $dir_fc.'connections/php_config.php'; //Inclueye configuración de sesiones para este proyecto

$cAccion   = new cTitulos();
/*--------------------------------------------------------------------------------------------------------*/
extract($_REQUEST);

$datos = array();

$dataList = $cAccion->getAllRegAjax();
while($row = $dataList->fetch(PDO::FETCH_OBJ)){
    $iId             = $row->id_titulo;
    $descripcion     = $row->descripcion;
    $abreviacion     = $row->abreviacion;
    /*-- 
    $iactivo         = $row->activo;
    $status     = "<span class='label label-danger'><i class='icon-blocked'></i> Inactivo</span>";

    if($iactivo == 1){
        $status      = "<span class='label label-success'><i class='icon-checkmark3'></i> Activo</span>";
        $bajaAlta    = "bajaModal";
        $icoAB       = "fa fa-caret-square-o-down text-warning";
        $titleAB     = "Dar de baja";
    }else{
        $bajaAlta    = "alta";
        $icoAB       = "fa fa-caret-square-o-up text-success";
        $titleAB     = "Dar de Alta";
    }   --*/

    $function = "";
    $function = "<ul class='icons-list'>";
        $function.= "<li class=\"text-blue-800\">
                        <a href='javascript:void(0)' data-popup='tooltip' title='Ver' 
                           onclick='openFrm(3, ".$iId.")'
                           data-target='#call' data-original-title='Ver'>
                            <i class=\"fa fa-eye\" aria-hidden=\"true\"></i>
                        </a>
                     </li>";
    if($_SESSION[edit] == 1){//Si puede editar
        $function.= "<li class=\"text-blue-800\">
                        <a href='javascript:void(0)' data-popup='tooltip' title='Editar' 
                           onclick='openFrm(2, ".$iId.")'
                           data-target='#call' data-original-title='Editar'>
                            <i class='icon-pencil4'></i>
                        </a>
                     </li>";
    }
    if($_SESSION[elim] == 1){//Si puede Eliminar
        $function.= "<li class='text-danger-600'>
                    <a href='#' onClick='eliminaModal(".$iId.")' data-popup='tooltip' title='Eliminar' data-toggle='modal' 
                       data-target='#call' data-original-title='Eliminar' data-placement='top'>
                        <i class=' icon-trash'></i>
                    </a>
                 </li>";
    }
    $function.= "</ul>";
    $datos[] = array(
        "id_titulo"      => $iId,
        "descripcion"	 => $descripcion,
        "abreviacion"	 => $abreviacion,
        "function"	     => $function
    );
}

echo json_encode(array("data" => $datos));
?>
