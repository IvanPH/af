<?php
session_start();
$dir_fc = "../../../../";
/*-----------------------------------      Estableciendo la Clase  --------------------------------------*/
include_once $dir_fc . 'data/cat_bancos.class.php';
/*--------------------------------------------------------------------------------------------------------*/
include_once $dir_fc.'connections/trop.php'; //Inclueye configuración de fecha y  hora de mexico
include_once $dir_fc.'connections/php_config.php'; //Inclueye configuración de fecha y  hora de mexico

$cDatos = new cBancos();
$resultadoEnBd = 0;
$done = 0;
$resp = "";

if(!isset($_SESSION[_type_])){ $_SESSION[_type_] = 0; }

$nombre = "";
$razon_social = "";
$activo = "";

extract($_REQUEST);

if($nombre == "" || $razon_social == "" || $activo == ""){ //Verficando datos vacios
    $resp = "Debes ingresar los datos correctamente";
} else {
    $cDatos->setNombre($nombre);
    $cDatos->setRazon_social($razon_social);
    $cDatos->setActivo($activo);
    $claveRepetida = 0;

    if($_SESSION[_type_] == 2){
        $cDatos->setId_banco($_SESSION[_editar_]);
        $claveRepetida = $cDatos->buscaCoincidencia($cDatos);
    }else{
        $claveRepetida = $cDatos->buscaBanco($cDatos);
    }

    if (!is_numeric($claveRepetida)){
        $resp = $claveRepetida;
    } else {
        if ($claveRepetida>0) {
            $resp = "El Banco capturado ya existe en la base de datos, intente con otro nombre ó modifique el banco correspondiente: (" . $nombre .")";
        } else {
            if($_SESSION[_type_] == 2){
                $resultadoEnBd = $cDatos->actualizaRegistro($cDatos);
            }else{
                $resultadoEnBd = $cDatos->agregarRegistro($cDatos);
            }

            if(is_numeric($resultadoEnBd) AND $resultadoEnBd>0){
                $done = 1;
                $resp = "Concepto agregado/actualizado correctamente.";
            }else{
                $resp = "Ocurrió un incoveniente con la base de datos: -- ".$resultadoEnBd;
            }
        }
    }
}
echo json_encode(array("done" => $done, "resp" => $resp));
?>
