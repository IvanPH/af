<?php
session_start();

$type = 0;
$id   = 0;

extract($_REQUEST);

/*--------------------------------------------------------------------------------------------------------*/
$dir_fc       = "../../../../";
include_once $dir_fc.'connections/trop.php';
include_once $dir_fc.'connections/php_config.php';
include_once $dir_fc . 'data/cat_bancos.class.php';

$id_banco = "";
$nombre = "";
$razon_social = "";
$activo = "";


if(($_SESSION[nuev] == 1 && $type == 1) || ($_SESSION[edit] == 1 && $type == 2 && $id > 0) || $type == 3){
    //Si tiene permisos de agregar un nuevo documento
    $_SESSION[_type_] = $type;
    if($type == 2 || $type == 3){
        $cAccion = new cBancos();
        $rsEditar = $cAccion->getRegbyid($id);
        $rsEditar->rowCount();

        if($rsEditar->rowCount() > 0){
            $_SESSION[_editar_] = $id; //Importante que si es ediciòn se asigne el ID que se va a editar
            $arrEdi             = $rsEditar->fetch(PDO::FETCH_OBJ);
            $nombre             = $arrEdi->nombre;
            $razon_social       = $arrEdi->razon_social;
            $activo             = $arrEdi->activo;
        }
    }
?>
<script type="text/javascript" src="<?php echo $raiz?>js/app/floatingLabels.js"></script>
<form id="frm_guardar" class="" method="post" action="javascript: guardar()" >
    <div class="modal-content">
        <!-- Caja de respuetas a la accion del usuario -->
        <div class="row">
            <div class="col-md-12">
                <div id="respuesta_ajax_modal">
                </div>
            </div>
        </div>

        <!-- Encabezado de la caja de captura -->
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h5 class="modal-title"> <?php
                if($type == 1){
                    echo "Agregando";
                } elseif($type == 2){
                    echo "Editando";
                }elseif($type == 3){
                    echo "Visualizando";
                }
                ?> Banco</h5>
        </div>

        <!-- Caja de captura -->
        <div class="modal-body">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-xs-12 col-md-12 col-sm-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h6 class="panel-title">Datos de Banco</h6>
                        </div>

                        <div class="panel-body">
                            <fieldset>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group ">
                                            <label class="control-label">
                                                Banco <span class="text-danger">*</span>
                                            </label>
                                            <input type="text" class="form-control" name="nombre" id="nombre" autocomplete="off"
                                                   required placeholder="Banco *" value="<?php echo  $nombre ?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-7">
                                        <div class="form-group ">
                                            <label  class="control-label" for="razon_social">
                                                Razón Social <span class="text-danger">*</span>
                                            </label>
                                            <input type="text" class="form-control" name="razon_social" id="razon_social" autocomplete="off"
                                                   value="<?php echo $razon_social ?>"
                                                   placeholder="Razón Social *">
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="form-group ">
                                            <label for="activo" class="control-label">
                                                Estado: <span class="text-danger">*</span>
                                            </label>
                                            <select name="activo" id="activo" class="form-control" required>
                                                <?php
                                                    if ($type == 1) {
                                                ?>
                                            <option value="1" <?php if($activo== "1"){ echo "selected";}?>>Activo</option>
                                                <?php
                                                } else {
                                                ?>
                                            <option value="">Estado *</option>
                                            <option value="1" <?php if($activo== "1"){ echo "selected";}?>>Activo</option>
                                            <option value="0" <?php if($activo == "0"){ echo "selected";}?>>Inactivo</option>
                                                <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Pie de la caja de captura con botones de acciones -->
        <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">
                <i class="icon-close2"></i> Cerrar
            </button>
            <?php
            if($type <> 3){
                ?>
                <button type="submit" class="btn btn-primary" id="btn_guardar">
                    <i class="icon-floppy-disk"></i> Guardar Cambios
                </button>
            <?php
            }
            ?>
        </div>
    </div>
</form>
<script>
    $(function() {
        <?php
        if($type == 3){
            ?>
        $('#frm_guardar :input:not(:button)').attr({
            'disabled': 'disabled'
        });
            <?php
        }
        ?>
    });
</script>
<?php
    }else{
        echo "<h1>No se puede acceder a la información solicitada</h1>";
    }
?>