<?php
session_start();
$dir_fc = "../../../../";
/*-----------------------------------      Estableciendo la Clase  --------------------------------------*/
include_once $dir_fc . 'data/cat_beneficiario.class.php';
include_once $dir_fc . 'data/movimientos.php';
include_once $dir_fc . 'common/function.class.php';
/*--------------------------------------------------------------------------------------------------------*/
include_once $dir_fc.'connections/trop.php'; //Inclueye configuración de fecha y  hora de mexico
include_once $dir_fc.'connections/php_config.php'; //Inclueye configuración de fecha y  hora de mexico
$cFuntion = new cFunction();
$cDatos = new cBeneficiario();
$cMov = new cMovimientos();
$resultadoEnBd = 0;
$done = 0;
$resp = "";

if(!isset($_SESSION[_type_])){ $_SESSION[_type_] = 0; }

$id_beneficiario  = "";
$rfc              = "";
$nombre           = "";
$apepa            = "";
$apema            = "";
$curp             = "";
$estudios         = "";
$domiciio         = "";
$tel              = "";
$fecha            = "";
$id_titulo        = "";
$id_centrotrabajo = "";
$id_siaff         = "";
$id_sicop         = "";
$sueldo           = "";
$f_nacimiento     = "";
$id_puesto        = "";
$noempleado       = "";
$email            = "";
$cuenta_bancaria  = "";
$segurosocial     = "";
$estadocivil      = "";
$id_banco         = "";
$resguardo        = "";
$celular          = "";
$contactoemergencia = "";
$telemergencia    = "";
$id_activo        ="";
$activo           = "";

extract($_REQUEST);
if ($fecha == "00/00/0000" || $fecha == NULL) {
    $fecha = "1901-01-01";
} else {
    $fecha = $cFuntion ->formatear_fecha_ins($fecha);
}
if ($f_nacimiento == "00/00/0000" || $f_nacimiento == NULL) {
    $f_nacimiento = "1901-01-01";
} else {
    $f_nacimiento = $cFuntion ->formatear_fecha_ins($f_nacimiento);
}
if($rfc == "" || $nombre == "" || $apepa == "" || $apema == ""|| $curp == ""){ //Verficando datos vacios
    $resp = "Debes ingresar los datos correctamente";
} else {
    $matricula_empleado = $rfc."-".$id_centrotrabajo."-".$id_puesto;
    $today = date("Y-m-d");     
    $tabla = "Empleados";     
    $cDatos->setRFC($rfc);
    $cDatos->setNombre($nombre);
    $cDatos->setApepa($apepa);
    $cDatos->setApema($apema);
    $cDatos->setCurp($curp);
    $cDatos->setEstudios($estudios);
    $cDatos->setDomiciiio($domiciio);
    $cDatos->setTel($tel);
    $cDatos->setFecha($fecha);
    $cDatos->setId_titulo($id_titulo);
    $cDatos->setId_centrotrabajo($id_centrotrabajo);
    $cDatos->setSueldo($sueldo);
    $cDatos->setF_nacimiento($f_nacimiento);
    $cDatos->setId_puesto($id_puesto);
    $cDatos->setNoempleado($matricula_empleado);
    $cDatos->setEmail($email);
    $cDatos->setCuenta_bancaria($cuenta_bancaria);
    $cDatos->setSegurosocial($segurosocial);
    $cDatos->setEstadocivil($estadocivil);
    $cDatos->setId_banco($id_banco);
    $cDatos->setResguardo($resguardo);
    $cDatos->setCelular($celular);
    $cDatos->setContactoemergencia($contactoemergencia);
    $cDatos->setTelemergencia($telemergencia);
    $cDatos->setActivo($activo);
    $cMov->setId_user($_SESSION[id_usr]);
    $cMov->setFecha($today);
    $cMov->setId_registro($_SESSION[_editar_]);
    $cMov->setNombre_tabla($tabla);

    $claveRepetida = 0;

    if($_SESSION[_type_] == 1){
        $claveRepetida = $cDatos->buscaCoincidencia($cDatos);
    }elseif($_SESSION[_type_] == 2){
        $cDatos->setId_beneficiario($_SESSION[_editar_]);
        $claveRepetida = $cDatos->buscaCoincidencia_Edit($cDatos);
    }else{
        $claveRepetida = $cDatos->buscaBeneficiario($cDatos);
    }

    if (!is_numeric($claveRepetida)){
        $resp = $claveRepetida;
    } else {
        if ($claveRepetida>0) {
            $resp = "El Empleado capturado esta relacionado con un CURP o RFC ya existente en la base de datos.";
        } else {
            if($_SESSION[_type_] == 2){
                $resultadoEnBd = $cDatos->actualizaRegistro($cDatos);
                $cMov->setTipo_mov("Actualizo datos");
                $cMov->insertReg();
            }else{
                $resultadoEnBd = $cDatos->agregarRegistro($cDatos);
                $ultimo = $cDatos->getMax();
                $cDatos->agregarAF($ultimo, $id_activo);
                $cMov->setTipo_mov("Agrego nuevo usuario");
                $cMov->setId_registro($ultimo);
                $cMov->insertReg();
            }

            if(is_numeric($resultadoEnBd) AND $resultadoEnBd>0){
                $done = 1;
                $resp = "Concepto agregado/actualizado correctamente.";
            }else{
                $resp = "Ocurrió un incoveniente con la base de datos: -- ".$resultadoEnBd;
            }
        }
    }
}
echo json_encode(array("done" => $done, "resp" => $resp));
?>
