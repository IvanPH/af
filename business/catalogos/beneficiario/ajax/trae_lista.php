<?php
session_start();
/*--------------------------------------------------------------------------------------------------------
Regresará un json con La lista que se pintará en la tabla
*/
$dir_fc       = "../../../../";
include_once $dir_fc.'data/cat_beneficiario.class.php';
/*--------------------------------------------------------------------------------------------------------*/
include_once $dir_fc.'connections/trop.php';        //Inclueye configuración de fecha y  hora de mexico
include_once $dir_fc.'connections/php_config.php'; //Inclueye configuración de sesiones para este proyecto

$cAccion   = new cBeneficiario();
/*--------------------------------------------------------------------------------------------------------*/
extract($_REQUEST);

$datos = array();

$dataList = $cAccion->getAllRegAjax();
while($row = $dataList->fetch(PDO::FETCH_OBJ)){
    $iId                 = $row->id_beneficiario;
    $RFC                 = $row->rfc;
    $nombre              = $row->nombre;
    $apepa               = $row->apepa;
    $apema               = $row->apema;
    $curp                = $row->curp;
    $estudios            = $row->estudios;
    $domiciio            = $row->domiciio;
    $tel                 = $row->tel;
    $fecha               = $row->fecha;
    $id_titulo           = $row->id_titulo;
    $id_centrotrabajo    = $row->id_centrotrabajo;
    $iactivo             = $row->activo;
    $f_nac               = $row->f_nacimiento;
    $puesto              = $row->id_puesto;
    $noempleado          = $row->noempleado;
    $email               = $row->email;
    $cuenta_bancaria     = $row->cuenta_bancaria;
    $segurosocial        = $row->segurosocial;
    $estadocivil         = $row->estadocivil;
    $banco               = $row->id_banco;
    $resguardo           = $row->resguardo;
    $celular             = $row->celular;     
    $contactoemergencia  = $row->contactoemergencia; 
    $telemergencia       = $row->telemergencia;
    $status              = "<span class='label label-danger'><i class='icon-blocked'></i> Inactivo</span>";
    $nombreCompleto      = $nombre. " " . $apepa . " " . $apema;
    if ($id_titulo == "0" || $id_titulo == NULL) {
        $titulo = "SIN OTORGAR";
    } else {
        $rsEditar = $cAccion->getTitulobyid($id_titulo);
        $rsEditar->rowCount();
        if($rsEditar->rowCount() > 0){
            $arrEdi    = $rsEditar->fetch(PDO::FETCH_OBJ);
            $titulo  = $arrEdi->descripcion;
        }
    }
    if ($id_centrotrabajo == "0" || $id_centrotrabajo == NULL) {
        $CT = "SIN OTORGAR";
    } else {
        $rsEditar = $cAccion->getCTrabajobyid($id_centrotrabajo);
        $rsEditar->rowCount();
            if($rsEditar->rowCount() > 0){
                $arrEdi  = $rsEditar->fetch(PDO::FETCH_OBJ);
                $CT      = $arrEdi->area;
            }
    } 
    if ($puesto == "0" || $puesto == NULL) {
        $puestoName = "SIN OTORGAR";
    } else {
        $rsEditar = $cAccion->getPuestobyid($puesto);
        $rsEditar->rowCount();
            if($rsEditar->rowCount() > 0){
                $arrEdi  = $rsEditar->fetch(PDO::FETCH_OBJ);
                $puestoName      = $arrEdi->desc_puesto;
            }
    } 
    if ($banco == "0" || $banco == NULL) {
        $bancoName = "SIN OTORGAR";
    } else {
        $rsEditar = $cAccion->getBancobyid($banco);
        $rsEditar->rowCount();
            if($rsEditar->rowCount() > 0){
                $arrEdi  = $rsEditar->fetch(PDO::FETCH_OBJ);
                $bancoName      = $arrEdi->nombre;
            }
    } 
    if ($resguardo == "0" || $banco == NULL) {
        $resguardoN = "No puede resguardar";
    } else {
        $resguardoN = "Puede resguardar";
    } 
    if($iactivo == 1){
        $status      = "<span class='label label-success'><i class='icon-checkmark3'></i> Activo</span>";
    }elseif($iactivo == 0){
        $status      = "<span class='label label-danger'><i class='icon-blocked'></i> Inactivo</span>";
    }
    else{
        $status      = "<span class='label label-danger '><i class='icon-blocked'></i> Baja</span>";
    }

    $function = "";
    $function = "<ul class='icons-list'>";
        $function.= "<li class=\"text-blue-800\">
                        <a href='javascript:void(0)' data-popup='tooltip' title='Ver' 
                           onclick='openFrm(3, ".$iId.")'
                           data-target='#call' data-original-title='Ver'>
                            <i class=\"fa fa-eye\" aria-hidden=\"true\"></i>
                        </a>
                     </li>";
    if($_SESSION[edit] == 1){//Si puede editar
        $function.= "<li class=\"text-blue-800\">
                        <a href='javascript:void(0)' data-popup='tooltip' title='Editar' 
                           onclick='openFrm(2, ".$iId.")'
                           data-target='#call' data-original-title='Editar'>
                            <i class='icon-pencil4'></i>
                        </a>
                     </li>";
    }
    if($_SESSION[elim] == 1){//Si puede Eliminar
        $function.= "<li class='text-danger-600'>
                    <a href='#' onClick='eliminaModal(".$iId.")' data-popup='tooltip' title='Eliminar' data-toggle='modal' 
                       data-target='#call' data-original-title='Eliminar' data-placement='top'>
                        <i class=' icon-trash'></i>
                    </a>
                 </li>";
    }
    $function.= "</ul>";
    $datos[] = array(
        "id_beneficiario"            => $iId,
        "rfc"                        => $RFC,
        "nombre"	                 => $nombreCompleto,
        "curp"	                     => $curp,
        "estudios"	                 => $estudios,
        "domiciio"	                 => $domiciio,
        "tel"	                     => $tel,
        "fecha"	                     => $fecha,
        "id_titulo"	                 => $titulo,
        "id_centrotrabajo"	         => $CT,
        "activo"	                 => $status,
        "f_nacimiento"	             => $f_nac,
        "id_puesto"	                 => $puestoName,
        "noempleado"	             => $noempleado,
        "email"	                     => $email,
        "cuenta_bancaria"	         => $cuenta_bancaria,
        "segurosocial"	             => $segurosocial,
        "estadocivil"	             => $estadocivil,
        "id_banco"	                 => $bancoName,
        "resguardo"	                 => $resguardoN,
        "celular"	                 => $celular,
        "function"	                 => $function,
        "contactoemergencia"         => $contactoemergencia,
        "telemergencia"              => $telemergencia
    );
}

echo json_encode(array("data" => $datos));
?>
