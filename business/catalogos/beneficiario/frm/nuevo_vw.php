<?php
session_start();

$type = 0;
$id   = 0;

extract($_REQUEST);

/*--------------------------------------------------------------------------------------------------------*/
$dir_fc       = "../../../../";
include_once $dir_fc.'connections/trop.php';
include_once $dir_fc.'connections/php_config.php';
include_once $dir_fc . 'data/cat_beneficiario.class.php';
include_once $dir_fc . 'common/function.class.php';
$cFuntion = new cFunction();

$id_beneficiario  = "";
$rfc              = "";
$nombre           = "";
$apepa            = "";
$apema            = "";
$curp             = "";
$estudios         = "";
$domiciio         = "";
$tel              = "";
$fecha            = "";
$id_titulo        = "";
$id_centrotrabajo = "";
$id_siaff         = "";
$id_sicop         = "";
$activo           = "";
$sueldo             = "";
$f_nacimiento       = "";
$id_puesto          = "";
$noempleado         = "";
$email              = "";
$cuenta_bancaria    = "";
$segurosocial       = "";
$estadocivil        = "";
$id_banco           = "";
$resguardo          = "";
$celular            = "";
$contactoemergencia = "";
$telemergencia      = "";
$id_activo      = "";


if(($_SESSION[nuev] == 1 && $type == 1) || ($_SESSION[edit] == 1 && $type == 2 && $id > 0) || $type == 3){
    //Si tiene permisos de agregar un nuevo documento
    $_SESSION[_type_] = $type;
    if($type == 2 || $type == 3){
        $cAccion = new cBeneficiario();
        $rsEditar = $cAccion->getRegbyid($id);
        $rsEditar->rowCount();

        if($rsEditar->rowCount() > 0){
            $_SESSION[_editar_] = $id; //Importante que si es ediciòn se asigne el ID que se va a editar
            $arrEdi             = $rsEditar->fetch(PDO::FETCH_OBJ);
            $rfc                = $arrEdi->rfc;
            $nombre             = $arrEdi->nombre;
            $apepa              = $arrEdi->apepa;
            $apema              = $arrEdi->apema;
            $curp               = $arrEdi->curp;
            $estudios           = $arrEdi->estudios;
            $domiciio           = $arrEdi->domiciio;
            $tel                = $arrEdi->tel;
            $fecha              = $arrEdi->fecha;
            $id_titulo          = $arrEdi->id_titulo;
            $id_centrotrabajo   = $arrEdi->id_centrotrabajo;
            $activo             = $arrEdi->activo;
            $sueldo             = $arrEdi->sueldo;
            $f_nacimiento       = $arrEdi->f_nacimiento;
            $id_puesto          = $arrEdi->id_puesto;
            $noempleado         = $arrEdi->noempleado;
            $email              = $arrEdi->email;
            $cuenta_bancaria    = $arrEdi->cuenta_bancaria;
            $segurosocial       = $arrEdi->segurosocial;
            $estadocivil        = $arrEdi->estadocivil;
            $id_banco           = $arrEdi->id_banco;
            $resguardo          = $arrEdi->resguardo;
            $celular            = $arrEdi->celular;
            $contactoemergencia = $arrEdi->contactoemergencia;
            $telemergencia      = $arrEdi->telemergencia;
        }
        if ($fecha == "1901-01-01" || $fecha == NULL || $fecha == "0000-00-00") {
            $fecha = "00/00/0000";
        } else {
            $fecha = $cFuntion ->formatear_fecha_show($fecha);
        }
        if ($f_nacimiento == "1901-01-01" || $f_nacimiento == NULL || $f_nacimiento == "0000-00-00") {
            $f_nacimiento = "00/00/0000";
        } else {
            $f_nacimiento = $cFuntion ->formatear_fecha_show($f_nacimiento);
        }
    }
?>
<script type="text/javascript" src="<?php echo $raiz?>js/app/floatingLabels.js"></script>
<form id="frm_guardar" class="" method="post" action="javascript: guardar()" >
    <div class="modal-content">
        <!-- Encabezado de la caja de captura -->
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h5 class="modal-title"> <?php
                if($type == 1){
                    echo "Agregando";
                } elseif($type == 2){
                    echo "Editando";
                }elseif($type == 3){
                    echo "Visualizando";
                }
                ?> Empleado</h5>
        </div>

        <!-- Caja de captura -->
        <div class="modal-body">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-xs-12 col-md-12 col-sm-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h6 class="panel-title">Datos del Empleado</h6>
                        </div>


                        <div class="panel-body">
                            <fieldset>
                            <div class="row">
                            <h2>Datos Personales</h2>
                            <hr>
                                    <div class="col-sm-4">
                                        <div class="form-group ">
                                            <label class="control-label">
                                                Nombre(s)<span class="text-danger">*</span>
                                            </label>
                                            <input onkeyup="mayus(this);" type="text" class="form-control" name="nombre" id="nombre" autocomplete="off"
                                                    placeholder="Nombre(s) *" value="<?php echo $nombre ?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group ">
                                            <label  class="control-label" for="apepa">
                                                Apellido Paterno <span class="text-danger">*</span>
                                            </label>
                                            <input onkeyup="mayus(this);" type="text" class="form-control" name="apepa" id="apepa" autocomplete="off"
                                                   value="<?php echo $apepa ?>"
                                                   placeholder="Apellido Paterno *">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group ">
                                            <label class="control-label" for="apema">Apellido Materno
                                                <span class="text-danger">*</span>
                                            </label>
                                            <input onkeyup="mayus(this);" type="text" class="form-control" name="apema" id="apema" autocomplete="off"
                                                   placeholder="Apellido Materno *" value="<?php echo $apema ?>">
                                        </div>
                                    </div>
                                </div>
                                <br>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group ">
                                            <label for="rfc" class="control-label">
                                                RFC <span class="text-danger">*</span>
                                            </label>
                                            <input maxlength="13" onkeyup="mayus(this);" type="text" class="form-control" name="rfc" id="rfc" autocomplete="off"
                                                    placeholder="RFC *"   value="<?php echo $rfc ?>"
                                                   title="RFC del empleado">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group ">
                                            <label for="curp" class="control-label">CURP
                                                <span class="text-danger">*</span>
                                            </label>
                                            <input maxlength="18" onkeyup="mayus(this);" type="curp" class="form-control" name="curp" id="curp"
                                                    autocomplete="off" placeholder="CURP *"
                                                   value="<?php echo $curp?>">
                                        </div>
                                    </div>
                                    
                                </div>
                                <br/>

                                <div class="row">
                                    <div class="col-sm-9">
                                        <div class="form-group ">
                                            <label for="domiciio" class="control-label">Domicilio
                                               
                                            </label>
                                            <input type="domiciio" class="form-control" name="domiciio" id="domiciio"
                                                    autocomplete="off" placeholder="Domicilio *"
                                                   value="<?php echo $domiciio?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label  class="control-label" for="tel">
                                                Telefono
                                            </label>
                                            <input type="text" class="form-control" name="tel" id="tel"
                                                autocomplete="off" 
                                                value="<?php echo $tel ?>"
                                                placeholder="Telefono *">
                                        </div>
                                    </div>
                                    
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group ">
                                            <label for="estudios" class="control-label">Estudios
                                                <span class="text-danger">*</span>
                                            </label>
                                            <input onkeyup="mayus(this);" type="estudios" class="form-control" name="estudios" id="estudios"
                                                    autocomplete="off" placeholder="Estudios *"
                                                   value="<?php echo $estudios?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="id_titulo" class="control-label">
                                            Titulo: 
                                            </label>
                                            <select name="id_titulo" id="id_titulo" class="form-control">
                                                <option value="0">SIN OTORGAR</option>
                                                <?php
                                                    $cAccion = new cBeneficiario();
                                                    $rs_ben = $cAccion->getTitulo();
                                            
                                                
                                                while($rw_ben = $rs_ben->fetch(PDO::FETCH_OBJ)){
                                                    $selected_ts = "";
                                                    if($type == 2 || $type == 3){
                                                        //Aquí serán las condiciones para poner los que van seleccionados
                                                        if($id_titulo == $rw_ben->id_titulo){
                                                            $selected_ts = "selected";
                                                        }
                                                    }
                                                    ?>
                                                    <option value="<?php echo $rw_ben->id_titulo?>"
                                                        <?php echo $selected_ts?>>
                                                        <?php echo $rw_ben->descripcion?>
                                                    </option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    
                                    <div class="col-sm-4" >
                                        <div class="form-group">
                                            <label  class="control-label" for="f_nacimiento">
                                                Fecha de nacimiento
                                            </label>
                                            <input type="text" class="form-control" name="f_nacimiento" id="f_nacimiento"
                                                autocomplete="off" 
                                                value="<?php echo $f_nacimiento?>"
                                                placeholder="Fecha de nacimiento *">
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <br>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group ">
                                            <label for="email" class="control-label">Email
                                                <span class="text-danger">*</span>
                                            </label>
                                            <input  type="email" class="form-control" name="email" id="email"
                                                    autocomplete="off" placeholder="Email *"
                                                   value="<?php echo $email?>">
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="id_banco" class="control-label">
                                            Banco: 
                                            </label>
                                            <select name="id_banco" id="id_banco" class="form-control">
                                                <option value="0">SIN OTORGAR</option>
                                                <?php
                                                    $cAccion = new cBeneficiario();
                                                    $rs_ben = $cAccion->getBancos();
                                            
                                                
                                                while($rw_ben = $rs_ben->fetch(PDO::FETCH_OBJ)){
                                                    $selected_ts = "";
                                                    if($type == 2 || $type == 3){
                                                        //Aquí serán las condiciones para poner los que van seleccionados
                                                        if($id_banco == $rw_ben->id_banco){
                                                            $selected_ts = "selected";
                                                        }
                                                    }
                                                    ?>
                                                    <option value="<?php echo $rw_ben->id_banco?>"
                                                        <?php echo $selected_ts?>>
                                                        <?php echo $rw_ben->nombre?>
                                                    </option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    
                                    <div class="col-sm-4">
                                        <div class="form-group ">
                                            <label for="celular" class="control-label">Celular
                                                <span class="text-danger">*</span>
                                            </label>
                                            <input type="celular" class="form-control" name="celular" id="celular"
                                                    autocomplete="off" placeholder="Numero de Celular *"
                                                   value="<?php echo $celular?>">
                                        </div>
                                    </div>
                                </div>
                                <br/>       
                                <br>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group ">
                                            <label for="cuenta_bancaria" class="control-label">Cuenta bancaria
                                                <span class="text-danger">*</span>
                                            </label>
                                            <input type="text" class="form-control" name="cuenta_bancaria" id="cuenta_bancaria"
                                                   autocomplete="off" placeholder="cuenta bancaria *"
                                                   value="<?php echo $cuenta_bancaria?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group ">
                                            <label for="segurosocial" class="control-label">Seguro social
                                                <span class="text-danger">*</span>
                                            </label>
                                            <input type="text" class="form-control" name="segurosocial" id="segurosocial"
                                                    autocomplete="off" placeholder="Seguro social*"
                                                   value="<?php echo $segurosocial?>">
                                        </div>
                                    </div>                    
                                    <div class="col-sm-4">
                                            <div class="form-group ">
                                                <label for="estadocivil" class="control-label">
                                                    Estado civil
                                                </label>
                                                <select name="estadocivil" id="estadocivil" class="form-control" >
                                                    <option value="">Estado civil *</option>
                                                    <option value="0" <?php if($estadocivil == '0'){ echo "selected";}?>>Soltero</option>
                                                    <option value="1" <?php if($estadocivil == '1'){ echo "selected";}?>>Casado</option>
                                                    <option value="2" <?php if($estadocivil == '2'){ echo "selected";}?>>Viudo/a</option>
                                                </select>
                                            </div>
                                        </div>
                                </div>
                                <br/>         
                                
                                
                                                
                               <!-- Datos del sistema -->   
                               <br>
                                <div class="row">
                                    <h2>Datos Sistema</h2><hr>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="id_centrotrabajo" class="control-label">
                                            Departamentos o áreas * 
                                            </label>
                                            <select name="id_centrotrabajo" id="id_centrotrabajo" class="form-control">
                                            <option value="0">SIN OTORGAR</option>
                                                <?php
                                                    $cAccion = new cBeneficiario();
                                                    $rs_ben = $cAccion->getCTrabajo();
                                            
                                                
                                                while($rw_ben = $rs_ben->fetch(PDO::FETCH_OBJ)){
                                                    $selected_ts = "";
                                                    if($type == 2 || $type == 3){
                                                        //Aquí serán las condiciones para poner los que van seleccionados
                                                        if($id_centrotrabajo == $rw_ben->id_centrotrabajo){
                                                            $selected_ts = "selected";
                                                        }
                                                    }
                                                    ?>
                                                    <option value="<?php echo $rw_ben->id_centrotrabajo?>"
                                                        <?php echo $selected_ts?>>
                                                        <?php echo $rw_ben->area?>
                                                    </option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="id_puesto" class="control-label">
                                            Puesto
                                            </label>
                                            <select name="id_puesto" id="id_puesto" class="form-control">
                                                <option value="0">SIN OTORGAR</option>
                                                <?php
                                                    $cAccion = new cBeneficiario();
                                                    $rs_ben = $cAccion->getPuesto();
                                            
                                                
                                                while($rw_ben = $rs_ben->fetch(PDO::FETCH_OBJ)){
                                                    $selected_ts = "";
                                                    if($type == 2 || $type == 3){
                                                        //Aquí serán las condiciones para poner los que van seleccionados
                                                        if($id_puesto == $rw_ben->id_puesto){
                                                            $selected_ts = "selected";
                                                        }
                                                    }
                                                    ?>
                                                    <option value="<?php echo $rw_ben->id_puesto?>"
                                                        <?php echo $selected_ts?>>
                                                        <?php echo $rw_ben->desc_puesto?>
                                                    </option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label  class=" control-label" for="noempleado">
                                                Matricula de empleado
                                            </label>
                                            <input disabled type="text" class="form-control" name="noempleado" id="noempleado"
                                                autocomplete="off" 
                                                value="<?php echo $noempleado ?>"
                                                placeholder="Se genera automaticamente *">
                                        </div>
                                    </div>
                                    <div class="col-sm-3" >
                                        <div class="form-group">
                                            <label  class="control-label" for="fecha">
                                                Fecha de ingreso
                                            </label>
                                            <input type="text" class="form-control" name="fecha" id="fecha"
                                                autocomplete="off" 
                                                value="<?php echo $fecha?>"
                                                placeholder="Fecha de ingreso *">
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-sm-3">
                                            <div class="form-group ">
                                                <label for="resguardo" class="control-label">
                                                    Permisos de Resguardo: 
                                                </label>
                                                <select name="resguardo" id="resguardo" class="form-control" >
                                                    <option value="">¿El usuario tiene permiso de resguardar? *</option>
                                                    <option value="0" <?php if($resguardo == '0'){ echo "selected";}?>>Sin permiso</option>
                                                    <option value="1" <?php if($resguardo == '1'){ echo "selected";}?>>Permitido resguardar</option>
                                                </select>
                                            </div>
                                        </div>
                                    <div class="col-sm-3" >
                                        <div class="form-group">
                                            <label  class="control-label" for="contactoemergencia">
                                                Contacto Emergencia
                                            </label>
                                            <input type="text" class="form-control" name="contactoemergencia" id="contactoemergencia"
                                                autocomplete="off" 
                                                value="<?php echo $contactoemergencia ?>"
                                                placeholder="Contacto Emergencia *">
                                        </div>
                                    </div>
                                    <div class="col-sm-3" >
                                        <div class="form-group">
                                            <label  class="control-label" for="telemergencia">
                                                Telefono de emergencia
                                            </label>
                                            <input type="text" class="form-control" name="telemergencia" id="telemergencia"
                                                autocomplete="off" 
                                                value="<?php echo $telemergencia ?>"
                                                placeholder="Telefono de emergencia *">
                                        </div>
                                    </div>
                                    <div class="col-sm-3" >
                                        <div class="form-group">
                                            <label  class="control-label" for="sueldo">
                                                Sueldo
                                            </label>
                                            <input type="text" class="form-control" name="sueldo" id="sueldo"
                                                autocomplete="off" 
                                                value="<?php echo $sueldo ?>"
                                                placeholder="Sueldo *">
                                        </div>
                                    </div>
                                    
                                </div>
                                <div class="row">
                                    <div class='col-sm-6'>
                                            <div class='form-group'>
                                                <label for='id_activo' class='control-label'>
                                                <?php
                                                if($type == 1){
                                                    echo "Equipos disponibles en activos fijos";
                                                } elseif($type == 2){
                                                    echo "Equipos Asignados";
                                                }
                                                ?>
                                                </label>
                                                <select name='id_activo' id='id_activo' class='form-control'>
                                                    
                                                    <?php
                                                        $cAccion = new cBeneficiario();
                                                        
                                                        if($type == 1){
                                                            echo "<option value='0'>SIN OTORGAR</option>";
                                                            $rs_ben = $cAccion->getAF();
                                                        } elseif($type == 2){
                                                            $rs_ben = $cAccion->getAFbyid($_SESSION[_editar_]);
                                                        }
                                                    while($rw_ben = $rs_ben->fetch(PDO::FETCH_OBJ)){
                                                        $selected_ts = '';
                                                        if($type == 2){
                                                            //Aquí serán las condiciones para poner los que van seleccionados
                                                            if($id_activo == $rw_ben->id_activo){
                                                                $selected_ts = 'selected';
                                                            }
                                                        }
                                                        ?>
                                                        <option value='<?php echo $rw_ben->id_activo?>'
                                                            <?php echo $selected_ts?>>
                                                            <?php echo $rw_ben->matricula_activo?>
                                                        </option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                      <div class="col-sm-6 ">
                                            <div class="form-group ">
                                                <label for="activo" class="control-label">
                                                    Estado: 
                                                </label>
                                                <select name="activo" id="activo" class="form-control" >
                                                    <option value="">Estado del empleado *</option>
                                                    <option value="0" <?php if($activo == '0'){ echo "selected";}?>>Inactivo</option>
                                                    <option value="1" <?php if($activo == '1'){ echo "selected";}?>>Activo</option>
                                                    <option value="2" <?php if($activo == '2'){ echo "selected";}?>>Baja</option>
                                                </select>
                                            </div>
                                        </div>
                                </div>
                                
                            </fieldset>
                        </div>
                    </div>
                        <!-- Caja de respuetas a la accion del usuario -->
                        <div class="row">
                            <div class="col-md-12">
                                <div id="respuesta_ajax_modal">
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>

        <!-- Pie de la caja de captura con botones de acciones -->
        <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">
                <i class="icon-close2"></i> Cerrar
            </button>
            <?php
            if($type == 1){
                ?>
                <button type="submit" class="btn btn-primary" id="btn_guardar">
                    <i class="icon-plus3"></i> Agregar Empleado
                </button>
            <?php
            }
            ?>
            <?php
            if($type == 2){
                ?>
                <button type="submit" class="btn btn-primary" id="btn_guardar">
                    <i class="icon-floppy-disk"></i> Guardar Cambios
                </button>
            <?php
            }
            ?>
        </div>
    </div>
</form>
<script>
    $(function() {
        $('.child-menu').hide();
        $('.ocultar').hide();
        $('#fecha').datetimepicker({
            format: 'L',
            locale: 'es',
        });
        $('#f_nacimiento').datetimepicker({
            format: 'L',
            locale: 'es',
        });
        <?php
        if($type == 3){
            ?>
        $('#frm_guardar :input:not(:button)').attr({
            'disabled': 'disabled'
        });
            <?php
        }
        ?>
    });
    function mayus(e) {
    e.value = e.value.toUpperCase();
    }   
</script>
<?php
    }else{
        echo "<h1>No se puede acceder a la información solicitada</h1>";
    }
?>