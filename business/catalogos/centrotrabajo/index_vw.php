<?php
/*--------------------------------------------------------------------------------------------------------*/
$dir_fc = "";
include_once $dir_fc.'connections/trop.php';
include_once $dir_fc.'connections/php_config.php';

$sys_id_men   = 59;	//Corresponde al menú 2
$sys_tipo     = 0;	//Indicando que es una lista (0)
$real_sis     = "catalogos/centrotrabajo";
$title_act    = "Centros de trabajo";
$sub_t_act    = "Lista";
$fmc		  = "";

extract($_REQUEST);

$dir          = dirname($_SERVER['PHP_SELF']);
$current_file = basename($_SERVER["PHP_SELF"]);
$checkMenu    = $server_name.$dir."/".$current_file;   //con este se checa si index està activo
$param        = "?fmc=".$fmc."&td=";

/*------------------------  Incluyendo e Instanciando Clases Inicial  -------------------------------*/
include_once $dir_fc.'data/inicial.class.php';
include_once $dir_fc.'data/centrotrabajo_adm.class.php';
include_once $dir_fc.'common/function.class.php';
$cInicial = new cInicial(); //Operaciones generales (Menu de incio, etc)
$cLista   = new cCentrotrabajo_adm();
$cFn      = new cFunction();
/*---------------------------------------------------------------------------------------------------*/
include_once 'business/sys/check_session.php';
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<title>Catalogos | <?php echo c_page_title?></title>
	<meta content="Administrador de Conceptos para proyecto <?php echo c_page_title?> " name="description"/>
	<meta content="Subgerencia de Informática - By JF! ;)" name="author"/>

	<?php include($dir_fc."inc/headercommon.php"); ?>
	<script type="text/javascript" src="<?php echo $raiz?>js/plugins/tables/datatables/datatables.min.js"></script>
	<script type="text/javascript" src="<?php echo $raiz?>js/plugins/forms/selects/select2.min.js"></script>
	<script type="text/javascript" src="<?php echo $raiz?>js/plugins/tables/datatables/extensions/col_reorder.min.js"></script>
	<script type="text/javascript" src="<?php echo $raiz?>js/plugins/forms/inputs/typeahead/typeahead.bundle.min.js"></script>
	<script type="text/javascript" src="<?php echo $raiz?>js/plugins/forms/tags/tagsinput.min.js"></script>
	<script type="text/javascript" src="<?php echo $raiz?>js/plugins/forms/tags/tokenfield.min.js"></script>
	<script type="text/javascript" src="<?php echo $raiz?>js/plugins/tables/datatables/extensions/jszip/jszip.min.js"></script>
	<script type="text/javascript" src="<?php echo $raiz?>js/plugins/tables/datatables/extensions/pdfmake/pdfmake.min.js"></script>
	<script type="text/javascript" src="<?php echo $raiz?>js/plugins/tables/datatables/extensions/pdfmake/vfs_fonts.min.js"></script>
	<script type="text/javascript" src="<?php echo $raiz?>js/plugins/tables/datatables/extensions/buttons.min.js"></script>

	<script src="<?php echo $raiz?>js/app/status_plugins.js?ver=1.1"></script>
</head>
<body class="navbar-top">
	<?php include ($dir_fc."inc/header.php")?>
	<!-- Page container -->
	<div class="page-container">
		<!-- Page content -->
		<div class="page-content">
			<?php include ($dir_fc."inc/menucommon.php")?>
			<!-- Main content -->
			<div class="content-wrapper">
				<!-- Page header -->
				<div class="page-header page-header-default">
					<div class="page-header-content">

					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="<?php echo $raiz?>business"><i class="icon-drawer position-left"></i> Catalogo</a></li>
							<li class="active"><?php echo $title_act?></li>
						</ul>
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">
					<div id="respuesta_ajax">
					</div>
					<!-- Ajax sourced data -->
					<div class="panel panel-flat">
						<div class="panel-heading">
						<h5 class="">Administrar <?php echo $title_act?>
								<span class="badge bg-info-300"><?php echo $cLista->getCounter();?></span>
							</h5>
							<div class="heading-elements">
								<ul class="icons-list">
                                    <li>
                                        <button onclick="openFrm(1,0);" id="nuevo_opciones" class="btn bg-blue " data-popup='tooltip' data-placement="top" title="Agregar Nuevo">
                                            <i class="fab-icon-open icon-plus3"></i>
                                        </button>
                                    </li>
								</ul>
							</div>
						</div>

						<div class="panel-body">
							<table class="table table-hover" id="data">
								<thead>
								<tr>
									<th>ID</th>
									<th>Estado</th>
									<th>Clave</th>
									<th>Descripcion</th>
									<th>Tipo</th>
									<th class="text-center">Funciones</th>
								</tr>
								</thead>
							</table>
						</div>


					</div>
					<!-- /ajax sourced data -->
					<?php include ($dir_fc."inc/footer.php")?>
				</div>
				<!-- /content area -->
			</div>
			<!-- /main content -->
		</div>
		<!-- /page content -->
	</div>
	<!-- /page container -->
</body>
<!-- Remote source -->
<div id="modal_remote" class="modal fade in" data-backdrop="static" >
	<div class="modal-dialog modal-full">
        <div class="col-sm-3"><!--Solo puse este div para alinearlo-->  </div>
		<div id="modal_form-container" class="col-sm-6">
            <!--Aquí va el modal del NUEVO-->
        </div>
    </div>
</div>
<!-- /remote source -->
<?php include($dir_fc."common/modals.php"); ?>
<script type="text/javascript" src="<?php echo $raiz?>js/app/dataTableDefaults.js?ver=1.4"></script>
<script>
	//inicia funcion buscar
	$(document).on("ready", edoinicial);

	function edoinicial(){
        $('#data').dataTable({
			ajax: '<?php echo $raiz?>business/catalogos/centrotrabajo/ajax/trae_lista.php',
			columns: [
				{ "data": "id_centrotrabajo" },
				{ "data": "status" },
				{ "data": "cve_centro" },
				{ "data": "area" },
				{ "data": "tipo" },
				{ "data": "function" }
			],
			autoWidth: true,
			columnDefs: [{
				orderable: false,
				width: '100px',
				targets: [ 4 ]
			}],
			scrollY: 400,
			stateSave: true,
			buttons: {
				dom: {
					button: {
						className: 'btn btn-default'
					}
				},
				buttons: [
					{
						text: 'Copiar',
						extend: 'copyHtml5',
						className: 'btn btn-default',
						exportOptions: {
							columns: [ 0, ':visible' ]
						}
					},
					{
						extend: 'excelHtml5',
						className: 'btn btn-default',
						exportOptions: {
							columns: ':visible'
						}
					},
					{
						extend: 'pdfHtml5',
						className: 'btn btn-default',
						exportOptions: {
							columns: [0, 2, 3]
						}
					},
					{
						extend: 'colvis',
						text: '<i class="icon-three-bars"></i> <span class="caret"></span>',
						className: 'btn bg-blue btn-icon'
					}
				]
			}
		});

		// Add placeholder to the datatable filter option
		$('.dataTables_filter input[type=search]').attr('placeholder','Escribe un término...');

		// Enable Select2 select for the length option
		$('.dataTables_length select').select2({
			minimumResultsForSearch: Infinity,
			width: 'auto'
		});

		$('#modal_remote').on('shown.bs.modal', function (e) {
			//Cuando se abra el modal se pondrán los selects que correspondan y foco.
			$("#id_rol").select2({
				placeholder: "Selecciona Perfil"
			});
			$('#nombre').focus();
		})
	}

	function openFrm(type, id) {
		/**
		 * Abrir Formulario dependiendo del tipo que se requiera (editar, agregar nuevo o ver)
		 *
		 * @param {type}   int      Indica el tipo de formulario a abrir, 1 nuevo, 2 editar, 3 ver
		 * @param {id} 	   int      En caso de que sea editar id tendrá que valer el id correspondiente
		 * by Fhohs!
		 **/
		deshabilitarboton('nuevo_opciones', 1);
		$("#modal_form-container").load('<?php echo $raiz?>business/catalogos/centrotrabajo/frm/nuevo_vw.php?type='+type+'&id='+id,
			function( response, status, xhr ) {
			if ( status == "error" ) {
				var msg = "Ocurrió un inconveniente al cargar la información Solicitada";
				$( "#respuesta_ajax" ).html( msg + xhr.status + " " + xhr.statusText );
				habilitaboton('nuevo_opciones');
			}else{
				$('#modal_remote').modal('show');
				habilitaboton('nuevo_opciones');
			}
		});

	}

	function guardar(){
		var form = document.getElementById('frm_guardar');
		formData = new FormData(form);
		var ruta = "<?php echo $raiz?>business/catalogos/centrotrabajo/ajax/nuevo.php";
		deshabilitarboton('btn_guardar', 1);
		$.ajax({
			url: ruta,
			type: "POST",
			data: formData,  //Enviando todo el formulario
			contentType: false,
			processData: false,
			success: function (respuesta) {
				try {
					var json = JSON.parse(respuesta);
					done = json.done;
					if (done == 0) {
						$("#respuesta_ajax_modal").html(custom_alert('danger', '¡Ocurrió un inconveniente!', json.resp, 1, 1));
						habilitaboton('btn_guardar');
					} else {
						$("#respuesta_ajax").html(custom_alert('success', '',  json.resp, 1, 1));
						habilitaboton('btn_guardar');
						$('#modal_remote').modal('hide');
                        $('#data').DataTable().ajax.reload();
					}
				}catch(err) {
					$("#respuesta_ajax_modal").html(
						custom_alert('danger', '¡Ocurrió un inconveniente! ...- ', respuesta, 1, 1)
					);
					habilitaboton('btn_guardar');
				}
			},
			error: function (request, status, error) {
				$("#respuesta_ajax_modal").html(
					custom_alert('danger', '¡Ocurrió un inconveniente! ***- ', request.responseText, 1, 1)
				);
				habilitaboton('btn_guardar');
				//habilitaboton('consultar');
			}
		});
	}

	function bajaModal(id) {
		$("#idBaja").val(id);
		$('#idConfimaModalBA').modal('show');
	}

	function eliminaModal(id) {
		$("#idEliminar").val(id);
		$('#idConfimaModalElimina').modal('show');
	}

	function baja() {
		var tipo = 0;
		var id   = $("#idBaja").val();
		deshabilitarboton('aceptar_baja',1);
		bajaAlta(id, tipo);
	}

	function alta(id) {
		var tipo = 1;
		bajaAlta(id, tipo);
	}
	function eliminar() {
		var tipo = 3;
		var id = $("#idEliminar").val();
		deshabilitarboton('aceptar_eliminar',1);
		bajaAlta(id, tipo);
	}

	function bajaAlta(id, tipo){
		var c_f = $('#current_file').val();
		// Importante definir el archivo al que se redireccionará

		$.post("<?php echo $raiz?>business/<?php echo $real_sis ?>/ajax/baja_alta.php",{'id': id, 'tipo':tipo },
			function(respuesta){
				if (isNaN(respuesta)) {
					$("#respuesta_ajax").html(respuesta);
					habilitaboton('aceptar_eliminar');
					habilitaboton('aceptar_baja');
				} else {
					window.location = "<?php echo $param?>index&attempt=done_ba&respuesta="+respuesta+"";
				}
			});
	}

</script>
</html>
