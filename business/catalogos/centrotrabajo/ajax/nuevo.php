<?php
session_start();
$dir_fc = "../../../../";
/*-----------------------------------      Estableciendo la Clases  --------------------------------------*/
include_once $dir_fc . 'data/conceptos_adm.class.php';
include_once $dir_fc.'data/concepto.class.php';
/*--------------------------------------------------------------------------------------------------------*/
include_once $dir_fc.'connections/trop.php'; //Inclueye configuración de fecha y  hora de mexico
include_once $dir_fc.'connections/php_config.php'; //Inclueye configuración de fecha y  hora de mexico

$cAccion  = new cConceptos_adm();
$cDatos = new cConcepto();
$resultadoEnBd = 0;
$done = 0;
$resp = "";

if(!isset($_SESSION[_type_])){ $_SESSION[_type_] = 0; }

$clave = "";
$descripcion = "";
$tipo = "";

extract($_REQUEST);

if($clave == "" || $descripcion == "" || $tipo == ""){ //Verficando datos vacios
    $resp = "Debes ingresar los datos correctamente";
} else {
    $cDatos->setClave($clave);
    $cDatos->setDescripcion($descripcion);
    $cDatos->setTipo($tipo);
    $claveRepetida = 0;

    if($_SESSION[_type_] == 2){
        $cDatos->setIdConcepto($_SESSION[_editar_]);
        $claveRepetida = $cAccion->buscaCoincidencia($cDatos);
    }else{
        $claveRepetida = $cAccion->buscaClave($cDatos);
    }

    if (!is_numeric($claveRepetida)){
        $resp = $claveRepetida;
    } else {
        if ($claveRepetida>0) {
            $resp = "La clave capturada ya existe en la base de datos, intentar con otra";
        } else {
            if($_SESSION[_type_] == 2){
                $resultadoEnBd = $cAccion->actualizaRegistro($cDatos);
            }else{
                $resultadoEnBd = $cAccion->agregarRegistro($cDatos);
            }

            if(is_numeric($resultadoEnBd) AND $resultadoEnBd>0){
                $done = 1;
                $resp = "Concepto agregado/actualizado correctamente.";
            }else{
                $resp = "Ocurrió un incoveniente con la base de datos: -- ".$resultadoEnBd;
            }
        }
    }
}
echo json_encode(array("done" => $done, "resp" => $resp));
?>
