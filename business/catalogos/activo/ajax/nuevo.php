<?php
session_start();
$dir_fc = "../../../../";
/*-----------------------------------      Estableciendo la Clase  --------------------------------------*/
include_once $dir_fc . 'data/cat_activo.class.php';
include_once $dir_fc . 'data/movimientos.php';
include_once $dir_fc . 'common/function.class.php';
/*--------------------------------------------------------------------------------------------------------*/
include_once $dir_fc.'connections/trop.php'; //Inclueye configuración de fecha y  hora de mexico
include_once $dir_fc.'connections/php_config.php'; //Inclueye configuración de fecha y  hora de mexico
$cFuntion = new cFunction();
$cDatos = new cActivo();
$cMov = new cMovimientos();
$resultadoEnBd = 0;
$done = 0;
$resp = "";

if(!isset($_SESSION[_type_])){ $_SESSION[_type_] = 0; }

$id_activo        = "";
$marca            = "";
$tipo             = "";
$categoria        = "";
$proposito        = "";
$tipo_usuario     = "";
$num_serie        = "";
$matricula_activo = "";
$matricula_usuario= "";
$estado           = "";

extract($_REQUEST);
/* if ($fecha == "00/00/0000" || $fecha == NULL) {
    $fecha = "1901-01-01";
} else {
    $fecha = $cFuntion ->formatear_fecha_ins($fecha);
} */
if($marca == "" || $tipo == "" || $categoria == "" || $proposito == ""|| $num_serie == ""){ //Verficando datos vacios
    $resp = "Debes ingresar los datos correctamente";
} else {
    $matricula= $marca ."-". $num_serie;
    $today = date("Y-m-d");     
    $tabla = "Equipo de computo";  
    $cDatos->setmarca($marca);
    $cDatos->settipo($tipo);
    $cDatos->setcategoria($categoria);
    $cDatos->setproposito($proposito);
    $cDatos->settipo_usuario($tipo_usuario);
    $cDatos->setnum_serie($num_serie);
    $cDatos->setmatricula_activo($matricula);
    $cDatos->setmatricula_usuario($matricula_usuario);
    $cDatos->setestado($estado);
    $claveRepetida = 0;
    
    $cMov->setId_user($_SESSION[id_usr]);
    $cMov->setFecha($today);
    $cMov->setId_registro($_SESSION[_editar_]);
    $cMov->setNombre_tabla($tabla);

    if($_SESSION[_type_] == 1){
        $claveRepetida = $cDatos->buscaCoincidencia($cDatos);
    }elseif($_SESSION[_type_] == 2){
        $cDatos->setid_activo($_SESSION[_editar_]);
        $claveRepetida = $cDatos->buscaCoincidencia_Edit($cDatos);
    }else{
        $claveRepetida = $cDatos->buscaActivo($cDatos);
    }

    if (!is_numeric($claveRepetida)){
        $resp = $claveRepetida;
    } else {
        if ($claveRepetida>0) {
            $resp = "El activo fijo ".$num_serie." capturado esta relacionado con activo fijo ya existente en la base de datos.";
        } else {
            if($_SESSION[_type_] == 2){
                $resultadoEnBd = $cDatos->actualizaRegistro($cDatos);
                $cMov->setTipo_mov("Actualizo datos");
                $cMov->insertReg();
            }else{
                $resultadoEnBd = $cDatos->agregarRegistro($cDatos);
                $cMov->setTipo_mov("Agrego nuevo equipo de computo");
                $cMov->setId_registro($_SESSION[_editar_]+1);
                $cMov->insertReg();
            }

            if(is_numeric($resultadoEnBd) AND $resultadoEnBd>0){
                $done = 1;
                $resp = "Concepto agregado/actualizado correctamente.";
            }else{
                $resp = "Ocurrió un incoveniente con la base de datos: -- ".$resultadoEnBd;
            }
        }
    }
}
echo json_encode(array("done" => $done, "resp" => $resp));
?>
