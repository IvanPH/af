<?php
session_start();

$type = 0;
$id   = 0;

extract($_REQUEST);

/*--------------------------------------------------------------------------------------------------------*/
$dir_fc       = "../../../../";
include_once $dir_fc.'connections/trop.php';
include_once $dir_fc.'connections/php_config.php';
include_once $dir_fc . 'data/cat_activo.class.php';
include_once $dir_fc . 'common/function.class.php';
$cFuntion = new cFunction();

$id_activo        = "";
$marca            = "";
$tipo             = "";
$categoria        = "";
$proposito        = "";
$tipo_usuario     = "";
$num_serie        = "";
$matricula_activo = "";
$matricula_usuario= "";
$estado           = "";


if(($_SESSION[nuev] == 1 && $type == 1) || ($_SESSION[edit] == 1 && $type == 2 && $id > 0) || $type == 3){
    //Si tiene permisos de agregar un nuevo documento
    $_SESSION[_type_] = $type;
    if($type == 2 || $type == 3){
        $cAccion = new cActivo();
        $rsEditar = $cAccion->getRegbyid($id);
        $rsEditar->rowCount();

        if($rsEditar->rowCount() > 0){
            $_SESSION[_editar_] = $id; //Importante que si es ediciòn se asigne el ID que se va a editar
            $arrEdi             = $rsEditar->fetch(PDO::FETCH_OBJ);
            $marca              = $arrEdi->marca;
            $tipo               = $arrEdi->tipo;
            $categoria          = $arrEdi->categoria;
            $proposito          = $arrEdi->proposito;
            $tipo_usuario       = $arrEdi->tipo_usuario;
            $num_serie          = $arrEdi->num_serie;
            $matricula_activo   = $arrEdi->matricula_activo;
            $matricula_usuario  = $arrEdi->matricula_usuario;
            $estado             = $arrEdi->estado;
        }
        /* if ($fecha == "1901-01-01" || $fecha == NULL || $fecha == "0000-00-00") {
            $fecha = "00/00/0000";
        } else {
            $fecha = $cFuntion ->formatear_fecha_show($fecha);
        } */
    }
?>
<script type="text/javascript" src="<?php echo $raiz?>js/app/floatingLabels.js"></script>
<form id="frm_guardar" class="" method="post" action="javascript: guardar()" >
    <div class="modal-content">
        <!-- Encabezado de la caja de captura -->
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h5 class="modal-title"> <?php
                if($type == 1){
                    echo "Agregando";
                } elseif($type == 2){
                    echo "Editando";
                }elseif($type == 3){
                    echo "Visualizando";
                }
                ?> Activo Fijo</h5>
        </div>

        <!-- Caja de captura -->
        <div class="modal-body">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-xs-12 col-md-12 col-sm-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h6 class="panel-title">Datos del Activo</h6>
                        </div>


                        <div class="panel-body">
                            <fieldset>
                            <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group ">
                                            <label class="control-label">
                                                Marca(s)<span class="text-danger">*</span>
                                            </label>
                                            <input type="text" class="form-control" name="marca" id="marca" autocomplete="off"
                                                   required placeholder="Marca(s) *" value="<?php echo $marca ?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label for="tipo" class="control-label">
                                                Equipo de computo <span class="text-danger">*</span>
                                            </label>
                                            <select name="tipo" id="tipo" class="form-control" required>
                                             
                                            <option value=""> Equipo de computo *</option>
                                            <option value="Computadora de Escritorio." <?php if($tipo== "Computadora de Escritorio."){ echo "selected";}?>>Computadora de Escritorio.</option>
                                            <option value="Laptop." <?php if($tipo == "Laptop."){ echo "selected";}?>>Laptop.</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group ">
                                            <label class="control-label" for="categoria">Categoría
                                                <span class="text-danger">*</span>
                                            </label>
                                            <select name="categoria" id="categoria" class="form-control" required>
                                             
                                            <option value=""> Equipo de computo *</option>
                                            <option value="Desarrollo." <?php if($categoria== "Desarrollo."){ echo "selected";}?>>Desarrollo.</option>
                                            <option value="Estándar." <?php if($categoria == "Estándar."){ echo "selected";}?>>Estándar.</option>
                                            <option value="Básica." <?php if($categoria == "Básica."){ echo "selected";}?>>Básica.</option>
                                            </select>
                                        </div>
                                    </div>

                            </div>

                                <br>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group ">
                                            <label for="proposito" class="control-label">
                                                Proposito <span class="text-danger">*</span>
                                            </label>
                                            <select name="proposito" id="proposito" class="form-control" required>
                                             
                                            <option value=""> Proposito *</option>
                                            <option value="Desarrollo e implementación de Sistemas. Instalación y mantenimiento de software específico." <?php if($proposito== "Desarrollo e implementación de Sistemas. Instalación y mantenimiento de software específico."){ echo "selected";}?>>Desarrollo e implementación de Sistemas. Instalación y mantenimiento de software específico.</option>
                                            <option value="Instalación y mantenimiento de software específico." <?php if($proposito == "Instalación y mantenimiento de software específico."){ echo "selected";}?>>Instalación y mantenimiento de software específico.</option>
                                            <option value="Instalación y mantenimiento de software básico." <?php if($proposito == "Instalación y mantenimiento de software básico."){ echo "selected";}?>>	Instalación y mantenimiento de software básico.</option>
                                            </select>
                                                   
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="tipo_usuario" class="control-label">
                                                Tipo de usuario <span class="text-danger">*</span>
                                            </label>
                                            <select name="tipo_usuario" id="tipo_usuario" class="form-control" required>
                                             
                                            <option value="">Tipo de usuario *</option>
                                            <option value="Personal de enlace o comunicación, mercadotecnia, ventas, contabilidad y producción." <?php if($tipo_usuario== "Personal de enlace o comunicación, mercadotecnia, ventas, contabilidad y producción."){ echo "selected";}?>>Personal de enlace o comunicación, mercadotecnia, ventas, contabilidad y producción.</option>
                                            <option value="Gerentes, Supervisores, Técnicos." <?php if($tipo_usuario == "Gerentes, Supervisores, Técnicos."){ echo "selected";}?>>Gerentes, Supervisores, Técnicos.</option>
                                            <option value="Empleados no gerenciales." <?php if($tipo_usuario == "Empleados no gerenciales."){ echo "selected";}?>>Empleados no gerenciales.</option>
                                            </select>
                                        </div>
                                    </div>
                                    
                                </div>
                                <br/>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group ">
                                            <label for="num_serie" class="control-label">#Serie
                                               
                                            </label>
                                            <input onkeyup="mayus(this);" type="num_serie" class="form-control" name="num_serie" id="num_serie"
                                                    autocomplete="off" placeholder="#Serie *"
                                                   value="<?php echo $num_serie?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group ">
                                            <label for="matricula_activo" class="control-label">Numero de inventario
                                                <span class="text-danger">*</span>                                            </label>
                                            <input onkeyup="mayus(this);" type="matricula_activo" class="form-control" name="matricula_activo" id="matricula_activo"
                                                   disabled autocomplete="off" placeholder="Matricula (Se genera automaticamente) *"
                                                   value="<?php echo $matricula_activo?>">
                                        </div>
                                    </div>
                                    
                                </div>

                                <br>

                                <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="matricula_usuario" class="control-label">
                                        Empleado: 
                                        </label>
                                        <select name="matricula_usuario" id="matricula_usuario" class="form-control">
                                            <option value="0">SIN OTORGAR</option>
                                            <?php
                                                $cAccion = new cActivo();
                                                $rs_ben = $cAccion->getEmpleado();
                                        
                                            
                                            while($rw_ben = $rs_ben->fetch(PDO::FETCH_OBJ)){
                                                $selected_ts = "";
                                                if($type == 2 || $type == 3){
                                                    //Aquí serán las condiciones para poner los que van seleccionados
                                                    if($matricula_usuario == $rw_ben->id_beneficiario){
                                                        $selected_ts = "selected";
                                                    }
                                                }
                                                ?>
                                                <option value="<?php echo $rw_ben->id_beneficiario?>"
                                                    <?php echo $selected_ts?>>
                                                    <?php echo $rw_ben->nombre?>
                                                </option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="estado" class="control-label">
                                                Estado: <span class="text-danger">*</span>
                                            </label>
                                            <select name="estado" id="estado" class="form-control" >
                                                <?php
                                                    if ($type == 1) {
                                                ?>
                                            <option value="1" <?php if($estado== "1"){ echo "selected";}?>>Activo</option>
                                                <?php
                                                } else {
                                                ?>
                                            <option value="">Estado *</option>
                                            <option value="1" <?php if($estado== "1"){ echo "selected";}?>>Activo</option>
                                            <option value="0" <?php if($estado == "0"){ echo "selected";}?>>Inactivo</option>
                                                <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                            </fieldset>
                        </div>
                    </div>
                        <!-- Caja de respuetas a la accion del usuario -->
                        <div class="row">
                            <div class="col-md-12">
                                <div id="respuesta_ajax_modal">
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>

        <!-- Pie de la caja de captura con botones de acciones -->
        <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">
                <i class="icon-close2"></i> Cerrar
            </button>
            <?php
            if($type == 1){
                ?>
                <button type="submit" class="btn btn-primary" id="btn_guardar">
                    <i class="icon-plus3"></i> Agregar Beneficiario
                </button>
            <?php
            }
            ?>
            <?php
            if($type == 2){
                ?>
                <button type="submit" class="btn btn-primary" id="btn_guardar">
                    <i class="icon-floppy-disk"></i> Guardar Cambios
                </button>
            <?php
            }
            ?>
        </div>
    </div>
</form>
<script>
    $(function() {
        $('.child-menu').hide();
        $('.ocultar').hide();
        $('#fecha').datetimepicker({
            format: 'L',
            locale: 'es',
        });
        <?php
        if($type == 3){
            ?>
        $('#frm_guardar :input:not(:button)').attr({
            'disabled': 'disabled'
        });
            <?php
        }
        ?>
    });
    function mayus(e) {
    e.value = e.value.toUpperCase();
    }   
</script>
<?php
    }else{
        echo "<h1>No se puede acceder a la información solicitada</h1>";
    }
?>