<?php
session_start();

$type = 0;
$id   = 0;

extract($_REQUEST);

/*--------------------------------------------------------------------------------------------------------*/
$dir_fc       = "../../../../";
include_once $dir_fc.'connections/trop.php';
include_once $dir_fc.'connections/php_config.php';
include_once $dir_fc.'data/rol.class.php';

//Comentario de prueba
//Segundo comentario

$cAccion       = new Rol();

$id_rol      = "";
$rol         = "";
$descripcion = "";
$activo      = "";

if(($_SESSION[nuev] == 1 && $type == 1) || ($_SESSION[edit] == 1 && $type == 2 && $id > 0) || $type == 3){//Si tiene permisos de agregar un nuevo documento
    $_SESSION[_type_] = $type;
    if($type == 2 || $type == 3){
        $cAccion->set_id($id);
        $rsEditar    = $cAccion->getRolID();
        $rsEditar->rowCount();

        if($rsEditar->rowCount() > 0){
            $arrEdi      = $rsEditar->fetch(PDO::FETCH_OBJ);
            $id_rol         = $arrEdi->id;
            $_SESSION[_editar_] = $id_rol;
            $rol            = $arrEdi->rol;
            $descripcion    = $arrEdi->descripcion;
            $activo         = $arrEdi->activo;
        }
    }
    ?>
<script type="text/javascript" src="<?php echo $raiz?>js/app/floatingLabels.js"></script>
<form id="frm_guardar" class="" method="post" action="javascript: guardar()" >
    <div class="modal-content">
        <div class="row">
            <div class="col-md-12">
                <div id="respuesta_ajax_modal">
                </div>
            </div>
        </div>
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h5 class="modal-title">Agregando Perfil
        </div>
        <div class="modal-body">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-xs-12 col-md-12 col-sm-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h6 class="panel-title">Datos del Rol</h6>
                            <!--<div class="heading-elements">
                                <ul class="icons-list">
                                    <li><a data-action="collapse"></a></li>
                                    <li><a data-action="reload"></a></li>
                                    <li><a data-action="close"></a></li>
                                </ul>
                            </div> -->
                        </div>

                        <div class="panel-body">
                            <fieldset>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group form-group-material">
                                            <label for="rol">
                                                Nombre del Perfil <span class="text-danger">*</span>
                                            </label>
                                            <input type="text" 
                                                    class="form-control"  
                                                    name="rol" 
                                                    id="rol" 
                                                    required
                                                    placeholder="Nombre del Perfil *" 
                                                    value="<?php echo $rol ?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group form-group-material">
                                            <label  class="control-label" for="descripcion">
                                                Descripción <span class="text-danger">*</span>
                                            </label>
                                            <input type="text" class="form-control" 
                                                    name="descripcion" 
                                                    id="descripcion" 
                                                    autocomplete="off"
                                                    value="<?php echo $descripcion ?>"
                                                    placeholder="Descripción *">
                                        </div>
                                    </div>
                                    <?php 
                                    if($type == 2){
                                    ?>
                                    <div class="col-sm-2">
                                        <div class="form-group form-group-material">
                                            <label for="checkbox">Aplicar cambios a usuarios 
                                                <span class="text-danger">*</span>
                                            </label>    
                                            <input type="checkbox" 
                                                    class="form-control" 
                                                    name="aplicar" 
                                                    id="aplicar"
                                                    value="1" checked>
                                            
                                        </div>
                                    </div>
                                    <?php
                                    }?>
                                </div>
                            </fieldset>
                        </div>
                    </div>

                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h6 class="panel-title">Permisos por default</h6>
                        </div>

                        <div class="panel-body">
                            <fieldset>
                                <article id="permisos">
                                    <div class="row">
                                        <div class="permisos-field">
                                            <?php
                                            if($type == 2){
                                                ?>
                                                <div class="checkbox checkbox-styled">
                                                    <label>
                                                        <input name="sall" id="sall" type="checkbox" value="1" title="Seleccionar Todos">
                                                        <strong>Seleccionar todos</strong>
                                                    </label>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                            <div id="permisos_ajax">
                                                <?php
                                                $arrCk = array();
                                                $rsRol           = $cAccion->parentsMenu();
                                                $totalRows_rsRol = $rsRol->rowCount();
                                                while ($rowReg   = $rsRol->fetch(PDO::FETCH_OBJ)) {
                                                    $chk = "";
                                                    $cAccion->set_id_menu($rowReg->id);
                                                    $checked_r  = $cAccion->checarRol_menu();
                                                    $rowsc1  = $checked_r->rowCount();
                                                    if ($rowsc1>0) {
                                                        $scriptf = "checarInput('menu_',".$rowReg->id.")";
                                                        array_push($arrCk, $scriptf);
                                                        $chk = "checked";
                                                    }
                                                    ?>
                                                    <div id="parents-menu_<?php echo $rowReg->id?>">
                                                        <div class="form-group">
                                                            <span>
                                                                <span id="mostrar_<?php echo $rowReg->id?>" class="btn-plus-ne mostrar"
                                                                      onclick="mostrar(<?php echo $rowReg->id?>)">
                                                                <i class="fa fa-plus-square-o"></i>
                                                                </span>
                                                                <span id="ocultar_<?php echo $rowReg->id?>" class="btn-plus-ne ocultar"
                                                                  onclick="ocultar(<?php echo $rowReg->id?>)">
                                                                            <i class="fa fa-minus-square-o"></i>
                                                                </span>
                                                            </span>
                                                            <label class="">
                                                                <input name="menus[]" id="menu_<?php echo $rowReg->id?>" type="checkbox" value="<?php echo $rowReg->id?>"
                                                                       title="<?php echo $rowReg->texto?>" onchange="sel_des_child(<?php echo $rowReg->id?>)">
                                                                <?php echo $rowReg->texto ?>
                                                            </label>
                                                        <input type="hidden" id="grupo_m_<?php echo $rowReg->id?>" name="grupo[<?php echo $rowReg->id?>]" value="<?php echo $rowReg->id_grupo?>">
                                                    </div>
                                                    <?php
                                                    $rsRol_c  = $cAccion->childsMenu($rowReg->id);
                                                    ?>
                                                    <div id="child-menu_<?php echo $rowReg->id?>" class="child-menu">
                                                        <?php

                                                        while ($rowReg_c = $rsRol_c->fetch(PDO::FETCH_OBJ)) {
                                                            $chk_imp     = "";
                                                            $chk_edit    = "";
                                                            $chk_nuevo   = "";
                                                            $chk_elim    = "";
                                                            $chk_exportar= "";
                                                            $chk_2 = "";
                                                            $cAccion->set_id_menu($rowReg_c->id);
                                                            if($type > 1){
                                                                $checked_r_2 = $cAccion->checarRol_menu();
                                                                $rowsc = $checked_r_2->rowCount();
                                                                if ($rowsc > 0) {
                                                                   // echo "Si entra aqui";
                                                                    $script0 = "checarInput('child_".$rowReg->id."_',".$rowReg_c->id.")";
                                                                    array_push($arrCk, $script0);
                                                                    $chk_2 = "checked";
                                                                    $rw_check = $checked_r_2->fetch(PDO::FETCH_OBJ);
                                                                    $chk_imp = $rw_check->imp;
                                                                    $chk_edit = $rw_check->edit;
                                                                    $chk_nuevo = $rw_check->nuevo;
                                                                    $chk_elim = $rw_check->elim;
                                                                    $chk_exportar = $rw_check->exportar;
                                                                }
                                                            }

                                                            ?>
                                                            <input type="hidden" id="grupo_m_<?php echo $rowReg_c->id?>" name="grupo[<?php echo $rowReg_c->id?>]"
                                                                   value="<?php echo $rowReg_c->id_grupo?>">
                                                            <div class="checkbox">
                                                                <label class="checkbox-inline separador-desc">
                                                                    <input name="menus[]" 
                                                                            id="child_<?php echo $rowReg->id?>_<?php echo $rowReg_c->id?>" 
                                                                            type="checkbox"
                                                                            value="<?php echo $rowReg_c->id?>" title="<?php echo $rowReg_c->texto?>"
                                                                            onchange="checamain(<?php echo $rowReg->id?>, <?php echo $rowReg_c->id?>)" >
                                                                    <?php echo $rowReg_c->texto ?>
                                                                </label>
                                                                <label class="checkbox-inline separador">
                                                                    <input type="checkbox" 
                                                                           name="permiso_imp[<?php echo $rowReg_c->id?>]" value="1"
                                                                           title="Editar" 
                                                                           id="permiso_imp<?php echo $rowReg_c->id?>"
                                                                        <?php if($chk_imp == 1){
                                                                            $script1 = "checarInput('permiso_imp',".$rowReg_c->id.")";
                                                                            array_push($arrCk, $script1);
                                                                        }
                                                                            ?>
                                                                    >
                                                                    Imprimir
                                                                </label>
                                                                <label class="checkbox-inline separador">
                                                                    <input type="checkbox" name="permiso_nuevo[<?php echo $rowReg_c->id?>]" value="1"
                                                                           title="Editar" id="permiso_nuevo<?php echo $rowReg_c->id?>"
                                                                        <?php if($chk_nuevo == 1){
                                                                            $script4 = "checarInput('permiso_nuevo',".$rowReg_c->id.")";
                                                                            array_push($arrCk, $script4);
                                                                        }?>>
                                                                    Nuevo
                                                                </label>
                                                                <label class="checkbox-inline separador">
                                                                    <input type="checkbox" name="permiso_edit[<?php echo $rowReg_c->id?>]" value="1"
                                                                           title="Editar" id="permiso_edit<?php echo $rowReg_c->id?>" <?php if($chk_edit == 1){
                                                                        $script5 = "checarInput('permiso_edit',".$rowReg_c->id.")";
                                                                        array_push($arrCk, $script5);
                                                                    }?>>
                                                                    Editar
                                                                </label>
                                                                <label class="checkbox-inline separador">
                                                                    <input type="checkbox" name="permiso_elim[<?php echo $rowReg_c->id?>]" value="1"
                                                                           title="Editar" id="permiso_elim<?php echo $rowReg_c->id?>" <?php if($chk_elim == 1){
                                                                        $script6 = "checarInput('permiso_elim',".$rowReg_c->id.")";
                                                                        array_push($arrCk, $script6);
                                                                    }?> >
                                                                    Eliminar
                                                                </label>
                                                                <label class="checkbox-inline separador">
                                                                    <input type="checkbox" name="permiso_exportar[<?php echo $rowReg_c->id?>]" value="1"
                                                                           title="Editar" id="permiso_exportar<?php echo $rowReg_c->id?>"  <?php if($chk_exportar == 1){
                                                                        $script7 = "checarInput('permiso_exportar',".$rowReg_c->id.")";
                                                                        array_push($arrCk, $script7);
                                                                    }?>>
                                                                    Exportar
                                                                </label>
                                                            </div>
                                                            <?php
                                                        }
                                                        ?>
                                                    </div>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">
                <i class="icon-close2"></i> Cerrar
            </button>
            <?php
            if($type <> 3){
                ?>
                <button type="submit" class="btn btn-primary" id="btn_guardar">
                    <i class="icon-floppy-disk"></i> Guardar Cambios
                </button>
            <?php
            }
            ?>
        </div>
    </div>
</form>
<script>
    $(function() {
        $("#sall").change(function () {
            if ($(this).is(':checked')) {
                $("#permisos input[type=checkbox]").prop('checked', true);
            } else {
                $("#permisos input[type=checkbox]").prop('checked', false);
            }
        });

        $('.child-menu').hide();
        $('.ocultar').hide();

        <?php
        if($type == 3){
            ?>
        $('#frm_guardar :input:not(:button)').attr({
            'disabled': 'disabled'
        });
            <?php
        }

        if(is_array($arrCk)){
            foreach ($arrCk as $idar){
                echo $idar.";";
            }
        }

        ?>

    });

    function checarInput(label, input){
        /*
        * Funcion para checar los inputs correspondientes.
        * */
        $("#"+label+input).prop('checked', true);
    }
</script>
<?php
}else{
    echo "<h1>No se puede acceder a la información solicitada</h1>";
}
?>