<?php
/*--------------------------------------------------------------------------------------------------------*/
$dir_fc       = "";
include_once $dir_fc.'connections/trop.php';
include_once $dir_fc.'connections/php_config.php';
//Comentario de prueba

extract($_REQUEST);

$sys_id_men   = 3;
$sys_tipo     = 2;//Editar

$current_file = basename($_SERVER["PHP_SELF"]);
$dir          = dirname($_SERVER['PHP_SELF'])."/".$controller;
$checkMenu    = $server_name.$dir."/";   //con este se checa si es archivo activo o no
$param        = "?controller=".$controller."&action=";

$ruta_app     = "";
$titulo_curr  = "Roles";
/*-----------------------------      LLamando a las clases   ---------------------------------------------*/
include_once $dir_fc.'data/inicial.class.php';
include_once $dir_fc.'data/rol.class.php';

$cInicial    = new cInicial();       //Operaciones generales
$cEditar     = new Rol();       //Operaciones con los roles
/*-----------------------------------   Verificando la sesión   ------------------------------------------*/
include_once 'business/sys/check_session.php'; //En este archivo se incia la sesión, por lo tanto no es necesario declarar el session_start()

$showinfo    = true;
$id_rol      = "";
$rol         = "";
$descripcion = "";
$activo      = "";
$titulo_edi  = "Visualizando";



if(!isset($pag)){ $pag=1;}
if(!isset($busqueda) || $busqueda == ""){$busqueda = "";}
if(!isset($columna) || $columna == ""){$columna = "";}

$return_paginacion = "&pag=".$pag."&busqueda=".$busqueda."&columna=".$columna;

/*---------------    Formateando informacion de mensajes a mostrar al usuario  ---------------------------*/
if (isset($attempt)) {
    if ($attempt == "done" ){
        $msgShow = "<div id=\"resp\" class=\"alert alert-success alert-dismissable\">
                        <i class=\"fa fa-check\"></i>
                        <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
                        <strong>¡Correcto! </strong> Registro insertado satisfactoriamente <i>Reg. No. (".$inserted.")</i>
                    </div>";
    }
    else {
        $msgShow = "";
    }
}
else{
    $msgShow = "";
}

if(!isset($_SESSION[_editar_]) || !is_numeric($_SESSION[_editar_]) || $_SESSION[_editar_]<= 0){
    $showinfo = false;
}else {
    $id = $_SESSION[_editar_];
    $cEditar->set_id($id);
    $rsEditar = $cEditar->getRolID();
    if ($rsEditar->rowCount() > 0) {
        $arrEdi         = $rsEditar->fetch(PDO::FETCH_OBJ);
        $id_rol         = $arrEdi->id;
        $rol            = $arrEdi->rol;
        $descripcion    = $arrEdi->descripcion;
        $activo         = $arrEdi->activo;
    } else {
        $showinfo = false;
    }

}
if($_SESSION[_is_view_] == 1){
    $titulo_edi = "Editando";
}
?>
<!DOCTYPE html>
<html>
<head>
    <title><?php echo $titulo_edi." ".$titulo_curr?> | <?php echo $titulo_paginas?></title>
    <meta content="" name="description"/>
    <meta content="" name="author"/>
    <?php include($dir_fc."inc/headercommon.php"); ?>
    <script src="<?php echo $dir_fc?>js/app/status_plugins.js"></script>
    <?php include($dir_fc."js/app/guardar.php"); ?>
    <script type="text/javascript">
        function borrarespuesta(){
            $("#respuesta_ajax").html("");
        }
    </script>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="menubar-hoverable header-fixed ">
<?php include ($dir_fc."inc/header.php")?>
<!-- BEGIN BASE-->
<div id="base">
    <!-- BEGIN OFFCANVAS LEFT -->
    <div class="offcanvas"></div><!--end .offcanvas-->
    <!-- END OFFCANVAS LEFT -->
    <!-- BEGIN CONTENT-->
    <div id="content">
        <section>
            <div class="section-body contain-lg">
                <!-- BEGIN INTRO -->
                <div class="row">
                    <div class="col-lg-8">

                    </div><!--end .col -->
                    <div class="col-lg-4">
                        <ol class="breadcrumb pull-right">
                            <li><a href="<?php echo $raiz?>business/">Inicio</a></li>
                            <li><a href="<?php echo $param."index".$return_paginacion?>">Lista de <?php echo $titulo_curr?></a></li>
                            <li class="active"><?php echo $titulo_edi." ".$titulo_curr?></li>
                        </ol>
                    </div>
                </div><!--end .row -->
                <!-- END INTRO -->
                <?php
                //Verifica si el usuario puede insertar registros
                if($_SESSION[edit] == "1" && $showinfo == true) {
                    ?>
                    <div class="card">
                        <?php
                        if($activo == 1){
                            $class_activo = "fa fa-2x fa-check-circle text-default";
                            $title_activo = "Usuario Activo";
                            $card_style   = "style-success";
                        }else{
                            $class_activo = "fa fa-2x fa-times-circle text-default";
                            $title_activo = "Usuario Inactivo";
                            $card_style   = "style-gray-light";
                        }
                        ?>
                        <!-- BEGIN SEARCH HEADER -->
                        <div class="card-head <?php echo $card_style?>">
                            <div class="tools pull-left">
                                <a class="btn ink-reaction btn-floating-action btn-accent-bright"
                                   href='<?php echo $param."index".$return_paginacion?>'  title="Regresar a la lista">
                                    <i class="md md-arrow-back"></i>
                                </a>
                                <!-- Agregar u opciones para regresar o algun movimiento extra  -->
                            </div>
                            <strong class="text-uppercase"><?php echo $titulo_edi?> INFORMACION DE <?php echo $titulo_curr?></strong>
                            <div class="tools">

                                <span class="<?php echo $class_activo?>" title="<?php echo $title_activo?>"></span>
                            </div>
                        </div><!--end .card-head -->
                        <div class="card-body">
                            <form id="frm_guardar" class="form" role="form" method="post" action="javascript: guardar('guardar','editar', '','respuesta_ajax','<?php echo $param?>', 'admin/sis_roles', 1)" >
                                <!-- Small boxes (Stat box) -->
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div id="respuesta_ajax"><?php echo $msgShow?></div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                        <input type="hidden" name="id_rol" id="id_rol" value="<?php echo $id?>"/>
                                        <fieldset><legend>Datos de <?php echo $titulo_curr?></legend>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="form-group floating-label">
                                                        <input type="text" class="form-control" name="rol" id="rol" required
                                                               value="<?php echo $rol?>">
                                                        <label for="rol">Nombre del Rol <span class="text-danger">*</span></label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group floating-label">
                                                        <input type="text" class="form-control" name="descripcion" id="descripcion"
                                                               value="<?php echo $descripcion?>" required>
                                                        <label for="descripcion">Descripción <span class="text-danger">*</span></label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-2">
                                                    <div class="form-group floating-label">
                                                        <input type="checkbox" class="form-control" name="aplicar" id="aplicar"
                                                               value="1" checked>
                                                        <label for="checkbox">Aplicar cambios a todos <span class="text-danger">*</span></label>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <fieldset>
                                            <legend>
                                                Permisos por default
                                            </legend>
                                            <div class="row">
                                                <div class="col-sm-9">
                                                    <article id="permisos">
                                                        <div class="permisos-field">
                                                            <div class="checkbox checkbox-styled">
                                                                <label>
                                                                    <input name="sall" id="sall" type="checkbox" value="1" title="Imprimir Registros">
                                                                    <strong>Seleccionar todos</strong>
                                                                </label>
                                                            </div>
                                                            <?php
                                                            $rsRol           = $cEditar->parentsMenu();
                                                            $totalRows_rsRol = $rsRol->rowCount();
                                                            while ($rowReg   = $rsRol->fetch(PDO::FETCH_OBJ)) {
                                                                $chk = "";
                                                                $cEditar->set_id_menu($rowReg->id);
                                                                $checked_r  = $cEditar->checarRol_menu();
                                                                $rowsc1  = $checked_r->rowCount();
                                                                if ($rowsc1>0) {
                                                                    $chk = "checked";
                                                                }
                                                                ?>
                                                                <div id="parents-menu_<?php echo $rowReg->id?>">

                                                                    <div class="checkbox checkbox-styled">
                                                                        <span id="mostrar_<?php echo $rowReg->id?>" class="btn-plus-ne mostrar"
                                                                              onclick="mostrar(<?php echo $rowReg->id?>)">
                                                                        <i class="fa fa-plus-square-o"></i>
                                                                        </span>
                                                                        <span id="ocultar_<?php echo $rowReg->id?>" class="btn-plus-ne ocultar"
                                                                              onclick="ocultar(<?php echo $rowReg->id?>)">
                                                                            <i class="fa fa-minus-square-o"></i>
                                                                        </span>
                                                                        <label>
                                                                            <input name="menus[]" id="menu_<?php echo $rowReg->id?>" type="checkbox"
                                                                                   value="<?php echo $rowReg->id?>" <?php echo $chk ?>
                                                                                   title="<?php echo $rowReg->texto?>" onchange="sel_des_child(<?php echo $rowReg->id?>)">
                                                                            <?php echo $rowReg->texto ?>
                                                                        </label>
                                                                    </div>
                                                                    <input type="hidden" id="grupo_m_<?php echo $rowReg->id?>" name="grupo[<?php echo $rowReg->id?>]" value="<?php echo $rowReg->id_grupo?>">
                                                                </div>
                                                                <?php
                                                                $rsRol_c  = $cEditar->childsMenu($rowReg->id);
                                                                ?>
                                                                <div id="child-menu_<?php echo $rowReg->id?>" class="child-menu">
                                                                    <?php
                                                                    while ($rowReg_c = $rsRol_c->fetch(PDO::FETCH_OBJ)) {
                                                                        $chk_imp     = "";
                                                                        $chk_edit    = "";
                                                                        $chk_nuevo   = "";
                                                                        $chk_elim    = "";
                                                                        $chk_exportar= "";
                                                                        $chk_2 = "";
                                                                        $cEditar->set_id_menu($rowReg_c->id);
                                                                        $checked_r_2 = $cEditar->checarRol_menu();
                                                                        $rowsc = $checked_r_2->rowCount();
                                                                        if ($rowsc > 0) {
                                                                            $chk_2 = "checked";
                                                                            $rw_check = $checked_r_2->fetch(PDO::FETCH_OBJ);
                                                                            $chk_imp = $rw_check->imp;
                                                                            $chk_edit = $rw_check->edit;
                                                                            $chk_nuevo = $rw_check->nuevo;
                                                                            $chk_elim = $rw_check->elim;
                                                                            $chk_exportar = $rw_check->exportar;
                                                                        }
                                                                        ?>
                                                                        <input type="hidden" id="grupo_m_<?php echo $rowReg_c->id?>" name="grupo[<?php echo $rowReg_c->id?>]"
                                                                               value="<?php echo $rowReg_c->id_grupo?>">
                                                                        <div class="checkbox checkbox-styled">
                                                                            <label class="separador-desc">
                                                                                <input name="menus[]" id="child_<?php echo $rowReg->id?> _<?php echo $rowReg_c->id?>" type="checkbox"
                                                                                       value="<?php echo $rowReg_c->id?>" title="<?php echo $rowReg_c->texto?>"
                                                                                       onchange="checamain(<?php echo $rowReg->id?>)" <?php echo $chk_2 ?>>
                                                                                <?php echo $rowReg_c->texto ?>
                                                                            </label>
                                                                            <label class="separador">
                                                                                <input type="checkbox" name="permiso_imp[<?php echo $rowReg_c->id?>]" value="1"
                                                                                       title="Editar" id="permiso_imp<?php echo $rowReg_c->id?>" <?php if($chk_imp == 1){ echo "checked";}?>>
                                                                                Imprimir

                                                                            </label>
                                                                            <label class="separador">
                                                                                <input type="checkbox" name="permiso_nuevo[<?php echo $rowReg_c->id?>]" value="1"
                                                                                       title="Editar" id="permiso_nuevo<?php echo $rowReg_c->id?>" <?php if($chk_nuevo == 1){ echo "checked";}?>>
                                                                                Nuevo
                                                                            </label>
                                                                            <label class="separador">
                                                                                <input type="checkbox" name="permiso_edit[<?php echo $rowReg_c->id?>]" value="1"
                                                                                       title="Editar" id="permiso_edit<?php echo $rowReg_c->id?>" <?php if($chk_edit == 1){ echo "checked";}?>>
                                                                                Editar
                                                                            </label>
                                                                            <label class="separador">
                                                                                <input type="checkbox" name="permiso_elim[<?php echo $rowReg_c->id?>]" value="1"
                                                                                       title="Editar" id="permiso_elim<?php echo $rowReg_c->id?>" <?php if($chk_elim == 1){ echo "checked";}?>>
                                                                                Eliminar
                                                                            </label>
                                                                            <label class="separador">
                                                                                <input type="checkbox" name="permiso_exportar[<?php echo $rowReg_c->id?>]" value="1"
                                                                                       title="Editar" id="permiso_exportar<?php echo $rowReg_c->id?>" <?php if($chk_exportar == 1){ echo "checked";}?>>
                                                                                Exportar
                                                                            </label>
                                                                        </div>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </div>
                                                                <?php
                                                            }
                                                            ?>
                                                        </div>
                                                    </article>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <fieldset>
                                            <?php
                                            if($_SESSION[_is_view_] == 1){ //Si es editar
                                            ?>
                                            <div class="row">
                                                <div class="col-sm-12 col-md-4 col-xs-12 col-lg-4">
                                                    <button type="submit" class="btn ink-reaction btn-block btn-primary" id="btn_guardar">
                                                        <span class="glyphicon glyphicon-floppy-disk"></span> Guardar Cambios
                                                    </button>
                                                </div>
                                            </div>
                                            <?php
                                            }
                                            ?>
                                        </fieldset>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                <?php
                }else{
                    //Incliur el código de permisos denegados
                    include("../../sys/permissions_d.php");
                }?>
            </div>
        </section>
    </div><!--end #content-->
    <!-- END CONTENT -->
    <?php include($dir_fc."inc/menucommon.php") ?>
</div><!--end #base-->
<!-- END BASE -->
<?php include($dir_fc."inc/footercommon.php"); ?>
<script>
    $(document).on("ready", edoinicial);
    //Funcion que se ejecutará al cargar el documento
    function edoinicial() {
        $("#sall").change(function () {
            if ($(this).is(':checked')) {
                $("#permisos input[type=checkbox]").prop('checked', true);
            } else {
                $("#permisos input[type=checkbox]").prop('checked', false);
            }
        });

        $('.child-menu').hide();
        $('.ocultar').hide();

        $('#rol').focus();
        $(":input").keydown(function () {
            borrarespuesta();
        });

        <?php
        if($_SESSION[_is_view_] <> 1){
            ?>
        $('input').attr({
            'disabled': 'disabled'
        });
        <?php
        }
        ?>
    }

    function mostrar(id){
        $("#child-menu_"+id).show(150,function() {
            $("#mostrar_"+id).hide();
            $("#ocultar_"+id).show();
        });
    }

    function ocultar(id){
        $("#child-menu_"+id).hide(150,function() {
            $("#mostrar_"+id).show();
            $("#ocultar_"+id).hide();
        });
    }

    function sel_des_child(id){
        if ($('#menu_'+id).is(':checked')) {
            $("#child-menu_"+id+" input[type=checkbox]").prop('checked', true);
        } else {
            $("#child-menu_"+id+" input[type=checkbox]").prop('checked', false);
        }
    }

    function checamain(id){
        if ($('#menu_'+id).is(':checked')) {

        }else{
            $('#menu_'+id).prop('checked', true);
        }
    }
</script>
</body>
</html>
