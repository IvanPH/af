<?php
$dir_fc = "../../../../";
/*-----------------------------------      Estableciendo la Clases  --------------------------------------*/
include_once $dir_fc.'data/rol.class.php';
/*--------------------------------------------------------------------------------------------------------*/
include_once $dir_fc.'connections/trop.php'; //Inclueye configuración de fecha y  hora de mexico
include_once $dir_fc.'connections/php_config.php'; //Inclueye configuración de fecha y  hora de mexico

$fig = 0;

$cAccion  = new Rol();

$id_rol             = "";
$rol                = "";
$descripcion        = "";
$permiso_imp[]      = "";
$permiso_nuevo[]    = "";
$permiso_edit[]     = "";
$permiso_elim[]     = "";
$permiso_exportar[] = "";
$imp                = 0;
$nuevo              = 0;
$edi                = 0;
$elim               = 0;
$exportar           = 0;
$done               = 0;
$aplicar            = 0;
extract($_REQUEST);

$cAccion->set_id($id_rol);
$cAccion->set_rol($rol);
$cAccion->set_descripcion($descripcion);
$actualiza = $cAccion->updateReg();

if ($actualiza>0) {
    $done = 1;
    $resp = "Registro actualizado Correctamten.";
    $cAccion->set_imp(0);
    $cAccion->set_nuevo(0);
    $cAccion->set_edit(0);
    $cAccion->set_elim(0);
    $cAccion->set_exportar(0);

    $cAccion->deleteRegRM();
    if(isset($menus)){
        foreach ($menus as $id_arr => $valor_arr) {
            $cAccion->set_id_menu($valor_arr);
            if(isset($grupo)){
                $grupo_rec = $grupo[$valor_arr];
                if($grupo_rec <> 0){
                    if(isset($permiso_imp[$valor_arr])){
                        $imp = $permiso_imp[$valor_arr];
                    }
                    if(isset($permiso_nuevo[$valor_arr])){
                        $nuevo = $permiso_nuevo[$valor_arr];
                    }
                    if(isset($permiso_edit[$valor_arr])){
                        $edi = $permiso_edit[$valor_arr];
                    }
                    if(isset($permiso_elim[$valor_arr])){
                        $elim = $permiso_elim[$valor_arr];
                    }
                    if(isset($permiso_exportar[$valor_arr])){
                        $exportar = $permiso_exportar[$valor_arr];
                    }

                    $cAccion->set_imp($imp);
                    $cAccion->set_nuevo($nuevo);
                    $cAccion->set_edit($edi);
                    $cAccion->set_elim($elim);
                    $cAccion->set_exportar($exportar);
                }
            }
            $cAccion->insertRegdtl();

            
        }
        
        if($aplicar == 1){
            //Si aplican para todos los usuarios estos perfiles entonces 

            // 1- Eliminar todos los permisos de los usuarios
            $del = $cAccion->deleteRegByRolUser();
            // 2. Consulta usuarios a afectar

            if(is_numeric($del)){
                $rsU = $cAccion->getUserByRol();

                // 3. Inserta los permisos
                while($rwR = $rsU->fetch(PDO::FETCH_OBJ)){
                    $cAccion->insertRegdtlByRol($rwR->id_usuario);
                }
            }else{
                echo $del;
            }
            
            
        }

        $done = 1;
    }

}else{
    $resp = "Ocurrió un inconveniente .. ".$actualiza;
}
echo json_encode(array("done" => $done, "resp" => $resp));
?>
