<?php
/*--------------------------------------------------------------------------------------------------------*/
$dir_fc       = "";
include_once $dir_fc.'connections/trop.php';
include_once $dir_fc.'connections/php_config.php';

extract($_REQUEST);

$current_file = basename($_SERVER["PHP_SELF"]);
$dir          = dirname($_SERVER['PHP_SELF'])."/".$controller;
$checkMenu    = $server_name.$dir."/";   //con este se checa si es archivo activo o no
$param        = "?controller=".$controller."&action=";

$sys_id_men   = 3;
$sys_tipo     = 1; // Nuevo.

$ruta_app     = "";
$titulo_curr  = "Rol";
/*-----------------------------      LLamando a las clases   ---------------------------------------------*/
include_once $dir_fc.'data/inicial.class.php';
include_once $dir_fc.'data/rol.class.php';
$cInicial   = new cInicial();       //Operaciones generales
$cRoles     = new Rol();       //Operaciones generales
/*-----------------------------------   Verificando la sesión   ------------------------------------------*/
include_once 'business/sys/check_session.php'; //En este archivo se incia la sesión, por lo tanto no es necesario declarar el session_start()


/*---------------    Formateando informacion de mensajes a mostrar al usuario  ---------------------------*/
if (isset($attempt)) {
    if ($attempt == "done" ){
        $msgShow = "<div id=\"resp\" class=\"alert alert-success alert-dismissable\">
                        <i class=\"fa fa-check\"></i>
                        <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
                        <strong>¡Correcto! </strong> Registro insertado satisfactoriamente <i>Reg. No. (".$inserted.")</i>
                    </div>";
    }
    else {
        $msgShow = "";
    }
}
else{
    $msgShow = "";
}
//Obteniendo los roles
$rsRol           = $cRoles->getReg();
$totalRows_rsRol = $rsRol->rowCount();
?>
<!DOCTYPE html>
<html>
<head>
    <title>Nuevo <?php echo $titulo_curr?> | <?php echo $titulo_paginas?></title>
    <meta content="" name="description"/>
    <meta content="" name="author"/>
    <?php include($dir_fc."inc/headercommon.php"); ?>
    <script src="<?php echo $dir_fc?>js/app/status_plugins.js"></script>
    <?php include($dir_fc."js/app/guardar.php"); ?>
        <script type="text/javascript">
            function borrarespuesta(){
                $("#respuesta_ajax").html("");
            }
        </script>
    </head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="menubar-hoverable header-fixed ">
<?php include ($dir_fc."inc/header.php")?>
<!-- BEGIN BASE-->
<div id="base">
    <!-- BEGIN OFFCANVAS LEFT -->
    <div class="offcanvas"></div><!--end .offcanvas-->
    <!-- END OFFCANVAS LEFT -->
    <!-- BEGIN CONTENT-->
    <div id="content">
        <section>
            <div class="section-body contain-lg">
                <!-- BEGIN INTRO -->
                <div class="row">
                    <div class="col-lg-8">

                    </div><!--end .col -->
                    <div class="col-lg-4">
                        <ol class="breadcrumb pull-right">
                            <li><a href="<?php echo $raiz?>business/">Inicio</a></li>
                            <li><a href="<?php echo $checkMenu?>">Lista de <?php echo $titulo_curr?></a></li>
                            <li class="active">Nuevo <?php echo $titulo_curr?></li>
                        </ol>
                    </div>
                </div><!--end .row -->
                <!-- END INTRO -->
                <?php
                //Verifica si el usuario puede insertar registros
                if($_SESSION[nuev] == "1") {
                    ?>
                    <div class="card">
                        <!-- BEGIN SEARCH HEADER -->
                        <div class="card-head style-success">
                            <div class="tools pull-left">
                                <a class="btn ink-reaction btn-floating-action btn-accent-bright" href='<?php echo $param."index"?>' title="Regresar a la lista">
                                    <i class="md md-arrow-back"></i>
                                </a>
                                <!-- Agregar u opciones para regresar o algun movimiento extra  -->
                            </div>
                            <strong class="text-uppercase">CREANDO NUEVO <?php echo $titulo_curr?></strong>
                            <div class="tools">
                            </div>
                        </div><!--end .card-head -->
                        <div class="card-body">
                            <form id="frm_guardar" class="form" role="form" method="post" action="javascript: guardar('guardar', 'nuevo', '','respuesta_ajax','<?php echo $param?>', 'admin/sis_roles')" >
                                <!-- Small boxes (Stat box) -->
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div id="respuesta_ajax"><?php echo $msgShow?></div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                        <fieldset><legend>Datos del <?php echo $titulo_curr?></legend>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div class="form-group floating-label">
                                                        <input type="text" class="form-control" name="rol" id="rol" required>
                                                        <label for="rol">Nombre del Perfil <span class="text-danger">*</span></label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div class="form-group floating-label">
                                                        <input type="text" class="form-control" name="descripcion" id="descripcion" required>
                                                        <label for="descripcion">Descripción <span class="text-danger">*</span></label>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <fieldset>
                                            <legend>
                                                Permisos por default
                                            </legend>
                                            <div class="row">
                                                <div class="col-sm-8">
                                                    <article id="permisos">
                                                        <div class="permisos-field">
                                                            <div class="checkbox checkbox-styled">
                                                                <label>
                                                                    <input name="sall" id="sall" type="checkbox" value="1" title="Imprimir Registros">
                                                                    <strong>Seleccionar todos</strong>
                                                                </label>
                                                            </div>
                                                            <?php
                                                            $rsRol           = $cRoles->parentsMenu();
                                                            $totalRows_rsRol = $rsRol->rowCount();
                                                            while ($rowReg   = $rsRol->fetch(PDO::FETCH_OBJ)) {
                                                                ?>
                                                                <div id="parents-menu_<?php echo $rowReg->id?>">

                                                                    <div class="checkbox checkbox-styled">
                                                                        <span id="mostrar_<?php echo $rowReg->id?>" class="btn-plus-ne mostrar"
                                                                              onclick="mostrar(<?php echo $rowReg->id?>)">
                                                                        <i class="fa fa-plus-square-o"></i>
                                                                        </span>
                                                                        <span id="ocultar_<?php echo $rowReg->id?>" class="btn-plus-ne ocultar"
                                                                              onclick="ocultar(<?php echo $rowReg->id?>)">
                                                                            <i class="fa fa-minus-square-o"></i>
                                                                        </span>
                                                                        <label>
                                                                            <input name="menus[]" id="menu_<?php echo $rowReg->id?>" 
                                                                                    type="checkbox" 
                                                                                    value="<?php echo $rowReg->id?>"
                                                                                   title="<?php echo $rowReg->texto?>" 
                                                                                   onchange="sel_des_child(<?php echo $rowReg->id?>)">
                                                                            <?php echo $rowReg->texto ?>
                                                                        </label>
                                                                    </div>
                                                                    <input type="hidden" id="grupo_m_<?php echo $rowReg->id?>" name="grupo[<?php echo $rowReg->id?>]" value="<?php echo $rowReg->id_grupo?>">
                                                                </div>
                                                                <?php
                                                                $rsRol_c  = $cRoles->childsMenu($rowReg->id);
                                                                ?>
                                                                <div id="child-menu_<?php echo $rowReg->id?>" class="child-menu">

                                                                    <?php
                                                                    while ($rowReg_c = $rsRol_c->fetch(PDO::FETCH_OBJ)) {
                                                                        ?>
                                                                        <input type="hidden" id="grupo_m_<?php echo $rowReg_c->id?>" name="grupo[<?php echo $rowReg_c->id?>]" value="<?php echo $rowReg_c->id_grupo?>">
                                                                        <div class="checkbox checkbox-styled">
                                                                            <label class="separador-desc">
                                                                                <input name="menus[]" id="child_<?php echo $rowReg->id?>_<?php echo $rowReg_c->id?>" type="checkbox"
                                                                                       value="<?php echo $rowReg_c->id?>" title="<?php echo $rowReg_c->texto?>"
                                                                                       onchange="checamain(<?php echo $rowReg->id?>, <?php echo $rowReg_c->id?>)" >
                                                                                <?php echo $rowReg_c->texto ?>
                                                                            </label>
                                                                            <label class="separador">
                                                                                <input type="checkbox" name="permiso_imp[<?php echo $rowReg_c->id?>]" value="1"
                                                                                       title="Editar" id="permiso_imp<?php echo $rowReg_c->id?>">
                                                                                Imprimir
                                                                            </label>
                                                                            <label class="separador">
                                                                                <input type="checkbox" name="permiso_nuevo[<?php echo $rowReg_c->id?>]" value="1"
                                                                                       title="Editar" id="permiso_nuevo<?php echo $rowReg_c->id?>">
                                                                                Nuevo
                                                                            </label>
                                                                            <label class="separador">
                                                                                <input type="checkbox" name="permiso_edit[<?php echo $rowReg_c->id?>]" value="1"
                                                                                       title="Editar" id="permiso_edit<?php echo $rowReg_c->id?>" >
                                                                                Editar
                                                                            </label>
                                                                            <label class="separador">
                                                                                <input type="checkbox" name="permiso_elim[<?php echo $rowReg_c->id?>]" value="1"
                                                                                       title="Editar" id="permiso_elim<?php echo $rowReg_c->id?>" >
                                                                                Eliminar
                                                                            </label>
                                                                            <label class="separador">
                                                                                <input type="checkbox" name="permiso_exportar[<?php echo $rowReg_c->id?>]" value="1"
                                                                                       title="Editar" id="permiso_exportar<?php echo $rowReg_c->id?>" >
                                                                                Exportar
                                                                            </label>
                                                                        </div>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </div>
                                                                <?php
                                                            }
                                                            ?>
                                                        </div>
                                                    </article>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <fieldset>
                                            <div class="row">
                                                <div class="col-sm-12 col-md-4 col-xs-12 col-lg-4">
                                                    <button type="submit" class="btn ink-reaction btn-block btn-primary" id="btn_guardar">
                                                        <span class="glyphicon glyphicon-floppy-disk"></span> Guardar
                                                    </button>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                <?php
                }else{
                    //Incliur el código de permisos denegados
                    include("../../sys/permissions_d.php");
                }?>
            </div>
        </section>
    </div><!--end #content-->
    <!-- END CONTENT -->
    <?php include($dir_fc."inc/menucommon.php") ?>
</div><!--end #base-->
<!-- END BASE -->
<?php include($dir_fc."inc/footercommon.php"); ?>
<script>
    $(document).on("ready", edoinicial);
    //Funcion que se ejecutará al cargar el documento
    function edoinicial() {
        $("#sall").change(function () {
            if ($(this).is(':checked')) {
                $("#permisos input[type=checkbox]").prop('checked', true);
            } else {
                $("#permisos input[type=checkbox]").prop('checked', false);
            }
        });

        $('.child-menu').hide();
        $('.ocultar').hide();

        $('#rol').focus();
        $(":input").keydown(function () {
            borrarespuesta();
        });
    }

    function mostrar(id){
        $("#child-menu_"+id).show(150,function() {
            $("#mostrar_"+id).hide();
            $("#ocultar_"+id).show();
        });
    }

    function ocultar(id){
        $("#child-menu_"+id).hide(150,function() {
            $("#mostrar_"+id).show();
            $("#ocultar_"+id).hide();
        });
    }

    function sel_des_child(id){
        if ($('#menu_'+id).is(':checked')) {
            $("#child-menu_"+id+" input[type=checkbox]").prop('checked', true);
        } else {
            $("#child-menu_"+id+" input[type=checkbox]").prop('checked', false);
        }
    }

    function checamain(id, idprov){
        if($('#menu_'+idprov).is(':checked')){
            if ($('#menu_'+id).is(':checked')) {

            }else{
                $('#menu_'+id).prop('checked', true);
            }
        }
        
    }
</script>
</body>
</html>
