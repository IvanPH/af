<?php
session_start();
/*--------------------------------------------------------------------------------------------------------
Regresará un json con La lista que se pintará en la tabla
*/
$dir_fc       = "../../../../";
include_once $dir_fc.'data/movimientos.php';
/*--------------------------------------------------------------------------------------------------------*/
include_once $dir_fc.'connections/trop.php';        //Inclueye configuración de fecha y  hora de mexico
include_once $dir_fc.'connections/php_config.php'; //Inclueye configuración de sesiones para este proyecto

$cAccion   = new cMovimientos();
/*--------------------------------------------------------------------------------------------------------*/
extract($_REQUEST);

$datos = array();

$dataList = $cAccion->getAllRegAjax();

while($row = $dataList->fetch(PDO::FETCH_OBJ)){
    $iId             = $row->id_movimiento;
    $usuario         = $row->id_user;
    $fecha           = $row->fecha;
    $tipo_mov        = $row->tipo_mov;
    $id_registro     = $row->id_registro;
    $nombre_tabla    = $row->nombre_tabla;

    if ($usuario == "0" || $usuario == NULL) {
        $usuarioM = "SIN OTORGAR";
    } else {
        $rsEditar = $cAccion->getUsuarioName($usuario);
        $rsEditar->rowCount();
            if($rsEditar->rowCount() > 0){
                $arrEdi  = $rsEditar->fetch(PDO::FETCH_OBJ);
                $usuarioM      = $arrEdi->usuario;
            }
    }
    if($nombre_tabla == "Empleados"){
    if ($id_registro == "0" || $id_registro == NULL) {
        $matricula = "SIN OTORGAR";
    } else {
        $rsEditar = $cAccion->getMatricula($id_registro);
        $rsEditar->rowCount();
            if($rsEditar->rowCount() > 0){
                $arrEdi  = $rsEditar->fetch(PDO::FETCH_OBJ);
                $matricula      = $arrEdi->noempleado;
            }
    } 
    }else{
        if ($id_registro == "0" || $id_registro == NULL) {
            $matricula = "SIN OTORGAR";
        } else {
            $rsEditar = $cAccion->getMAF($id_registro);
            $rsEditar->rowCount();
                if($rsEditar->rowCount() > 0){
                    $arrEdi  = $rsEditar->fetch(PDO::FETCH_OBJ);
                    $matricula      = $arrEdi->matricula_activo;
                }
        } 

    }
    $datos[] = array(
        "id_movimiento"  => $iId,
        "usuario"	     => $usuarioM,
        "fecha"	         => $fecha,
        "tipo_mov"	     => $tipo_mov,
        "id_registro"	 => $matricula,
        "nombre_tabla"	 => $nombre_tabla
    );
}

echo json_encode(array("data" => $datos));
?>
