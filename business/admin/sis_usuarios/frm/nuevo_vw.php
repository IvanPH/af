<?php
session_start();

$type = 0;
$id   = 0;

extract($_REQUEST);

/*--------------------------------------------------------------------------------------------------------*/
$dir_fc       = "../../../../";
include_once $dir_fc.'connections/trop.php';
include_once $dir_fc.'connections/php_config.php';
include_once $dir_fc.'data/users.class.php';
include_once $dir_fc.'data/rol.class.php';

$cRoles       = new Rol();

$apepa = "";
$correo = "";
$sexo = "";
$nombre = "";
$apema = "";
$admin = "";
$id_rol = "";
$usuario = "";

if(($_SESSION[nuev] == 1 && $type == 1) || ($_SESSION[edit] == 1 && $type == 2 && $id > 0) || $type == 3){//Si tiene permisos de agregar un nuevo documento
    $_SESSION[_type_] = $type;
    if($type == 2 || $type == 3){
        $cAccion = new cUsers();
        $cAccion->setIdUsuario($id);
        $rsEditar    = $cAccion->getRegbyid();
        $rsEditar->rowCount();

        if($rsEditar->rowCount() > 0){
            $_SESSION[_editar_] = $id;
            $arrEdi      = $rsEditar->fetch(PDO::FETCH_OBJ);
            $id_usuario  = $arrEdi->id_usuario;
            $_SESSION[_editar_] = $id_usuario; //Importante que si es ediciòn se asigne el ID que se va a editar
            $id_rol      = $arrEdi->id_rol;
            $id_responsable     = $arrEdi->id_responsable;
            $usuario     = $arrEdi->usuario;
            $sexo        = $arrEdi->sexo;
            $nombre      = $arrEdi->nombre;
            $apepa       = $arrEdi->apepa;
            $apema       = $arrEdi->apema;
            $correo      = $arrEdi->correo;
            $activo      = $arrEdi->activo;
            $admin       = $arrEdi->admin;
            //$id_respo    = $cAccion->getResponsableByUser();
        }
    }
    ?>
<script type="text/javascript" src="<?php echo $raiz?>js/app/floatingLabels.js"></script>
<form id="frm_guardar" class="" method="post" action="javascript: guardar()" >
    <div class="modal-content">
        <div class="row">
            <div class="col-md-12">
                <div id="respuesta_ajax_modal">
                </div>
            </div>
        </div>
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h5 class="modal-title">Agregando Usuario
        </div>
        <div class="modal-body">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-xs-12 col-md-12 col-sm-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h6 class="panel-title">Datos de Usuario</h6>
                            <!--<div class="heading-elements">
                                <ul class="icons-list">
                                    <li><a data-action="collapse"></a></li>
                                    <li><a data-action="reload"></a></li>
                                    <li><a data-action="close"></a></li>
                                </ul>
                            </div> -->
                        </div>

                        <div class="panel-body">
                            <fieldset>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group form-group-material">
                                            <label class="control-label">
                                                Nombre(s)<span class="text-danger">*</span>
                                            </label>
                                            <input type="text" class="form-control" name="nombre" id="nombre" autocomplete="off"
                                                   required placeholder="Nombre(s) *" value="<?php echo $nombre ?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group form-group-material">
                                            <label  class="control-label" for="apepat">
                                                Apellido Paterno <span class="text-danger">*</span>
                                            </label>
                                            <input type="text" class="form-control" name="apepat" id="apepat" autocomplete="off"
                                                   value="<?php echo $apepa ?>"
                                                   placeholder="Apellido Paterno *">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group form-group-material">
                                            <label class="control-label" for="apemat">Apellido Materno
                                                <span class="text-danger">*</span>
                                            </label>
                                            <input type="text" class="form-control" name="apemat" id="apemat" autocomplete="off"
                                                   placeholder="Apellido Materno *" value="<?php echo $apema ?>">
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group form-group-material">
                                            <label for="usuario" class="control-label">
                                                Nombre de Usuario <span class="text-danger">*</span>
                                            </label>
                                            <input type="text" class="form-control" name="usuario" id="usuario" autocomplete="off"
                                                   required placeholder="Nombre de Usuario *"  pattern="^@?(\w){1,16}$" value="<?php echo $usuario ?>"
                                                   title="El Nombre de usuario no debe de contener caracteres especiales">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group form-group-material">
                                            <label for="clave" class="control-label">
                                                Password (clave de acceso)<span class="text-danger">*</span>
                                            </label>
                                            <input type="password" class="form-control" name="clave" id="clave" autocomplete="off"
                                                <?php if($type == 2){ echo "disabled";}else{ echo "required"; } ?>  required
                                                   placeholder="Password (clave de acceso) *">
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group form-group-material">
                                            <label for="correo" class="control-label">Correo electrónico
                                                <span class="text-danger">*</span>
                                            </label>
                                            <input type="email" class="form-control" name="correo" id="correo"
                                                   required autocomplete="off" placeholder="Correo electrónico *"
                                                   value="<?php echo $correo?>">
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <div class="row">
                                    <div class="col-sm-2">
                                        <div class="form-group form-group-material">
                                            <label for="sexo" class="control-label">
                                                Género: <span class="text-danger">*</span>
                                            </label>
                                            <select name="sexo" id="sexo" class="form-control" required>
                                                <option value="">Género *</option>
                                                <option value="1" <?php if($sexo == 1){ echo "selected";}?>>Masculino</option>
                                                <option value="2" <?php if($sexo == 2){ echo "selected";}?>>Femenino</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group form-group-material">
                                            <label for="id_rol" class="control-label">
                                                Perfil: <span class="text-danger">*</span>
                                            </label>
                                            <select name="id_rol" id="id_rol" class="form-control"
                                                    required >
                                                <option value="">Perfil *</option>
                                                <?php
                                                $rs_rol = $cRoles->getAllRoles();
                                                while($rw_rol = $rs_rol->fetch(PDO::FETCH_OBJ)){
                                                    $selected_r = "";
                                                    if($type == 2 || $type == 3){
                                                        if($id_rol == $rw_rol->id){
                                                            $selected_r = "selected";
                                                        }
                                                    }
                                                    ?>
                                                    <option value="<?php echo $rw_rol->id?>" <?php echo $selected_r?> <?php echo $type.$id_rol?>>
                                                        <?php echo $rw_rol->rol." - ".$rw_rol->descripcion?>
                                                    </option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <?php
                                    if(isset($_SESSION[admin]) && $_SESSION[admin] == 1){
                                        ?>
                                        <div class="col-sm-2">
                                            <div class="form-group form-group-material">
                                                <label for="admin" class="control-label">
                                                    Tipo de usuario <span class="text-danger">*</span>
                                                </label>
                                                <select name="admin" id="admin" class="form-control" required>
                                                    <option value="">Tipo de usuario *</option>
                                                    <option value="0" <?php if($admin == 0){echo "selected";}?>>Usuario Estándar</option>
                                                    <option value="1" <?php if($admin == 1){echo "selected";}?>>Usuario Administrativo</option>
                                                </select>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </fieldset>
                        </div>
                    </div>

                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h6 class="panel-title">Permisos</h6>
                            <!--<div class="heading-elements">
                                <ul class="icons-list">
                                    <li><a data-action="collapse"></a></li>
                                    <li><a data-action="reload"></a></li>
                                    <li><a data-action="close"></a></li>
                                </ul>
                            </div> -->
                        </div>

                        <div class="panel-body">
                            <fieldset>
                                <article id="permisos">
                                    <div class="row">
                                        <div class="permisos-field">
                                            <?php
                                            if($type == 2){
                                                ?>
                                                <div class="checkbox checkbox-styled">
                                                    <label>
                                                        <input name="sall" id="sall" type="checkbox" value="1" title="Seleccionar Todos">
                                                        <strong>Seleccionar todos</strong>
                                                    </label>
                                                </div>
                                                <?php
                                            }
                                            ?>

                                            <div id="permisos_ajax">
                                                <?php
                                                if($type == 2 || $type == 3){
                                                    $cRoles->set_id($id_rol);

                                                    $rsRol           = $cRoles->parentsMenu();
                                                    $totalRows_rsRol = $rsRol->rowCount();
                                                    while ($rowReg   = $rsRol->fetch(PDO::FETCH_OBJ)) {
                                                        $chk = "";
                                                        $cAccion->set_id_menu($rowReg->id);
                                                        $checked_r  = $cAccion->checarMenuUser();
                                                        $rowsc1  = $checked_r->rowCount();
                                                        if ($rowsc1>0) {
                                                            $chk = "checked";
                                                        }
                                                        ?>
                                                        <div id="parents-menu_<?php echo $rowReg->id?>">
                                                            <div class="form-group">
                                                                <span>
                                                                    <span id="mostrar_<?php echo $rowReg->id?>" class="btn-plus-ne mostrar"
                                                                          onclick="mostrar(<?php echo $rowReg->id?>)">
                                                                        <i class="fa fa-plus-square-o"></i>
                                                                    </span>
                                                                    <span id="ocultar_<?php echo $rowReg->id?>" class="btn-plus-ne ocultar"
                                                                          onclick="ocultar(<?php echo $rowReg->id?>)">
                                                                        <i class="fa fa-minus-square-o"></i>
                                                                    </span>
                                                                </span>
                                                                <label class="checkbox-inline">
                                                                    <input name="menus[]" id="menu_<?php echo $rowReg->id?>" type="checkbox"
                                                                           value="<?php echo $rowReg->id?>" <?php echo $chk ?>
                                                                           title="<?php echo $rowReg->texto?>"
                                                                           onchange="sel_des_child(<?php echo $rowReg->id?>)">
                                                                    <?php echo $rowReg->texto ?>
                                                                </label>
                                                            </div>
                                                            <input type="hidden" id="grupo_m_<?php echo $rowReg->id?>" name="grupo[<?php echo $rowReg->id?>]" value="<?php echo $rowReg->id_grupo?>">
                                                        </div>
                                                        <?php
                                                        $rsRol_c  = $cRoles->childsMenu($rowReg->id);
                                                        ?>
                                                        <div id="child-menu_<?php echo $rowReg->id?>" class="child-menu">
                                                            <?php
                                                            while ($rowReg_c = $rsRol_c->fetch(PDO::FETCH_OBJ)) {
                                                                $chk_imp     = "";
                                                                $chk_edit    = "";
                                                                $chk_nuevo   = "";
                                                                $chk_elim    = "";
                                                                $chk_exportar= "";
                                                                $chk_2 = "";
                                                                $cAccion->set_id_menu($rowReg_c->id);
                                                                $checked_r_2 = $cAccion->checarMenuUser();
                                                                $rowsc = $checked_r_2->rowCount();
                                                                if ($rowsc > 0) {
                                                                    $chk_2 = "checked";
                                                                    $rw_check = $checked_r_2->fetch(PDO::FETCH_OBJ);
                                                                    $chk_imp = $rw_check->imp;
                                                                    $chk_edit = $rw_check->edit;
                                                                    $chk_nuevo = $rw_check->nuevo;
                                                                    $chk_elim = $rw_check->elim;
                                                                    $chk_exportar = $rw_check->exportar;
                                                                }
                                                                ?>
                                                                <input type="hidden" id="grupo_m_<?php echo $rowReg_c->id?>" name="grupo[<?php echo $rowReg_c->id?>]"
                                                                       value="<?php echo $rowReg_c->id_grupo?>">
                                                                <div class="checkbox">
                                                                    <label class="checkbox-inline separador-desc">
                                                                        <input name="menus[]" id="child_<?php echo $rowReg->id?> _<?php echo $rowReg_c->id?>" type="checkbox"
                                                                               value="<?php echo $rowReg_c->id?>" title="<?php echo $rowReg_c->texto?>"
                                                                               onchange="checamain(<?php echo $rowReg->id?>)" <?php echo $chk_2 ?>>
                                                                        <?php echo $rowReg_c->texto ?>
                                                                    </label>
                                                                    <label class="checkbox-inline separador">
                                                                        <input type="checkbox" name="permiso_imp[<?php echo $rowReg_c->id?>]" value="1"
                                                                               title="Editar" id="permiso_imp<?php echo $rowReg_c->id?>" <?php if($chk_imp == 1){ echo "checked";}else{echo "checked='false'";}?>>
                                                                        Imprimir

                                                                    </label>
                                                                    <label class="checkbox-inline separador">
                                                                        <input type="checkbox" name="permiso_nuevo[<?php echo $rowReg_c->id?>]" value="1"
                                                                               title="Editar" id="permiso_nuevo<?php echo $rowReg_c->id?>" <?php if($chk_nuevo == 1){ echo "checked";}else{echo "checked='false'";}?>>
                                                                        Nuevo
                                                                    </label>
                                                                    <label class="checkbox-inline separador">
                                                                        <input type="checkbox" name="permiso_edit[<?php echo $rowReg_c->id?>]" value="1"
                                                                               title="Editar" id="permiso_edit<?php echo $rowReg_c->id?>" <?php if($chk_edit == 1){ echo "checked";}else{echo "checked='false'";}?>>
                                                                        Editar
                                                                    </label>
                                                                    <label class="checkbox-inline separador">
                                                                        <input type="checkbox" name="permiso_elim[<?php echo $rowReg_c->id?>]" value="1"
                                                                               title="Editar" id="permiso_elim<?php echo $rowReg_c->id?>" <?php if($chk_elim == 1){ echo "checked";}else{echo "checked='false'";}?>>
                                                                        Eliminar
                                                                    </label>
                                                                    <label class="checkbox-inline separador">
                                                                        <input type="checkbox" name="permiso_exportar[<?php echo $rowReg_c->id?>]" value="1"
                                                                               title="Editar" id="permiso_exportar<?php echo $rowReg_c->id?>" <?php if($chk_exportar == 1){ echo "checked";}else{echo "checked='false'";}?>>
                                                                        Exportar
                                                                    </label>
                                                                </div>
                                                                <?php
                                                            }
                                                            ?>
                                                        </div>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-danger" data-dismiss="modal">
                <i class="icon-close2"></i> Cerrar
            </button>
            <?php
            if($type <> 3){
                ?>
                <button type="submit" class="btn btn-primary" id="btn_guardar">
                    <i class="icon-floppy-disk"></i> Guardar Cambios
                </button>
            <?php
            }
            ?>
        </div>
    </div>
</form>
<script>
    $(function() {
        $("#sall").change(function () {
            if ($(this).is(':checked')) {
                $("#permisos input[type=checkbox]").prop('checked', true);
            } else {
                $("#permisos input[type=checkbox]").prop('checked', false);
            }
        });
        $('.child-menu').hide();
        $('.ocultar').hide();
        $('#id_rol')[0].setAttribute('onchange', 'traePermisosPorRol()');

        <?php
        if($type == 3){
            ?>
        $('#frm_guardar :input:not(:button)').attr({
            'disabled': 'disabled'
        });
            <?php
        }
        ?>
    });
    
    function traePermisosPorRol() {
		var rol = $('#id_rol').val();
		if(rol > 0){
			$.post("<?php echo $raiz?>business/admin/sis_usuarios/ajax/trae_menu_rol.php",{'id_rol': rol},
				function(respuesta){
					$("#permisos_ajax").html(respuesta);
					$('.child-menu').hide();
					$('.ocultar').hide();
				});
		}

	}

</script>
<?php
}else{
    echo "<h1>No se puede acceder a la información solicitada</h1>";
}
?>