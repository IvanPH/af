<?php
/*--------------------------------------------------------------------------------------------------------*/
$dir_fc = "";
include_once $dir_fc . 'connections/trop.php';
include_once $dir_fc . 'connections/php_config.php';

$sys_id_men   = 2;    //Corresponde al menú
$sys_tipo     = 1;    //Indicando que es un arhcivo nuevo Sirve para corroborar permisos de usuarios.
$real_sis     = "admin/sis_usuarios";
$title_act    = "Usuarios";
$sub_t_act    = "Agregando";
$fmc          = "";
$busqueda     = "";


$type = 0;
$id   = 0;

extract($_REQUEST);


$dir          = dirname($_SERVER['PHP_SELF']);
$current_file = basename($_SERVER["PHP_SELF"]);
$checkMenu    = $server_name . $dir . "/" . $current_file;   //con este se checa si index està activo
$param        = "?fmc=" . $fmc . "&td=";

/*------------------------  Incluyendo e Instanciando Clases Inicial  -------------------------------*/
include_once $dir_fc . 'data/inicial.class.php';
include_once $dir_fc . 'data/users.class.php';
include_once $dir_fc . 'data/rol.class.php';
include_once $dir_fc . 'common/parametros.class.php';
include_once $dir_fc . 'common/function.class.php';

$cInicial  = new cInicial(); //Operaciones generales (Menu de incio, etc)
$cRoles    = new Rol();
$cFn       = new cFunction();
/*---------------------------------------------------------------------------------------------------*/
include_once 'business/sys/check_session.php';



$type = $_SESSION[_is_view_];
$_SESSION[_type_] = $type;

$id = $_SESSION[_editar_];

$showBusqueda = "";
$msgBusqueda  = "";

$id_filtro_mov = 0;

if (isset($_SESSION[fil_tipo_mov_ofi])) {
    /*
     * Nuevo Filtro de Búsqueda con el Select. para seleccionar los tipos de movimientos
     * */
    if ($_SESSION[fil_tipo_mov_ofi] != "") {
        $id_filtro_mov = $_SESSION[fil_tipo_mov_ofi];
    } else {
        $_SESSION[fil_tipo_mov_ofi] = 0;
    }
}

if ($busqueda == "do_search_please") {
    if (isset($_SESSION[fil_descripcion])) {
        if ($_SESSION[fil_descripcion] != "") {
            $showBusqueda .= "<br> <strong>Descripción: </strong>" . $_SESSION[fil_descripcion];
        }
    }
    if (isset($_SESSION[fil_asuntos])) {
        if ($_SESSION[fil_asuntos] != "") {
            $showBusqueda .= "<br> <strong>Asuntos: </strong>" . $_SESSION[fil_asuntos];
        }
    }
    if (isset($_SESSION[fil_id])) {
        if ($_SESSION[fil_id] != "") {
            $showBusqueda .= "<br> <strong>ID: </strong>" . $_SESSION[fil_id];
        }
    }

    if (isset($_SESSION[fil_fecha_ini]) && isset($_SESSION[fil_fecha_fin])) {
        if ($_SESSION[fil_fecha_ini] != "" && $_SESSION[fil_fecha_fin] != "") {
            $showBusqueda .= "<br> <strong>Fecha </strong> Del: " . $cFn->formatear_fecha_show($_SESSION[fil_fecha_ini]) . " 
                   al " . $cFn->formatear_fecha_show($_SESSION[fil_fecha_fin]);
        }
    }
    $msgBusqueda = $cFn->custom_alert('info', 'Resultados con la busqueda:', $showBusqueda, 1, 1);
}

$apepa = "";
$correo = "";
$sexo = "";
$nombre = "";
$apema = "";
$admin = "";
$id_rol = "";
$usuario = "";


?>
<!DOCTYPE html>
<html lang="es">

<head>
    <title><?php echo c_page_title ?></title>
    <meta content="Administrador Sistema (Proyecto <?php echo c_page_title ?>) " name="description" />
    <meta content="Subgerencia de Informática - By JF! ;)" name="author" />

    <?php include($dir_fc . "inc/headercommon.php"); ?>

    <link href="<?php echo $raiz ?>css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo $raiz ?>css/bootstrap-datetimepicker-standalone.css" rel="stylesheet" type="text/css" />
    <script src="<?php echo $raiz ?>js/app/moment.min.js" type="text/javascript"></script>
    <script src="<?php echo $raiz ?>js/app/bootstrap-datetimepicker.min.js" type="text/javascript"></script>

    <script src="<?php echo $raiz ?>js/app/status_plugins.js?ver=1.1"></script>
</head>

<body class="navbar-top">
    <?php include($dir_fc . "inc/header.php") ?>
    <!-- Page container -->
    <div class="page-container">
        <!-- Page content -->
        <div class="page-content">
            <?php include($dir_fc . "inc/menucommon.php") ?>
            <!-- Main content -->
            <div class="content-wrapper">
                <!-- Page header -->
                <div class="page-header page-header-default">

                    <div class="page-header-content">

                    </div>

                    <div class="breadcrumb-line">
                        <ul class="breadcrumb">
                            <li>
                                <a href="<?php echo $raiz ?>business"><i class="icon-home2 position-left"></i> Inicio</a>
                            </li>
                            <li class="active"><?php echo $title_act ?></li>
                        </ul>
                    </div>
                </div>
                <!-- /page header -->
                <!-- Content area -->
                <div class="content">
                    <!-- Ajax sourced data -->
                    <div class="panel panel-flat">
                        <div class="panel-body">
                            <?php
                            if(($_SESSION[nuev] == 1 && $type == 1) || ($_SESSION[edit] == 1 && $type == 2 && $id > 0) || $type == 3){//Si tiene permisos de agregar un nuevo documento
                                $_SESSION[_type_] = $type;
                                if($type == 2 || $type == 3){
                                    $cAccion = new cUsers();
                                    $cAccion->setIdUsuario($id);
                                    $rsEditar    = $cAccion->getRegbyid();
                                    $rsEditar->rowCount();
                            
                                    if($rsEditar->rowCount() > 0){
                                        $_SESSION[_editar_] = $id;
                                        $arrEdi      = $rsEditar->fetch(PDO::FETCH_OBJ);
                                        $id_usuario  = $arrEdi->id_usuario;
                                        $_SESSION[_editar_] = $id_usuario; //Importante que si es ediciòn se asigne el ID que se va a editar
                                        $id_rol      = $arrEdi->id_rol;
                                        $id_responsable     = $arrEdi->id_responsable;
                                        $usuario     = $arrEdi->usuario;
                                        $sexo        = $arrEdi->sexo;
                                        $nombre      = $arrEdi->nombre;
                                        $apepa       = $arrEdi->apepa;
                                        $apema       = $arrEdi->apema;
                                        $correo      = $arrEdi->correo;
                                        $activo      = $arrEdi->activo;
                                        $admin       = $arrEdi->admin;
                                        //$id_respo    = $cAccion->getResponsableByUser();
                                    }
                                }
                                ?>
                                <script type="text/javascript" src="<?php echo $raiz ?>js/app/floatingLabels.js"></script>
                                <form id="frm_guardar" class="" method="post" action="javascript: guardar()">
                                    <input type="hidden" name="primera_carga" id="primera_carga" value="1">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div id="respuesta_ajax_modal">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h5 class="modal-title">Agregando Usuario
                                    </div>
                                    <div class="modal-body">
                                        <!-- Small boxes (Stat box) -->
                                        <div class="row">
                                            <div class="col-xs-12 col-md-12 col-sm-12">
                                                <div class="panel panel-primary">
                                                    <div class="panel-heading">
                                                        <h6 class="panel-title">Datos de Usuario</h6>
                                                        <!--<div class="heading-elements">
                                                            <ul class="icons-list">
                                                                <li><a data-action="collapse"></a></li>
                                                                <li><a data-action="reload"></a></li>
                                                                <li><a data-action="close"></a></li>
                                                            </ul>
                                                        </div> -->
                                                    </div>

                                                    <div class="panel-body">
                                                        <fieldset>
                                                            <div class="row">
                                                                <div class="col-sm-4">
                                                                    <div class="form-group form-group-material">
                                                                        <label class="control-label">
                                                                            Nombre(s)<span class="text-danger">*</span>
                                                                        </label>
                                                                        <input type="text" class="form-control" name="nombre" id="nombre" autocomplete="off"
                                                                            required placeholder="Nombre(s) *" value="<?php echo $nombre ?>">
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-4">
                                                                    <div class="form-group form-group-material">
                                                                        <label  class="control-label" for="apepat">
                                                                            Apellido Paterno <span class="text-danger">*</span>
                                                                        </label>
                                                                        <input type="text" class="form-control" name="apepat" id="apepat" autocomplete="off"
                                                                            value="<?php echo $apepa ?>"
                                                                            placeholder="Apellido Paterno *">
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-4">
                                                                    <div class="form-group form-group-material">
                                                                        <label class="control-label" for="apemat">Apellido Materno
                                                                            <span class="text-danger">*</span>
                                                                        </label>
                                                                        <input type="text" class="form-control" name="apemat" id="apemat" autocomplete="off"
                                                                            placeholder="Apellido Materno *" value="<?php echo $apema ?>">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <br>
                                                            <div class="row">
                                                                <div class="col-sm-4">
                                                                    <div class="form-group form-group-material">
                                                                        <label for="usuario" class="control-label">
                                                                            Nombre de Usuario <span class="text-danger">*</span>
                                                                        </label>
                                                                        <input type="text" class="form-control" name="usuario" id="usuario" autocomplete="off"
                                                                            required placeholder="Nombre de Usuario *"  pattern="^@?(\w){1,16}$" value="<?php echo $usuario ?>"
                                                                            title="El Nombre de usuario no debe de contener caracteres especiales">
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-4">
                                                                    <div class="form-group form-group-material">
                                                                        <label for="clave" class="control-label">
                                                                            Password (clave de acceso)<span class="text-danger">*</span>
                                                                        </label>
                                                                        <input type="password" class="form-control" name="clave" id="clave" autocomplete="off"
                                                                            <?php if($type == 2){ echo "disabled";}else{ echo "required"; } ?>  required
                                                                            placeholder="Password (clave de acceso) *">
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-4">
                                                                    <div class="form-group form-group-material">
                                                                        <label for="correo" class="control-label">Correo electrónico
                                                                            <span class="text-danger">*</span>
                                                                        </label>
                                                                        <input type="email" class="form-control" name="correo" id="correo"
                                                                            required autocomplete="off" placeholder="Correo electrónico *"
                                                                            value="<?php echo $correo?>">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <br/>
                                                            <div class="row">
                                                                <div class="col-sm-2">
                                                                    <div class="form-group form-group-material">
                                                                        <label for="sexo" class="control-label">
                                                                            Género: <span class="text-danger">*</span>
                                                                        </label>
                                                                        <select name="sexo" id="sexo" class="form-control" required>
                                                                            <option value="">Género *</option>
                                                                            <option value="1" <?php if($sexo == 1){ echo "selected";}?>>Masculino</option>
                                                                            <option value="2" <?php if($sexo == 2){ echo "selected";}?>>Femenino</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <div class="form-group form-group-material">
                                                                        <label for="id_rol" class="control-label">
                                                                            Perfil: <span class="text-danger">*</span>
                                                                        </label>
                                                                        <select name="id_rol" id="id_rol" class="form-control"
                                                                                required >
                                                                            <option value="">Perfil *</option>
                                                                            <?php
                                                                            $rs_rol = $cRoles->getAllRoles();
                                                                            while($rw_rol = $rs_rol->fetch(PDO::FETCH_OBJ)){
                                                                                $selected_r = "";
                                                                                if($type == 2 || $type == 3){
                                                                                    if($id_rol == $rw_rol->id){
                                                                                        $selected_r = "selected";
                                                                                    }
                                                                                }
                                                                                ?>
                                                                                <option value="<?php echo $rw_rol->id?>" <?php echo $selected_r?> <?php echo $type.$id_rol?>>
                                                                                    <?php echo $rw_rol->rol." - ".$rw_rol->descripcion?>
                                                                                </option>
                                                                                <?php
                                                                            }
                                                                            ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <?php
                                                                if(isset($_SESSION[admin]) && $_SESSION[admin] == 1){
                                                                    ?>
                                                                    <div class="col-sm-2">
                                                                        <div class="form-group form-group-material">
                                                                            <label for="admin" class="control-label">
                                                                                Tipo de usuario <span class="text-danger">*</span>
                                                                            </label>
                                                                            <select name="admin" id="admin" class="form-control" required>
                                                                                <option value="">Tipo de usuario *</option>
                                                                                <option value="0" <?php if($admin == 0){echo "selected";}?>>Usuario Estándar</option>
                                                                                <option value="1" <?php if($admin == 1){echo "selected";}?>>Usuario Administrativo</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </div>
                                                        </fieldset>
                                                    </div>
                                                </div>

                                                <div class="panel panel-primary">
                                                    <div class="panel-heading">
                                                        <h6 class="panel-title">Permisos</h6>
                                                        <!--<div class="heading-elements">
                                                            <ul class="icons-list">
                                                                <li><a data-action="collapse"></a></li>
                                                                <li><a data-action="reload"></a></li>
                                                                <li><a data-action="close"></a></li>
                                                            </ul>
                                                        </div> -->
                                                    </div>

                                                    <div class="panel-body">
                                                        <fieldset>
                                                            <article id="permisos">
                                                                <div class="row">
                                                                    <div class="permisos-field">
                                                                        <?php
                                                                        if($type == 2){
                                                                            ?>
                                                                            <div class="checkbox checkbox-styled">
                                                                                <label>
                                                                                    <input name="sall" id="sall" type="checkbox" value="1" title="Seleccionar Todos">
                                                                                    <strong>Seleccionar todos</strong>
                                                                                </label>
                                                                            </div>
                                                                            <?php
                                                                        }
                                                                        ?>

                                                                        <div id="permisos_ajax">
                                                                            <?php
                                                                            if($type == 2 || $type == 3){
                                                                                $cRoles->set_id($id_rol);

                                                                                $rsRol           = $cRoles->parentsMenu();
                                                                                $totalRows_rsRol = $rsRol->rowCount();
                                                                                while ($rowReg   = $rsRol->fetch(PDO::FETCH_OBJ)) {
                                                                                    $chk = "";
                                                                                    $cAccion->set_id_menu($rowReg->id);
                                                                                    $checked_r  = $cAccion->checarMenuUser();
                                                                                    $rowsc1  = $checked_r->rowCount();
                                                                                    if ($rowsc1>0) {
                                                                                        $chk = "checked";
                                                                                    }
                                                                                    ?>
                                                                                    <div id="parents-menu_<?php echo $rowReg->id?>">
                                                                                        <div class="form-group">
                                                                                            <span>
                                                                                                <span id="mostrar_<?php echo $rowReg->id?>" class="btn-plus-ne mostrar"
                                                                                                    onclick="mostrar(<?php echo $rowReg->id?>)">
                                                                                                    <i class="fa fa-plus-square-o"></i>
                                                                                                </span>
                                                                                                <span id="ocultar_<?php echo $rowReg->id?>" class="btn-plus-ne ocultar"
                                                                                                    onclick="ocultar(<?php echo $rowReg->id?>)">
                                                                                                    <i class="fa fa-minus-square-o"></i>
                                                                                                </span>
                                                                                            </span>
                                                                                            <label class="checkbox-inline">
                                                                                                <input name="menus[]" id="menu_<?php echo $rowReg->id?>" type="checkbox"
                                                                                                    value="<?php echo $rowReg->id?>" <?php echo $chk ?>
                                                                                                    title="<?php echo $rowReg->texto?>"
                                                                                                    onchange="sel_des_child(<?php echo $rowReg->id?>)">
                                                                                                <?php echo $rowReg->texto ?>
                                                                                            </label>
                                                                                        </div>
                                                                                        <input type="hidden" id="grupo_m_<?php echo $rowReg->id?>" name="grupo[<?php echo $rowReg->id?>]" value="<?php echo $rowReg->id_grupo?>">
                                                                                    </div>
                                                                                    <?php
                                                                                    $rsRol_c  = $cRoles->childsMenu($rowReg->id);
                                                                                    ?>
                                                                                    <div id="child-menu_<?php echo $rowReg->id?>" class="child-menu">
                                                                                        <?php
                                                                                        while ($rowReg_c = $rsRol_c->fetch(PDO::FETCH_OBJ)) {
                                                                                            $chk_imp     = "";
                                                                                            $chk_edit    = "";
                                                                                            $chk_nuevo   = "";
                                                                                            $chk_elim    = "";
                                                                                            $chk_exportar= "";
                                                                                            $chk_2 = "";
                                                                                            $cAccion->set_id_menu($rowReg_c->id);
                                                                                            $checked_r_2 = $cAccion->checarMenuUser();
                                                                                            $rowsc = $checked_r_2->rowCount();
                                                                                            if ($rowsc > 0) {
                                                                                                $chk_2 = "checked";
                                                                                                $rw_check = $checked_r_2->fetch(PDO::FETCH_OBJ);
                                                                                                $chk_imp = $rw_check->imp;
                                                                                                $chk_edit = $rw_check->edit;
                                                                                                $chk_nuevo = $rw_check->nuevo;
                                                                                                $chk_elim = $rw_check->elim;
                                                                                                $chk_exportar = $rw_check->exportar;
                                                                                            }
                                                                                            ?>
                                                                                            <input type="hidden" id="grupo_m_<?php echo $rowReg_c->id?>" name="grupo[<?php echo $rowReg_c->id?>]"
                                                                                                value="<?php echo $rowReg_c->id_grupo?>">
                                                                                            <div class="checkbox">
                                                                                                <label class="checkbox-inline separador-desc">
                                                                                                    <input name="menus[]" id="child_<?php echo $rowReg->id?>_<?php echo $rowReg_c->id?>" 
                                                                                                    type="checkbox"
                                                                                                        value="<?php echo $rowReg_c->id?>" title="<?php echo $rowReg_c->texto?>"
                                                                                                        onchange="checamain(<?php echo $rowReg->id?>)" <?php echo $chk_2 ?>>
                                                                                                    <?php echo $rowReg_c->texto ?>
                                                                                                </label>
                                                                                                <label class="checkbox-inline separador">
                                                                                                    <input type="checkbox" name="permiso_imp[<?php echo $rowReg_c->id?>]" 
                                                                                                        value="1"
                                                                                                        title="Editar" 
                                                                                                        id="permiso_imp<?php echo $rowReg_c->id?>" 
                                                                                                        <?php if($chk_imp == 1){ echo "checked";}?>>
                                                                                                    Imprimir

                                                                                                </label>
                                                                                                <label class="checkbox-inline separador">
                                                                                                    <input type="checkbox" 
                                                                                                            name="permiso_nuevo[<?php echo $rowReg_c->id?>]" value="1"
                                                                                                            title="Editar" 
                                                                                                            id="permiso_nuevo<?php echo $rowReg_c->id?>" 
                                                                                                            <?php if($chk_nuevo == 1){ echo "checked";}?>>
                                                                                                    Nuevo
                                                                                                </label>
                                                                                                <label class="checkbox-inline separador">
                                                                                                    <input type="checkbox" 
                                                                                                            name="permiso_edit[<?php echo $rowReg_c->id?>]" value="1"
                                                                                                            title="Editar" 
                                                                                                            id="permiso_edit<?php echo $rowReg_c->id?>" 
                                                                                                            <?php if($chk_edit == 1){ echo "checked";}?>>
                                                                                                    Editar
                                                                                                </label>
                                                                                                <label class="checkbox-inline separador">
                                                                                                    <input type="checkbox" 
                                                                                                            name="permiso_elim[<?php echo $rowReg_c->id?>]" 
                                                                                                            value="1"
                                                                                                            title="Editar" id="permiso_elim<?php echo $rowReg_c->id?>" 
                                                                                                        <?php if($chk_elim == 1){ echo "checked";}?>>
                                                                                                    Eliminar
                                                                                                </label>
                                                                                                <label class="checkbox-inline separador">
                                                                                                    <input type="checkbox" 
                                                                                                            name="permiso_exportar[<?php echo $rowReg_c->id?>]" 
                                                                                                            value="1"
                                                                                                            title="Editar" id="permiso_exportar<?php echo $rowReg_c->id?>" 
                                                                                                        <?php if($chk_exportar == 1){ echo "checked";}?>>
                                                                                                    Exportar
                                                                                                </label>
                                                                                            </div>
                                                                                            <?php
                                                                                        }
                                                                                        ?>
                                                                                    </div>
                                                                                    <?php
                                                                                }
                                                                            }
                                                                            ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </article>
                                                        </fieldset>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <a href="<?php echo $param ?>index" class="btn btn-danger">
                                                <i class="fa fa-chevron-left"></i> Regresar
                                            </a>
                                            <?php
                                            if($type <> 3){
                                                ?>
                                                <button type="submit" class="btn btn-primary" id="btn_guardar">
                                                    <i class="icon-floppy-disk"></i> Guardar Cambios
                                                </button>
                                            <?php
                                            }
                                            ?>
                                        </div>
                                        
                                    </div>
                                </form>
                                <script>
                                    $(function() {

                                        <?php
                                        if ($type == 3) {
                                            ?>
                                            $('#frm_guardar :input:not(:button)').attr({
                                                'disabled': 'disabled'
                                            });
                                        <?php
                                    }

                                    ?>

                                    });
                                </script>
                            <?php
                        } else {
                            echo "<h1>No se puede acceder a la información solicitada</h1>";
                        }
                        ?>
                        </div>


                    </div>
                    <?php include($dir_fc . "inc/footer.php") ?>
                </div>
                <!-- /content area -->
            </div>
            <!-- /main content -->
        </div>
        <!-- /page content -->
    </div>
    <!-- /page container -->
</body>
<script>
    $(function() {
        $("#sall").change(function () {
            if ($(this).is(':checked')) {
                $("#permisos input[type=checkbox]").prop('checked', true);
            } else {
                $("#permisos input[type=checkbox]").prop('checked', false);
            }
        });
        $('.child-menu').hide();
        $('.ocultar').hide();
        $('#id_rol')[0].setAttribute('onchange', 'traePermisosPorRol()');

        <?php
        if($type == 3){
            ?>
        $('#frm_guardar :input:not(:button)').attr({
            'disabled': 'disabled'
        });
            <?php
        }
        ?>
    });
    
    function traePermisosPorRol() {
		var rol = $('#id_rol').val();
		if(rol > 0){
			$.post("<?php echo $raiz?>business/admin/sis_usuarios/ajax/trae_menu_rol.php",{'id_rol': rol},
				function(respuesta){
					$("#permisos_ajax").html(respuesta);
					$('.child-menu').hide();
					$('.ocultar').hide();
				});
		}

	}

</script>
<?php
include($dir_fc . "common/modals.php");
?>
<script>

    //inicia funcion buscar
    $(document).on("ready", edoinicial);

    function edoinicial() {
        <?php
        if ($type == 3) {
                ?>
                $('#frm_guardar :input:not(:button)').attr({
                    'disabled': 'disabled'
                });
            <?php
        }
        ?>
    }

    function guardar() {
        var form = document.getElementById('frm_guardar');
        formData = new FormData(form);
        var ruta = "<?php echo $raiz ?>business/<?php echo $real_sis ?>/ajax/nuevo.php";
        deshabilitarboton('btn_clave', 1);

        $.ajax({
            url: ruta,
            type: "POST",
            data: formData, //Enviando todo el formulario
            contentType: false,
            processData: false,
            success: function(respuesta) {
                try {
                    var json = JSON.parse(respuesta);
                    if (json.done == 0) {
                        $("#respuesta_ajax").html(
                            custom_alert(json.alert, '¡Ocurrió un inconveniente!', json.resp, 1, 1)
                        );
                        habilitaboton('btn_clave');
                    } else {
                        window.location.assign("<?php echo $param?>index");
                    }
                } catch (err) {
                    $("#respuesta_ajax").html(
                        custom_alert('danger', '¡Ocurrió un inconveniente! ...- ', respuesta, 1, 1)
                    );
                    habilitaboton('btn_clave');
                }
            },
            error: function(request, status, error) {
                $("#respuesta_ajax").html(
                    custom_alert('danger', '¡Ocurrió un inconveniente! ***- ', request.responseText, 1, 1)
                );
                habilitaboton('btn_clave');
            }
        });
    }

    function bajaModal(id) {
        $("#idBaja").val(id);
        $('#idConfimaModalBA').modal('show');
    }

    function eliminaModal(id) {
        $("#idEliminar").val(id);
        $('#idConfimaModalElimina').modal('show');
    }

    function baja() {
        var tipo = 0;
        var id = $("#idBaja").val();
        deshabilitarboton('aceptar_baja', 1);

        bajaAlta(id, tipo);
    }

    function alta(id) {
        var tipo = 1;
        bajaAlta(id, tipo);
    }

    function eliminaDtl() {
        var id = $("#id_eliminar_asignado").val();
        deshabilitarboton('aceptar_baja_e', 1);
        $.post("<?php echo $raiz ?>business/<?php echo $real_sis ?>/ajax/elimina_dtl.php", {
                'id': id
            },
            function(respuesta) {
                try {
                    var json = JSON.parse(respuesta);
                    if (json.done == 1) {
                        $('#id_modal_eliminar').modal('hide');
                        $("#respuesta_ajax").html(
                            custom_alert('success', '', json.resp, 1, 1)
                        );
                        $('#importe').val(json.cantidad);
                        cargaDatos(json.id_oficio, <?php echo $_SESSION[_type_] ?>);
                    } else {
                        $("#respuesta_error").html(
                            custom_alert('danger', '¡Ocurrió un inconveniente! ', json.resp, 1, 1)
                        );
                    }
                } catch (err) {
                    $("#respuesta_error").html(
                        custom_alert('danger', '¡Ocurrió un inconveniente! - ', err + " -- " + respuesta, 1, 1)
                    );
                }
            });
    }
/*
	* Para los perfiles cuando se seleccionan
	*/
	function mostrar(id){
		$("#child-menu_"+id).show(150,function() {
			$("#mostrar_"+id).hide();
			$("#ocultar_"+id).show();
		});
	}

	function ocultar(id){
		$("#child-menu_"+id).hide(150,function() {
			$("#mostrar_"+id).show();
			$("#ocultar_"+id).hide();
		});
	}

	function sel_des_child(id){
        var  primerac = $("#primera_carga").val();
        if(primerac > 1){
            if ($('#menu_'+id).is(':checked')) {
                $("#child-menu_"+id+" input[type=checkbox]").prop('checked', true);
            } else {
                $("#child-menu_"+id+" input[type=checkbox]").prop('checked', false);
            }
        }
        
	}

	function checamain(id){
        var  primerac = $("#primera_carga").val();

        if(primerac > 1){
            if ($('#menu_'+id).is(':checked')) {

            }else{
                $('#menu_'+id).prop('checked', true);
            }
        }
		
	}
</script>

</html>