<?php
session_start();
$dir_fc = "../../../../";
/*-----------------------------------      Estableciendo la Clases  --------------------------------------*/
include_once $dir_fc.'data/users.class.php';
/*--------------------------------------------------------------------------------------------------------*/
include_once $dir_fc.'connections/trop.php'; //Inclueye configuración de fecha y  hora de mexico
include_once $dir_fc.'connections/php_config.php'; //Inclueye configuración de fecha y  hora de mexico

$cAccion  = new cUsers();

if(!isset($_SESSION[_type_])){
    $_SESSION[_type_] = 0;
}
$id_rol         = 0;
$admin          = 0;
$usuario        = "";
$nombre         = "";
$apepat         = "";
$apemat         = "";
$correo         = "";
$sexo           = "";
$clave          = "";

$imp            = 0;
$nuevo          = 0;
$edi            = 0;
$elim           = 0;
$exportar       = 0;
$done           = 0;
$id_responsable = 0;

if($_SESSION[_type_] == 2){
    $cAccion->setIdUsuario($_SESSION[_editar_]);
    $clave = "- Con valor -";
}

extract($_REQUEST);

if($usuario == "" || $nombre == "" || $apepat == "" || $apemat == "" || $sexo == "" || $clave ==""){ //Verficando datos vacios
    $resp = "Debes de ingresar correctamente los datos";
}elseif(!filter_var($correo, FILTER_VALIDATE_EMAIL)){ //Verificando email
    $resp = "La dirección de correo electrónico es inválida";
}else{
    if(isset($_SESSION[admin]) && $_SESSION[admin] == 1){
        $user_admin = $admin;
    }else{
        $user_admin = 0;
    }
    //buscar si existe un usuario con el mismo nombre
    $cAccion->setUsuario($usuario);

    if($_SESSION[_type_] == 2){
        $userCoincidencia = $cAccion->foundUserConcidencia();
        if ($userCoincidencia == 1){
            //Si se encuentra coincidencia quiere decir que no cambio su nombre de usuario
            $userFound = 0;
        }else{
            //De lo contrario buscar si existe un usuario con el mismo nombre
            $userFound = $cAccion->foundUser();
        }
    }else{
        $userFound = $cAccion->foundUser();
    }

    if ($userFound>0) {
        $resp = "El nombre de usuario seleccionado ya existe en la base de datos, intentar con otro";
    } else {
        //Obtenemos fecha y hora
        if($id_responsable == ""){
            $id_responsable = 0;
        }
        $fecha  = date("Y-m-d");
        $cAccion->setFechaCaptura(date("Y-m-d H:i"));
        $cAccion->setIdUsuarioCaptura($_SESSION[id_usr]);
        $cAccion->setIdRol($id_rol);
        $cAccion->setClave($clave);
        $cAccion->setNombre($nombre);
        $cAccion->setApePa($apepat);
        $cAccion->setApeMa($apemat);
        $cAccion->setCorreo($correo);
        $cAccion->setSexo($sexo);
        $cAccion->setImpresion($imp);
        $cAccion->setEditar(1);
        $cAccion->setEliminar($elim);
        $cAccion->setNuevoUsuario(1);
        $cAccion->set_admin($user_admin);
        //$cAccion->setIdResponsable($id_responsable);

        if($_SESSION[_type_] == 2){
            $inserted = $cAccion->updateReg();
        }else{
            $inserted = $cAccion->insertReg($fecha);
        }

        if(is_numeric($inserted) AND $inserted>0){
            $del = $cAccion->deleteRegResponsable();
           // $cAccion->setIdUsuario($inserted);
            $insert_resp = $cAccion->insertResponsable();
                if(isset($menus)){
                    $cAccion->deleteRegUsMenu();
                    foreach ($menus as $id_arr => $valor_arr) {
                        $cAccion->set_imp(0);
                        $cAccion->set_nuevo(0);
                        $cAccion->set_edit(0);
                        $cAccion->set_elim(0);
                        $cAccion->set_exportar(0);

                        $imp      = 0;
                        $nuevo    = 0;
                        $edi      = 0;
                        $elim     = 0;
                        $exportar = 0;

                        $cAccion ->set_id_menu($valor_arr);
                        if(isset($grupo)){
                            //var_dump($grupo);
                            //die();
                            $grupo_rec = $grupo[$valor_arr];
                            if($grupo_rec <> 0){
                                if(isset($permiso_imp[$valor_arr])){
                                    $imp = $permiso_imp[$valor_arr];
                                }
                                if(isset($permiso_nuevo[$valor_arr])){
                                    $nuevo = $permiso_nuevo[$valor_arr];
                                }
                                if(isset($permiso_edit[$valor_arr])){
                                    $edi = $permiso_edit[$valor_arr];
                                }
                                if(isset($permiso_elim[$valor_arr])){
                                    $elim = $permiso_elim[$valor_arr];
                                }
                                if(isset($permiso_exportar[$valor_arr])){
                                    $exportar = $permiso_exportar[$valor_arr];
                                }
                                $cAccion->set_imp($imp);
                                $cAccion->set_nuevo($nuevo);
                                $cAccion->set_edit($edi);
                                $cAccion->set_elim($elim);
                                $cAccion->set_exportar($exportar);
                            }
                        }
                        $correcto = $cAccion->insertRegdtluser();
                        if(!is_numeric($correcto)){
                            die($correcto);
                        }
                    }
                }
            $done  = 1;
            $resp  = "Usuario agregado correctamente.";
        }else{
            $done  = 0;
            $resp  = "Ocurrió un incoveniente con la base de datos: -- ".$inserted;
        }
    }
}
echo json_encode(array("done" => $done, "resp" => $resp));
?>
