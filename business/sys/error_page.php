<?php
$dir_fc       = "../../";
/*--------------------------------------------------------------------------------------------------------*/
include_once $dir_fc.'connections/trop.php';
include_once $dir_fc.'connections/php_config.php';
$sys_id_men   = 0;
$dir          = dirname($_SERVER['PHP_SELF']);
$current_file = basename($_SERVER["PHP_SELF"]);

$checkMenu    = $server_name.$dir."/";   //con este se checa si es archivo activo o no
/*-----------------------------      LLamando a las clases   ---------------------------------------------*/
include_once $dir_fc.'data/inicial.class.php';
include_once $dir_fc.'common/function.class.php';
/*-----------------------------------   Verificando la sesión   ------------------------------------------*/
$cInicial = new cInicial();           //Operaciones generales  (Menu de incio, etc)
$cFn      = new cFunction();          //Operaciones comunes entre archivos como paginado o conversión de fechas etc.
/*----------------------------------- Verificando la sesión   ------------------------------------------*/
include_once '../sys/check_session.php'; //En este archivo se incia la sesión, por lo tanto no es necesario declarar el session_start()
$ruta_app = "";
?>

<!DOCTYPE html>
<html>
<head>
    <title><?php echo $titulo_paginas?></title>
    <meta content="" name="description"/>
    <meta content="" name="author"/>
    <?php include($dir_fc."inc/headercommon.php"); ?>
    <link href="<?php echo $raiz.$ruta_app ?>css/bootstrap-slider.min.css" rel="stylesheet" type="text/css"/>
    <script src="<?php echo $raiz.$ruta_app ?>js/app/status_plugins.js"></script>
    <script src="<?php echo $raiz.$ruta_app ?>js/app/baja_alta.js"></script>
    <script type="text/javascript">
        function camArTrPag(){
            var pag = $("#pager").val();
            window.location = "<?php echo $current_file?>?pag="+pag+"<?php echo $fPaginacion?>";
        }

        function borrarespuesta(){
            $("#respuesta_ajax").html("");
        }
    </script>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="menubar-hoverable header-fixed ">
<?php include ($dir_fc."inc/header.php")?>
<!-- BEGIN BASE -->
<div id="base">
    <div class="offcanvas"></div>
    <!-- BEGIN CONTENT-->
    <div id="content">
        <section>
            <div class="col-lg-12 col-xs-12">
                <div class="error-page">
                    <h2 class="text-info text-center"> Acceso</h2>
                    <div class="error-content text-center">
                        <h3><i class="fa fa-warning text-yellow"></i> No se puede acceder a esta página</h3>
                        <p>
                            La página solicitada no se encuentra disponible actualmente.</p>
                    </div><!-- /.error-content -->
                </div><!-- /.error-page -->
            </div><!-- ./col -->
        </section>
    </div><!--end #content-->
    <!-- END CONTENT -->
    <?php include($dir_fc."inc/menucommon.php") ?>
</div><!--end #base-->
<!-- END BASE -->
<?php include($dir_fc."inc/footercommon.php"); ?>
</body>
</html>