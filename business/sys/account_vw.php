<?php
/*--------------------------------------------------------------------------------------------------------*/
$dir_fc = "";
include_once $dir_fc.'connections/trop.php';
include_once $dir_fc.'connections/php_config.php';

$sys_id_men   = 0;
$dir          = dirname($_SERVER['PHP_SELF']);
$current_file = basename($_SERVER["PHP_SELF"]);
$checkMenu    = $server_name.$dir."/".$current_file;   //con este se checa si es archivo activo o no

/*------------------------  Incluyendo e Instanciando Clases Inicial  -------------------------------*/
include_once $dir_fc.'data/inicial.class.php';
include_once $dir_fc.'data/business.class.php';
include_once $dir_fc.'data/users.class.php';
$cInicial = new cInicial(); //Operaciones generales (Menu de incio, etc)
$cAccion = new cUsers(); //Operaciones generales (Menu de incio, etc)
/*---------------------------------------------------------------------------------------------------*/

include_once 'check_session.php'; //En este archivo se incia la sesión

$id = $_SESSION[id_usr];
if(!isset($id) || !is_numeric($id) || $id<= 0){
    $showinfo = false;
}else{
    $cAccion->setIdUsuario($id);
    $rsEditar    = $cAccion->getRegbyid();
    if($rsEditar->rowCount() > 0){
        $arrEdi      = $rsEditar->fetch(PDO::FETCH_OBJ);
        $id_usuario  = $arrEdi->id_usuario;
        $id_rol      = $arrEdi->id_rol;
        $usuario     = $arrEdi->usuario;
        $sexo        = $arrEdi->sexo;
        $nombre      = $arrEdi->nombre;
        $apepa       = $arrEdi->apepa;
        $apema       = $arrEdi->apema;
        $correo      = $arrEdi->correo;
        $imp         = $arrEdi->imp;
        $edit        = $arrEdi->edit;
        $elim        = $arrEdi->elim;
        $nuev        = $arrEdi->nuev;
        $activo      = $arrEdi->activo;
        $admin       = $arrEdi->admin;
    }else{
        $showinfo = false;
    }

}

?>
<!DOCTYPE html>
<html lang="es">
<head>
	<title>Mi perfil  | <?php echo c_page_title?></title>
	<meta content="Administrador para Auditorias / Página Inicial" name="description"/>
	<meta content="Subgerencia de Informática - By JF! ;)" name="author"/>
	<!-- /theme JS files -->
	<?php include($dir_fc."inc/headercommon.php"); ?>
	<script type="text/javascript" src="<?php echo $raiz ?>js/plugins/visualization/d3/d3.min.js"></script>
	<script type="text/javascript" src="<?php echo $raiz ?>js/plugins/visualization/d3/d3_tooltip.js"></script>
	<script type="text/javascript" src="<?php echo $raiz ?>js/plugins/forms/styling/switchery.min.js"></script>
	<script type="text/javascript" src="<?php echo $raiz ?>js/plugins/forms/styling/uniform.min.js"></script>
	<script type="text/javascript" src="<?php echo $raiz ?>js/plugins/forms/selects/bootstrap_multiselect.js"></script>
    <script type="text/javascript" src="<?php echo $raiz?>js/plugins/forms/selects/select2.min.js"></script>
    <script src="<?php echo $raiz?>js/app/status_plugins.js?ver=1.1"></script>
</head>
<body class="navbar-top">
	<?php include ($dir_fc."inc/header.php")?>
	<!-- Page container -->
	<div class="page-container">
		<!-- Page content -->
		<div class="page-content">
			<?php include ($dir_fc."inc/menucommon.php")?>
			<!-- Main content -->
			<div class="content-wrapper">
				<!-- Page header -->
				<div class="page-header page-header-default">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Mi perfil</h4>
						</div>

						<!-- <div class="heading-elements">
							<div class="heading-btn-group">
								<a href="#" class="btn btn-link btn-float text-size-small has-text"><i class="icon-bars-alt text-primary"></i><span>Statistics</span></a>
								<a href="#" class="btn btn-link btn-float text-size-small has-text"><i class="icon-calculator text-primary"></i> <span>Invoices</span></a>
								<a href="#" class="btn btn-link btn-float text-size-small has-text"><i class="icon-calendar5 text-primary"></i> <span>Schedule</span></a>
							</div>
						</div> -->
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="<?php echo $raiz?>"><i class="icon-home2 position-left"></i> Inicio</a></li>
							<li class="active"></li>
						</ul>

					<!--	<ul class="breadcrumb-elements">
							<li><a href="#"><i class="icon-comment-discussion position-left"></i> Support</a></li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<i class="icon-gear position-left"></i>
									Settings
									<span class="caret"></span>
								</a>

								<ul class="dropdown-menu dropdown-menu-right">
									<li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
									<li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
									<li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
									<li class="divider"></li>
									<li><a href="#"><i class="icon-gear"></i> All settings</a></li>
								</ul>
							</li>
						</ul> -->
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">

					<!-- Main charts -->
					<div class="row">
						<div class="col-lg-12">

							<!-- Traffic sources -->
                            <!-- Profile info -->
                            <div class="panel panel-flat">
                                <div class="panel-heading">
                                    <h6 class="panel-title">Información de Perfil</h6>
                                </div>
                                    <div class="col-md-12">
                                        <div id="respuesta_ajax">
                                        </div>
                                    </div>

                                <div class="panel-body">
                                    <form id="frm_guardar"
                                          method="post" action="javascript: guardar()" >
                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label for="nombre">Nombre(s)<span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control" name="nombre" id="nombre" autocomplete="off"
                                                           required value="<?php echo $nombre?>">
                                                </div>
                                                <div class="col-md-4">
                                                    <label for="apepat">Apellido Paterno <span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control" name="apepat" id="apepat" autocomplete="off"
                                                           value="<?php echo $apepa?>">
                                                </div>
                                                <div class="col-md-4">
                                                    <label for="apemat">Apellido Materno <span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control" name="apemat" id="apemat" autocomplete="off"
                                                           value="<?php echo $apema?>">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label for="usuario">Nombre de Usuario <span class="text-danger">*</span></label>
                                                    <input type="text" class="form-control" name="usuario" id="usuario" autocomplete="off"
                                                           required value="<?php echo $usuario?>">
                                                </div>
                                                <div class="col-md-4">
                                                    <label for="correo">Correo electrónico <span class="text-danger">*</span></label>
                                                    <input type="email" class="form-control" name="correo" id="correo"
                                                    required autocomplete="off" value="<?php echo $correo?>">
                                                </div>
                                                <div class="col-md-4">
                                                    <label for="sexo">Género: <span class="text-danger">*</span></label>
                                                    <select class="select" name="sexo" id="sexo" required>
                                                        <option value=""></option>
                                                        <option value="1" <?php if ($sexo == 1) { echo "selected";} ?>>Masculino</option>
                                                        <option value="2" <?php if ($sexo == 2) { echo "selected";} ?>>Femenino</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="text-right">
                                            <button type="button" class="btn btn-success" id="btn_cp" onclick="cpwModal()">
                                                <i class="fa fa-key"></i> Cambiar Contraseña
                                            </button>
                                            <button type="submit" class="btn btn-primary" id="btn_guardar">
                                                <i class="icon-floppy-disk"></i> Guardar Cambios
                                            </button>

                                        </div>
                                    </form>
                                </div>
                            </div>
							<!-- /traffic sources -->

						</div>

					</div>
					<!-- /main charts -->
                    <?php include ($dir_fc."inc/footer.php")?>

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

</body>
<!-- ModalCambiar Contraseña User-->
<div class="modal small fade" id="idModalcpw" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel">Cambiar Contraseña</h3>
            </div>
            <form role="form" action="javascript: cambiarpw()" id="idCPW" name="idCPW" class="form">
                <div class="modal-body">
                    <article id="container-info">
                        <div class="row">
                            <div class="col-md-12">
                                <div id="respuesta_cpw"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="clave">Contraseña Actual</label>
                                    <input type="password" class="form-control" id="clave" name="clave"  required>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="nuevaclave">Contraseña Nueva</label>
                                    <input type="password" class="form-control" id="nuevaclave" name="nuevaclave" required>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="confclave">Confirmar Contraseña</label>
                                    <input type="password" class="form-control" id="confclave" name="confclave" required>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
                <div class="modal-footer">
                    <div class="col-lg-offset-6 col-md-offset-6 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <button type="button" class="btn btn-block ink-reaction btn-danger" data-dismiss="modal" aria-hidden="true">
                            <i class="fa fa-times"></i> Cancelar
                        </button>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <button type="submit" id="btnGuardarcpw" class="btn btn-block ink-reaction btn-success">
                            <i class="fa fa-check"></i> Aceptar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Fin ModalCambiar Contraseña User-->
<script>
    //inicia funcion buscar
    $(document).on("ready", edoinicial);

    function edoinicial() {
        $('.select').select2({
            minimumResultsForSearch: Infinity
        });

        var modal_cpw = $('#idModalcpw');

        modal_cpw.on('shown.bs.modal', function (e) {
            //Colocar el cursor cuando se abra el modal
            $("#clave").focus();
        });

        modal_cpw.on('hidden.bs.modal', function (e) {
            document.getElementById("idCPW").reset();
            $("#respuesta_cpw").html("");
        });
    }

    function guardar(){
        var form = document.getElementById('frm_guardar');
        formData = new FormData(form);
        var ruta = "<?php echo $raiz?>business/sys/ajax/edit_acount.php";
        deshabilitarboton('btn_guardar', 1);

        $.ajax({
            url: ruta,
            type: "POST",
            data: formData,  //Enviando todo el formulario
            contentType: false,
            processData: false,
            success: function (respuesta) {
                try {
                    var json = JSON.parse(respuesta);
                    if (json.done == 0) {
                        $("#respuesta_ajax_modal").html(custom_alert(json.alert, '¡Ocurrió un inconveniente!', json.resp, 1, 1));
                        habilitaboton('btn_guardar');
                    } else {
                        $("#respuesta_ajax").html(custom_alert(json.alert, '',  json.resp, 1, 1));
                        habilitaboton('btn_guardar');
                        $('#modal_remote').modal('hide');
                    }
                }catch(err) {
                    $("#respuesta_ajax_modal").html(
                        custom_alert('danger', '¡Ocurrió un inconveniente! ...- ', respuesta, 1, 1)
                    );
                    habilitaboton('btn_guardar');
                }
            },
            error: function (request, status, error) {
                $("#respuesta_ajax_modal").html(
                    custom_alert('danger', '¡Ocurrió un inconveniente! ***- ', request.responseText, 1, 1)
                );
                habilitaboton('btn_guardar');
                //habilitaboton('consultar');
            }
        });
    }

    function cpwModal(){
        $('#idModalcpw').modal('show')
    }


    function cambiarpw() {
        var form = document.getElementById('idCPW');
        formData = new FormData(form);
        var ruta = "<?php echo $raiz?>business/sys/ajax/cwp.php";
        deshabilitarboton('btnGuardarcpw', 1);

        $.ajax({
            url: ruta,
            type: "POST",
            data: formData,  //Enviando todo el formulario
            contentType: false,
            processData: false,
            success: function (respuesta) {
                try {
                    var json = JSON.parse(respuesta);
                    if (json.done == 0) {
                        $("#respuesta_cpw").html(custom_alert(json.alert, '¡Ocurrió un inconveniente!', json.resp, 1, 1));
                        habilitaboton('btnGuardarcpw');
                    } else {
                        $("#respuesta_ajax").html(custom_alert(json.alert, '',  json.resp, 1, 1));
                        habilitaboton('btnGuardarcpw');
                        $('#idModalcpw').modal('hide');
                    }
                }catch(err) {
                    $("#respuesta_cpw").html(
                        custom_alert('danger', '¡Ocurrió un inconveniente! ...- ', respuesta, 1, 1)
                    );
                    habilitaboton('btnGuardarcpw');
                }
            },
            error: function (request, status, error) {
                $("#respuesta_cpw").html(
                    custom_alert('danger', '¡Ocurrió un inconveniente! ***- ', request.responseText, 1, 1)
                );
                habilitaboton('btnGuardarcpw');
                //habilitaboton('consultar');
            }
        });
    }

    function openLink(type, id) {
        /**
         * Abrir FormularioExtern
         *
         * @param {type}   int      Indica el tipo de formulario a abrir 1 nuevo, 2 editar, 3 ver
         * @param {id} 	   int      En caso de que sea editar id tendrá que valer el id correspondiente
         * by Fhohs!
         **/
        $.post("<?php echo $raiz?>common/delega.php",{'id': id, 'type':type },
            function(respuesta){
                if (isNaN(respuesta)) {
                    $("#respuesta_ajax").html(respuesta);
                    habilitaboton('aceptar_eliminar');
                    habilitaboton('aceptar_baja');
                } else {
                    window.location = "<?php echo $raiz?>?fmc=auditoria/lista&td=seguimiento";
                }
            });
    }
</script>
</html>
