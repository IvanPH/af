<?php
/*--------------------------------------------------------------------------------------------------------*/
$dir_fc       = "../../";
include_once $dir_fc.'connections/trop.php';
include_once $dir_fc.'connections/php_config.php';

$sys_id_men   = 0;
$dir          = dirname($_SERVER['PHP_SELF']);
$current_file = basename($_SERVER["PHP_SELF"]);

$checkMenu    = $server_name.$dir."/";   //con este se checa si es archivo activo o no
$ruta_app     = "";
/*-----------------------------      LLamando a las clases   ---------------------------------------------*/
include_once $dir_fc.'data/inicial.class.php';
include_once $dir_fc.'data/rol.class.php';
include_once $dir_fc.'data/users.class.php';

$cInicial   = new cInicial();       //Operaciones generales
$cRoles     = new Rol();       //Operaciones con los roles
$cEditar    = new cUsers();       //Operaciones generales de edicion con la clase correspondiente
/*-----------------------------------   Verificando la sesión   ------------------------------------------*/
include_once '../sys/check_session.php'; //En este archivo se incia la sesión, por lo tanto no es necesario declarar el session_start()

$showinfo   = true;
$id_usuario = "";
$id_rol     = "";
$usuario    = "";
$sexo       = "";
$nombre     = "";
$apepa      = "";
$apema      = "";
$correo     = "";
$imp        = "";
$edit       = "";
$elim       = "";
$nuev       = "";
$activo     = "";
$admin      = "";

extract($_REQUEST);

if(!isset($pag)){ $pag=1;}
if(!isset($busqueda) || $busqueda == ""){$busqueda = "";}
if(!isset($columna) || $columna == ""){$columna = "";}

$return_paginacion = "";

/*---------------    Formateando informacion de mensajes a mostrar al usuario  ---------------------------*/
if (isset($attempt)) {
    if ($attempt == "done" ){
        $msgShow = "<div id=\"resp\" class=\"alert alert-success alert-dismissable\">
                        <i class=\"fa fa-check\"></i>
                        <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>
                        <strong>¡Correcto! </strong> Tus datos de usuario se han actualizado satisfactoriamente.</i>
                    </div>";
    }
    else {
        $msgShow = "";
    }
}
else{
    $msgShow = "";
}
//Obteniendo los roles
$rsRol           = $cRoles->getReg();
$totalRows_rsRol = $rsRol->rowCount();

$id = $_SESSION[id_usr];
if(!isset($id) || !is_numeric($id) || $id<= 0){
    $showinfo = false;
}else{
    $cEditar->setIdUsuario($id);
    $rsEditar    = $cEditar->getRegbyid();
    if($rsEditar->rowCount() > 0){
        $arrEdi      = $rsEditar->fetch(PDO::FETCH_OBJ);
        $id_usuario  = $arrEdi->id_usuario;
        $id_rol      = $arrEdi->id_rol;
        $usuario     = $arrEdi->usuario;
        $sexo        = $arrEdi->sexo;
        $nombre      = $arrEdi->nombre;
        $apepa       = $arrEdi->apepa;
        $apema       = $arrEdi->apema;
        $correo      = $arrEdi->correo;
        $imp         = $arrEdi->imp;
        $edit        = $arrEdi->edit;
        $elim        = $arrEdi->elim;
        $nuev        = $arrEdi->nuev;
        $activo      = $arrEdi->activo;
        $admin       = $arrEdi->admin;
    }else{
        $showinfo = false;
    }

}


?>
<!DOCTYPE html>
<html>
<head>
    <title>Mi cuenta | <?php echo $titulo_paginas?></title>
    <meta content="" name="description"/>
    <meta content="" name="author"/>
    <?php include($dir_fc."inc/headercommon.php"); ?>
    <script src="<?php echo $dir_fc?>js/app/status_plugins.js"></script>
    <script src="<?php echo $dir_fc?>js/app/guardar.js"></script>
    <script type="text/javascript">
        function borrarespuesta(){
            $("#respuesta_ajax").html("");
        }
    </script>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="menubar-hoverable header-fixed ">
<?php include ($dir_fc."inc/header.php")?>
<!-- BEGIN BASE-->
<div id="base">
    <!-- BEGIN OFFCANVAS LEFT -->
    <div class="offcanvas"></div><!--end .offcanvas-->
    <!-- END OFFCANVAS LEFT -->
    <!-- BEGIN CONTENT-->
    <div id="content">
        <section>
            <div class="section-body contain-lg">
                <!-- BEGIN INTRO -->
                <div class="row">
                    <div class="col-lg-8">

                    </div><!--end .col -->
                    <div class="col-lg-4">
                        <ol class="breadcrumb pull-right">
                            <li><a href="<?php echo $raiz?>business/">Inicio</a></li>
                            <li class="active">Mis datos</li>
                        </ol>
                    </div>
                </div><!--end .row -->
                <!-- END INTRO -->
                <?php
                //Verifica si el usuario puede insertar registros
                if($showinfo == true) {
                    ?>
                    <div class="card">
                        <?php
                        if($activo == 1){
                            $class_activo = "fa fa-2x fa-check-circle text-default";
                            $title_activo = "Usuario Activo";
                            $card_style   = "style-success";
                        }else{
                            $class_activo = "fa fa-2x fa-times-circle text-default";
                            $title_activo = "Usuario Inactivo";
                            $card_style   = "style-gray-light";
                        }
                        ?>
                        <!-- BEGIN SEARCH HEADER -->
                        <div class="card-head style-success">
                            <div class="tools pull-left">
                                <a class="btn ink-reaction btn-floating-action btn-primary"
                                   href='<?php echo $raiz?>business/' title="Regresar al inicio">
                                    <i class="md md-arrow-back"></i>
                                </a>
                                <!-- Agregar u opciones para regresar o algun movimiento extra  -->
                            </div>
                            <strong>Mi cuenta</strong>
                            <div class="tools">
                                <span class="<?php echo $class_activo?>" title="<?php echo $title_activo?>"></span>
                            </div>
                        </div><!--end .card-head -->
                        <div class="card-body">
                            <form id="frm_guardar" class="form" role="form" method="post"
                                  action="javascript: guardar('btn_guardar','frm_guardar','edit_acount', '','respuesta_ajax','account')" >
                                <!-- Small boxes (Stat box) -->
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div id="respuesta_ajax"><?php echo $msgShow?></div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12 col-md-12 col-sm-12">
                                        <input type="hidden" name="id_usuario" id="id_usuario" value="<?php echo $id?>"/>
                                        <fieldset><legend>Datos de Usuario</legend>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="form-group floating-label">
                                                        <input type="text" class="form-control" name="nombre" id="nombre" autocomplete="off"
                                                               required value="<?php echo $nombre?>">
                                                        <label for="nombre">Nombre(s)<span class="text-danger">*</span></label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="form-group floating-label">
                                                        <input type="text" class="form-control" name="apepat" id="apepat" autocomplete="off"
                                                               value="<?php echo $apepa?>">
                                                        <label for="apepat">Apellido Paterno <span class="text-danger">*</span></label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="form-group floating-label">
                                                        <input type="text" class="form-control" name="apemat" id="apemat" autocomplete="off"
                                                               value="<?php echo $apema?>">
                                                        <label for="apemat">Apellido Materno <span class="text-danger">*</span></label>
                                                    </div>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div class="form-group floating-label">
                                                        <input type="text" class="form-control" name="usuario" id="usuario" autocomplete="off"
                                                               required value="<?php echo $usuario?>">
                                                        <label for="usuario">Nombre de Usuario <span class="text-danger">*</span></label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="form-group floating-label">
                                                        <input type="email" class="form-control" name="correo" id="correo"
                                                               required autocomplete="off" value="<?php echo $correo?>">
                                                        <label for="correo">Correo electrónico <span class="text-danger">*</span></label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="form-group floating-label">
                                                        <select name="sexo" id="sexo" class="form-control" required>
                                                            <option value=""></option>
                                                            <option value="1" <?php if ($sexo == 1) { echo "selected";} ?>>Masculino</option>
                                                            <option value="2" <?php if ($sexo == 2) { echo "selected";} ?>>Femenino</option>
                                                        </select>
                                                        <label for="sexo">Sexo: <span class="text-danger">*</span></label>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <fieldset>
                                            <div class="row">
                                                <div class="col-sm-12 col-md-4 col-xs-12 col-lg-4">
                                                    <button type="button" class="btn ink-reaction btn-block btn-default" id="btn_cp" onclick="cpwModal()">
                                                        <i class="fa fa-key"></i> Cambiar Contraseña
                                                    </button>
                                                </div>
                                                <div class="col-sm-12 col-md-4 col-xs-12 col-lg-4">
                                                    <button type="submit" class="btn ink-reaction btn-block btn-primary" id="btn_guardar">
                                                        <span class="glyphicon glyphicon-floppy-disk"></span> Guardar Cambios
                                                    </button>
                                                </div>
                                            </div>
                                        </fieldset>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <?php
                }else{
                    //Incliur el código de permisos denegados
                    include("../sys/permissions_d.php");
                }?>
            </div>
        </section>
    </div><!--end #content-->
    <!-- END CONTENT -->
    <?php include($dir_fc."inc/menucommon.php") ?>
</div><!--end #base-->
<!-- END BASE -->
<?php include_once $dir_fc.'inc/footercommon.php'; ?>
<script>
    $(document).on("ready", edoinicial);
    //Funcion que se ejecutará al cargar el documento
    function edoinicial() {
        $("#sall").change(function () {
            if ($(this).is(':checked')) {
                $("#permisos input[type=checkbox]").prop('checked', true);
            } else {
                $("#permisos input[type=checkbox]").prop('checked', false);
            }
        });

        $('#nombre').focus();
        $(":input").keydown(function () {
            borrarespuesta();
        });

        var modal_cpw = $('#idModalcpw');

        modal_cpw.on('shown.bs.modal', function (e) {
            //Colocar el cursor cuando se abra el modal
            $("#clave").focus();
        });

        modal_cpw.on('hidden.bs.modal', function (e) {
            document.getElementById("idCPW").reset();
            $("#respuesta_cpw").html("");
        })
    }

    function cpwModal(){
        $('#idModalcpw').modal('show')
    }

    function cambiarpw() {
        deshabilitarboton('btnGuardarcpw',1);
        var delista    = 0;
        var clave 	   = $("#clave").val();
        var nuevaclave = $("#nuevaclave").val();
        var confclave  = $("#confclave").val();
        $.post("ajax/cwp.php",{'clave': clave, 'nuevaclave': nuevaclave, 'confclave': confclave, 'delista':delista},
            function(respuesta){
                if (isNaN(respuesta)) {
                    $("#respuesta_cpw").html(custom_alert('danger', '¡Ocurrió un inconveniente!', respuesta, 1, 1));
                    habilitaboton('btnGuardarcpw');
                } else {
                    var text = "Constraseña actualizada con éxito, para comprobar los cambios vuelve a iniciar sesión.";
                    $("#respuesta_ajax").html(custom_alert('success', '¡Correcto!', text, 1, 1));
                    habilitaboton('btnGuardarcpw');
                    document.getElementById("idCPW").reset();
                    $('#idModalcpw').modal('hide');
                }
            });
    }
</script>
<!-- ModalCambiar Contraseña User-->
<div class="modal small fade" id="idModalcpw" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="myModalLabel">Cambiar Contraseña</h3>
            </div>
            <form role="form" action="javascript: cambiarpw()" id="idCPW" class="form">
                <div class="modal-body">
                    <article id="container-info">
                        <div class="row">
                            <div class="col-md-12">
                                <div id="respuesta_cpw"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group floating-label">
                                    <input type="password" class="form-control" id="clave" name="clave"  required>
                                    <label for="clave">Contraseña Actual</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group floating-label">
                                    <input type="password" class="form-control" id="nuevaclave" name="nuevaclave" required>
                                    <label for="nuevaclave">Contraseña Nueva</label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group floating-label">
                                    <input type="password" class="form-control" id="confclave" name="confclave" required>
                                    <label for="confclave">Confirmar Contraseña</label>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
                <div class="modal-footer">
                    <div class="col-lg-offset-6 col-md-offset-6 col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <button type="button" class="btn btn-block ink-reaction btn-danger" data-dismiss="modal" aria-hidden="true">
                            <i class="fa fa-times"></i> Cancelar
                        </button>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <button type="submit" id="btnGuardarcpw" class="btn btn-block ink-reaction btn-success">
                            <i class="fa fa-check"></i> Aceptar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Fin ModalCambiar Contraseña User-->
</body>
</html>
