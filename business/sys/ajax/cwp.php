<?php
$dir_fc = "../../../";
session_start();
/*-----------------------------------      Estableciendo la Clases  --------------------------------------*/
include_once $dir_fc.'data/users.class.php';
include_once $dir_fc.'connections/trop.php'; //Inclueye configuración de fecha y  hora de mexico
include_once $dir_fc.'connections/php_config.php'; //Inclueye configuración de constantes globales para el proyecto men

$cAccion    = new cUsers();

$delista    = 0;
$id_user_pw = 0;
$nuevaclave = "";
$confclave  = "";
$done       = 0;
$resp       = "";
$alert      = "warning";


extract($_REQUEST);

$cAccion->setIdUsuario($_SESSION[id_usr]);
$cAccion->setClave(md5($clave));
$rows   = $cAccion->getRegbyPW();

if($rows == 1){
    if($nuevaclave == $confclave){
        $cAccion->setNvaclave(md5($nuevaclave));
        $update = $cAccion->updateRegPW();
        if(is_numeric($update)){
            $done  = 1;
            $resp  = "Cambios Realizados correctamente.";
            $alert = "success";
        }else{
            $resp  = "Ocurrió un incoveniente. ".$update;
            $alert = "danger";
        }
    }else{
        $resp = "Confirmación de Contraseña no coincide";
    }
}else{
    $resp = "Contraseña actual incorrecta";
}

echo json_encode(array("done" => $done, "resp" => $resp, "alert" =>$alert));
?>
