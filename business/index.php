<?php
/*--------------------------------------------------------------------------------------------------------*/
$dir_fc = "../";
include_once $dir_fc.'connections/trop.php';
include_once $dir_fc.'connections/php_config.php';

$sys_id_men   = 0;
$dir          = dirname($_SERVER['PHP_SELF']);
$current_file = basename($_SERVER["PHP_SELF"]);
$checkMenu    = $server_name.$dir."/".$current_file;   //con este se checa si es archivo activo o no

/*------------------------  Incluyendo e Instanciando Clases Inicial  -------------------------------*/
include_once $dir_fc.'data/inicial.class.php';
include_once $dir_fc.'data/business.class.php';
$cInicial = new cInicial(); //Operaciones generales (Menu de incio, etc)
$cBusines = new cBusiness(); //Operaciones generales (Menu de incio, etc)
/*---------------------------------------------------------------------------------------------------*/

include_once 'sys/check_session.php'; //En este archivo se incia la sesión
$cBusines->setAdmin($_SESSION[admin]);
$cBusines->setIdResponsable($_SESSION[id_area]);
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <title><?php echo c_page_title?></title>
    <meta content="HEX / Página Inicial" name="description"/>
    <!-- /theme JS files -->
    <?php include($dir_fc."inc/headercommon.php"); ?>
    <script type="text/javascript" src="<?php echo $raiz ?>js/plugins/visualization/d3/d3.min.js"></script>
    <script type="text/javascript" src="<?php echo $raiz ?>js/plugins/visualization/d3/d3_tooltip.js"></script>
    <script type="text/javascript" src="<?php echo $raiz ?>js/plugins/forms/styling/switchery.min.js"></script>
    <script type="text/javascript" src="<?php echo $raiz ?>js/plugins/forms/styling/uniform.min.js"></script>
    <script type="text/javascript" src="<?php echo $raiz ?>js/plugins/forms/selects/bootstrap_multiselect.js"></script>

    <script type="text/javascript" src="<?php echo $raiz?>js/plugins/ui/moment/moment.min.js"></script>
    <script type="text/javascript" src="<?php echo $raiz?>js/plugins/ui/fullcalendar/fullcalendar.min.js"></script>
    <script type="text/javascript" src="<?php echo $raiz?>js/plugins/ui/fullcalendar/lang/es.js"></script>
</head>
<body class="navbar-top">
<?php include ($dir_fc."inc/header.php")?>
<!-- Page container -->
<div class="page-container">
    <!-- Page content -->
    <div class="page-content">
        <?php include ($dir_fc."inc/menucommon.php")?>
        <!-- Main content -->
        <div class="content-wrapper">
            <!-- Page header -->
<!--           <div class="page-header page-header-default">
                <div class="page-header-content">
                    <div class="page-title">
                        <h4> Inicio</h4>
                    </div>
                </div>
            </div> -->
            <!-- /page header -->


            <!-- Content area -->
            <div class="content">

                <!-- Main charts -->
                <div class="row">
                    <div class="col-lg-12">

                        <!-- Traffic sources -->
                        <div class="panel panel-flat">
                            <div class="panel-heading">
                            </div>
                            <div class="container-fluid">
                                <div  class="container">
                                <h1>Integrantes de equipo</h1>
                                    <ul class="list-group">
                                    <li class="list-group-item">Bonilla Jacome Osvaldo</li>
                                    <li class="list-group-item">Martínez Sánchez Fernando</li>
                                    <li class="list-group-item">Garay Gambia Christian Gybran</li>
                                    <li class="list-group-item">Juan Carlos Rojas Castrejón</li>
                                    <li class="list-group-item">Sergio Avendaño Hernández</li>
                                    <li class="list-group-item">Eliot Ivan Palacios Hernandez </li>
                                    </ul>
                                </div>
                                <div class="cards">
                                <h1 class="panel-title">Menú rápido</h1>
                                    <div class="card">
                                    <div class="card__image-holder">
                                        <img class="card__image" src="https://e.rpp-noticias.io/normal/2017/03/09/193019_361199.jpg" style="height:225px" alt="empleados" />
                                    </div>
                                    <div class="card-title">
                                    <a href="<?php echo $raiz?>?fmc=catalogos/beneficiario&td=index" class="toggle-info btn" style="padding-top: 7px;">
                                        <i class="fa fa-building"></i>
                                        </a>
                                        <h2>
                                            Empresa
                                            <small>Catálogo de empleados</small>
                                        </h2>
                                    </div>
                                    </div>

                                    <div class="card">
                                    <div class="card__image-holder">
                                        <img class="card__image" src="https://kapali.com.mx/wp-content/uploads/2017/07/offices-1181385_1280-1024x576.jpg" style="height:225px" alt="activosFijos" />
                                    </div>
                                    <div class="card-title">
                                    <a href="<?php echo $raiz?>?fmc=catalogos/activo&td=index"  class="toggle-info btn" style="padding-top: 7px;">
                                        <i class="fa fa-desktop"></i>
                                        </a>
                                        <h2>
                                            Area de Informática
                                            <small>Sistema de Activos fijos</small>
                                        </h2>
                                    </div>
                                    </div>

                                    <div class="card">
                                    <div class="card__image-holder">
                                        <img class="card__image" src="https://3.bp.blogspot.com/-xa_quxk3g8Q/WvOCfem-TJI/AAAAAAAACF0/c0lKMFvXRKYvrEVuVtEaef_tiNeR0v86gCLcBGAs/s750/Teor%25C3%25ADa_General_de_la_Administraci%25C3%25B3n.jpg" style="height:225px" alt="Administración" />
                                    </div>
                                    <div class="card-title">
                                    <a href="<?php echo $raiz?>?fmc=admin/sis_usuarios&td=index"  class="toggle-info btn" style="padding-top: 7px;">
                                        <i class="fa fa-cogs"></i>
                                        </a>
                                        <h2>
                                            Administración
                                            <small>Configuración del sistema</small>
                                        </h2>
                                    </div>
                                    </div>

                                    </div>

                            </div>

                            <div class="position-relative" id="traffic-sources"></div>
                        </div>
                        <!-- /traffic sources -->

                    </div>

                </div>
                <!-- /main charts -->

                <?php include ($dir_fc."inc/footer.php")?>

            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</div>
<!-- /page container -->

</body>
<script>
    //inicia funcion buscar
    $(document).on("ready", edoinicial);

    function edoinicial(){

    }

    function btnDoFilterBySt(id){
        /**
         * Establecer el Id dentro del arreglo de estatus para que el user haga el filtro únicamente de estatus seleccionado
         *
         * @param {id}   int     id del estatus del cual se quiere hacer filtro.
         * By FhohS!, aka. JF!, aka. Jc!
         **/
        $.post("<?php echo $raiz?>business/ajax/buscar_btn.php",{'id': id },
            function(respuesta){
                if (isNaN(respuesta)) {
                    $("#respuesta_ajax").html(respuesta);
                    habilitaboton('aceptar_eliminar');
                    habilitaboton('aceptar_baja');
                } else {
                    window.location = "<?php echo $raiz?>?fmc=agenda/lista&td=index";
                }
            });

    }
    function openLink(type, id) {
        /**
         * Abrir FormularioExtern
         *
         * @param {type}   int      Indica el tipo de formulario a abrir 1 nuevo, 2 editar, 3 ver
         * @param {id} 	   int      En caso de que sea editar id tendrá que valer el id correspondiente
         * by Fhohs!
         **/
        $.post("<?php echo $raiz?>common/delega.php",{'id': id, 'type':type },
            function(respuesta){
                if (isNaN(respuesta)) {
                    $("#respuesta_ajax").html(respuesta);
                    habilitaboton('aceptar_eliminar');
                    habilitaboton('aceptar_baja');
                } else {
                    window.location = "<?php echo $raiz?>?fmc=agenda/lista&td=seguimiento";
                }
            });
    }
</script>
</html>
