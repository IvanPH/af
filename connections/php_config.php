<?php
$proydes = "af";                                 //En caso de que sea producción cambiar este valor.

define('c_page_title','HEX');            //Titulo del proyecto
define('c_num_reg',10);                                 //Número general de registros en páginas a ocupar

//Sesiones Generales de Administración  ---
define('id_usr','cve_admin_'.$proydes);                 //Sesión principal del Usuario.
define('id_rol','tram_id_rol_'.$proydes);               //Para el id del rol
define('admin','tram_admin_'.$proydes);                 //Para checar si es usuaruo Admin o standard
define('rol','tram_rol_'.$proydes);                     //Para el nombre o desc. rol
define('imp','tram_imp_'.$proydes);                     //Si tiene permisos de imprimir
define('edit','tram_edit_'.$proydes);                   //Si tiene permisos de editar
define('elim','tram_elim_'.$proydes);                   //Si tiene permisos de eliminar
define('nuev','tram_nuev_'.$proydes);                   //Si tiene permisos para agregar un nuevo registro
define('export','tram_exp_'.$proydes);                  //Si tiene permisos para exportar
define('s_sexo','tram_sexo_'.$proydes);                 //Género
define('s_img','tram_img_'.$proydes);                   //Imagen del user
define('s_nombre','tram_nombre_'.$proydes);             //Nombre del user
define('s_ncompleto','tram_ncompleto_'.$proydes);       //Nombre completo del user
define('s_f_i','tram_fecha_ingreso_'.$proydes);         //Fecha de ingreso
define('looked','tram_lock_session_'.$proydes);         //Sesión bloqueada

define('_editar_', 's_peditar_tram_'.$proydes);

define('_editar_dtl_', 's_peditar_dtl_tram_'.$proydes);

define('_editar_master_', 's_peditar_master_tram_'.$proydes);
define('_is_view_', 's_is_v_tram_'.$proydes);
define('_type_', 's_is_type_'.$proydes);
define('_id_estatus_', 'id_estatus_'.$proydes);
define('_menu_', 'id_menu_navega_'.$proydes);

define('_P_ANIO_', 'anio_'.$proydes);
define('_P_MES_', 'mes_'.$proydes);

//Sesiones extras ---
define('s_puesto', 's_puestp_tram_'.$proydes);
define('s_area', 's_area_tram_'.$proydes);
define('s_estatusvar', 's_estatus_val_'.$proydes);            //Para las barras de estado o bien filtros de estado
define('s_estatusvar_tram', 's_estatus_val_tram_'.$proydes);

//Sesiones para filtros en búsquedas
define('id_area', 'id_area'.$proydes);
define('fil_id', 'fil_id_'.$proydes);
define('fil_area', 'fil_area_'.$proydes);
define('fil_estatus', 'fil_estatus_'.$proydes);
define('fil_estado', 'fil_estado_'.$proydes);
define('fil_descripcion', 'fil_descripcion_'.$proydes);
define('fil_total', 'fil_total_'.$proydes);
define('fil_clave', 'fil_clave_'.$proydes);
define('partida_fil', 'partida_fil_'.$proydes);
define('concepto_fil', 'concepto_fil_'.$proydes);
define('capitulo_fil', 'capitulo_fil_'.$proydes);
define('mes_fil', 'mes_fil_'.$proydes);
define('anio_fil', 'anio_fil_'.$proydes);

//Para Módulo auditoria
define('fil_uaa', 'fil_uaa_'.$proydes);
define('fil_id_tipo_junta', 'fil_no_auditoria_'.$proydes);
define('fil_acuerdo', 'fil_area_resp_'.$proydes);
define('fil_asuntos', 'fil_area_auditora_'.$proydes);
define('fil_estatus_arr_reunion', 'fil_estatus_arr_reunion_'.$proydes);
define('fil_fecha_ini', 'fil_fecha_ini_'.$proydes);
define('fil_fecha_fin', 'fil_fecha_fin_'.$proydes);

define('fil_beneficiario', 'fil_beneficiario'.$proydes);
define('fil_cetro_trabajo', 'fil_cetro_trabajo'.$proydes);
define('fil_tipo_mov', 'fil_tipo_mov'.$proydes);
define('fil_tipo_mov_ofi', 'fil_tipo_mov_ofi'.$proydes);
define('fil_no_contrato', 'fil_no_contrato'.$proydes);
define('fil_no_fua', 'fil_no_fua'.$proydes);
define('fil_tipo', 'fil_tipo_'.$proydes);
define('fil_concepto', 'fil_concepto_'.$proydes);
define('fil_usuario_captura', 'fil_usuario_captura_'.$proydes);
define('fil_fechaini', 'fil_fechaini_'.$proydes);
define('fil_fechafin', 'fil_fechafin_'.$proydes);

define('fil_act_inst', 'fil_act_inst_'.$proydes);
define('fil_prog_presu', 'fil_prog_presu_'.$proydes);
define('fil_ff', 'fil_ff_'.$proydes);
//
define('archivo_alt', 'archivo_alt_d_'.$proydes);
define('id_responsable', 'id_responsable_'.$proydes);
define('id_oficio', 'id_oficio_d_'.$proydes);

define('year_desc', 2); //Numero de años a restar para que muestre en los formularios
define('year_combo', 10); //Numero de años a mostrar en los combos


?>
