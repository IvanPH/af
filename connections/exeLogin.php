<?php
$dir_fc = "../";
include_once $dir_fc.'data/users.class.php';						//Operaciones con los usuarios
include_once $dir_fc.'connections/php_config.php';
include_once $dir_fc.'common/function.class.php';

$cUsers	 =	new cUsers();
$cFn	 =	new cFunction();
$txtPass = "";

extract($_REQUEST);

if (!isset($txtUser) || empty($txtUser) || empty($txtPass)) 		//Error 2 - campos vacios
{
	echo "Los campos están vacios";
}
else
{
	$cUsers->setUsuario($cFn->get_sub_string($txtUser,40));
	$cUsers->setClave(md5($txtPass));

	$selectUser = $cUsers->getUser();
	$num_rows = 0;
	$carpeta_go="";
	$tipo = gettype($selectUser);
	if($tipo == "string"){
		echo json_encode(array("res" => 0, "goto" => "Ocurrió un incoveniente con los datos proporcionados."));
	}else{
		while ($datos=$selectUser->fetch(PDO::FETCH_ASSOC)) {
			session_start();

			$_SESSION[s_ncompleto] = $datos['nombrecompleto'];
			$_SESSION[s_nombre]    = $datos['nombre'];
			$_SESSION[s_sexo]      = $datos['sexo'];
			$_SESSION[s_img]       = $datos['img'];
			$_SESSION[s_f_i]       = $datos['fecha_ingreso'];
			$_SESSION[id_usr]      = $datos['id_usuario']; //contiene la clave de usuario
			$_SESSION[id_rol]      = $datos['id_rol'];
			$_SESSION[id_area]     = $datos['id_area'];
			$_SESSION[rol]         = $datos['rol'];
			$_SESSION[admin]       = $datos['admin'];      //contiene si si es admin o no
			$_SESSION[s_estatusvar]= 0;
			$carpeta_go		       = $datos["carpeta"];
			$num_rows = 1;
			$cUsers->setIdUsuario($_SESSION[id_usr]);

			$anio  = $cUsers->getParametro(2);
			$mes   = $cUsers->getParametro(3);


            $_SESSION[capitulo_fil] = "";
			$_SESSION[concepto_fil] = "";
			$_SESSION[fil_clave] = "";
			$_SESSION[partida_fil] = "";
			$_SESSION[mes_fil] = "";
			$_SESSION[anio_fil] = "";

			$_SESSION[_P_ANIO_] = $anio;
			$_SESSION[_P_MES_]  = $mes;
        }

		if ($num_rows > 0)
		{
            /*
             * En este caso solo será para el rol de participante (Rol 3):
             * (Llevarlo directamente al registro más reciente ativo).
             * */
            $idMax = 0;
            if($_SESSION[id_rol] == 3){
                $idMax = $cUsers->getLastIdByRol();
            }

			if($carpeta_go == "" || is_null($carpeta_go)){
				$carpeta_go = "business/";
			}
			echo json_encode(array("res" => 1,"rolex"=> $_SESSION[id_rol], "id"=> $idMax, "goto" => $carpeta_go));
		}
		else 													// Error 1 - información incorrecta
		{
			echo json_encode(array("res" => 0,
									"rolex"=> "none",
									"id"=> "none",
									"goto" => "Los datos ingresados con incorrectos."));
		}
	}

}
?>
