<?php
session_start();
$dir = "";
include_once 'connections/php_config.php';
include_once 'connections/trop.php';
include_once 'common/function.class.php';
if (!isset($_SESSION[id_usr]))
{
	$cFn      = new cFunction();
extract($_REQUEST);
//Recibiendo intentos de acceso al sistema
if (isset($attempt)) {
	switch ($attempt) {
		case "error_1":
			$msjError= $cFn->custom_alert("danger", "","Los datos insertados son incorrectos",1,1);
			break;
		case "error_2":
			$msjError= $cFn->custom_alert("danger", "", "Los campos están vacios",1,1);
			break;
		case "login":
			$msjError= $cFn->custom_alert("danger", "", "Debes de acceder para consultar la página",1,1);
			break;
		default:
			$msjError= "";
			break;
	}
}else{
	$msjError= "";
}
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php echo $titulo_paginas?> | Ingresar</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="<?php echo $dir?>css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="<?php echo $dir?>css/icons/fontawesome/styles.min.css" rel="stylesheet" type="text/css">
	<link href="<?php echo $dir?>css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="<?php echo $dir?>css/core.css" rel="stylesheet" type="text/css">
	<link href="<?php echo $dir?>css/components.css" rel="stylesheet" type="text/css">
	<link href="<?php echo $dir?>css/colors.css" rel="stylesheet" type="text/css">
	<link href="<?php echo $raiz ?>css/style.css?v=1.03" rel="stylesheet" type="text/css">

	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="<?php echo $dir?>js/plugins/loaders/pace.min.js"></script>
	<script type="text/javascript" src="<?php echo $dir?>js/core/libraries/jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo $dir?>js/core/libraries/bootstrap.min.js"></script>
	<!-- /core JS files -->


	<!-- Theme JS files -->
	<script type="text/javascript" src="<?php echo $dir?>js/core/app.js"></script>

	<script type="text/javascript" src="<?php echo $dir?>js/plugins/ui/ripple.min.js"></script>
	<script src="<?php echo $dir?>js/app/status_plugins.js?ver=1.1"></script>
	<script src="<?php echo $dir?>js/app/login.js?ver=1.3"></script>
    <?php
    $param        = "?fmc=junta/lista&td=";
    ?>
    <script>
        function openLink3(type, id, controller) {
            /**2
             * Abrir FormularioExtern
             *
             * @param {type}   int      Indica el tipo de formulario a abrir 1 nuevo, 2 editar, 3 ver
             * @param {id} 	   int      En caso de que sea editar id tendrá que valer el id correspondiente
             * @param {controller} string  recibe el controlador al que se tendrá que apuntar
             * by Fhohs!
             **/
            $.post("<?php echo $raiz?>common/delega.php",{'id': id, 'type':type },
                function(respuesta){
                    if (isNaN(respuesta)) {
                        $("#respuesta_ajax").html(respuesta);
                        habilitaboton('aceptar_eliminar');
                        habilitaboton('aceptar_baja');
                    } else {
                        window.location = "<?php echo $param?>"+controller;
                    }
                });
        }

    </script>
	<!-- /theme JS files -->

</head>

<body class="login-container">

<!-- Main navbar -->
<div class="navbar navbar-inverse bg-blue">
	<div class="navbar-header">
		<!-- <a class="navbar-brand" href="javascript:void(0)"><img src="images/logo_nav.png" alt=""></a> -->

		<ul class="nav navbar-nav pull-left ">
			<!-- <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li> -->
			<li><a ><img  width="30" height="30"src="images/hex.png" alt=""></a></li>
		</ul>
	</div>

</div>
<!-- /main navbar -->


<!-- Page container -->
<div class="page-container">
	<!-- Page content -->
	<div class="page-content">
		<!-- Main content -->
		<div class="content-wrapper">
			<!-- Content area -->
			<div class="content">
				<!-- Simple login form -->
				<form action="javascript: login()" method="post" id="frm_login">
					<div class="panel panel-body login-form">
						<div class="text-center">
						<div class="sidebar-user">
                    <h2 id="ISF" class="blue text-center sif">HEX</h2>
                    <p class="text-center sif"><strong><small class="text-center">Sistema de activos fijos</small></strong></p>
				</div>
				<hr>
							<h5 class="content-group">Acceder al sistema <small class="display-block">Introduce tus datos</small></h5>
						</div>
						<div id="error">
							<?php if($msjError != ""){ echo $msjError;}?>
						</div>
						<div class="form-group has-feedback has-feedback-left">
							<input type="text" class="form-control" placeholder="Usuario" required
								   name="txtUser" id="user">
							<div class="form-control-feedback">
								<i class="icon-user text-muted"></i>
							</div>
						</div>
						<div class="form-group has-feedback has-feedback-left">
							<input type="password" class="form-control" placeholder="Contraseña" required
								   id="password" name="txtPass">
							<div class="form-control-feedback">
								<i class="icon-lock2 text-muted"></i>
							</div>
						</div>
						<div class="form-group">
							<button type="submit" class="btn bg-blue btn-block" id="btn_login">
								Acceder <i class="icon-circle-right2 position-right"></i>
							</button>
						</div>
						<div class="text-center">
							<a href="javascript:void(0)">Si olvidaste tu contraseña, contactar al administrador de sistema</a>
						</div>
					</div>
				</form>
				<!-- /simple login form -->
				<!-- Footer -->
				<div class="footer text-muted text-center">
					&copy; <?php echo date('Y')?> <a href="javascript:void(0)">Administrador <?php echo $titulo_paginas?></a>
				</div>
				<!-- /footer -->
			</div>
			<!-- /content area -->
		</div>
		<!-- /main content -->
	</div>
	<!-- /page content -->
</div>
<!-- /page container -->
</body>
<script>
	$(document).on("ready", edoinicial);

	function edoinicial(){
		//Cuando se inicial el documento ubicará el cursor en el primer campo
		$('#user').focus();
	}
</script>
</html>
	<?php
}
else{
	header("location:business/");                    //En caso de que tenga la session iniciada redireccionarlo a business
}
?>