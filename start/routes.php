<?php
function call($controller, $action) {
    //die($controller. " ".$action);
    require_once($controller . '/index_ctrl.php');

    switch($controller) {
        case 'controlador':
            $controller = new Inicio();
            break;
        case 'controlador/admin/sis_usuarios':
            $controller = new Admin_SisUsuarios();
            break;
        case 'controlador/admin/sis_roles':
            $controller = new Admin_SisRoles();
            break;
        case 'controlador/admin/movimientos':
            $controller = new movimientos();
            break;
        case 'controlador/catalogos/claves':
            $controller = new claves();
            break;
        case 'controlador/sys':
            $controller = new sysEm();
            break;
        //catalogo de empleados prueba dago 3
        case 'controlador/catalogos/empleados':
            $controller = new empleados();
            break;   
        case 'controlador/catalogos/plazas':
            $controller = new plazas();
            break;    
        case 'controlador/catalogos/areas':
            $controller = new areas();
            break;  
        case 'controlador/catalogos/bancos':
            $controller = new bancos();
            break; 
        case 'controlador/catalogos/titulos':
            $controller = new titulos();
            break; 
        case 'controlador/catalogos/beneficiario':
            $controller = new beneficiario();
            break;  
        case 'controlador/catalogos/conceptos':
            $controller = new conceptos();
            break; 
        case 'controlador/catalogos/centrotrabajo':
            $controller = new centrotrabajo();
            break;  
        case 'controlador/catalogos/clasificador':
            $controller = new clasificador();
            break;    
        case 'controlador/catalogos/programa':
            $controller = new programa();
            break;   
        case 'controlador/catalogos/componentes':
            $controller = new componentes();
            break;  
        case 'controlador/catalogos/empresas':
            $controller = new empresas();
            break; 
        case 'controlador/catalogos/escuelas':
            $controller = new escuelas();
            break;  
        case 'controlador/catalogos/activo':
            $controller = new activo();
            break;  
        case 'controlador/manual/indice':
            $controller = new IndiceManual();
        break;
        }

    $controller->{ $action }();
    //require_once($controller.'/'.$action.'_dpp.php');
}

// we're adding an entry for the new controller and its actions

$controllers = array( 
                    'controlador'                           => ['index'],
                    'controlador/admin/sis_usuarios'        => ['ver', 'index', 'nuevo'],
                    'controlador/sys'                       => ['acount'],
                    'controlador/admin/sis_roles' 	        => ['ver', 'index', 'nuevo'],
                    'controlador/admin/movimientos' 	    => ['ver', 'index', 'nuevo'],
                    'controlador/catalogos/claves'          => ['ver', 'index', 'nuevo'],
                    'controlador/catalogos/empleados'       => ['ver', 'index', 'nuevo'],
                    'controlador/catalogos/plazas'          => ['ver', 'index', 'nuevo'],
                    'controlador/catalogos/areas'           => ['ver', 'index', 'nuevo'],
                    'controlador/catalogos/conceptos'       => ['ver', 'index', 'nuevo'],
                    'controlador/catalogos/bancos'          => ['ver', 'index', 'nuevo'],
                    'controlador/catalogos/titulos'         => ['ver', 'index', 'nuevo'],
                    'controlador/catalogos/beneficiario'    => ['ver', 'index', 'nuevo'],
                    'controlador/catalogos/centrotrabajo'   => ['ver', 'index', 'nuevo'],
                    'controlador/catalogos/clasificador'    => ['ver', 'index', 'nuevo'],
                    'controlador/catalogos/programa'        => ['ver', 'index', 'nuevo'],
                    'controlador/catalogos/empresas'        => ['ver', 'index', 'nuevo'],
                    'controlador/catalogos/componentes'     => ['ver', 'index', 'nuevo'],
                    'controlador/catalogos/escuelas'        => ['ver', 'index', 'nuevo'],
                    'controlador/catalogos/activo'          => ['ver', 'index', 'nuevo'],
                    'controlador/manual/indice'             => ['index','cap1','cap2','cap3','cap4','cap5','cap6','cap7','cap8','cap9','cap10']
                    );

if (array_key_exists($controller, $controllers)) {
    if (in_array($action, $controllers[$controller])) {
        call($controller, $action);
    } else {
        call('business/', 'error');
    }
} else {
    call('business/', 'error');
}
?>