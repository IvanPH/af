<?php
//Incluyendo la conexión a la base de datos
require_once $dir_fc."connections/conn_data.php";

class cBancos extends BD
{
    //Variables
    private $id_banco;
    private $nombre;
    private $razon_social;
    private $activo;


    //Get & SET
    public function setId_banco($id_banco){
        $this->id_banco = $id_banco; 
    }
    public function setNombre($nombre){
        $this->nombre = $nombre;
    }
    public function setRazon_social($razon_social){
        $this->razon_social = $razon_social;
    }
    public function setActivo($activo){
        $this->activo = $activo;
    }
    public function setIdUsuario($strIdUsuario){
        $this->idUsuario = $strIdUsuario;
    }
    public function setIdUsuarioCaptura($id_usuario_captura)
    {
        $this->id_usuario_captura = $id_usuario_captura;
    }
    public  function  set_id_menu($menu){
        $this->id_menu = $menu;
    }

    public function getId_banco(){
        return $this->id_banco;
    }
    public function getNombre(){
        return $this->nombre;
    }
    public function getRS(){
        return $this->razon_social;
    }
    public function getActivo(){
        return $this->activo;
    }
    public function getIdRol(){
        return $this->IdRol;
    }
    public function get_id_menu(){
        return $this->id_menu;
    }

    //Funciones
    function __construct()
    {
        //Esta es la que llama a la base de datos
        //parent::__construct();
        $this->conn = new BD();
    }
    public function getRegbyid($id){
        $sqlCmd = "SELECT id_banco, nombre, razon_social, activo FROM cat_bancos WHERE id_banco=".$id." LIMIT 1";
        try {
            $sqlPrepare = $this->conn->prepare($sqlCmd);
            $sqlPrepare->execute();
            return $sqlPrepare;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    public function getCounter()
    {
        $no = "";
        try {
            $queryUser = "SELECT COUNT(id_banco) as count
                          FROM cat_bancos";
            //echo $queryUser;
            $result = $this->conn->prepare($queryUser);
            $result->execute();

            while($row = $result->fetch(PDO::FETCH_ASSOC)){
                $no =  $row['count'];
            }
            return $no;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    public function getAllRegAjax()
    {

        try {
            $queryUser = "SELECT id_banco, nombre, 
                                 razon_social, activo
                          FROM cat_bancos ORDER BY id_banco DESC";

            $result = $this->conn->prepare($queryUser);
            $result->execute();
        
            return $result;
            
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    public function updateStatus($tipo, $id){
        $correcto = 1;
        $sqlCmd = "UPDATE cat_bancos SET activo=".$tipo." WHERE id_banco=".$id;
        try {
            $sqlPrepare = $this->conn->prepare($sqlCmd);
            $sqlPrepare->execute();
            return $correcto;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }

    public function deleteReg($id){
        $correcto = 2;
        $sqlCmd = "DELETE FROM cat_bancos WHERE id_banco=".$id;
        try {
            $sqlPrepare = $this->conn->prepare($sqlCmd);
            $sqlPrepare->execute();
            return $correcto;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }

    public function buscaBanco(cBancos $datos){
        // Busca si la clave ya existe en la tabla
        $resultado = 0;
        try {
            $sqlCmd = "SELECT id_banco FROM cat_bancos WHERE nombre='".$datos->getNombre()."'";
            $sqlPrepare = $this->conn->prepare($sqlCmd);
            $sqlPrepare->execute();
            $resultado = $sqlPrepare->rowCount();
        }
        catch(\PDOException $e)
        {
            $resultado = "Error!: " . $e->getMessage();
        }
        return $resultado;
    }

    public function buscaCoincidencia(cBancos $datos){
        // Busca si la clave del concepto se repite en la tabla
        $resultado = 0;
        try{
            $sqlCmd = "SELECT id_banco FROM cat_bancos WHERE nombre='".$datos->getNombre()."' AND id_banco!=".$datos->getId_banco();
            $sqlPrepare = $this->conn->prepare($sqlCmd);
            $sqlPrepare->execute();
            $resultado = $sqlPrepare->rowCount();
        }
        catch (\PDOException $e){
            $resultado = "Error: ". $e->getMessage();
        }
        return $resultado;
    }
    public function agregarRegistro(cBancos $datos){
        $resultado = 0;
        $baseDatos = $this->conn->conexion();
        $sqlCmd = "INSERT INTO cat_bancos(nombre, razon_social, activo) VALUES (".
                    "'".$datos->getNombre()."'".
                    ",'".$datos->getRS()."'".
                    ",".$datos->getActivo().")";
        try {
            $sqlPrep = $this->conn->prepare($sqlCmd);
            $baseDatos->beginTransaction();
            $sqlPrep->execute();
            $resultado = $baseDatos->lastInsertId();
            $baseDatos->commit();
        }
        catch(\PDOException $e)
        {
            $baseDatos->rollBack();
            $resultado = "Error!: " . $e->getMessage();
        }
        return $resultado;
    }

    public function actualizaRegistro(cBancos $datos){
        $resultado = 1;
        $conexionBd = $this->conn->conexion();
        $sqlCmd = "UPDATE cat_bancos". 
                        " SET nombre='".$datos->getNombre()."'".
                        ",razon_social='".$datos->getRS()."'".
                        ",activo='".$datos->getActivo()."'".
                        " WHERE id_banco=".$datos->getId_banco();
        try {
            $sqlPrepare = $this->conn->prepare($sqlCmd);
            $conexionBd->beginTransaction();
            $sqlPrepare->execute();
            $conexionBd->commit();
        }catch (\PDOException $e){
            $conexionBd->rollBack();
            $resultado =  "Error!: " . $e->getMessage();
        }
        return $resultado;
    }
    
}
