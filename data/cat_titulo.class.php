<?php
//Incluyendo la conexión a la base de datos
require_once $dir_fc."connections/conn_data.php";

class cTitulos extends BD
{
    //Variables
    private $id_titulo;
    private $descripcion;
    private $abreviacion;
    private $activo;


    //Get & SET
    public function setId_titulo($id_titulo){
        $this->id_titulo = $id_titulo; 
    }
    public function setDescripcion($descripcion){
        $this->descripcion = $descripcion;
    }
    public function setAbreviacion($abreviacion){
        $this->abreviacion = $abreviacion;
    }
    public function setActivo($activo){
        $this->activo = $activo;
    }
    public function setIdUsuario($strIdUsuario){
        $this->idUsuario = $strIdUsuario;
    }
    public function setIdUsuarioCaptura($id_usuario_captura)
    {
        $this->id_usuario_captura = $id_usuario_captura;
    }
    public  function  set_id_menu($menu){
        $this->id_menu = $menu;
    }

    public function getId_titulo(){
        return $this->id_titulo;
    }
    public function getDescripcion(){
        return $this->descripcion;
    }
    public function getAbreviacion(){
        return $this->abreviacion;
    }
    public function getActivo(){
        return $this->activo;
    }
    public function getIdRol(){
        return $this->IdRol;
    }
    public function get_id_menu(){
        return $this->id_menu;
    }

    //Funciones
    function __construct()
    {
        //Esta es la que llama a la base de datos
        //parent::__construct();
        $this->conn = new BD();
    }
    public function getRegbyid($id){
        $sqlCmd = "SELECT id_titulo, descripcion, abreviacion FROM cat_titulo WHERE id_titulo=".$id." LIMIT 1";
        try {
            $sqlPrepare = $this->conn->prepare($sqlCmd);
            $sqlPrepare->execute();
            return $sqlPrepare;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    public function getCounter()
    {
        $no = "";
        try {
            $queryUser = "SELECT COUNT(id_titulo) as count
                          FROM cat_titulo";
            //echo $queryUser;
            $result = $this->conn->prepare($queryUser);
            $result->execute();

            while($row = $result->fetch(PDO::FETCH_ASSOC)){
                $no =  $row['count'];
            }
            return $no;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    public function getAllRegAjax()
    {

        try {
            $queryUser = "SELECT id_titulo, descripcion, 
                                 abreviacion
                          FROM cat_titulo ORDER BY id_titulo DESC";

            $result = $this->conn->prepare($queryUser);
            $result->execute();
        
            return $result;
            
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    public function updateStatus($tipo, $id){
        $correcto = 1;
        $sqlCmd = "UPDATE cat_titulo SET activo=".$tipo." WHERE id_titulo=".$id;
        try {
            $sqlPrepare = $this->conn->prepare($sqlCmd);
            $sqlPrepare->execute();
            return $correcto;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }

    public function deleteReg($id){
        $correcto = 2;
        $sqlCmd = "DELETE FROM cat_titulo WHERE id_titulo=".$id;
        try {
            $sqlPrepare = $this->conn->prepare($sqlCmd);
            $sqlPrepare->execute();
            return $correcto;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }

    public function buscaTitulo(cTitulos $datos){
        // Busca si la clave ya existe en la tabla
        $resultado = 0;
        try {
            $sqlCmd = "SELECT id_titulo FROM cat_titulo WHERE descripcion='".$datos->getDescripcion()."'";
            $sqlPrepare = $this->conn->prepare($sqlCmd);
            $sqlPrepare->execute();
            $resultado = $sqlPrepare->rowCount();
        }
        catch(\PDOException $e)
        {
            $resultado = "Error!: " . $e->getMessage();
        }
        return $resultado;
    }

    public function buscaCoincidencia(cTitulos $datos){
        // Busca si la clave del concepto se repite en la tabla
        $resultado = 0;
        try{
            $sqlCmd = "SELECT id_titulo FROM cat_titulo WHERE descripcion='".$datos->getDescripcion()."' AND id_titulo!=".$datos->getId_titulo();
            $sqlPrepare = $this->conn->prepare($sqlCmd);
            $sqlPrepare->execute();
            $resultado = $sqlPrepare->rowCount();
        }
        catch (\PDOException $e){
            $resultado = "Error: ". $e->getMessage();
        }
        return $resultado;
    }
    public function agregarRegistro(cTitulos $datos){
        $resultado = 0;
        $baseDatos = $this->conn->conexion();
        $sqlCmd = "INSERT INTO cat_titulo(descripcion, abreviacion) VALUES (".
                    "'".$datos->getDescripcion()."'".
                    ",'".$datos->getAbreviacion()."')";
        try {
            $sqlPrep = $this->conn->prepare($sqlCmd);
            $baseDatos->beginTransaction();
            $sqlPrep->execute();
            $resultado = $baseDatos->lastInsertId();
            $baseDatos->commit();
        }
        catch(\PDOException $e)
        {
            $baseDatos->rollBack();
            $resultado = "Error!: " . $e->getMessage();
        }
        return $resultado;
    }

    public function actualizaRegistro(cTitulos $datos){
        $resultado = 1;
        $conexionBd = $this->conn->conexion();
        $sqlCmd = "UPDATE cat_titulo". 
                        " SET descripcion='".$datos->getDescripcion()."'".
                        ",abreviacion='".$datos->getAbreviacion()."'".
                        " WHERE id_titulo=".$datos->getId_titulo();
        try {
            $sqlPrepare = $this->conn->prepare($sqlCmd);
            $conexionBd->beginTransaction();
            $sqlPrepare->execute();
            $conexionBd->commit();
        }catch (\PDOException $e){
            $conexionBd->rollBack();
            $resultado =  "Error!: " . $e->getMessage();
        }
        return $resultado;
    }
    
}
