<?php
//Incluyendo la conexión a la base de datos
require_once $dir_fc."connections/conn_data.php";

class cActivo extends BD
{
    //Variables
    private $id_activo;
    private $marca;
    private $tipo;
    private $categoria;
    private $proposito;
    private $tipo_usuario;
    private $num_serie;
    private $matricula_activo;
    private $matricula_usuario;
    private $estado;

    //Get & Set
    public function getid_activo() { return $this->id_activo; }
    public function setid_activo($id_activo) { $this->id_activo = $id_activo; }

    public function getmarca() { return $this->marca; }
    public function setmarca($marca) { $this->marca = $marca; }

    public function gettipo() { return $this->tipo; }
    public function settipo($tipo) { $this->tipo = $tipo; }

    public function getcategoria() { return $this->categoria; }
    public function setcategoria($categoria) { $this->categoria = $categoria; }

    public function getproposito() { return $this->proposito; }
    public function setproposito($proposito) { $this->proposito = $proposito; }

    public function gettipo_usuario() { return $this->tipo_usuario; }
    public function settipo_usuario($tipo_usuario) { $this->tipo_usuario = $tipo_usuario; }

    public function getnum_serie() { return $this->num_serie; }
    public function setnum_serie($num_serie) { $this->num_serie = $num_serie; }

    public function getmatricula_activo() { return $this->matricula_activo; }
    public function setmatricula_activo($matricula_activo) { $this->matricula_activo = $matricula_activo; }

    public function getmatricula_usuario() { return $this->matricula_usuario; }
    public function setmatricula_usuario($matricula_usuario) { $this->matricula_usuario = $matricula_usuario; }    

    public function getestado() { return $this->estado; }
    public function setestado($estado) { $this->estado = $estado; }   

    //Funciones
    function __construct()
    {
        //Esta es la que llama a la base de datos
        //parent::__construct();
        $this->conn = new BD();
    }
    public function getRegbyid($id){
        $sqlCmd = "SELECT id_activo, marca, tipo, categoria, proposito, tipo_usuario, num_serie, matricula_activo,
         matricula_usuario, estado
         FROM cat_activo_fijo WHERE id_activo=".$id." LIMIT 1";
        try {
            $sqlPrepare = $this->conn->prepare($sqlCmd);
            $sqlPrepare->execute();
            return $sqlPrepare;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    public function getCounter()
    {
        $no = "";
        try {
            $queryUser = "SELECT COUNT(id_activo) as count
                          FROM cat_activo_fijo";
            //echo $queryUser;
            $result = $this->conn->prepare($queryUser);
            $result->execute();

            while($row = $result->fetch(PDO::FETCH_ASSOC)){
                $no =  $row['count'];
            }
            return $no;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    public function getAllRegAjax()
    {

        try {
            $queryUser = "SELECT id_activo, marca, tipo, categoria, proposito, tipo_usuario, num_serie, 
                        matricula_activo, matricula_usuario, estado 
                        FROM cat_activo_fijo ORDER BY id_activo DESC";

            $result = $this->conn->prepare($queryUser);
            $result->execute();
        
            return $result;
            
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    public function updateStatus($estado, $id){
        $correcto = 1;
        $sqlCmd = "UPDATE cat_activo_fijo SET estado=".$estado." WHERE id_activo=".$id;
        try {
            $sqlPrepare = $this->conn->prepare($sqlCmd);
            $sqlPrepare->execute();
            return $correcto;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }

    public function deleteReg($id){
        $correcto = 2;
        $sqlCmd = "DELETE FROM cat_activo_fijo WHERE id_activo=".$id;
        try {
            $sqlPrepare = $this->conn->prepare($sqlCmd);
            $sqlPrepare->execute();
            return $correcto;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }

    public function buscaActivo(cActivo $datos){
        // Busca si el Empleado ya existe en la tabla
        $resultado = 0;
        try {
            $sqlCmd = "SELECT id_activo FROM cat_activo_fijo WHERE num_serie='".$datos->getnum_serie()."'";
            $sqlPrepare = $this->conn->prepare($sqlCmd);
            $sqlPrepare->execute();
            $resultado = $sqlPrepare->rowCount();
        }
        catch(\PDOException $e)
        {
            $resultado = "Error!: " . $e->getMessage();
        }
        return $resultado;
    }

    public function buscaCoincidencia_Edit(cActivo $datos){
        // Busca si el Empleado se repite en la tabla
        $resultado = 0;
        try{
            $sqlCmd = "SELECT id_activo FROM cat_activo_fijo WHERE num_serie='".$datos->getnum_serie()."' AND id_activo !=".$datos->getid_activo();
            $sqlPrepare = $this->conn->prepare($sqlCmd);
            $sqlPrepare->execute();
            $resultado = $sqlPrepare->rowCount();
        }
        catch (\PDOException $e){
            $resultado = "Error: ". $e->getMessage();
        }
        return $resultado;
    }
    public function buscaCoincidencia(cActivo $datos){
        // Busca si la el Empleado se repite en la tabla verificando tipo_usuario y marca
        $resultado = 0;
        try{
            $sqlCmd = "SELECT id_activo FROM cat_activo_fijo WHERE num_serie='".$datos->getnum_serie()."' ";
            $sqlPrepare = $this->conn->prepare($sqlCmd);
            $sqlPrepare->execute();
            $resultado = $sqlPrepare->rowCount();
        }
        catch (\PDOException $e){
            $resultado = "Error: ". $e->getMessage();
        }
        return $resultado;
    }
    public function agregarRegistro(cActivo $datos){
        $resultado = 0;
        $baseDatos = $this->conn->conexion();
        $sqlCmd = "INSERT INTO cat_activo_fijo(marca, tipo, categoria, proposito, tipo_usuario, num_serie,
         matricula_activo, matricula_usuario, estado) VALUES (".
                    "'".$datos->getmarca()."'".
                    ",'".$datos->gettipo()."'".
                    ",'".$datos->getcategoria()."'".
                    ",'".$datos->getproposito()."'".
                    ",'".$datos->gettipo_usuario()."'".
                    ",'".$datos->getnum_serie()."'".
                    ",'".$datos->getmatricula_activo()."'".
                    ",'".$datos->getmatricula_usuario()."'".
                    ",".$datos->getestado().")";
        try {
            $sqlPrep = $this->conn->prepare($sqlCmd);
            $baseDatos->beginTransaction();
            $sqlPrep->execute();
            $resultado = $baseDatos->lastInsertId();
            $baseDatos->commit();
        }
        catch(\PDOException $e)
        {
            $baseDatos->rollBack();
            $resultado = "Error!: " . $e->getMessage();
        }
        return $resultado;
    }

    public function actualizaRegistro(cActivo $datos){
        $resultado = 1;
        $conexionBd = $this->conn->conexion();
        $sqlCmd = "UPDATE cat_activo_fijo". 
                        " SET marca='".$datos->getmarca()."'".
                        ",tipo='".$datos->gettipo()."'".
                        ",categoria='".$datos->getcategoria()."'".
                        ",proposito='".$datos->getproposito()."'".
                        ",tipo_usuario='".$datos->gettipo_usuario()."'".
                        ",num_serie='".$datos->getnum_serie()."'".
                        ",matricula_activo='".$datos->getmatricula_activo()."'".
                        ",matricula_usuario='".$datos->getmatricula_usuario()."'".
                        ",estado='".$datos->getestado()."'".
                        " WHERE id_activo=".$datos->getid_activo();
        try {
            $sqlPrepare = $this->conn->prepare($sqlCmd);
            $conexionBd->beginTransaction();
            $sqlPrepare->execute();
            $conexionBd->commit();
        }catch (\PDOException $e){
            $conexionBd->rollBack();
            $resultado =  "Error!: " . $e->getMessage();
        }
        return $resultado;
    }

    public function getEmpleado(){
        try {
            $queryMP = "SELECT id_beneficiario, CONCAT(nombre, ' ' ,apepa,' ', apema) as nombre
                    FROM cat_beneficiario ";
             echo $queryMP;
            $result = $this->conn->prepare($queryMP);
            $result->execute();
           
            return $result;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    public function getEmpleadobyid($id){
        $sqlCmd = "SELECT noempleado FROM cat_beneficiario WHERE id_beneficiario=".$id." LIMIT 1";
        try {
            $sqlPrepare = $this->conn->prepare($sqlCmd);
            $sqlPrepare->execute();
            return $sqlPrepare;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    public function getCTrabajo(){
        try {
            $queryMP = "SELECT id_centrotrabajo, area
                    FROM cat_centrotrabajo ";
             echo $queryMP;
            $result = $this->conn->prepare($queryMP);
            $result->execute();
           
            return $result;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    public function getCTrabajobyid($id){
        $sqlCmd = "SELECT area FROM cat_centrotrabajo WHERE id_centrotrabajo=".$id." LIMIT 1";
        try {
            $sqlPrepare = $this->conn->prepare($sqlCmd);
            $sqlPrepare->execute();
            return $sqlPrepare;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    
}
