<?php
//Incluyendo la conexión a la base de datos
require_once $dir_fc."connections/conn_data.php";

class Rol extends BD
{
    private $id;
    private $rol;
    private $pag_ini;
    private $descripcion;
    private $id_menu;
    private $id_usuario;

    /**
     * @return mixed
     */
    public function getIdUsuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param mixed $id_usuario
     */
    public function setIdUsuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
    }




    public function get_id(){
        return $this->id;
    }
    public function get_rol(){
        return $this->rol;
    }
    public function get_pag_ini(){
        return $this->pag_ini;
    }
    public function get_descripcion(){
        return $this->descripcion;
    }
    public function get_id_menu(){
        return $this->id_menu;
    }

    public function set_id($id){
        $this->id = $id;
    }
    public function set_rol($rol){
        $this->rol = $rol;
    }
    public function set_pag_ini($pag_ini){
        $this->pag_ini = $pag_ini;
    }
    public function set_descripcion($descripcion){
        $this->descripcion = $descripcion;
    }

    public  function  set_id_menu($menu){
        $this->id_menu = $menu;
    }

    private $conn;

    private $filtro;
    private $inicio;
    private $fin;
    private $limite;

    private $back;
    private $num_reg;

    private $imp;
    private $nuevo;
    private $edit;
    private $elim;
    private $exportar;

    public function set_imp($imp){
        $this->imp = $imp;
    }
    public function set_nuevo($nuevo){
        $this->nuevo = $nuevo;
    }
    public function set_edit($edit){
        $this->edit = $edit;
    }
    public function set_elim($elim){
        $this->elim = $elim;
    }
    public function set_exportar($exportar){
        $this->exportar = $exportar;
    }
    public function set_filtro($filtro){
        $this->filtro = $filtro;
    }
    public function set_inicio($inicio){
        $this->inicio = $inicio;
    }
    public function set_fin($fin){
        $this->fin = $fin;
    }
    public function set_limite($limite){
        $this->limite = $limite;
    }

    public function set_back($back){
        $this->back = $back;
    }
    public function set_num_reg($num_reg){
        $this->num_reg = $num_reg;
    }

    public function get_filtro(){
        return $this->filtro;
    }
    public function get_inicio(){
        return $this->inicio;
    }
    public function get_fin(){
        return $this->fin;
    }
    public function get_limite(){
        return $this->limite;
    }
    public function get_back(){
        return $this->back;
    }
    public function get_num_reg(){
        return $this->num_reg;
    }

    public function get_imp(){
        return $this->imp;
    }
    public function get_nuevo(){
        return $this->nuevo;
    }
    public function get_edit(){
        return $this->edit;
    }
    public function get_elim(){
        return $this->elim;
    }
    public function get_exportar(){
        return $this->exportar;
    }

    function __construct()
    {
        //Esta es la que llama a la base de datos
        //parent::__construct();
        $this->conn = new BD();
    }

    public function getAllRegAjax()
    {

        try {
            $queryUser = "SELECT id, rol, descripcion, pag_ini,  activo
                          FROM ws_rol ORDER BY id DESC";

            $result = $this->conn->prepare($queryUser);
            $result->execute();

            return $result;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    public function getAllReg(){
        //Incio fin son para paginado
        try {
            $milimite = "";
            $condition = "";

            if ($this->get_limite() == 1) {
                $milimite = " LIMIT " . $this->get_inicio() . ", " . $this->get_fin();
            }

            $filtro = $this->get_filtro();
            if ($filtro != "") {
                $condition = " WHERE rol LIKE '%$filtro%' ";
            }

            $queryReg = "SELECT id, rol, descripcion, activo
                           FROM ws_rol $condition
                          ORDER BY id DESC " . $milimite;

            $result = $this->conn->prepare($queryReg);
            $result->execute();
            return $result;

        }catch (\PDOException $e){
            return "Error!: " . $e->getMessage();
        }

    }


    public function getAllRoles(){
        try {
            $query = "SELECT id, rol, descripcion  FROM ws_rol ORDER BY id ASC";

            $result = $this->conn->prepare($query);
            $result->execute();
            return $result;

        }catch (\PDOException $e){
            return "Error!: " . $e->getMessage();
        }
    }
    
    public function getUserByRol(){
        try {
            $query = "SELECT id_usuario 
                        FROM ws_usuario
                       WHERE id_rol = ".$this->get_id();

            $result = $this->conn->prepare($query);
            $result->execute();
            return $result;

        }catch (\PDOException $e){
            return "Error!: " . $e->getMessage();
        }
    }

    public function getReg(){
        try {
            $queryMP = "SELECT id, rol, descripcion FROM ws_rol ORDER BY id DESC";
            $result = $this->conn->prepare($queryMP);
            $result->execute();
            return $result;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }

    public function getPublicResponsable(){
        try {
            $queryMP = "SELECT id_area as id_responsable, area as responsable 
                          FROM cat_areas ORDER BY id_responsable DESC";
            $result = $this->conn->prepare($queryMP);
            $result->execute();
            return $result;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }

    public function getRolID(){
        try {
            $queryReg = "SELECT id, rol, descripcion, activo FROM ws_rol WHERE id = ".$this->get_id()." LIMIT 1";
            $result   = $this->conn->prepare($queryReg);
            $result->execute();
            return $result;
        }catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }

    public function countRol_User($id_rol){
        try {
            $rw_res   = 0;
            $queryReg = "SELECT count(id_rol) as roles FROM ws_usuario
                          WHERE id_rol = $id_rol";
            $result = $this->conn->prepare($queryReg);
            $result->execute();
            if($result->rowCount() > 0){
                $registrosf = $result->fetch(PDO::FETCH_OBJ);
                $rw_res     = $registrosf->roles;
            }
            return $rw_res;
        }
        catch (\PDOException $e){
            return "Error!: " . $e->getMessage();
        }
    }

    public function insertReg(){
        $exec = $this->conn->conexion();
        try {
            $correcto   = 1;
            $insert     = "INSERT INTO ws_rol(rol, pag_ini, descripcion)
                           VALUES ('".$this->get_rol()."', 0, '".$this->get_descripcion()."')";
            $result = $this->conn->prepare($insert);
            $exec->beginTransaction();
            $result->execute();
            if ($correcto == 1){
                $correcto= $exec->lastInsertId();
                $this->set_id($correcto);
            }
            $exec->commit();
            return $correcto;
        }
        catch (\PDOException $e){
            return "Error!: " . $e->getMessage();
        }
    }

    public function insertRegdtl(){
        $exec = $this->conn->conexion();
        try {
            $correcto   = 1;
            $insert_dtl ="INSERT INTO ws_rol_menu (id_rol, id_menu, imp,edit,elim,nuevo,exportar) 
                               VALUES (".$this->get_id().", ".$this->get_id_menu().", ".$this->get_imp().", 
                                        ".$this->get_edit().", ".$this->get_elim().", ".$this->get_nuevo().",
                                        ".$this->get_exportar().")";

            $result = $this->conn->prepare($insert_dtl);
            $exec->beginTransaction();
            $result->execute();
            if ($correcto == 1){
                $correcto= $exec->lastInsertId();
            }
            $exec->commit();
            return $correcto;
        }
            catch (\PDOException $e){
            return "Error!: " . $e->getMessage();
            }

    }
    
    public function insertRegdtlByRol($id_usuario){
        $exec = $this->conn->conexion();
        try {
            $correcto   = 1;
            $insert_dtl ="INSERT INTO ws_usuario_menu(id_usuario, id_menu, imp,edit,elim,nuevo,exportar) 
                               SELECT '$id_usuario', id_menu, imp,edit,elim,nuevo,exportar 
                                 FROM ws_rol_menu 
                                 WHERE id_rol = ".$this->get_id();

            $result = $this->conn->prepare($insert_dtl);
            $exec->beginTransaction();
            $result->execute();
            if ($correcto == 1){
                $correcto= $exec->lastInsertId();
            }
            $exec->commit();
            return $correcto;
        }
            catch (\PDOException $e){
            return "Error!: " . $e->getMessage();
            }

    }

    public function actualizaReg($tipo, $id){
        $correcto   = 1;
        $exec       = $this->conn->conexion();
        try{
            $queryUpdate ="UPDATE ws_rol SET activo = $tipo
    				        WHERE id = '$id'";

            $result = $this->conn->prepare($queryUpdate);
            $exec->beginTransaction();
            $result->execute();
            $exec->commit();
        }catch (\PDOException $e){
            $exec->rollBack();
            $correcto =  "Error!: " . $e->getMessage();
        }
        return $correcto;

    }

    public function parentsMenu(){
        try {
        $query= "SELECT id, id_grupo, texto, link  FROM ws_menu
                  WHERE id_grupo = 0 ORDER BY id ASC";
            $result = $this->conn->prepare($query);
            $result->execute();
            return $result;
        }catch (\PDOException $e){
            return "Error!: " . $e->getMessage();
        }
    }

    public function parentsMenuByRol(){
        try {
        $query= "SELECT m.id, m.id_grupo, m.texto, m.link  
                   FROM ws_menu as m
                   INNER JOIN ws_rol_menu as rm ON m.id = rm.id_menu
                  WHERE m.id_grupo = 0 AND rm.id_rol = ".$this->get_id()." ORDER BY id ASC";
            $result = $this->conn->prepare($query);
            $result->execute();
            return $result;
        }catch (\PDOException $e){
            return "Error!: " . $e->getMessage();
        }
    }

    public function childsMenu($id_menu){
        try {
        $query= "SELECT id, id_grupo, texto, link  FROM ws_menu
                WHERE id_grupo = $id_menu ORDER BY id ASC";
            $result = $this->conn->prepare($query);
            $result->execute();
            return $result;
        }catch (\PDOException $e){
            return "Error!: " . $e->getMessage();
        }
    }

    public function checarRol_menu(){
        try {
            $queryCM ="SELECT id, imp, edit, nuevo, elim, exportar 
                         FROM ws_rol_menu 
                        WHERE id_menu='".$this->get_id_menu()."' AND id_rol='".$this->get_id()."'";
            $result = $this->conn->prepare($queryCM);
            $result->execute();
            //echo $queryCM;
            //$rows = $result->rowCount();
            return $result;
        }catch (\PDOException $e){
            return "Error!: " . $e->getMessage();
        }
    }

    public function checarUser_menu(){
        try {
            $queryCM ="SELECT id_usuario_menu as id, imp, edit, nuevo, elim, exportar 
                         FROM ws_usuario_menu 
                        WHERE id_menu='".$this->get_id_menu()."' AND id_usuario ='".$this->getIdUsuario()."'";
            $result = $this->conn->prepare($queryCM);
            $result->execute();
            //$rows = $result->rowCount();
            return $result;
        }catch (\PDOException $e){
            return "Error!: " . $e->getMessage();
        }
    }

    public function updateReg(){
        $correcto   = 1;
        $exec       = $this->conn->conexion();
        try{
            $queryUpdate = "UPDATE ws_rol SET rol = '".$this->get_rol()."', descripcion='".$this->get_descripcion()."'
                                     WHERE id = ".$this->get_id();
            $result = $this->conn->prepare($queryUpdate);
            $exec->beginTransaction();
            $result->execute();
            $exec->commit();
        }catch (\PDOException $e){
            $exec->rollBack();
            $correcto =  "Error!: " . $e->getMessage();
        }
        return $correcto;
    }

    public function deleteRegRM(){
        $correcto   = 2;
        try {
            $delete     = "DELETE FROM ws_rol_menu WHERE id_rol = ".$this->get_id();

            $result = $this->conn->prepare($delete);
            $result->execute();

            return $correcto;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    public function deleteRegByRolUser(){
        $correcto   = 2;
        try {
            $delete     = "DELETE FROM ws_usuario_menu 
                                 WHERE id_usuario IN 
                                 (SELECT id_usuario FROM ws_usuario WHERE id_rol = ".$this->get_id().")";

            $result = $this->conn->prepare($delete);
            $result->execute();

            return $correcto;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }

    public function deleteRegUsMenu(){
        $correcto   = 2;
        try {
            $delete     = "DELETE FROM ws_usuario_menu WHERE id_usuario = ".$this->getIdUsuario();

            $result = $this->conn->prepare($delete);
            $result->execute();

            return $correcto;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }

    public function updateStatus($tipo){
        $correcto   = 1;
        $exec       = $this->conn->conexion();
        try{
            $queryUpdate ="UPDATE ws_rol SET activo = $tipo
    				    WHERE id = ".$this->get_id();

            $result = $this->conn->prepare($queryUpdate);
            $exec->beginTransaction();
            $result->execute();
            $exec->commit();
        }catch (\PDOException $e){
            $exec->rollBack();
            $correcto =  "Error!: " . $e->getMessage();
        }
        return $correcto;
    }

    public function deleteReg(){
        $correcto   = 2;
        try {
            $delete     = "DELETE FROM ws_rol WHERE id = ".$this->get_id();

            $result = $this->conn->prepare($delete);
            $result->execute();

            return $correcto;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }

    public function foundRol(){
        try {
            $queryCM ="SELECT rol FROM ws_rol WHERE rol='".$this->get_rol()."'";
            $result = $this->conn->prepare($queryCM);
            $result->execute();
            $rows = $result->rowCount();
            return $rows;
        }catch (\PDOException $e){
            return "Error!: " . $e->getMessage();
        }
    }
}
