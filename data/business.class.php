<?php
//Incluyendo la conexión a la base de datos
require_once $dir_fc."connections/conn_data.php";
/**
 * * Operaciones y movimientos que se realizan para el menú y otras herramientas de inciio que viene de la base de datos
 */
class cBusiness extends BD
{

    private $conn;
    private $id_estatus;
    private $admin;
    private $id_responsable;

    /**
     * @return mixed
     */
    public function getAdmin()
    {
        return $this->admin;
    }

    /**
     * @param mixed $admin
     */
    public function setAdmin($admin)
    {
        $this->admin = $admin;
    }

    /**
     * @return mixed
     */
    public function getIdResponsable()
    {
        return $this->id_responsable;
    }

    /**
     * @param mixed $id_responsable
     */
    public function setIdResponsable($id_responsable)
    {
        $this->id_responsable = $id_responsable;
    }



    /**
     * @return mixed
     */
    public function getIdEstatus()
    {
        return $this->id_estatus;
    }

    /**
     * @param mixed $id_estatus
     */
    public function setIdEstatus($id_estatus)
    {
        $this->id_estatus = $id_estatus;
    }



    function __construct()
    {
        //Esta es la que llama a la base de datos
        //parent::__construct();
        $this->conn = new BD();
    }
    private $id_usuario_captura;

    public function set_id_usuario_captura($id_usuario_captura){
        $this->id_usuario_captura = $id_usuario_captura;
    }

    public function get_id_usuario_captura(){
        return $this->id_usuario_captura;
    }

    public function getCountByEstatus(){
        try {
            $avg[0] = 0;
            $avg[1] = 0;
            $queryMP = "SELECT COUNT(id_reunion) as counter
                    FROM tbl_reunion as  aa 
                    WHERE id_estatus = ".$this->getIdEstatus()." AND activo = 1";
            $result = $this->conn->prepare($queryMP);
            $result->execute();
            if($result->rowCount() > 0){
                $rw = $result->fetch(PDO::FETCH_OBJ);
                $avg[0] = $rw->counter;
            }
            return $avg;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }

    public function getPublicEstatus(){
        try {
            $queryMP = "SELECT id_estatus, estatus, class, class_color
                    FROM cat_estatus WHERE activo = 1 ORDER BY id_estatus";
            // echo $queryMP;
            $result = $this->conn->prepare($queryMP);
            $result->execute();
            return $result;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }

    public function getReunionesPendientes(){
        try {
            $admin_cond = "";
            if($this->getAdmin() == 0){

                $admin_cond = " AND ".$this->getIdResponsable()." 
                        IN (SELECT ra.id_responsable FROM a_hallazgo_responsable as ra WHERE ra.id_hallazgo = h.id_hallazgo) ";
            }

            $queryMP = "SELECT id_reunion, descripcion, DATE_FORMAT(fecha_inicio, '%d/%m/%Y-%H:%i' ) AS fecha_inicio, 
                               lugar
                    FROM tbl_reunion as a
                    WHERE a.activo = 1 AND id_estatus <> 4 OR id_estatus <> 3 $admin_cond ORDER BY fecha_inicio desc LIMIT 10";
            // echo $queryMP;
            $result = $this->conn->prepare($queryMP);
            $result->execute();
            return $result;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }


}
?>
