<?php
//Incluyendo la conexión a la base de datos
require_once $dir_fc."connections/conn_data.php";
//require_once $dir_fc."connections/conn_config.php";

class cUsers extends BD
{
    private $idUsuario;
    private $IdRol;
    private $usuario;
    private $clave;
    private $nombre;
    private $apepa;
    private $apema;
    private $sexo;
    private $correo;
    private $fecha_ingreso;
    private $imprimir;
    private $editar;
    private $eliminar;
    private $nuevo_usuario;
    private $imagen;
    private $activo;
    private $nvaclave;
    private $admin;
    private $filtro;
    private $inicio;
    private $fin;
    private $limite;
    private $conn;

    private $imp;
    private $nuevo;
    private $edit;
    private $elim;
    private $exportar;
    private $id_menu;
    private $id_responsable;
    private $id_usuario_captura;
    private $fecha_captura;

    /**
     * @return mixed
     */
    public function getIdUsuarioCaptura()
    {
        return $this->id_usuario_captura;
    }

    /**
     * @param mixed $id_usuario_captura
     */
    public function setIdUsuarioCaptura($id_usuario_captura)
    {
        $this->id_usuario_captura = $id_usuario_captura;
    }

    /**
     * @return mixed
     */
    public function getFechaCaptura()
    {
        return $this->fecha_captura;
    }

    /**
     * @param mixed $fecha_captura
     */
    public function setFechaCaptura($fecha_captura)
    {
        $this->fecha_captura = $fecha_captura;
    }



    /**
     * @return mixed
     */
    public function getIdResponsable()
    {
        return $this->id_responsable;
    }

    /**
     * @param mixed $id_responsable
     */
    public function setIdResponsable($id_responsable)
    {
        $this->id_responsable = $id_responsable;
    }



    public function get_id_menu(){
        return $this->id_menu;
    }

    public  function  set_id_menu($menu){
        $this->id_menu = $menu;
    }


    public function getIdUsuario(){
        return $this->idUsuario;
    }

    public function setIdUsuario($strIdUsuario){
        $this->idUsuario = $strIdUsuario;
    }

    public function getIdRol(){
        return $this->IdRol;
    }

    public function setIdRol($strIdRol){
        $this->IdRol = $strIdRol;
    }

    public function getUsuario(){
        return $this->usuario;
    }

    public function setUsuario($usuario){
        $this->usuario = $usuario;
    }

    public function getClave(){
        return $this->clave;
    }

    public function getNuevaClave(){
        return $this->nvaclave;
    }

    public function setClave($clave){
        $this->clave = $clave;
    }

    public function setNvaclave($nvaclave){
        $this->nvaclave = $nvaclave;
    }

    public function getNombre(){
        return $this->nombre;
    }

    public function setNombre($nombre){
        $this->nombre = $nombre;
    }

    public function getApePa(){
        return $this->apepa;
    }

    public function setApePa($apepa){
        $this->apepa = $apepa;
    }

    public function getApeMa(){
        return $this->apema;
    }

    public function setApeMa($apema){
        $this->apema = $apema;
    }

    public function getSexo(){
        return $this->sexo;
    }

    public function get_admin(){
        return $this->admin;
    }

    public function set_admin($admin){
        $this->admin = $admin;
    }

    public function setSexo($sexo){
        $this->sexo = $sexo;
    }

    public function getCorreo(){
        return $this->correo;
    }

    public function setCorreo($correo){
        $this->correo = $correo;
    }

    public function getFechaIngreso(){
        return $this->fecha_ingreso;
    }

    public function setFechaIngreso($fecha_ingreso){
        $this->fecha_ingreso = $fecha_ingreso;
    }

    public function getImpresion(){
        return $this->imprimir;
    }

    public function setImpresion($impresion){
        $this->imprimir = $impresion;
    }

    public function getEditar(){
        return $this->editar;
    }

    public function setEditar($editar){
        $this->editar = $editar;
    }

    public function getNuevoUsuario(){
        return $this->nuevo_usuario;
    }

    public function setNuevoUsuario($nuevo_usuario){
        $this->nuevo_usuario = $nuevo_usuario;
    }

    public function getEliminar(){
        return $this->eliminar;
    }

    public function setEliminar($eliminar){
        $this->eliminar = $eliminar;
    }

    public function getImagen(){
        return $this->imagen;
    }

    public function setImagen($imagen){
        $this->imagen = $imagen;
    }

    public function getActivo(){
        return $this->activo;
    }

    public function setActivo($activo){
        $this->activo = $activo;
    }

    public function set_filtro($filtro){
        $this->filtro = $filtro;
    }
    public function set_inicio($inicio){
        $this->inicio = $inicio;
    }
    public function set_fin($fin){
        $this->fin = $fin;
    }
    public function set_limite($limite){
        $this->limite = $limite;
    }

    public function get_filtro(){
        return $this->filtro;
    }
    public function get_inicio(){
        return $this->inicio;
    }
    public function get_fin(){
        return $this->fin;
    }
    public function get_limite(){
        return $this->limite;
    }

    public function get_imp(){
        return $this->imp;
    }
    public function get_nuevo(){
        return $this->nuevo;
    }
    public function get_edit(){
        return $this->edit;
    }
    public function get_elim(){
        return $this->elim;
    }
    public function get_exportar(){
        return $this->exportar;
    }

    public function set_imp($imp){
        $this->imp = $imp;
    }
    public function set_nuevo($nuevo){
        $this->nuevo = $nuevo;
    }
    public function set_edit($edit){
        $this->edit = $edit;
    }
    public function set_elim($elim){
        $this->elim = $elim;
    }
    public function set_exportar($exportar){
        $this->exportar = $exportar;
    }

    function __construct()
    {
        //Esta es la que llama a la base de datos
        //parent::__construct();
        $this->conn = new BD();
    }


    public function getUser()
    {
        //Verificando si existe el usuario a logearse
        try {
            $user = $this->getUsuario();

            $queryUser = "SELECT u.id_usuario, u.id_rol, id_responsable as id_area, u.usuario, u.nombre, u.admin,
                             CONCAT_WS(' ', u.nombre, u.apepa, u.apema) AS nombrecompleto, u.correo,
                             u.sexo, u.img, DATE_FORMAT( u.fecha_ingreso	,  '%d/%m/%Y' ) AS fecha_ingreso,
                             u.imp, u.edit, u.elim, u.nuev, r.rol, c.link AS carpeta
					   FROM ws_usuario u
					   LEFT JOIN ws_rol r ON r.id = u.id_rol
					   LEFT JOIN ws_menu c ON u.id_carpeta = c.id
					  WHERE usuario = :usuario 
					    AND clave = '" . $this->getClave() . "'
					   AND u.activo = 1 LIMIT 1";
            $result = $this->conn->prepare($queryUser);
            $result->bindParam(":usuario", $user, PDO::PARAM_STR);
            $result->execute();
            return $result;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }

    public function getParametro($id)
    {
        //Verificando si existe el usuario a logearse
        try {
            $valor = 0;
            $queryUser = "SELECT valor
					   FROM ws_parametro u WHERE id_parametro = $id";
            $result = $this->conn->prepare($queryUser);
            //$result->bindParam(":usuario", $user, PDO::PARAM_STR);
            $result->execute();
            if($result->rowCount() >0 ){
                $rw = $result->fetch(PDO::FETCH_OBJ);
                $valor = $rw->valor;
            }
            return $valor;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }

    public function getLastIdByRol(){
        try {
            $id = 0;
            $queryMP = "SELECT MAX(id_junta_gobierno) as id
                    FROM t_junta_gobierno  LIMIT 1";
            $result = $this->conn->prepare($queryMP);
            $result->execute();
            if($result->rowCount() > 0){
                $rw = $result->fetch(PDO::FETCH_OBJ);
                $id = $rw->id;
            }
            return $id;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }

    public function getUserLock(){
        //Verificando si existe el usuario a logearse
        try {
            $queryUser = "SELECT id_usuario, id_rol, usuario, nombre,
                             CONCAT_WS(' ', nombre, apepa, apema) AS nombrecompleto, correo,
                              sexo, img, DATE_FORMAT( fecha_ingreso	,  '%d/%m/%Y' ) AS fecha_ingreso,
                              imp, edit, elim, nuev
					   FROM ws_usuario where id_usuario='".$this->getIdUsuario()."' and clave='".$this->getClave()."'
					   AND activo = 1";
            $result = $this->conn->prepare($queryUser);
            $result->execute();
            return $result;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }

    public function getRegbyid(){
        try {
            $queryMP = "SELECT id_usuario, id_rol, id_responsable, usuario, sexo, nombre, apepa, apema, correo,
                             imp, edit, elim, nuev, img, admin, activo
                    FROM ws_usuario WHERE id_usuario=".$this->getIdUsuario() ." LIMIT 1";
            $result = $this->conn->prepare($queryMP);
            $result->execute();
            return $result;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    /*
        public function getRegbyPW(){
            //Infromación de registro obtenida por el id
            $queryUser = "SELECT id_usuario, usuario FROM ws_usuario
                           WHERE id_usuario = ".$this->getIdUsuario()." AND clave='".$this->getClave()."' LIMIT 1";
            //Ejecutando la consulta
            $oUser = $this->_DB->query($queryUser) or trigger_error("Query Error: ".$this->_DB->error);
            //Regresando el resultado.
            $rows = $oUser->num_rows;
            return $rows;
        }*/

    public function getRegbyPW(){
        try {
            $queryMP = "SELECT id_usuario, usuario FROM ws_usuario
		               WHERE id_usuario = ".$this->getIdUsuario()." AND clave='".$this->getClave()."' LIMIT 1";
            //echo $queryMP;
            $result = $this->conn->prepare($queryMP);
            $result->execute();
            $registrosf = $result->rowCount();
            return $registrosf;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }

    public function getResponsableByUser(){
        try {
            $id_resp = 0;
            $queryMP = "SELECT id_responsable FROM f_usuario_responsable
		               WHERE id_usuario = ".$this->getIdUsuario()."  LIMIT 1";
            $result = $this->conn->prepare($queryMP);
            $result->execute();
            if($result->rowCount() > 0){
                $rw = $result->fetch(PDO::FETCH_OBJ);
                $id_resp = $rw->id_responsable;
            }
            return $id_resp;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }

    public function foundUser(){
        try {
            $queryMP = "SELECT usuario FROM ws_usuario WHERE usuario='".$this->getUsuario()."'";
            $result = $this->conn->prepare($queryMP);
            $result->execute();
            $registrosf = $result->rowCount();
            return $registrosf;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }

    public function checarRol_user(){
        try {
            $queryCM = "SELECT id_rol_usuario FROM ws_rol_usuario WHERE id_rol = '" . $this->getIdRol() . "' AND id_usuario='" . $this->getIdUsuario() . "'";
            $result = $this->conn->prepare($queryCM);
            $result->execute();
            $registrosf = $result->rowCount();
            return $registrosf;
        }
        catch (\PDOException $e){
            return "Error!: " . $e->getMessage();
        }
    }

    public function getRolUser(){
        try {
            $queryCM = "SELECT id_rol FROM ws_rol_usuario WHERE id_usuario='" . $this->getIdUsuario() . "'";
            $result = $this->conn->prepare($queryCM);
            $result->execute();
            return $result;
        }
        catch (\PDOException $e){
            return "Error!: " . $e->getMessage();
        }
    }

    public function foundUserConcidencia(){
        //Busca si existe un usuario con el nombre
        try{
            $queryUser = "SELECT usuario FROM ws_usuario WHERE usuario='".$this->getUsuario()."' AND id_usuario = '".$this->getIdUsuario()."'";
            //Ejecutando la consulta
            $result    = $this->conn->prepare($queryUser);
            $result->execute();
            //Regresando el resultado.
            $registrosf = $result->rowCount();
            return $registrosf;
        }
        catch (\PDOException $e){
            return "Error: ". $e->getMessage();
        }

    }

    public function getAllReg()
    {
        //Incio fin son para paginado
        $milimite = "";
        $condition = "";
        if ($this->get_limite() == 1){ $milimite = "LIMIT ".$this->get_inicio().", ".$this->get_fin();}

        $filtro = $this->get_filtro();

        if ($filtro != ""){
            $condition = " WHERE CONCAT_WS(' ', nombre, apepa, apema) LIKE '%$filtro%' ";
        }
        try {
            $queryUser = "SELECT id_usuario, usuario, CONCAT_WS(' ', nombre, apepa, apema) AS nombre, correo, activo, admin
                          FROM ws_usuario $condition ORDER BY id_usuario DESC ".$milimite;

            $result = $this->conn->prepare($queryUser);
            $result->execute();
            return $result;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }

    public function getAllRegAjax()
    {

        try {
            $queryUser = "SELECT id_usuario, usuario, CONCAT_WS(' ', nombre, apepa, apema) AS nombre, 
                                 correo, activo, admin
                          FROM ws_usuario ORDER BY id_usuario DESC";

            $result = $this->conn->prepare($queryUser);
            $result->execute();

            return $result;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }

    public function getPublicReg(){
        //Incio fin son para paginado
        try {
            $queryUsers = "SELECT id_usuario, usuario, CONCAT_WS(' ', nombre, apepa, apema) AS nombre, correo
                          FROM ws_usuario WHERE activo = 1 AND id_rol > 1 ORDER BY nombre ASC";

            $result = $this->conn->prepare($queryUsers);
            $result->execute();
            return $result;
        }catch (\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }

    }

    public function checarMenuUser(){
        try {
            $queryCM = "SELECT id_usuario_menu, imp, edit, elim, nuevo, exportar
                          FROM ws_usuario_menu
                         WHERE id_menu = '" . $this->get_id_menu() . "' AND id_usuario='" . $this->getIdUsuario() . "'";
            $result = $this->conn->prepare($queryCM);
            $result->execute();
            //echo $queryCM;
            return $result;
        }
        catch (\PDOException $e){
            return "Error!: " . $e->getMessage();
        }
    }

    public function insertReg($fecha){
        $correcto= 1;
        $sexo = $this->getSexo();
        if($sexo == 1){
            $img= "avatar5.png";
        }else{
            $img= "avatar2.png";
        }
        $exec = $this->conn->conexion();
        try {
            $queryMP = "INSERT INTO ws_usuario(id_rol, id_usuario_captura,
                                    usuario, clave, nombre,
                                    apepa, apema, correo, sexo,
                                    imp, edit, elim, 
                                    nuev,
                                    fecha_ingreso, img, admin, activo)
                             VALUES (".$this->getIdRol().", ".$this->getIdUsuarioCaptura().",
                                     '".$this->getUsuario()."', MD5('".$this->getClave()."'), '".$this->getNombre()."',
                                     '".$this->getApePa()."', '".$this->getApeMa()."', '".$this->getCorreo()."', ".$sexo.",
                                     '".$this->getImpresion()."', '".$this->getEditar()."', '".$this->getEliminar()."', 
                                     '".$this->getNuevoUsuario()."',
                                     '$fecha', '$img', ".$this->get_admin().", 1)";

            $result = $this->conn->prepare($queryMP);
            $exec->beginTransaction();
            $result->execute();
            if ($correcto == 1){
                //IMPORTANTE el last inserted tiene que ir antes del commit cuando son transacciones
                //$correcto= $conn->_connection->lastInsertId();
                $correcto= $exec->lastInsertId();
                $this->setIdUsuario($correcto);
            }
            $exec->commit();
            return $correcto;
        }
        catch(\PDOException $e)
        {
            $exec->rollBack();
            return "Error!: " . $e->getMessage();
        }
    }

    public function insertRegdtl(){
        $correcto   = 1;
        $exec = $this->conn->conexion();
        try {
            $insert_dtl ="INSERT INTO ws_rol_usuario (id_rol,  id_usuario) VALUES (".$this->getIdRol().", ".$this->getIdUsuario().")";
            $result = $this->conn->prepare($insert_dtl);
            $exec->beginTransaction();
            $result->execute();
            if ($correcto == 1){
                //IMPORTANTE el last inserted tiene que ir antes del commit cuando son transacciones
                //$correcto= $conn->_connection->lastInsertId();
                $correcto= $exec->lastInsertId();
            }
            $exec->commit();
            return $correcto;
        }
        catch(\PDOException $e)
        {
            $exec->rollBack();
            return "Error!: " . $e->getMessage();
        }
    }

    public function insertResponsable(){
        $correcto   = 1;
        $exec = $this->conn->conexion();
        try {
            $insert_dtl ="INSERT INTO f_usuario_responsable (id_usuario, id_responsable) VALUES (".$this->getIdUsuario().", ".$this->getIdResponsable().")";
            $result = $this->conn->prepare($insert_dtl);
            $exec->beginTransaction();
            $result->execute();
            if ($correcto == 1){
                //IMPORTANTE el last inserted tiene que ir antes del commit cuando son transacciones
                //$correcto= $conn->_connection->lastInsertId();
                $correcto= $exec->lastInsertId();
            }
            $exec->commit();
            return $correcto;
        }
        catch(\PDOException $e)
        {
            $exec->rollBack();
            return "Error!: " . $e->getMessage();
        }
    }

    public function insertRegdtluser(){
        $exec = $this->conn->conexion();
        try {
            $correcto   = 1;
            $insert_dtl ="INSERT INTO ws_usuario_menu(id_usuario, id_menu, imp,edit,elim,nuevo,exportar) 
                               VALUES (".$this->getIdUsuario().", ".$this->get_id_menu().", ".$this->get_imp().", 
                                        ".$this->get_edit().", ".$this->get_elim().", ".$this->get_nuevo().",
                                        ".$this->get_exportar().")";
//die($insert_dtl);
            $result = $this->conn->prepare($insert_dtl);
            $exec->beginTransaction();
            $result->execute();
            if ($correcto == 1){
                $correcto= $exec->lastInsertId();
            }
            $exec->commit();
            return $correcto;
        }
        catch (\PDOException $e){
            return "Error!: " . $e->getMessage();
        }

    }

    public function updateReg(){
        $correcto   = 1;
        $exec       = $this->conn->conexion();
        $sexo       = $this->getSexo();

        if($sexo == 1){
            $img= "avatar5.png";
        }else{
            $img= "avatar2.png";
        }

        try {
            $queryUpdate = "UPDATE ws_usuario
                           SET id_rol = '" . $this->getIdRol() . "', usuario = '" . $this->getUsuario() . "', sexo = '" . $sexo . "',
                               nombre = '" . $this->getNombre() . "', apepa='" . $this->getApePa() . "',
                               apema='" . $this->getApeMa() . "', correo='" . $this->getCorreo() . "',
                               imp='" . $this->getImpresion() . "', edit='" . $this->getEditar() . "',
                               elim='" . $this->getEliminar() . "', nuev='" . $this->getNuevoUsuario() . "', img = '$img',
                               admin = ".$this->get_admin()."
                         WHERE id_usuario = " . $this->getIdUsuario();
            $result = $this->conn->prepare($queryUpdate);
            $exec->beginTransaction();
            $result->execute();
            $exec->commit();
        }catch (\PDOException $e){
            $exec->rollBack();
            $correcto =  "Error!: " . $e->getMessage();
        }
        return $correcto;
    }

    public function updateRegacount(){
        $correcto   = 1;
        $exec       = $this->conn->conexion();
        $sexo = $this->getSexo();
        if($sexo == 1){
            $img= "avatar5.png";
        }else{
            $img= "avatar2.png";
        }
        try {
            $queryUpdate = "UPDATE ws_usuario
                           SET usuario = '" . $this->getUsuario() . "', sexo = '" . $sexo . "',
                               nombre = '" . $this->getNombre() . "', apepa='" . $this->getApePa() . "',
                               apema='" . $this->getApeMa() . "', correo='" . $this->getCorreo() . "', img = '$img'
                         WHERE id_usuario = " . $this->getIdUsuario();
            $result = $this->conn->prepare($queryUpdate);
            $exec->beginTransaction();
            $result->execute();
            $exec->commit();
            return $correcto;
        }catch (\PDOException $e){
            $exec->rollBack();
            return "Error!: " . $e->getMessage();
        }
    }

    public function updateRegwop($id_usuario,$usuario, $nombre, $apepat, $apemat, $correo, $sexo){
        if($sexo == 1){
            $img= "avatar5.png";
        }else{
            $img= "avatar2.png";
        }
        $correcto    = 1;
        $exec        = $this->conn->conexion();
        try {
            $queryUpdate = "UPDATE ws_usuario
                           SET usuario = '$usuario', sexo = $sexo, nombre = '$nombre', apepa='$apepat',
                               apema='$apemat', correo='$correo', img = '$img'
                         WHERE id_usuario = $id_usuario";
            $result = $this->conn->prepare($queryUpdate);
            $exec->beginTransaction();
            $result->execute();
            $exec->commit();
            return $correcto;
        }catch (\PDOException $e){
            $exec->rollBack();
            return "Error!: " . $e->getMessage();
        }
    }

    public function updateRegPW(){
        $correcto   = 1;
        $exec = $this->conn->conexion();
        try {

            $queryMP = "UPDATE ws_usuario
                           SET clave = '".$this->getNuevaClave()."'
                         WHERE id_usuario = ".$this->getIdUsuario();
            $result = $this->conn->prepare($queryMP);
            $exec->beginTransaction();
            $result->execute();
            $exec->commit();
        }
        catch(\PDOException $e)
        {
            $exec->rollBack();
            $correcto   = 0;
            print "Error!: " . $e->getMessage();
        }
        return $correcto;
    }

    public function updateStatus($tipo){
        $correcto   = 1;
        try {
            $queryMP = "UPDATE ws_usuario SET activo = $tipo
    				    WHERE id_usuario = ".$this->getIdUsuario();
            $result = $this->conn->prepare($queryMP);
            $result->execute();

            return $correcto;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }

    public function deleteReg(){
        $correcto   = 2;
        try {
            $queryMP = "DELETE FROM ws_usuario WHERE id_usuario = ".$this->getIdUsuario();
            $result = $this->conn->prepare($queryMP);
            $result->execute();

            return $correcto;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }

    public function deleteRegResponsable(){
        $correcto   = 2;
        try {
            $queryMP = "DELETE FROM f_usuario_responsable WHERE id_usuario = ".$this->getIdUsuario();
            $result = $this->conn->prepare($queryMP);
            $result->execute();

            return $correcto;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }

    public function deleteRegRM(){
        $correcto   = 2;
        try {
            $delete = "DELETE FROM ws_rol_usuario WHERE id_usuario = ".$this->getIdUsuario();

            $result = $this->conn->prepare($delete);
            $result->execute();

            return $correcto;
        }catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }

    public function deleteRegUsMenu(){
        $correcto   = 2;
        try {
            $delete     = "DELETE FROM ws_usuario_menu WHERE id_usuario = ".$this->getIdUsuario();

            $result = $this->conn->prepare($delete);
            $result->execute();

            return $correcto;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }

}
