<?php
//Incluyendo la conexión a la base de datos
require_once $dir_fc."connections/conn_data.php";
//require_once $dir_fc."connections/conn_config.php";

class cMovimientos extends BD
{
    private $id_movimiento;
    private $id_user;
    private $fecha;
    private $tipo_mov;
    private $id_registro;
    private $nombre_tabla;



    public function getId_movimiento(){
        return $this->id_movimiento;
    }
    public function setId_movimiento($id_movimiento){
        $this->id_movimiento = $id_movimiento;
    }
    public function getId_user(){
        return $this->id_user;
    }
    public function setId_user($id_user){
        $this->id_user = $id_user;
    }
    public function getFecha(){
        return $this->fecha;
    }
    public function setFecha($fecha){
        $this->fecha = $fecha;
    }
    public function getTipo_mov(){
        return $this->tipo_mov;
    }
    public function setTipo_mov($tipo_mov){
        $this->tipo_mov = $tipo_mov;
    }
    public function getId_registro(){
        return $this->id_movimiento;
    }
    public function setId_registro($id_registro){
        $this->id_registro = $id_registro;
    }
    public function getNombre_tabla(){
        return $this->id_movimiento;
    }
    public function setNombre_tabla($nombre_tabla){
        $this->nombre_tabla = $nombre_tabla;
    }
    


    function __construct()
    {
        //Esta es la que llama a la base de datos
        //parent::__construct();
        $this->conn = new BD();
    }
    public function rowCount()
    {
        $no = "";
        try {
            $queryUser = "SELECT COUNT(id_beneficiario) as count
                          FROM cat_beneficiario";
            //echo $queryUser;
            $result = $this->conn->prepare($queryUser);
            $result->execute();

            while($row = $result->fetch(PDO::FETCH_ASSOC)){
                $no =  $row['count'];
            }
            return $no;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    public function getAllRegAjax()
    {

        try {
            $queryUser = "SELECT id_movimiento, id_user, fecha, tipo_mov, id_registro, nombre_tabla
            FROM movimientos ORDER BY id_movimiento DESC";

            $result = $this->conn->prepare($queryUser);
            $result->execute();

            return $result;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    
    public function insertReg(){
        $correcto= 1;
        $exec = $this->conn->conexion();
        try {
            $queryMP = "INSERT INTO movimientos(id_user, fecha, tipo_mov, id_registro, nombre_tabla)
                             VALUES (   '".$this->id_user."',
                                        '".$this->fecha."',
                                        '".$this->tipo_mov."',
                                        '".$this->id_registro."',
                                        '".$this->nombre_tabla."'
                                       
                                     )";
           
            //lala
            $result = $this->conn->prepare($queryMP);
            $exec->beginTransaction();
            $result->execute();
            $exec->commit();
            return $correcto;
        }
        catch(\PDOException $e)
        {
            $exec->rollBack();
            return "Error!: " . $e->getMessage();
        }
    }

    public function getMatricula($id){
        $sqlCmd = "SELECT noempleado FROM cat_beneficiario WHERE id_beneficiario=".$id." LIMIT 1";
        try {
            $sqlPrepare = $this->conn->prepare($sqlCmd);
            $sqlPrepare->execute();
            return $sqlPrepare;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    public function getMAF($id){
        $sqlCmd = "SELECT matricula_activo FROM cat_activo_fijo WHERE id_activo=".$id." LIMIT 1";
        try {
            $sqlPrepare = $this->conn->prepare($sqlCmd);
            $sqlPrepare->execute();
            return $sqlPrepare;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    public function getUsuarioName($id){
        $sqlCmd = "SELECT usuario FROM ws_usuario WHERE id_usuario=".$id." LIMIT 1";
        try {
            $sqlPrepare = $this->conn->prepare($sqlCmd);
            $sqlPrepare->execute();
            return $sqlPrepare;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
}
