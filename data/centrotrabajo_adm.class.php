<?php
require_once $dir_fc."connections/conn_data.php";
require_once $dir_fc."data/centrotrabajo.class.php";

class cCentrotrabajo_adm extends BD
{

    function __construct() { $this->conn = new BD(); }

    public function getRegbyid($id){
        $sqlCmd = "SELECT id_centrotrabajo, cve_centro, area, tipo, activo FROM cat_centrotrabajo WHERE id_centrotrabajo=".$id." LIMIT 1";
        try {
            $sqlPrepare = $this->conn->prepare($sqlCmd);
            $sqlPrepare->execute();
            return $sqlPrepare;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }

    public function buscaClave(cCentrotrabajo $datos){
        // Busca si la clave ya existe en la tabla
        $resultado = 0;
        try {
            $sqlCmd = "SELECT id_centrotrabajo FROM cat_centrotrabajo WHERE cve_centro='".$datos->getClave()."'";
            $sqlPrepare = $this->conn->prepare($sqlCmd);
            $sqlPrepare->execute();
            $resultado = $sqlPrepare->rowCount();
        }
        catch(\PDOException $e)
        {
            $resultado = "Error!: " . $e->getMessage();
        }
        return $resultado;
    }

    public function buscaCoincidencia(cCentrotrabajo $datos){
        // Busca si la clave del concepto se repite en la tabla
        $resultado = 0;
        try{
            $sqlCmd = "SELECT id_centrotrabajo FROM cat_centrotrabajo WHERE clave='".$datos->getClave()."' AND id_centrotrabajo!=".$datos->getIdCentrotrabajo();
            $sqlPrepare = $this->conn->prepare($sqlCmd);
            $sqlPrepare->execute();
            $resultado = $sqlPrepare->rowCount();
        }
        catch (\PDOException $e){
            $resultado = "Error: ". $e->getMessage();
        }
        return $resultado;
    }

    public function getAllRegAjax()
    {
        $sqlCmd = "SELECT id_centrotrabajo, cve_centro, area, tipo, activo FROM cat_centrotrabajo ORDER BY id_centrotrabajo ASC";
        try {
            $result = $this->conn->prepare($sqlCmd);
            $result->execute();
            return $result;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }

    public function agregarRegistro(cCentrotrabajo $datos){
        $resultado = 0;
        $baseDatos = $this->conn->conexion();
        $sqlCmd = "INSERT INTO cat_centrotrabajo(cve_centro, area, tipo, activo) VALUES (".
                    "'".$datos->getClave()."'".
                    ",'".$datos->getDescripcion()."'".
                    ",'".$datos->getTipo()."'".
                    ",".$datos->getActivo().")";
        try {
            $sqlPrep = $this->conn->prepare($sqlCmd);
            $baseDatos->beginTransaction();
            $sqlPrep->execute();
            $resultado = $baseDatos->lastInsertId();
            $baseDatos->commit();
        }
        catch(\PDOException $e)
        {
            $baseDatos->rollBack();
            $resultado = "Error!: " . $e->getMessage();
        }
        return $resultado;
    }

    public function actualizaRegistro(cCentrotrabajo $datos){
        $resultado = 1;
        $conexionBd = $this->conn->conexion();
        $sqlCmd = "UPDATE cat_centrotrabajo". 
                        " SET cve_centro='".$datos->getClave()."'".
                        ",area='".$datos->getDescripcion()."'".
                        ",tipo='".$datos->getTipo()."'".
                        " WHERE id_centrotrabajo=".$datos->getIdCentrotrabajo();
        try {
            $sqlPrepare = $this->conn->prepare($sqlCmd);
            $conexionBd->beginTransaction();
            $sqlPrepare->execute();
            $conexionBd->commit();
        }catch (\PDOException $e){
            $conexionBd->rollBack();
            $resultado =  "Error!: " . $e->getMessage();
        }
        return $resultado;
    }

    public function updateStatus($activo, $id){
        $correcto = 1;
        $sqlCmd = "UPDATE cat_centrotrabajo SET activo=".$activo." WHERE id_centrotrabajo=".$id;
        try {
            $sqlPrepare = $this->conn->prepare($sqlCmd);
            $sqlPrepare->execute();
            return $correcto;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }

    public function deleteReg($id){
        $correcto = 2;
        $sqlCmd = "DELETE FROM cat_centrotrabajo WHERE id_centrotrabajo=".$id;
        try {
            $sqlPrepare = $this->conn->prepare($sqlCmd);
            $sqlPrepare->execute();
            return $correcto;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    public function getCounter()
    {
        $no = "";
        try {
            $queryUser = "SELECT COUNT(id_centrotrabajo) as count
                          FROM cat_centrotrabajo";
            //echo $queryUser;
            $result = $this->conn->prepare($queryUser);
            $result->execute();

            while($row = $result->fetch(PDO::FETCH_ASSOC)){
                $no =  $row['count'];
            }
            return $no;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
}
?>