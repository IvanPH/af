<?php
//Incluyendo la conexión a la base de datos
require_once $dir_fc."connections/conn_data.php";
//require_once $dir_fc."connections/conn_config.php";

class cConServiciosP extends BD
{
    private $conn;
    private $filtro;
    private $inicio;
    private $fin;
    private $limite;
    private $id_usuario_captura;
    private $id_rol;
    private $fecha_captura;
    private $orden;
    private $identa;
    private $numero;

    private $id_contrato;
    private $id_tipomovto;
    private $servicio;
    private $fecha;
    private $mes_afec;
    private $id_beneficiario;
    private $importe;
    private $no_fua;
    private $identificador;
    private $afecta_presu;
    private $observaciones;
    private $id_usuario;
    private $id_estatus;
    private $id_contrato_sec;
    private $id_contrato_sec_imp;

    private $id_cvepresu;
    private $id_centrotrabajo;
    private $imp_secuencia;
    private $mes;
    private $imp_mes;
    private $nombre_real;
    private $nombre;
    private $id_documento;
    private $ampliacion;
    private $id_historia;

    private $estatus_filtro;
    private $id_banco;
    private $cuenta;

    /**
     * @return mixed
     */
    public function getIdHistoria()
    {
        return $this->id_historia;
    }

    /**
     * @param mixed $id_historia
     */
    public function setIdHistoria($id_historia)
    {
        $this->id_historia = $id_historia;
    }

    /**
     * @return mixed
     */
    public function getAmpliacion()
    {
        return $this->ampliacion;
    }

    /**
     * @param mixed $ampliacion
     */
    public function setAmpliacion($ampliacion)
    {
        $this->ampliacion = $ampliacion;
    }



    /**
     * @return mixed
     */
    public function getIdDocumento()
    {
        return $this->id_documento;
    }

    /**
     * @param mixed $id_documento
     */
    public function setIdDocumento($id_documento)
    {
        $this->id_documento = $id_documento;
    }



    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }


    /**
     * @return mixed
     */
    public function getNombreReal()
    {
        return $this->nombre_real;
    }

    /**
     * @param mixed $nombre_real
     */
    public function setNombreReal($nombre_real)
    {
        $this->nombre_real = $nombre_real;
    }



    /**
     * @return mixed
     */
    public function getMes()
    {
        return $this->mes;
    }

    /**
     * @param mixed $mes
     */
    public function setMes($mes)
    {
        $this->mes = $mes;
    }

    /**
     * @return mixed
     */
    public function getImpMes()
    {
        return $this->imp_mes;
    }

    /**
     * @param mixed $imp_mes
     */
    public function setImpMes($imp_mes)
    {
        $this->imp_mes = $imp_mes;
    }


    /**
     * @return mixed
     */
    public function getIdServicioSecImp()
    {
        return $this->id_contrato_sec_imp;
    }

    /**
     * @param mixed $id_contrato_sec_imp
     */
    public function setIdServicioSecImp($id_contrato_sec_imp)
    {
        $this->id_contrato_sec_imp = $id_contrato_sec_imp;
    }



    /**
     * @return mixed
     */
    public function getIdCvepresu()
    {
        return $this->id_cvepresu;
    }

    /**
     * @param mixed $id_cvepresu
     */
    public function setIdCvepresu($id_cvepresu)
    {
        $this->id_cvepresu = $id_cvepresu;
    }

    /**
     * @return mixed
     */
    public function getIdCentrotrabajo()
    {
        return $this->id_centrotrabajo;
    }

    /**
     * @param mixed $id_centrotrabajo
     */
    public function setIdCentrotrabajo($id_centrotrabajo)
    {
        $this->id_centrotrabajo = $id_centrotrabajo;
    }

    /**
     * @return mixed
     */
    public function getImpSecuencia()
    {
        return $this->imp_secuencia;
    }

    /**
     * @param mixed $imp_secuencia
     */
    public function setImpSecuencia($imp_secuencia)
    {
        $this->imp_secuencia = $imp_secuencia;
    }



    /**
     * @return mixed
     */
    public function getIdServicioSec()
    {
        return $this->id_contrato_sec;
    }

    /**
     * @param mixed $id_contrato_sec
     */
    public function setIdServicioSec($id_contrato_sec)
    {
        $this->id_contrato_sec = $id_contrato_sec;
    }




    /**
     * @return mixed
     */
    public function getIdEstatus()
    {
        return $this->id_estatus;
    }

    /**
     * @param mixed $id_estatus
     */
    public function setIdEstatus($id_estatus)
    {
        $this->id_estatus = $id_estatus;
    }



    /**
     * @return mixed
     */
    public function getFiltro()
    {
        return $this->filtro;
    }

    /**
     * @param mixed $filtro
     */
    public function setFiltro($filtro)
    {
        $this->filtro = $filtro;
    }

    /**
     * @return mixed
     */
    public function getInicio()
    {
        return $this->inicio;
    }

    /**
     * @param mixed $inicio
     */
    public function setInicio($inicio)
    {
        $this->inicio = $inicio;
    }

    /**
     * @return mixed
     */
    public function getFin()
    {
        return $this->fin;
    }

    /**
     * @param mixed $fin
     */
    public function setFin($fin)
    {
        $this->fin = $fin;
    }

    /**
     * @return mixed
     */
    public function getLimite()
    {
        return $this->limite;
    }

    /**
     * @param mixed $limite
     */
    public function setLimite($limite)
    {
        $this->limite = $limite;
    }

    /**
     * @return mixed
     */
    public function getIdUsuarioCaptura()
    {
        return $this->id_usuario_captura;
    }

    /**
     * @param mixed $id_usuario_captura
     */
    public function setIdUsuarioCaptura($id_usuario_captura)
    {
        $this->id_usuario_captura = $id_usuario_captura;
    }

    /**
     * @return mixed
     */
    public function getIdRol()
    {
        return $this->id_rol;
    }

    /**
     * @param mixed $id_rol
     */
    public function setIdRol($id_rol)
    {
        $this->id_rol = $id_rol;
    }

    /**
     * @return mixed
     */
    public function getFechaCaptura()
    {
        return $this->fecha_captura;
    }

    /**
     * @param mixed $fecha_captura
     */
    public function setFechaCaptura($fecha_captura)
    {
        $this->fecha_captura = $fecha_captura;
    }

    /**
     * @return mixed
     */
    public function getOrden()
    {
        return $this->orden;
    }

    /**
     * @param mixed $orden
     */
    public function setOrden($orden)
    {
        $this->orden = $orden;
    }

    /**
     * @return mixed
     */
    public function getIdenta()
    {
        return $this->identa;
    }

    /**
     * @param mixed $identa
     */
    public function setIdenta($identa)
    {
        $this->identa = $identa;
    }

    /**
     * @return mixed
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * @param mixed $numero
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;
    }

    /**
     * @return mixed
     */
    public function getIdServicio()
    {
        return $this->id_contrato;
    }

    /**
     * @param mixed $id_contrato
     */
    public function setIdServicio($id_contrato)
    {
        $this->id_contrato = $id_contrato;
    }

    /**
     * @return mixed
     */
    public function getIdTipomovto()
    {
        return $this->id_tipomovto;
    }

    /**
     * @param mixed $id_tipomovto
     */
    public function setIdTipomovto($id_tipomovto)
    {
        $this->id_tipomovto = $id_tipomovto;
    }

    /**
     * @return mixed
     */
    public function getServicio()
    {
        return $this->servicio;
    }

    /**
     * @param mixed $servicio
     */
    public function setServicio($servicio)
    {
        $this->servicio = $servicio;
    }

    /**
     * @return mixed
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * @param mixed $fecha
     */
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;
    }

    /**
     * @return mixed
     */
    public function getMesAfec()
    {
        return $this->mes_afec;
    }

    /**
     * @param mixed $mes_afec
     */
    public function setMesAfec($mes_afec)
    {
        $this->mes_afec = $mes_afec;
    }

    /**
     * @return mixed
     */
    public function getIdBeneficiario()
    {
        return $this->id_beneficiario;
    }

    /**
     * @param mixed $id_beneficiario
     */
    public function setIdBeneficiario($id_beneficiario)
    {
        $this->id_beneficiario = $id_beneficiario;
    }

    /**
     * @return mixed
     */
    public function getImporte()
    {
        return $this->importe;
    }

    /**
     * @param mixed $importe
     */
    public function setImporte($importe)
    {
        $this->importe = $importe;
    }

    /**
     * @return mixed
     */
    public function getNoFua()
    {
        return $this->no_fua;
    }

    /**
     * @param mixed $no_fua
     */
    public function setNoFua($no_fua)
    {
        $this->no_fua = $no_fua;
    }

    /**
     * @return mixed
     */
    public function getIdentificador()
    {
        return $this->identificador;
    }

    /**
     * @param mixed $identificador
     */
    public function setIdentificador($identificador)
    {
        $this->identificador = $identificador;
    }

    /**
     * @return mixed
     */
    public function getAfectaPresu()
    {
        return $this->afecta_presu;
    }

    /**
     * @param mixed $afecta_presu
     */
    public function setAfectaPresu($afecta_presu)
    {
        $this->afecta_presu = $afecta_presu;
    }

    /**
     * @return mixed
     */
    public function getObservaciones()
    {
        return $this->observaciones;
    }

    /**
     * @param mixed $observaciones
     */
    public function setObservaciones($observaciones)
    {
        $this->observaciones = $observaciones;
    }

    /**
     * @return mixed
     */
    public function getIdUsuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param mixed $id_usuario
     */
    public function setIdUsuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;
    }

    /**
     * @return mixed
     */
    public function getEstatusFiltro()
    {
        return $this->estatus_filtro;
    }

    /**
     * @param mixed $estatus_filtro
     */
    public function setEstatusFiltro($estatus_filtro)
    {
        $this->estatus_filtro = $estatus_filtro;
    }



    function __construct()
    {
        //Esta es la que llama a la base de datos
        //parent::__construct();
        $this->conn = new BD();
    }

    /*public function getPublicEstatusFilter(){
        try {
            $queryUser = "SELECT id_estatus, estatus as nombre, CONCAT(class, ' ', class_color) as class
                        FROM cat_estatus WHERE activo = 1 ORDER BY id_estatus ASC";
            //echo $queryUser;
            $result = $this->conn->prepare($queryUser);
            $result->execute();

            return $result;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }*/


    public function getCounter()
    {
        $no = "";
        try {
            $queryUser = "SELECT COUNT(id_presupuesto) as count
                          FROM tbl_presupuesto";
            //echo $queryUser;
            $result = $this->conn->prepare($queryUser);
            $result->execute();

            while($row = $result->fetch(PDO::FETCH_ASSOC)){
                $no =  $row['count'];
            }
            return $no;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }

    public function getCounterPresupuesto()
    {
        $no = "";
        try {
            $queryUser = "SELECT COUNT(id_presupuesto) as count
                          FROM tbl_presupuesto";
            //echo $queryUser;
            $result = $this->conn->prepare($queryUser);
            $result->execute();

            while($row = $result->fetch(PDO::FETCH_ASSOC)){
                $no =  $row['count'];
            }
            return $no;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }

    public function getCounterServicio($were=false)
    {
        $no = "";
        try {
            if($were)
            {
                $queryUser = "SELECT COUNT(id_contrato) as count
                FROM tbl_contrato where id_tipomovto='$were'";
            }
           else{
            $queryUser = "SELECT COUNT(id_contrato) as count
            FROM tbl_contrato";
           }
            //echo $queryUser;
            $result = $this->conn->prepare($queryUser);
            $result->execute();

            while($row = $result->fetch(PDO::FETCH_ASSOC)){
                $no =  $row['count'];
            }
            return $no;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    

    public function getAllRegAjaxPresupuesto()
    {
        $condition = "";

        if ($this->getFiltro() != ""){
            /*
            if($this->getFechaIni() != "" AND $this->getFechaFin() != ""){
                $condition.=   " AND DATE_FORMAT (fecha, '%Y-%m-%d') between '".$this->getFechaIni()."' and '".$this->getFechaFin()."'";
            }

            if(is_array($this->getIdEstado())){
                $str3 = implode(",", $this->getIdEstado());
                $condition.= " AND r.id_estado IN (".$str3.")";
            }
            */

            if($this->getDescripcion() != ""){
                $condition.= " AND r.descripcion like '%".$this->getDescripcion(). "%' ";
            }

        }

        if($this->getIdRol() == 3){
            $condition.= " AND j.activo = 1";
        }

        /*if(is_array($this->getEstatusFiltro())){
            $str3 = implode(",", $this->getEstatusFiltro());
            $condition.= " AND r.id_estatus IN (".$str3.")";
        }else{
            $arrayDefault = array(1, 2, 3);
            $str4 = implode(",", $arrayDefault);
            $condition.= " AND r.id_estatus IN (".$str4.")";
        }
        */
        try {
            $queryUser = "SELECT id_presupuesto, id_clavepresu, concepto, mes, asig_orig, adi_aut, 
                                 amp_aut, red_aut, com_sol, com_aut, ejer_sol, ejer_dev, ejer_pag
                            FROM tbl_presupuesto as  p
                    WHERE 1 = 1 $condition
                    ORDER BY f.id_presupuesto ASC";
            //echo $queryUser;
            $result = $this->conn->prepare($queryUser);
            $result->execute();

            return $result;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }

    public function getAllRegAjaxCotrato($filtro,
                                         $descripcion_fil,
                                         $no_contrato,
                                         $id_contrato_fil,
                                         $id_beneficiario_fil,
                                         $fecha_ini,
                                         $fecha_fin,
                                         $id_estatus,
                                         $rol,
                                         $tipo_mov,
                                         $id_usuario)
    {
        $condition = "";
        
        if ($filtro != ""){
            if($fecha_ini != "" AND $fecha_fin != ""){
                $condition.= " AND DATE_FORMAT (s.fecha, '%Y-%m-%d') between '".$fecha_ini."' and '".$fecha_fin."'";
            }
            if(is_array($id_beneficiario_fil)){
                $str3 = implode(",", $id_beneficiario_fil);
                $condition.= " AND s.id_beneficiario IN (".$str3.")";
            }
            if($no_contrato != ""){
                $condition.= " AND s.no_contrato like '%".$no_contrato. "%' ";
            }
            if($descripcion_fil != ""){
                $condition.= " AND s.descripcion like '%".$descripcion_fil. "%' ";
            }
            if($id_contrato_fil != ""){
                $condition.= " AND no_fua like '%".$id_contrato_fil. "%' ";
            }
           

        }

        if($tipo_mov > 0){
            $condition.= " AND s.id_tipomovto = ".$tipo_mov;
        }
       
        if($id_estatus == 1){
            //Los estatus que pueden ver son
            $arrEstatus = array(1,2,3,4,5,6);
        }else{
            $arrEstatus = array(2);
        }
        try {
            $queryUser = "SELECT id_contrato, 
                                 s.id_tipomovto, m.tipomovto,
                                 no_contrato, 
                                 descripcion,
                                 iva,
                                 isr,
                                 id_estado,
                                 iva_retenido,
                                 s.saldo,
                                 DATE_FORMAT(s.fecha, '%d/%m/%Y' ) as fecha, 
                                 DATE_FORMAT(s.fecha_ini, '%d/%m/%Y' ) as fecha_ini, 
                                 DATE_FORMAT(s.fecha_fin, '%d/%m/%Y' ) as fecha_fin, 
                                 mes_afec, 
                                 s.id_beneficiario,
                                 CONCAT_WS(' ', b.nombre, b.apepa, b.apema) as beneficiario,
                                 b.rfc,
                                 importe, 
                                 importe_total, 
                                 no_fua, identificador, s.id_estatus, 
                                 e.estatus, class,
                                 afecta_presu, observaciones, s.id_usuario
                            FROM tbl_contrato as  s
                    LEFT JOIN  cat_estatus as e on e.id_estatus = s.id_estatus
                    LEFT JOIN  cat_tipomovto as m on m.id_tipomovto = s.id_tipomovto
                    LEFT JOIN  cat_beneficiario as b on b.id_beneficiario = s.id_beneficiario
                    LEFT JOIN  ws_usuario as u on s.id_usuario = u.id_usuario
                    WHERE 1 = 1 $condition 	AND $id_usuario s.id_estatus in (".implode(",", $arrEstatus).")
                    ORDER BY s.id_contrato ASC";
            //echo $queryUser;
            $result = $this->conn->prepare($queryUser);
            $result->execute();
        
            return $result;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    public function getAllRegAjaxSolPago($filtro,
                                         $descripcion_fil,
                                         $no_contrato,
                                         $id_contrato_fil,
                                         $id_beneficiario_fil,
                                         $fecha_ini,
                                         $fecha_fin,
                                         $id_estatus,
                                         $rol,
                                         $tipo_mov,
                                         $id_usuario)
    {
        $condition = "";

        if ($filtro != ""){
            if($fecha_ini != "" AND $fecha_fin != ""){
                $condition.= " AND DATE_FORMAT (fecha, '%Y-%m-%d') between '".$fecha_ini."' and '".$fecha_fin."'";
            }
            if(is_array($id_beneficiario_fil)){
                $str3 = implode(",", $id_beneficiario_fil);
                $condition.= " AND s.id_beneficiario IN (".$str3.")";
            }
            if($no_contrato != ""){
                $condition.= " AND s.no_contrato like '%".$no_contrato. "%' ";
            }
            if($descripcion_fil != ""){
                $condition.= " AND s.descripcion like '%".$descripcion_fil. "%' ";
            }
            if($id_contrato_fil != ""){
                $condition.= " AND s.id_contrato like '%".$id_contrato_fil. "%' ";
            }

        }

        if($tipo_mov > 0){
            $condition.= " AND s.id_tipomovto = ".$tipo_mov;
        }

        if($id_estatus == 1){
            //Los estatus que pueden ver son
            $arrEstatus = array(1,3,4);
        }else{
            $arrEstatus = array(2);
        }
        try {
            $queryUser = "SELECT id_contrato, 
                                 s.id_tipomovto, m.tipomovto,
                                 no_contrato, 
                                 descripcion,
                                 iva,
                                 isr,
                                 id_estado,
                                 iva_retenido,
                                 b.rfc,
                                 DATE_FORMAT(s.fecha, '%d/%m/%Y' ) as fecha, 
                                 DATE_FORMAT(s.fecha_ini, '%d/%m/%Y' ) as fecha_ini, 
                                 DATE_FORMAT(s.fecha_fin, '%d/%m/%Y' ) as fecha_fin, 
                                 mes_afec, anio,
                                 s.id_beneficiario, 
                                 CONCAT_WS(' ', b.nombre, b.apepa, b.apema) as beneficiario,
                                 importe, importe_total, abonos, saldo,
                                 no_fua, identificador, s.id_estatus, 
                                 e.estatus, class,
                                 afecta_presu, observaciones, s.id_usuario
                            FROM tbl_contrato as  s
                    LEFT JOIN  cat_estatus as e on e.id_estatus = s.id_estatus
                    LEFT JOIN  cat_tipomovto as m on m.id_tipomovto = s.id_tipomovto
                    LEFT JOIN  cat_beneficiario as b on b.id_beneficiario = s.id_beneficiario
                    LEFT JOIN  ws_usuario as u on s.id_usuario = u.id_usuario
                    WHERE 1 = 1 $condition AND $id_usuario s.id_estatus in (".implode(",", $arrEstatus).")
                   
                    ORDER BY s.id_contrato ASC";
            //echo $queryUser;
            $result = $this->conn->prepare($queryUser);
            $result->execute();

            return $result;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    public function getAllRegAjaxPago($filtro,
                                         $descripcion_fil,
                                         $no_contrato,
                                         $id_contrato_fil,
                                         $id_beneficiario_fil,
                                         $fecha_ini,
                                         $fecha_fin,
                                         $id_estatus,
                                         $rol,
                                         $tipo_mov)
    {
        $condition = "";

        if ($filtro != ""){
            if($fecha_ini != "" AND $fecha_fin != ""){
                $condition.= " AND DATE_FORMAT (s.fecha, '%Y-%m-%d') between '".$fecha_ini."' and '".$fecha_fin."'";
            }
            if(is_array($id_beneficiario_fil)){
                $str3 = implode(",", $id_beneficiario_fil);
                $condition.= " AND s.id_beneficiario IN (".$str3.")";
            }
            if($no_contrato != ""){
                $condition.= " AND s.no_contrato like '%".$no_contrato. "%' ";
            }
            if($descripcion_fil != ""){
                $condition.= " AND s.descripcion like '%".$descripcion_fil. "%' ";
            }
            if($id_contrato_fil != ""){
                $condition.= " AND s.id_contrato like '%".$id_contrato_fil. "%' ";
            }

        }

        if($tipo_mov > 0){
            $condition.= " AND s.id_tipomovto = ".$tipo_mov;
        }

        if($id_estatus == 1){
            //Los estatus que pueden ver son
            $arrEstatus = array(1);
        }else{
            $arrEstatus = array(2,3);
        }
        try {
            $queryUser = "SELECT id_contrato, 
                                 s.id_tipomovto, m.tipomovto,
                                 no_contrato, 
                                 descripcion,
                                 iva,
                                 isr,
                                 id_estado,
                                 iva_retenido,
                                 b.rfc,
                                 DATE_FORMAT(s.fecha, '%d/%m/%Y' ) as fecha, 
                                 DATE_FORMAT(s.fecha_ini, '%d/%m/%Y' ) as fecha_ini, 
                                 DATE_FORMAT(s.fecha_fin, '%d/%m/%Y' ) as fecha_fin, 
                                 mes_afec, anio,
                                 s.id_beneficiario, 
                                 CONCAT_WS(' ', b.nombre, b.apepa, b.apema) as beneficiario,
                                 importe, importe_total, abonos, saldo,
                                 no_fua, identificador, s.id_estatus, 
                                 e.estatus, class,
                                 afecta_presu, observaciones, s.id_usuario
                            FROM tbl_contrato as  s
                    LEFT JOIN  cat_estatus as e on e.id_estatus = s.id_estatus
                    LEFT JOIN  cat_tipomovto as m on m.id_tipomovto = s.id_tipomovto
                    LEFT JOIN  cat_beneficiario as b on b.id_beneficiario = s.id_beneficiario
                    LEFT JOIN  ws_usuario as u on s.id_usuario = u.id_usuario
                    WHERE 1 = 1 $condition AND s.id_estatus in (".implode(",", $arrEstatus).")
                   
                    ORDER BY s.id_contrato ASC";
            //echo $queryUser;
            $result = $this->conn->prepare($queryUser);
            $result->execute();

            return $result;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    public function getPublicPre()
        {
        try
            {
            $queryMP = "SELECT id_suficiencia, numero_pre
                    FROM tbl_suficiencia WHERE id_tipomovto = 11 ";
            // echo $queryMP;
            $result  = $this->conn->prepare($queryMP);
            $result->execute();
            return $result;
            }
        catch (\PDOException $e)
            {
            return "Error!: " . $e->getMessage();
            }
        }
    public function getRegbyidContrato($id_contrato){
        try {
            $queryMP = "SELECT id_contrato, id_tipomovto, id_estado, 
                               descripcion, no_contrato, 
                               iva_retenido, iva, isr,
                               DATE_FORMAT(fecha, '%d/%m/%Y' ) as fecha, 
                               DATE_FORMAT(fecha_ini, '%d/%m/%Y' ) as fecha_ini, 
                               DATE_FORMAT(fecha_fin, '%d/%m/%Y' ) as fecha_fin, 
                               mes_afec, anio, id_beneficiario, 
                               importe,importe_total, saldo, abonos, no_fua, identificador, 
                               id_estatus, afecta_presu, 
                               observaciones, justificacion, actividades, objetivo, id_usuario,
                               id_banco, cuenta, id_suficiencia
                    FROM tbl_contrato as e
                    WHERE id_contrato = ".$id_contrato ." LIMIT 1";
            //echo $queryMP;
            $result = $this->conn->prepare($queryMP);
            $result->execute();
            return $result;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    public function getContrato($id_contrato){
        try {
            $queryMP = "
            SELECT
            c.id_contrato,
            c.descripcion,
            c.no_contrato,
            DATE_FORMAT( c.fecha, '%d/%m/%Y' ) AS fecha,
            DATE_FORMAT( c.fecha_ini, '%d/%m/%Y' ) AS fecha_ini,
            DATE_FORMAT( c.fecha_fin, '%d/%m/%Y' ) AS fecha_fin,
            c.mes_afec,
            c.anio,
            c.id_beneficiario,
            c.importe,
            c.importe_total,
            c.no_fua,
            c.observaciones,
            c.justificacion,
            c.actividades,
            c.objetivo,
            c.id_banco,
            c.cuenta,
           
            b.*
        FROM
            tbl_contrato AS c,
            cat_beneficiario as b
        WHERE
        c.id_beneficiario=b.id_beneficiario
        and
            c.id_contrato =$id_contrato 
            LIMIT 1
            ";
            //echo $queryMP;
            $result = $this->conn->prepare($queryMP);
            $result->execute();
            return $result;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    public function getPagoContrato($id_contrato){
        try {
            $queryMP = "
            select 
            *
            FROM
            tbl_contrato_sec_imp
            WHERE
            id_contrato_sec=$id_contrato
            AND
            imp_mes>0
            ";
            //echo $queryMP;
            $result = $this->conn->prepare($queryMP);
            $result->execute();
            return $result;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    public function getDataByAprovacion($id_contrato){
        try {
            $queryMP = "SELECT s.id_tipomovto, mes_afec, imp_secuencia,
                          sec.id_cvepresu,
                          CASE
                          WHEN sec.ampliacion = 1 THEN m.ampliacion
                          WHEN sec.ampliacion = 0 THEN m.reduccion
                          ELSE '-'
                          END as campo
                        FROM tbl_contrato as s
                          LEFT JOIN tbl_contrato_sec as sec ON s.id_contrato = sec.id_contrato
                          LEFT JOIN cat_tipomovto as m ON m.id_tipomovto = s.id_tipomovto
                        WHERE s.id_contrato = $id_contrato";
            // echo $queryMP;
            $result = $this->conn->prepare($queryMP);
            $result->execute();
            return $result;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }

    public function getDataByAprovacionDtl($id_contrato){
        try {
            $queryMP = "SELECT s.id_tipomovto, seci.mes as mes_afec, imp_mes as imp_secuencia,
                          sec.id_cvepresu,	m.ampliacion ,
			m.reduccion, 
            cve.partida,
            cve.ff,
            s.id_beneficiario,
            s.no_contrato,
            s.importe_total,
	CONCAT_WS(' ',ben.nombre,ben.apepa,ben.apema) as nombre,
                          CASE
                          WHEN sec.ampliacion = 1 THEN m.ampliacion
                          WHEN sec.ampliacion = 0 THEN m.reduccion
                          ELSE '-'
                          END as campo
                        FROM tbl_contrato_sec_imp as seci
                        LEFT JOIN tbl_contrato_sec as sec ON sec.id_contrato_sec = seci.id_contrato_sec
                        LEFT JOIN tbl_contrato as s ON s.id_contrato = sec.id_contrato
                        LEFT JOIN cat_tipomovto as m ON m.id_tipomovto = s.id_tipomovto
                        LEFT JOIN cat_clavepresu AS cve ON cve.id_clavepresu = sec.id_cvepresu 
                        LEFT JOIN cat_beneficiario AS ben ON s.id_beneficiario=ben.id_beneficiario
                        WHERE s.id_contrato = $id_contrato";
            //echo $queryMP;
            $result = $this->conn->prepare($queryMP);
            $result->execute();
            return $result;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    public function getMomento($id_momento,$ptda,$ff){
        try {
            $queryMP = "
            SELECT
            id_cta_cargo as cargo,
            id_cta_abono as abono,
            operacion_c,
            operacion_a
            FROM
            tbl_contabilidad_matriz
            WHERE
            id_momento='$id_momento'
            AND
            ptda=$ptda
            AND
            ff=$ff
            ";
           //echo $queryMP;
            $result = $this->conn->prepare($queryMP);
            $result->execute();
            return $result;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    public function getPoliza($id_poliza){
        try {
            $queryMP = "
            SELECT
           *
        FROM
            tbl_poliza
        WHERE
        
            id_poliza =$id_poliza 
            LIMIT 1
            ";
            //echo $queryMP;
            $result = $this->conn->prepare($queryMP);
            $result->execute();
            return $result;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    public function getPolizaDetalle($id_poliza){
        try {
            $queryMP = "
            SELECT
           *
        FROM
            tbl_poliza_det
        WHERE
        
            id_poliza =$id_poliza 
          
            ";
            //echo $queryMP;
            $result = $this->conn->prepare($queryMP);
            $result->execute();
            return $result;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    public function insertaPoliza(
            $id_usuario_captura,
            $id_estatus_poliza,
            $fecha_captura,
            $fecha,
            $tipo,
            $concepto,
            $importe,
            $activo){
        $correcto= 1;

        $exec = $this->conn->conexion();
        try {
        $queryMP = "INSERT INTO tbl_poliza(id_usuario_captura,id_estatus_poliza,fecha_captura,
                                            fecha,tipo,concepto,importe,activo)
        VALUES (?,?,?,?,?,?,?,?)";
        $result = $this->conn->prepare($queryMP);
        $exec->beginTransaction();
        $result->execute(array(
            $id_usuario_captura,
            $id_estatus_poliza,
            $fecha_captura,
            $fecha,
            $tipo,
            $concepto,
            $importe,
            $activo));


        if ($correcto == 1){
        $correcto= $exec->lastInsertId();
        }
        $exec->commit();

        return $correcto;

        }
        catch(\PDOException $e)
        {
        $exec->rollBack();

        return "Error!: " . $e->getMessage();
        }
    }
    public function insertaPolizaDetalle(
        $id_poliza,
        $id_cuenta,
        $id_usuario_captura,
        $fecha_captura,
        $descripcion,
        $cargo,
        $abono,
        $activo){
    $correcto= 1;

    $exec = $this->conn->conexion();
    try {
    $queryMP = "INSERT INTO tbl_poliza_det(
        id_poliza,
        id_cuenta,
        id_usuario_captura,
        fecha_captura,
        descripcion,
        cargo,
        abono,
        activo
    )
    VALUES (?,?,?,?,?,?,?,?)";
    $result = $this->conn->prepare($queryMP);
    $exec->beginTransaction();
    $result->execute(array(
        $id_poliza,
        $id_cuenta,
        $id_usuario_captura,
        $fecha_captura,
        $descripcion,
        $cargo,
        $abono,
        $activo));


    if ($correcto == 1){
    $correcto= $exec->lastInsertId();
    }
    $exec->commit();

    return $correcto;

    }
    catch(\PDOException $e)
    {
    $exec->rollBack();

    return "Error!: " . $e->getMessage();
    }
}
    public function getRegbyidServicioSec($id_contrato){
        try {
            $mes=date("m");
            $queryMP = "
            SELECT
            e.id_contrato_sec,
            e.id_contrato,
            e.id_cvepresu,
            e.ampliacion,
            e.id_centrotrabajo,
            e.imp_secuencia,
            imp.mes,
            imp.imp_mes,
            con.anio
            FROM
                tbl_contrato_sec AS e,
                tbl_contrato_sec_imp as imp,
                tbl_contrato as con
            WHERE
            e.id_contrato_sec=imp.id_contrato_sec
            AND
            e.id_contrato=con.id_contrato
            AND
                e.id_contrato = $id_contrato
            AND
                imp.mes <= $mes
            AND
                imp.imp_mes>0
            ";
           //echo $queryMP;
            $result = $this->conn->prepare($queryMP);
            $result->execute();
            return $result;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    public function getReportePago(){
        try {
            $mes=date("m");
            $queryMP = "
            SELECT
            pago.*,
            con.id_banco,
            con.cuenta,
            con.no_contrato,
            ban.nombre as banco,
            CONCAT_WS(' ', ben.nombre, ben.apepa, ben.apema) as nombre
            FROM
            tbl_contrato_pago as pago,
            tbl_contrato as con,
            cat_bancos as ban,
            cat_beneficiario as ben
            WHERE
            pago.id_contrato=con.id_contrato
            AND
            ban.id_banco= con.id_banco
            AND
            con.id_beneficiario= ben.id_beneficiario
            AND
            pago.activo=1
            AND
            pago.pagado=0
            ";
           //echo $queryMP;
            $result = $this->conn->prepare($queryMP);
            $result->execute();
            return $result;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    
    public function getPagoMes($id_contrato,$id_cvepresu,$imp_mes,$mes,$anio){
        try {
            $activo = true;
            $queryMP = "SELECT *
                          FROM tbl_contrato_pago as e
                         WHERE id_contrato = ".$id_contrato."
                            AND id_clavepresu = ".$id_cvepresu."
                            AND cantidad = ".$imp_mes."
                            AND mes = ".$mes."
                            AND anio = ".$anio;
            //echo $queryMP;
            $result = $this->conn->prepare($queryMP);
            $result->execute();
            $row = $result->fetchAll();
           
            if(count($row)>0)
            {
                $activo= false;
            }
       
    
            return $activo;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    public function getRegbyidServicioSecImp($id_contrato_sec){
        try {
            $arrMes = array();
            $queryMP = "SELECT id_contrato_sec_imp ,id_contrato_sec,
                               mes, imp_mes
                          FROM tbl_contrato_sec_imp as e
                         WHERE id_contrato_sec = ".$id_contrato_sec;
          
            //echo $queryMP;
            $result = $this->conn->prepare($queryMP);
            $result->execute();
            while($rw = $result->fetch(PDO::FETCH_OBJ)){
                $arrMes[$rw->mes] = $rw->imp_mes;
            }
            return $arrMes;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }


    public function getRegDocumentosEvento(){
        try {
            $queryMP = "SELECT id_documento, nombre, nombre_real, activo
                    FROM t_documento as  aa 
                    WHERE id_evento= ".$this->getIdEvento()." 
                      AND id_financiamiento = 0
                      AND activo = 1";
            $result = $this->conn->prepare($queryMP);
            $result->execute();

            return $result;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }


    public function getRegHistorial($id_contrato){
        try {
            $queryMP = "SELECT id_historia, id_estatus, 
                               aa.id_usuario_captura, 
                              DATE_FORMAT(fecha_captura, '%d/%m/%Y %H:%i' ) as fecha_captura,
                               CONCAT_WS(' ', u.nombre, u.apepa, u.apema) as captura, observaciones
                    FROM tbl_contrato_historia as  aa 
                      LEFT JOIN  ws_usuario as u on aa.id_usuario_captura = u.id_usuario
                    WHERE id_contrato = ".$id_contrato;
            $result = $this->conn->prepare($queryMP);
            $result->execute();

            return $result;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }

    public function getRegHistorialPago($id_contrato){
        try {
            $queryMP = "SELECT id_pago, aa.id_clavepresu, aa.id_usuario, cve.clave,
                              DATE_FORMAT(fecha_captura, '%d/%m/%Y' ) as fecha_captura,
                              CONCAT_WS(' ', u.nombre, u.apepa, u.apema) as captura,
                              cantidad,folio_fact,
                              mes,aa.anio,aa.activo,aa.pagado
                    FROM tbl_contrato_pago as  aa 
                      LEFT JOIN  cat_clavepresu as cve on cve.id_clavepresu = aa.id_clavepresu
                      LEFT JOIN  ws_usuario as u on aa.id_usuario = u.id_usuario
                    WHERE id_contrato = ".$id_contrato;
            //echo $queryMP;        
            $result = $this->conn->prepare($queryMP);
            $result->execute();

            return $result;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    public function getTotalPAgos($id_contrato){
        try {
            $queryMP = "SELECT
            sum( cantidad ) importe
        FROM
            tbl_contrato_pago AS aa
            LEFT JOIN cat_clavepresu AS cve ON cve.id_clavepresu = aa.id_clavepresu
            LEFT JOIN ws_usuario AS u ON aa.id_usuario = u.id_usuario 
        WHERE
            id_contrato =".$id_contrato." 
        AND
            aa.activo=0";
            //echo $queryMP;        
            $result = $this->conn->prepare($queryMP);
            $result->execute();

            return $result;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    public function getTotalPAgos2($id_contrato){
        try {
            $queryMP = "SELECT
            sum( cantidad ) importe
        FROM
            tbl_contrato_pago AS aa
            LEFT JOIN cat_clavepresu AS cve ON cve.id_clavepresu = aa.id_clavepresu
            LEFT JOIN ws_usuario AS u ON aa.id_usuario = u.id_usuario 
        WHERE
            id_contrato =".$id_contrato." 
        AND
            aa.activo=1
        AND
            aa.pagado=0";
            //echo $queryMP;        
            $result = $this->conn->prepare($queryMP);
            $result->execute();

            return $result;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    /**
     * Regresa el id del estatus con que se inicia
     * Last modify ... 12 / 11 / 2017
     */
    public function getIdEstatusByInicio(){
        try {
            $id_estatus = 0;

            $queryMP = "SELECT id_estatus
                          FROM cat_estatus
                         WHERE inicia = 1 LIMIT 1";
            $result = $this->conn->prepare($queryMP);
            $result->execute();

            if($result->rowCount() > 0){
                $rw = $result->fetch(PDO::FETCH_OBJ);
                $id_estatus = $rw->id_estatus;
            }

            return $id_estatus;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }

    public function getFinalizadoByEstatus(){
        try {
            $finaliza = 0;

            $queryMP = "SELECT finaliza
                          FROM cat_estatus
                         WHERE id_estatus = ".$this->getIdEstatus()." LIMIT 1";
            $result = $this->conn->prepare($queryMP);
            $result->execute();

            if($result->rowCount() > 0){
                $rw = $result->fetch(PDO::FETCH_OBJ);
                $finaliza = $rw->finaliza;
            }

            return $finaliza;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }

    public function getNoAcuerdos(){
        try {
            $count = 0;

            $queryMP = "SELECT COUNT(id_acuerdo) as count
                          FROM tbl_acuerdos
                         WHERE id_reunion = ".$this->getIdReunion()." LIMIT 1";
            $result = $this->conn->prepare($queryMP);
            $result->execute();

            if($result->rowCount() > 0){
                $rw = $result->fetch(PDO::FETCH_OBJ);
                $count = $rw->count;
            }

            return $count;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }

    public function getRegAvacenGeneral(){
        try {
            $count = 0;

            $queryMP = "SELECT AVG(avance) as avg
                          FROM tbl_acuerdos
                         WHERE id_reunion = ".$this->getIdReunion()." LIMIT 1";
            $result = $this->conn->prepare($queryMP);
            $result->execute();

            if($result->rowCount() > 0){
                $rw = $result->fetch(PDO::FETCH_OBJ);
                $count = $rw->avg;
            }

            return $count;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    
    public function getRegIdEstatusByReunion(){
        try {
            $count = 0;

            $queryMP = "SELECT id_estatus
                          FROM tbl_acuerdos
                         WHERE id_reunion = ".$this->getIdReunion()." LIMIT 1";
            $result = $this->conn->prepare($queryMP);
            $result->execute();

            if($result->rowCount() > 0){
                $rw = $result->fetch(PDO::FETCH_OBJ);
                $count = $rw->id_estatus;
            }

            return $count;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }

    public function getRegDocumentos($id_contrato){
        try {
            $queryMP = "SELECT id_documento, nombre, nombre_real, activo
                    FROM tbl_documento_contrato as  aa 
                    WHERE id_contrato = ".$id_contrato." 
                    AND activo = 1";
            //echo $queryMP;
            $result = $this->conn->prepare($queryMP);
            $result->execute();
            return $result;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }

    public function getPublicCargos(){
        try {
            $queryMP = "SELECT id_cargo, cargo
                    FROM cat_cargos
                    WHERE activo = 1";
            $result = $this->conn->prepare($queryMP);
            $result->execute();
            return $result;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }

    public function getPublicEstatus(){
        try {
            $queryMP = "SELECT id_estatus, estatus
                    FROM cat_estatus
                    WHERE activo = 1";
            $result = $this->conn->prepare($queryMP);
            $result->execute();
            return $result;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }

    public function getPublicEstado(){
        try {
            $queryMP = "SELECT id_estatus, estatus
                    FROM cat_estatus
                    WHERE activo = 1";
            $result = $this->conn->prepare($queryMP);
            $result->execute();
            return $result;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }


    public function getPublicTipoServicio($tipo){
        try {
            $queryMP = "SELECT id_tipomovto, tipomovto
                    FROM cat_tipomovto WHERE activo = 1 AND id_tipomovto IN ".$tipo;
            // echo $queryMP;
            $result = $this->conn->prepare($queryMP);
         
            $result->execute();
            return $result;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }

    public function getPublicTipoServicioIn(){
        try {
            $queryMP = "SELECT id_tipomovto, tipomovto
                          FROM cat_tipomovto 
                         WHERE activo = 1 AND id_tipomovto IN (SELECT  valor FROM ws_parametro WHERE id_parametro = 25 LIMIT 1)";
            // echo $queryMP;
            $result = $this->conn->prepare($queryMP);
            $result->execute();
            return $result;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }

    public function getPublicBeneficiario(){
        try {
            $queryMP = "SELECT id_beneficiario, CONCAT_WS(' ', nombre, apepa, apema) as nombre
                    FROM cat_beneficiario WHERE activo = 1";
            // echo $queryMP;
            $result = $this->conn->prepare($queryMP);
            $result->execute();
            return $result;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }

    public function getPublicClave(){
        try {
            $queryMP = "SELECT id_clavepresu, clave
                    FROM cat_clavepresu";
            // echo $queryMP;
            $result = $this->conn->prepare($queryMP);
            $result->execute();
            return $result;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    public function getPublicEstados(){
        try {
            $queryMP = "SELECT id_estado, estado
                    FROM cat_estados ORDER BY id_estado";
            $result = $this->conn->prepare($queryMP);
            $result->execute();
            return $result;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    public function getPublicBancos(){
        try {
            $queryMP = "SELECT id_banco, nombre
                    FROM cat_bancos";
            $result = $this->conn->prepare($queryMP);
            $result->execute();
            return $result;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    public function getCveSeparada($id_clave)
    {
        try {
            $queryUser = "SELECT s.cve, s.ini, s.fin, substring(c.clave,s.ini,s.fin) as cadena
                          FROM cat_substraecve s
                          INNER JOIN  cat_clavepresu c 
                          WHERE s.anio = 2018 and c.id_clavepresu = $id_clave
                          ORDER BY s.orden";
            $result = $this->conn->prepare($queryUser);
            $result->execute();

            return $result;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    public function getCveSeparadaStr($id_clave, $anio)
    {
        try {

            $cve_str = "";

            $queryUser = "SELECT s.cve, s.ini, s.fin, substring(c.clave,s.ini,s.fin) as cadena
                          FROM cat_substraecve s
                          INNER JOIN  cat_clavepresu c 
                          WHERE s.anio = $anio and c.id_clavepresu = $id_clave
                          ORDER BY s.orden";
            $result = $this->conn->prepare($queryUser);
            $result->execute();

            while($rwSep = $result->fetch(PDO::FETCH_OBJ)){
                $cve_str.= $rwSep->cadena."-";
            }

            $cve_str = substr ($cve_str, 0, -1);
            return $cve_str;
            
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    public function validaSaldoDisp($clave,$mes){
        $correcto   = 1;
        $exec       = $this->conn->conexion();
        try {
            $queryUser = "SELECT asig_orig,com_sol 
                          FROM tbl_presupuesto
                          WHERE id_clavepresu=$clave 
                          AND mes=$mes";
            //echo $queryUser;
            $result = $this->conn->prepare($queryUser);
            $result->execute();
          
            while($row = $result->fetch(PDO::FETCH_ASSOC)){
                $saldo =  $row['asig_orig']+$row['com_sol'];
            }
    
            return $saldo;
        }catch (\PDOException $e){
            $exec->rollBack();
            $correcto =  "Error!: " . $e->getMessage();
        }
        return $correcto;
        
    }
    public function insertRegServivio($id_tipomovto,
                                    $no_contrato,
                                    $fecha,
                                    $mes_afec,
                                    $anio,
                                    $id_beneficiario,
                                    $importe,
                                    $importe_total,
                                    $no_fua,
                                    $identificador,
                                    $id_estatus,
                                    $afecta_presu,
                                    $observaciones,
                                    $id_usuario,
                                    $id_estado,
                                    $fecha_ini,
                                    $descripcion,
                                    $iva,
                                    $iva_retenido,
                                    $isr,
                                    $fecha_fin,
                                    $objetivo,
                                    $actividades,
                                    $id_banco,
                                    $cuenta,
                                    $id_suficiencia,
                                    $justificacion){
        $correcto= 1;

        $exec = $this->conn->conexion();
        try {
            $queryMP = "INSERT INTO tbl_contrato(
                                    id_tipomovto,
                                    no_contrato,
                                    fecha,
                                    mes_afec,
                                    anio,
                                    id_beneficiario,
                                    importe,
                                    importe_total,
                                    saldo,
                                    no_fua,
                                    identificador,
                                    id_estatus,
                                    afecta_presu,
                                    observaciones,
                                    id_usuario,
                                    id_estado,
                                    fecha_ini,
                                    descripcion,
                                    iva,
                                    iva_retenido,
                                    isr,
                                    fecha_fin,
                                    objetivo,
                                    actividades,
                                    id_banco,
                                    cuenta,
                                    id_suficiencia,
                                    justificacion)
                             VALUES (?,
                                     ?,
                                     ?,
                                     ?,
                                     ?,
                                     ?,
                                     ?,
                                     ?,
                                     ?,
                                     ?,
                                     ?,
                                     ?,
                                     ?,
                                     ?,
                                     ?,
                                     ?,
                                     ?,
                                     ?,
                                     ?,
                                     ?,
                                     ?,
                                     ?,
                                     ?,
                                     ?,
                                     ?,
                                     ?,
                                     ?,
                                     ?)";
            $result = $this->conn->prepare($queryMP);
            $exec->beginTransaction();
            $result->execute(array($id_tipomovto,
                                   $no_contrato,
                                   $fecha,
                                   $mes_afec,
                                   $anio,
                                   $id_beneficiario,
                                   $importe,
                                   $importe_total,
                                   $importe_total,
                                   $no_fua,
                                   $identificador,
                                   $id_estatus,
                                   $afecta_presu,
                                   $observaciones,
                                   $id_usuario,
                                   $id_estado,
                                   $fecha_ini,
                                   $descripcion,
                                   $iva,
                                   $iva_retenido,
                                   $isr,
                                   $fecha_fin,
                                   $objetivo,
                                   $actividades,
                                   $id_banco,
                                   $cuenta,
                                   $id_suficiencia,
                                   $justificacion));
               
           
           if ($correcto == 1){
                $correcto= $exec->lastInsertId();
            }
            $exec->commit();
        
            return $correcto;

        }
        catch(\PDOException $e)
        {
            $exec->rollBack();
        
            return "Error!: " . $e->getMessage();
        }
    }

    public function insertRegServivioSec($id_contrato,
                                         $id_cvepresu,
                                         $ampliacion,
                                         $id_centrotrabajo,
                                         $imp_secuencia
                                         ){
        $correcto= 1;

        $exec = $this->conn->conexion();
        try {
            $queryMP = "INSERT INTO tbl_contrato_sec(id_contrato,
                                    id_cvepresu,
                                    ampliacion,
                                    id_centrotrabajo,
                                    imp_secuencia
                                    )
                             VALUES (?,
                                     ?,
                                     ?,
                                     ?,
                                     
                                     ?)";
            $result = $this->conn->prepare($queryMP);
            $exec->beginTransaction();
            $result->execute(array($id_contrato,
                                   $id_cvepresu,
                                   $ampliacion,
                                   $id_centrotrabajo,
                                   $imp_secuencia
                                ));
            if ($correcto == 1){
                $correcto= $exec->lastInsertId();
            }
            $exec->commit();
            return $correcto;
        }
        catch(\PDOException $e)
        {
            $exec->rollBack();
            return "Error!: " . $e->getMessage();
        }
    }

    public function insertRegPago($cadenavalues){
        $correcto= 1;

        $exec = $this->conn->conexion();
        try {
            $queryMP = "INSERT INTO tbl_contrato_pago(id_contrato, 
                                              id_clavepresu, 
                                              id_usuario, 
                                              fecha_captura, 
                                              cantidad, 
                                              mes, 
                                              anio,
                                              activo,
                                              folio_fact
                                              )
                             $cadenavalues";
            //echo $queryMP;
            $result = $this->conn->prepare($queryMP);
           $exec->beginTransaction();
            $result->execute();
            if ($correcto == 1){
                $correcto= $exec->lastInsertId();
            }
            $exec->commit();
            return $correcto;
        }
        catch(\PDOException $e)
        {
            $exec->rollBack();
            return "Error!: " . $e->getMessage(). " $queryMP";
        }
    }
    public function selectRegPago($id_contrato,$id_clave){
        $correcto= 1;

        $exec = $this->conn->conexion();
        try {
            $queryUser = "(SELECT  SUM(cantidad) as saldo FROM tbl_contrato_pago as p 
            wHERE p.id_contrato = $id_contrato AND p.id_clavepresu = $id_clave)";
           
            //echo $queryUser;
            $result = $this->conn->prepare($queryUser);
            $result->execute();
            while($row = $result->fetch(PDO::FETCH_ASSOC)){
                $saldo =  $row['saldo'];
            }
            return $saldo;
        
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    

    public function insertRegServivioSecImp($values){
        $correcto= 1;

        $exec = $this->conn->conexion();
        try {
            $queryMP = "INSERT INTO tbl_contrato_sec_imp(id_contrato_sec,
                                    mes,
                                    imp_mes) $values";
            $result = $this->conn->prepare($queryMP);
            $exec->beginTransaction();
            $result->execute();
            if ($correcto == 1){
                $correcto= $exec->lastInsertId();
                //$this->setIdServicioSec($correcto);
            }
            $exec->commit();
            return $correcto;
        }
        catch(\PDOException $e)
        {
            $exec->rollBack();
            return "Error!: " . $e->getMessage();
        }
    }




    public function insertRegHistoria($id_contrato,
                                      $id_estatus,
                                      $id_usuario_captura,
                                      $fecha_captura,
                                      $observaciones,
                                      $activo)
    {
        $correcto= 1;

        $exec = $this->conn->conexion();
        try {
            $queryMP = "INSERT INTO tbl_contrato_historia(id_contrato, 
                                    id_estatus, id_usuario_captura, 
                                    fecha_captura, observaciones, 
                                    activo)
                             VALUES (?,
                                    ?,?,
                                    ?,?,
                                    ?)";
            $result = $this->conn->prepare($queryMP);
            $exec->beginTransaction();
            $result->execute(array($id_contrato,
                                   $id_estatus,
                                   $id_usuario_captura,
                                   $fecha_captura,
                                   $observaciones,
                                   $activo));
            if ($correcto == 1){
                $correcto= $exec->lastInsertId();
                $this->setIdHistoria($correcto);
            }
            $exec->commit();
            return $correcto;
        }
        catch(\PDOException $e)
        {
            $exec->rollBack();
            return "Error!: " . $e->getMessage();
        }
    }

    public function insertDocumento($id_usuario,
                                    $id_tipomovto,
                                    $id_contrato,
                                    $fecha,
                                    $nombre_real,
                                    $activo)
    {
        $correcto= 1;
        $exec = $this->conn->conexion();
        try {
            $queryMP = "INSERT INTO tbl_documento_contrato(id_usuario, id_tipomovto, id_contrato,
                                                  fecha, nombre_real, activo)
                             VALUES (?,?,?,?,?,?)";

            $result = $this->conn->prepare($queryMP);
            $exec->beginTransaction();
            $result->execute(array($id_usuario,
                                   $id_tipomovto,
                                   $id_contrato,
                                   $fecha,
                                   $nombre_real,
                                   $activo));
            if ($correcto == 1){
                $correcto= $exec->lastInsertId();
                $this->setIdDocumento($correcto);
            }
            $exec->commit();
            return $correcto;
        }
        catch(\PDOException $e)
        {
            $exec->rollBack();
            return "Error!: " . $e->getMessage();
        }
    }

    public function updateDocumento($nombre,
                                    $id_documento){
        $correcto   = 1;
        $exec       = $this->conn->conexion();
        try {
            $queryUpdate = "UPDATE tbl_documento_contrato
                           SET nombre = '" . $nombre . "'
                         WHERE id_documento = ".$id_documento;
            $result = $this->conn->prepare($queryUpdate);
            $exec->beginTransaction();
            $result->execute();
            $exec->commit();
        }catch (\PDOException $e){
            $exec->rollBack();
            $correcto =  "Error!: " . $e->getMessage();
        }
        return $correcto;
    }


    public function updateRegServicio($id_tipomovto,
                                      $no_contrato,
                                      $fecha,
                                      $mes_afec,
                                      $anio,
                                      $id_beneficiario,
                                      $importe,
                                      $no_fua,
                                      $identificador,
                                      $afecta_presu,
                                      $observaciones,
                                      $id_estado,
                                      $fecha_ini,
                                      $descripcion,
                                      $iva,
                                      $iva_retenido,
                                      $isr,
                                      $fecha_fin,
                                      $importe_total,
                                      $objetivo,
                                      $actividades,
                                      $justificacion,
                                      $id_banco,
                                      $cuenta,
                                      $id_suficiencia,
                                      $id_contrato)
    {
        $correcto   = 1;
        $exec       = $this->conn->conexion();
        try {
            $queryMP = "UPDATE tbl_contrato
            SET id_tipomovto =' $id_tipomovto',
                no_contrato = '$no_contrato',
                fecha = '$fecha',
                mes_afec = '$mes_afec',
                anio = '$anio',
                id_beneficiario = '$id_beneficiario',
                importe = '$importe',
                no_fua = '$no_fua',
                identificador = '$identificador',
                afecta_presu = '$afecta_presu',
                observaciones = '$observaciones',
                id_estado = '$id_estado',
                fecha_ini = '$fecha_ini',
                descripcion = '$descripcion',
                iva = '$iva',
                iva_retenido = '$iva_retenido',
                isr = '$isr',
                fecha_fin = '$fecha_fin',
                importe_total = '$importe_total',
                objetivo = '$objetivo',
                actividades = '$actividades',
                justificacion = '$justificacion',
                id_banco = '$id_banco',
                cuenta = '$cuenta',
                id_suficiencia = '$id_suficiencia'
          WHERE id_contrato = '$id_contrato' ";

//echo $queryMP;
$result = $this->conn->prepare($queryMP);
$exec->beginTransaction();
$result->execute();
           
          
            $exec->commit();
        }catch (\PDOException $e){
            $exec->rollBack();
            $correcto =  "Error!: " . $e->getMessage();
        }
        return $correcto;
    }

    public function updateRegServicioByEstatus($id_estatus,
                                               $observaciones,
                                               $id_contrato)
    {
        $correcto   = 1;
        $exec       = $this->conn->conexion();
        try {
            $queryUpdate = "UPDATE tbl_contrato
                               SET id_estatus = ?, 
                                   observaciones = ?
                             WHERE id_contrato = ? ";
            $result = $this->conn->prepare($queryUpdate);
            $exec->beginTransaction();
            $result->execute(array($id_estatus,
                                   $observaciones,
                                   $id_contrato));
            $exec->commit();
        }catch (\PDOException $e){
            $exec->rollBack();
            $correcto =  "Error!: " . $e->getMessage();
        }
        return $correcto;
    }
    public function updateRegServicioByMovimiento($id_tipomovto,
    $observaciones,
    $id_contrato)
    {
    $correcto   = 1;
    $exec       = $this->conn->conexion();
    try {
    $queryUpdate = "UPDATE tbl_contrato
    SET id_tipomovto = ?, 
    observaciones = ?
    WHERE id_contrato = ? ";
    $result = $this->conn->prepare($queryUpdate);
    $exec->beginTransaction();
    $result->execute(array($id_tipomovto,
    $observaciones,
    $id_contrato));
    $exec->commit();
    }catch (\PDOException $e){
    $exec->rollBack();
    $correcto =  "Error!: " . $e->getMessage();
    }
    return $correcto;
    }
    public function updatePago($id_pago,$time)
    {
    $correcto   = 1;
    $exec       = $this->conn->conexion();
    try {
        $queryUpdate = "UPDATE tbl_contrato_pago
        SET 
        activo = 1,
        creacion=$time
      WHERE id_pago =$id_pago";
        $result = $this->conn->prepare($queryUpdate);
        $exec->beginTransaction();
        $result->execute(array($id_pago));
        $exec->commit();


   //echo $queryUpdate;
    }catch (\PDOException $e){
    $exec->rollBack();
    $correcto =  "Error!: " . $e->getMessage();
    }
    return $correcto;
    }
    public function updatePagoSol($id_pago,$time)
    {
    $correcto   = 1;
    $exec       = $this->conn->conexion();
    try {
        $queryUpdate = "UPDATE tbl_contrato_pago
        SET 
        pagado = 1,
        modificacion=$time
      WHERE id_pago =$id_pago";
    $result = $this->conn->prepare($queryUpdate);
    $exec->beginTransaction();
    $result->execute(array($id_pago));
    $exec->commit();


   //echo $queryUpdate;
    }catch (\PDOException $e){
    $exec->rollBack();
    $correcto =  "Error!: " . $e->getMessage();
    }
    return $correcto;
    }
    public function updateFolioSol($id_pago,$folio_pres)
    {
        $correcto   = 1;
        $exec       = $this->conn->conexion();
        try {
            $queryUpdate = "UPDATE tbl_contrato_pago
                SET 
               
                folio_pres='$folio_pres'
                WHERE id_pago =$id_pago";
               // echo $queryUpdate;
                $result = $this->conn->prepare($queryUpdate);
                $exec->beginTransaction();
                $result->execute(array($id_pago));
                $exec->commit();
                
        }catch (\PDOException $e){
            $exec->rollBack();
            $correcto =  "Error!: " . $e->getMessage();
        }
        return $correcto;
    }
    public function updateFechaSol($id_pago,$fecha_pago)
    {
        $correcto   = 1;
        $exec       = $this->conn->conexion();
        try {
            $queryUpdate = "UPDATE tbl_contrato_pago
                SET 
               
                fecha_pago='$fecha_pago'
                WHERE id_pago =$id_pago";
               // echo $queryUpdate;
                $result = $this->conn->prepare($queryUpdate);
                $exec->beginTransaction();
                $result->execute(array($id_pago));
                $exec->commit();
                
        }catch (\PDOException $e){
            $exec->rollBack();
            $correcto =  "Error!: " . $e->getMessage();
        }
        return $correcto;
    }
    public function updateRegServicioByImporte($importe,
                                               $importe_total,
                                               $saldo,
                                               $id_contrato)
    {
        $correcto   = 1;
        $exec       = $this->conn->conexion();
        try {
            $queryUpdate = "UPDATE tbl_contrato
                               SET importe = ?, 
                                   importe_total = ?,
                                   saldo = ?
                             WHERE id_contrato = ? ";
            $result = $this->conn->prepare($queryUpdate);
            $exec->beginTransaction();
            $result->execute(array($importe,
                                   $importe_total,
                                   $saldo,
                                   $id_contrato));
            $exec->commit();
        }catch (\PDOException $e){
            $exec->rollBack();
            $correcto =  "Error!: " . $e->getMessage();
        }
        return $correcto;
    }
    public function updateRegServicioSec($id_cvepresu,
                                         $ampliacion,
                                         $id_centrotrabajo,
                                         $imp_secuencia,
                                         $id_contrato_sec){
        $correcto   = 1;
        $exec       = $this->conn->conexion();
        try {
            $queryUpdate = "UPDATE tbl_contrato_sec
                           SET id_cvepresu = ?,
                               ampliacion = ?,
                               id_centrotrabajo = ?,
                               imp_secuencia = ?
                         WHERE id_contrato_sec = ? ";
           
            $result = $this->conn->prepare($queryUpdate);
           
            $exec->beginTransaction();
            $result->execute(array($id_cvepresu,
                                   $ampliacion,
                                   $id_centrotrabajo,
                                   $imp_secuencia,
                                   $id_contrato_sec));
            $exec->commit();
        }catch (\PDOException $e){
            $exec->rollBack();
            $correcto =  "Error!: " . $e->getMessage();
        }
        return $correcto;
    }

    public function updateRegContraroPago($id_contrato){
        $correcto   = 1;
        $exec       = $this->conn->conexion();
        try {
            $queryUpdate = "UPDATE tbl_contrato
                           SET abonos = (SELECT  SUM(cantidad) FROM tbl_contrato_pago as p 
                                          wHERE p.id_contrato = $id_contrato AND p.activo = 1),
                               saldo = importe_total - abonos
                         WHERE id_contrato = ? ";
            $result = $this->conn->prepare($queryUpdate);
            $exec->beginTransaction();
            $result->execute(array($id_contrato));
            $exec->commit();
        }catch (\PDOException $e){
            $exec->rollBack();
            $correcto =  "Error!: " . $e->getMessage();
        }
        return $correcto;
    }
    
    public function updateContratoPagado($id_contrato){
        $correcto   = 1;
        $exec       = $this->conn->conexion();
        try {
            $queryUpdate = "UPDATE tbl_contrato
                           SET id_estatus=6
                         WHERE id_contrato = ? ";
            $result = $this->conn->prepare($queryUpdate);
            $exec->beginTransaction();
            $result->execute(array($id_contrato));
            $exec->commit();
        }catch (\PDOException $e){
            $exec->rollBack();
            $correcto =  "Error!: " . $e->getMessage();
        }
        return $correcto;
    }
    public function validaSaldo($id_contrato){
        $correcto   = 1;
        $exec       = $this->conn->conexion();
        try {
            $queryUser = "SELECT saldo 
                          FROM tbl_contrato
                          WHERE id_contrato=$id_contrato";
            //echo $queryUser;
            $result = $this->conn->prepare($queryUser);
            $result->execute();
           
            while($row = $result->fetch(PDO::FETCH_ASSOC)){
                $saldo =  $row['saldo'];
            }
    
            return $saldo;
        }catch (\PDOException $e){
            $exec->rollBack();
            $correcto =  "Error!: " . $e->getMessage();
        }
        return $correcto;
        
    }

    public function updateStatus($tipo, $id_contrato){
        $correcto   = 1;
        $exec       = $this->conn->conexion();
        try {
            $queryUpdate = "UPDATE tbl_contrato
                           SET id_estatus = ? WHERE id_contrato = ?";
            $result = $this->conn->prepare($queryUpdate);
            $exec->beginTransaction();
            $result->execute(array($tipo, $id_contrato));
            $exec->commit();
        }catch (\PDOException $e){
            $exec->rollBack();
            $correcto =  "Error!: " . $e->getMessage();
        }
        return $correcto;
    }


    public function deleteReg($id_contrato){
        $correcto   = 2;
        try {
            $queryMP = "DELETE FROM tbl_contrato WHERE id_contrato = ".$id_contrato;
            $result = $this->conn->prepare($queryMP);
            $result->execute();

            return $correcto;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }

    public function deleteRegDocumento($id_documento){
        $correcto   = 2;
        try {
            $queryMP = "DELETE FROM tbl_documento_contrato WHERE id_documento = ".$id_documento;
            $result = $this->conn->prepare($queryMP);
            $result->execute();

            return $correcto;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }


    public function deleteRegServiciosSec($id_contrato){
        $correcto   = 2;
        try {
            $queryMP = "DELETE FROM tbl_contrato_sec WHERE id_contrato = ".$id_contrato;
            $result = $this->conn->prepare($queryMP);
            $result->execute();

            return $correcto;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }

    public function deleteRegServicioSecImp($id_contrato_sec){
        $correcto   = 2;
        try {
            $queryMP = "DELETE FROM tbl_contrato_sec_imp 
                              WHERE id_contrato_sec = $id_contrato_sec";
            $result = $this->conn->prepare($queryMP);
            $result->execute();

            return $correcto;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }



}
