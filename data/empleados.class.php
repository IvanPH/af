<?php
//Incluyendo la conexión a la base de datos
require_once $dir_fc."connections/conn_data.php";
//require_once $dir_fc."connections/conn_config.php";

class cEmpleados extends BD
{
    private $id_empleado;
    private $noempleado;
    private $estatus;
    private $estatus_laboral;
    private $nombre;
    private $apepat;
    private $apemat;
    private $rfc;
    private $plaza;
    private $segsoc;
    private $percentpto;
    private $f_nombramiento;
    private $f_inicio;
    private $f_termino;
    private $f_ingreso;
    private $sueldo_neto;
    private $AREA;
    private $ID_AREA;
    private $cve_puesto;

    //varibales d einsert

    public function setId_empleado($id_empleado){
        $this->id_empleado = $id_empleado;
    }
    public function setNoempleado($noempleado){
        $this->noempleado = $noempleado;
    }
    public function setEstatus($estatus){
        $this->estatus = $estatus;
    }
    public function setEstatus_laboral($estatus_laboral){
        $this->estatus_laboral = $estatus_laboral;
    }
    public function setNombre($nombre){
        $this->nombre = $nombre;
    }
    public function setApepat($apepat){
        $this->apepat = $apepat;
    }
    public function setApemat($apemat){
        $this->apemat = $apemat;
    }
    public function setRfc($rfc){
        $this->rfc = $rfc;
    }
    public function setPlaza($plaza){
        $this->plaza = $plaza;
    }
    public function setSegsoc($segsoc){
        $this->segsoc = $segsoc;
    }
    public function setPercentpto($percentpto){
        $this->percentpto = $percentpto;
    }
    public function setF_nombramiento($f_nombramiento){
        $this->f_nombramiento = $f_nombramiento;
    }
    public function setF_inicio($f_inicio){
        $this->f_inicio = $f_inicio;
    }
    public function setF_termino($f_termino){
        $this->f_termino = $f_termino;
    }
    public function setF_ingreso($f_ingreso){
        $this->f_ingreso = $f_ingreso;
    }
    public function setSueldo_neto($sueldo_neto){
        $this->sueldo_neto = $sueldo_neto;
    }
    public function setAREA($AREA){
        $this->AREA = $AREA;
    }
    public function setID_AREA($ID_AREA){
        $this->ID_AREA = $ID_AREA;
    }
    public function setCve_puesto($cve_puesto){
        $this->cve_puesto = $cve_puesto;
    }
    public function getIdUsuario(){
        return $this->idUsuario;
    }

    public function setIdUsuario($strIdUsuario){
        $this->idUsuario = $strIdUsuario;
    }
    public function setIdUsuarioCaptura($id_usuario_captura)
    {
        $this->id_usuario_captura = $id_usuario_captura;
    }
    public function getIdRol(){
        return $this->IdRol;
    }
    public function get_id_menu(){
        return $this->id_menu;
    }

    public  function  set_id_menu($menu){
        $this->id_menu = $menu;
    }
    function __construct()
    {
        //Esta es la que llama a la base de datos
        //parent::__construct();
        $this->conn = new BD();
    }

    public function getRegbyid(){
        try {
            $queryMP = "SELECT * FROM cat_empleado WHERE id_empleado=".$this->getIdUsuario() ." LIMIT 1";
            $result = $this->conn->prepare($queryMP);
         
            $result->execute();
            return $result;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    
    public function getAllRegAjax()
    {

        try {
            $queryUser = "SELECT id_empleado, CONCAT_WS(' ', nombre, apepat, apemat) AS nombre, 
                                 noempleado, estatus_laboral, estatus
                          FROM cat_empleado ORDER BY id_empleado DESC";

            $result = $this->conn->prepare($queryUser);
            $result->execute();
           
            return $result;
            
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    public function getCounter()
    {
        $no = "";
        try {
            $queryUser = "SELECT COUNT(id_empleado) as count
                          FROM cat_empleado";
            //echo $queryUser;
            $result = $this->conn->prepare($queryUser);
            $result->execute();

            while($row = $result->fetch(PDO::FETCH_ASSOC)){
                $no =  $row['count'];
            }
            return $no;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    public function getAreas(){
        try {
            $queryMP = "SELECT id_area, area
                    FROM cat_area ";
             echo $queryMP;
            $result = $this->conn->prepare($queryMP);
            $result->execute();
           
            return $result;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    public function getPuestos(){
        try {
            $queryMP = "SELECT id_puesto, desc_puesto
                    FROM cat_puesto ";
             echo $queryMP;
            $result = $this->conn->prepare($queryMP);
            $result->execute();
           
            return $result;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    public function insertReg($fecha){
        $correcto= 1;
        $exec = $this->conn->conexion();
        try {
            $queryMP = "INSERT INTO cat_empleado(noempleado, rfc,
                                    plaza, segsoc, apepat,
                                    apemat, nombre, f_nombramiento, f_inicio,
                                    f_termino, f_ingreso, sueldo_neto, AREA,ID_AREA, estatus_laboral,
                                    cve_puesto,
                                    fecha_mov,usert)
                             VALUES (   '".$this->noempleado."',
                                        UPPER('".$this->rfc."'),
                                        '".$this->plaza."',
                                        '".$this->segsoc."',
                                        '".$this->apepat."',
                                        '".$this->apemat."',
                                        '".$this->nombre."',
                                        '".$this->f_nombramiento."',
                                        '".$this->f_inicio."',
                                        '".$this->f_termino."',
                                        '".$this->f_ingreso."',
                                        '".$this->sueldo_neto."',
                                        '".$this->AREA."',
                                        '".$this->ID_AREA."',
                                        '".$this->estatus_laboral."',
                                        '".$this->cve_puesto."',
                                        '".$fecha."',
                                        '".$_SESSION['cve_admin__presupuesto']."'
                                       
                                     )";
           
            //lala
            $result = $this->conn->prepare($queryMP);
            $exec->beginTransaction();
            $result->execute();
            $exec->commit();
            return $correcto;
        }
        catch(\PDOException $e)
        {
            $exec->rollBack();
            return "Error!: " . $e->getMessage();
        }
    }
    public function updateReg(){
        $correcto   = 1;
        $exec       = $this->conn->conexion();
     
        try {
            $queryUpdate = "UPDATE cat_empleado
                           SET  noempleado = '" . $this->noempleado . "', 
                                rfc =  UPPER('" . $this->rfc . "'), 
                                plaza = '" . $this->plaza . "', 
                                segsoc='" . $this->segsoc . "',
                                apepat='" . $this->apepat . "', 
                                apemat='" . $this->apemat . "',
                                nombre='" . $this->nombre . "', 
                                percentpto='" . $this->percentpto . "',
                                f_nombramiento='" . $this->f_nombramiento . "', 
                                f_inicio='" . $this->f_inicio . "', 
                                f_termino = '$this->f_termino',
                                f_ingreso='" . $this->f_ingreso . "', 
                                sueldo_neto='" . $this->sueldo_neto . "', 
                                estatus_laboral = '$this->estatus_laboral',
                                AREA='" . $this->AREA . "', 
                                ID_AREA='" . $this->ID_AREA . "', 
                                cve_puesto = '$this->cve_puesto'
                         WHERE id_empleado = " . $this->id_empleado;
           
            $result = $this->conn->prepare($queryUpdate);
            $exec->beginTransaction();
            $result->execute();
            $exec->commit();
        }catch (\PDOException $e){
            $exec->rollBack();
            $correcto =  "Error!: " . $e->getMessage();
        }
        return $correcto;
    }
    public function updateRegCat(){
        $correcto   = 1;
        $exec       = $this->conn->conexion();
     
        try {
            $queryUpdate = "UPDATE cat_empleado
                           SET  noempleado = '" . $this->noempleado . "', 
                                rfc =  UPPER('" . $this->rfc . "'), 
                                plaza = '" . $this->plaza . "', 
                                segsoc='" . $this->segsoc . "',
                                apepat='" . $this->apepat . "', 
                                apemat='" . $this->apemat . "',
                                nombre='" . $this->nombre . "',
                                f_nombramiento='" . $this->f_nombramiento . "', 
                                f_inicio='" . $this->f_inicio . "', 
                                f_termino = '$this->f_termino',
                                f_ingreso='" . $this->f_ingreso . "', 
                                sueldo_neto='" . $this->sueldo_neto . "', 
                                estatus_laboral = '$this->estatus_laboral',
                                AREA='" . $this->AREA . "', 
                                ID_AREA='" . $this->ID_AREA . "', 
                                cve_puesto = '$this->cve_puesto'
                         WHERE id_empleado = " . $this->id_empleado;
           
            $result = $this->conn->prepare($queryUpdate);
            $exec->beginTransaction();
            $result->execute();
            $exec->commit();
        }catch (\PDOException $e){
            $exec->rollBack();
            $correcto =  "Error!: " . $e->getMessage();
        }
        return $correcto;
    }
    public function deleteRegbyid($id){
        $correcto = 2;
        $sqlCmd = "DELETE FROM cat_empleado WHERE id_empleado=".$id;
        try {
            $sqlPrepare = $this->conn->prepare($sqlCmd);
            $sqlPrepare->execute();
            return $correcto;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    
}
