<?php
require_once $dir_fc."connections/conn_data.php";
require_once $dir_fc."data/concepto.class.php";

class cConceptos_adm extends BD
{

    function __construct()
    {
        //Esta es la que llama a la base de datos
        //parent::__construct();
        $this->conn = new BD();
    }

    public function getRegbyid($id){
        $sqlCmd = "SELECT id_concepto, clave, descripcion, tipo, activo FROM cat_conceptos WHERE id_concepto=".$id." LIMIT 1";
        try {
            $sqlPrepare = $this->conn->prepare($sqlCmd);
            $sqlPrepare->execute();
            return $sqlPrepare;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }

    public function buscaClave(cConcepto $datos){
        // Busca si la clave ya existe en la tabla
        $resultado = 0;
        try {
            $sqlCmd = "SELECT id_concepto FROM cat_conceptos WHERE clave='".$datos->getClave()."'";
            $sqlPrepare = $this->conn->prepare($sqlCmd);
            $sqlPrepare->execute();
            $resultado = $sqlPrepare->rowCount();
        }
        catch(\PDOException $e)
        {
            $resultado = "Error!: " . $e->getMessage();
        }
        return $resultado;
    }

    public function buscaCoincidencia(cConcepto $datos){
        // Busca si la clave del concepto se repite en la tabla
        $resultado = 0;
        try{
            $sqlCmd = "SELECT id_concepto FROM cat_conceptos WHERE clave='".$datos->getClave()."' AND id_concepto!=".$datos->getIdConcepto();
            $sqlPrepare = $this->conn->prepare($sqlCmd);
            $sqlPrepare->execute();
            $resultado = $sqlPrepare->rowCount();
        }
        catch (\PDOException $e){
            $resultado = "Error: ". $e->getMessage();
        }
        return $resultado;
    }

    public function getAllRegAjax()
    {
        $sqlCmd = "SELECT id_concepto, clave, descripcion, tipo, activo FROM cat_conceptos ORDER BY id_concepto ASC";
        try {
            $result = $this->conn->prepare($sqlCmd);
            $result->execute();
            return $result;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }

    public function agregarRegistro(cConcepto $datos){
        $resultado = 0;
        $baseDatos = $this->conn->conexion();
        $sqlCmd = "INSERT INTO cat_conceptos(clave, descripcion, tipo, activo) VALUES (".
                    "'".$datos->getClave()."'".
                    ",'".$datos->getDescripcion()."'".
                    ",'".$datos->getTipo()."'".
                    ",".$datos->getActivo().")";
        try {
            $sqlPrep = $this->conn->prepare($sqlCmd);
            $baseDatos->beginTransaction();
            $sqlPrep->execute();
            $resultado = $baseDatos->lastInsertId();
            $baseDatos->commit();
        }
        catch(\PDOException $e)
        {
            $baseDatos->rollBack();
            $resultado = "Error!: " . $e->getMessage();
        }
        return $resultado;
    }

    public function actualizaRegistro(cConcepto $datos){
        $resultado = 1;
        $conexionBd = $this->conn->conexion();
        $sqlCmd = "UPDATE cat_conceptos". 
                        " SET clave='".$datos->getClave()."'".
                        ",descripcion='".$datos->getDescripcion()."'".
                        ",tipo='".$datos->getTipo()."'".
                        " WHERE id_concepto=".$datos->getIdConcepto();
        try {
            $sqlPrepare = $this->conn->prepare($sqlCmd);
            $conexionBd->beginTransaction();
            $sqlPrepare->execute();
            $conexionBd->commit();
        }catch (\PDOException $e){
            $conexionBd->rollBack();
            $resultado =  "Error!: " . $e->getMessage();
        }
        return $resultado;
    }

    public function updateStatus($tipo, $id){
        $correcto = 1;
        $sqlCmd = "UPDATE cat_conceptos SET activo=".$tipo." WHERE id_concepto=".$id;
        try {
            $sqlPrepare = $this->conn->prepare($sqlCmd);
            $sqlPrepare->execute();
            return $correcto;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }

    public function deleteReg($id){
        $correcto = 2;
        $sqlCmd = "DELETE FROM cat_conceptos WHERE id_concepto=".$id;
        try {
            $sqlPrepare = $this->conn->prepare($sqlCmd);
            $sqlPrepare->execute();
            return $correcto;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    public function getCounter()
    {
        $no = "";
        try {
            $queryUser = "SELECT COUNT(id_concepto) as count
                          FROM cat_conceptos";
            //echo $queryUser;
            $result = $this->conn->prepare($queryUser);
            $result->execute();

            while($row = $result->fetch(PDO::FETCH_ASSOC)){
                $no =  $row['count'];
            }
            return $no;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
}
?>