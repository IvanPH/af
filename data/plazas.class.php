<?php
//Incluyendo la conexión a la base de datos
require_once $dir_fc."connections/conn_data.php";
//require_once $dir_fc."connections/conn_config.php";

class cPlazas extends BD
{
    private $id_puesto;
    private $desc_puesto;
    private $tipo_puesto;
    private $cve_alta;
    private $cve_shcp;
    private $cvesecodam;
    private $nivel;
    private $nivel_viat;
    private $p_estrategico;
    private $responsable;
    private $t_funcionp;
   

    //varibales d einsert

    public function setId_puesto($id_puesto){
        $this->id_puesto = $id_puesto;
    }
    public function setDesc_puesto($desc_puesto){
        $this->desc_puesto = $desc_puesto;
    }
    public function setTipo_puesto($tipo_puesto){
        $this->tipo_puesto = $tipo_puesto;
    }
    public function setCve_alta($cve_alta){
        $this->cve_alta = $cve_alta;
    }
    public function setCve_shcp($cve_shcp){
        $this->cve_shcp = $cve_shcp;
    }
    public function setCvesecodam($cvesecodam){
        $this->cvesecodam = $cvesecodam;
    }
    public function setNivel($nivel){
        $this->nivel = $nivel;
    }
    public function setNivel_viat($nivel_viat){
        $this->nivel_viat = $nivel_viat;
    }
    public function setP_estrategico($p_estrategico){
        $this->p_estrategico = $p_estrategico;
    }
    public function getIdUsuario(){
        return $this->idUsuario;
    }

    public function setIdUsuario($strIdUsuario){
        $this->idUsuario = $strIdUsuario;
    }
    public function setIdUsuarioCaptura($id_usuario_captura)
    {
        $this->id_usuario_captura = $id_usuario_captura;
    }
    public function getIdRol(){
        return $this->IdRol;
    }
    public function get_id_menu(){
        return $this->id_menu;
    }

    public  function  set_id_menu($menu){
        $this->id_menu = $menu;
    }
    function __construct()
    {
        //Esta es la que llama a la base de datos
        //parent::__construct();
        $this->conn = new BD();
    }

    public function getRegbyid(){
        try {
            $queryMP = "SELECT * FROM cat_puesto WHERE id_puesto=".$this->getIdUsuario() ." LIMIT 1";
            $result = $this->conn->prepare($queryMP);
         
            $result->execute();
            return $result;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    
    public function getAllRegAjax()
    {

        try {
            $queryUser = "SELECT id_puesto, desc_puesto, 
                                 tipo_puesto, cve_shcp
                          FROM cat_puesto ORDER BY id_puesto DESC";

            $result = $this->conn->prepare($queryUser);
            $result->execute();
        
            return $result;
            
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    public function getCounter()
    {
        $no = "";
        try {
            $queryUser = "SELECT COUNT(id_puesto) as count
                          FROM cat_puesto";
            //echo $queryUser;
            $result = $this->conn->prepare($queryUser);
            $result->execute();

            while($row = $result->fetch(PDO::FETCH_ASSOC)){
                $no =  $row['count'];
            }
            return $no;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    public function insertReg(){
        $correcto= 1;
        $exec = $this->conn->conexion();
        try {
            $queryMP = "INSERT INTO cat_puesto(desc_puesto, tipo_puesto,
                                    cve_alta, cve_shcp, cvesecodam,
                                    nivel, nivel_viat, p_estrategico, responsable,
                                    t_funcionp)
                             VALUES (   '".$this->desc_puesto."',
                                        '".$this->tipo_puesto."',
                                        '".$this->cve_alta."',
                                        '".$this->cve_shcp."',
                                        '".$this->cvesecodam."',
                                        '".$this->nivel."',
                                        '".$this->nivel_viat."',
                                        '".$this->p_estrategico."',
                                        '".$this->responsable."',
                                        '".$this->t_funcionp."'
                                      
                                       
                                     )";
           
 
            $result = $this->conn->prepare($queryMP);
            $exec->beginTransaction();
            $result->execute();
            $exec->commit();
            return $correcto;
        }
        catch(\PDOException $e)
        {
            $exec->rollBack();
            return "Error!: " . $e->getMessage();
        }
    }
    public function updateReg(){
        $correcto   = 1;
        $exec       = $this->conn->conexion();
     

    
        try {
            $queryUpdate = "UPDATE cat_puesto
                           SET  desc_puesto = '" . $this->desc_puesto . "', 
                           tipo_puesto = '" . $this->tipo_puesto . "', 
                           cve_alta = '" . $this->cve_alta . "', 
                           cve_shcp='" . $this->cve_shcp . "',
                           cvesecodam='" . $this->cvesecodam . "', 
                           nivel='" . $this->nivel . "',
                           nivel_viat='" . $this->nivel_viat . "', 
                           p_estrategico='" . $this->p_estrategico . "',
                           responsable='" . $this->responsable . "', 
                           t_funcionp='" . $this->t_funcionp . "'
                          
                         WHERE id_puesto = " . $this->id_puesto;
            $result = $this->conn->prepare($queryUpdate);
            $exec->beginTransaction();
            $result->execute();
            $exec->commit();
        }catch (\PDOException $e){
            $exec->rollBack();
            $correcto =  "Error!: " . $e->getMessage();
        }
        return $correcto;
    }
    public function deleteRegbyid($id){
        $correcto = 2;
        $sqlCmd = "DELETE FROM cat_puesto WHERE id_puesto=".$id;
        try {
            $sqlPrepare = $this->conn->prepare($sqlCmd);
            $sqlPrepare->execute();
            return $correcto;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    
}
