<?php

class cConcepto
{
    private $id_concepto = 0;
    private $clave = "";
    private $descripcion = "";
    private $tipo = "";
    private $activo = 1;

    public function getIdConcepto() { return $this->id_concepto; }
    public function setIdConcepto($id_concepto) { $this->id_concepto = $id_concepto; }

    public function getClave() { return $this->clave; }
    public function setClave($clave) { $this->clave = $clave; }

    public function getDescripcion() { return $this->descripcion; }
    public function setDescripcion($descripcion) { $this->descripcion = $descripcion; }

    public function getTipo() { return $this->tipo; }
    public function setTipo($tipo) { $this->tipo = $tipo; }

    public function getActivo() { return $this->activo; }
    public function setActivo($activo) { $this->activo = $activo; }
}