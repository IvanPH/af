<?php
//Incluyendo la conexión a la base de datos
require_once $dir_fc."connections/conn_data.php";

class cBeneficiario extends BD
{
    //Variables
    private $id_beneficiario;
    private $rfc;
    private $nombre;
    private $apepa;
    private $apema;
    private $curp;
    private $estudios;
    private $domiciio;
    private $tel;
    private $fecha;
    private $id_titulo;
    private $id_centrotrabajo;
    private $activo;
    //nuevos
    private $sueldo;
    private $f_nacimiento;
    private $id_puesto;
    private $noempleado;
    private $email;
    private $cuenta_bancaria;
    private $segurosocial;
    private $estadocivil;
    private $id_banco;
    private $resguardo;
    private $contactoemergencia;
    private $telemergencia;



    //Get & Set
    public function getId_beneficiario() { return $this->id_beneficiario; }
    public function setId_beneficiario($id_beneficiario) { $this->id_beneficiario = $id_beneficiario; }

    public function getRFC() { return $this->rfc; }
    public function setRFC($rfc) { $this->rfc = $rfc; }

    public function getNombre() { return $this->nombre; }
    public function setNombre($nombre) { $this->nombre = $nombre; }

    public function getApepa() { return $this->apepa; }
    public function setApepa($apepa) { $this->apepa = $apepa; }

    public function getApema() { return $this->apema; }
    public function setApema($apema) { $this->apema = $apema; }

    public function getCurp() { return $this->curp; }
    public function setCurp($curp) { $this->curp = $curp; }

    public function getEstudios() { return $this->estudios; }
    public function setEstudios($estudios) { $this->estudios = $estudios; }

    public function getDomiciiio() { return $this->domiciio; }
    public function setDomiciiio($domiciio) { $this->domiciio = $domiciio; }

    public function getTel() { return $this->tel; }
    public function setTel($tel) { $this->tel = $tel; }

    public function getFecha() { return $this->fecha; }
    public function setFecha($fecha) { $this->fecha = $fecha; }

    public function getId_titulo() { return $this->id_titulo; }
    public function setId_titulo($id_titulo) { $this->id_titulo = $id_titulo; }

    public function getId_centrotrabajo() { return $this->id_centrotrabajo; }
    public function setId_centrotrabajo($id_centrotrabajo) { $this->id_centrotrabajo = $id_centrotrabajo; }

    public function getActivo() { return $this->activo; }
    public function setActivo($activo) { $this->activo = $activo; }
//nuevos
    public function getSueldo() { return $this->sueldo; }
    public function setSueldo($sueldo) { $this->sueldo = $sueldo; }
    
    public function getF_nacimiento() { return $this->f_nacimiento; }
    public function setF_nacimiento($f_nacimiento) { $this->f_nacimiento = $f_nacimiento; }
    
    public function getId_puesto() { return $this->id_puesto; }
    public function setId_puesto($id_puesto) { $this->id_puesto = $id_puesto; }
    
    public function getNoempleado() { return $this->noempleado; }
    public function setNoempleado($noempleado) { $this->noempleado = $noempleado; }
    
    public function getEmail() { return $this->email; }
    public function setEmail($email) { $this->email = $email; }
    
    public function getCuenta_bancaria() { return $this->cuenta_bancaria; }
    public function setCuenta_bancaria($cuenta_bancaria) { $this->cuenta_bancaria = $cuenta_bancaria; }
    
    public function getSegurosocial() { return $this->segurosocial; }
    public function setSegurosocial($segurosocial) { $this->segurosocial = $segurosocial; }
    
    public function getEstadocivil() { return $this->estadocivil; }
    public function setEstadocivil($estadocivil) { $this->estadocivil = $estadocivil; }
    
    public function getId_banco() { return $this->id_banco; }
    public function setId_banco($id_banco) { $this->id_banco = $id_banco; }
    
    public function getResguardo() { return $this->resguardo; }
    public function setResguardo($resguardo) { $this->resguardo = $resguardo; }
    
    public function getCelular() { return $this->celular; }
    public function setCelular($celular) { $this->celular = $celular; }
    
    public function getContactoemergencia() { return $this->contactoemergencia; }
    public function setContactoemergencia($contactoemergencia) { $this->contactoemergencia = $contactoemergencia; }
    
    public function getTelemergencia() { return $this->telemergencia; }
    public function setTelemergencia($telemergencia) { $this->telemergencia = $telemergencia; }
    

    //Funciones
    function __construct()
    {
        //Esta es la que llama a la base de datos
        //parent::__construct();
        $this->conn = new BD();
    }
    public function getRegbyid($id){
        $sqlCmd = "SELECT 
        id_beneficiario, rfc, nombre, apepa, apema, curp, estudios, domiciio, 
        tel, fecha, id_titulo, id_centrotrabajo, activo, sueldo, f_nacimiento,
        id_puesto, noempleado, email, cuenta_bancaria, segurosocial, estadocivil, id_banco, resguardo,
        celular,contactoemergencia,telemergencia
        FROM cat_beneficiario
        WHERE id_beneficiario=".$id." LIMIT 1";
        try {
            $sqlPrepare = $this->conn->prepare($sqlCmd);
            $sqlPrepare->execute();
            return $sqlPrepare;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    public function getCounter()
    {
        $no = "";
        try {
            $queryUser = "SELECT COUNT(id_beneficiario) as count
                          FROM cat_beneficiario";
            //echo $queryUser;
            $result = $this->conn->prepare($queryUser);
            $result->execute();

            while($row = $result->fetch(PDO::FETCH_ASSOC)){
                $no =  $row['count'];
            }
            return $no;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    public function getMax()
    {
        $no = "";
        try {
            $queryUser = "SELECT MAX(id_beneficiario) as count FROM cat_beneficiario";
            //echo $queryUser;
            $result = $this->conn->prepare($queryUser);
            $result->execute();

            while($row = $result->fetch(PDO::FETCH_ASSOC)){
                $no =  $row['count'];
            }
            return $no;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    public function getAllRegAjax()
    {

        try {
            $queryUser = "SELECT 
            id_beneficiario, rfc, nombre, apepa, apema, curp, estudios, domiciio, 
            tel, DATE_FORMAT(fecha,'%d/%m/%Y') as fecha, id_titulo, id_centrotrabajo, activo, sueldo, DATE_FORMAT(f_nacimiento,'%d/%m/%Y') as f_nacimiento,
            id_puesto, noempleado, email, cuenta_bancaria, segurosocial, estadocivil, id_banco, resguardo,
            celular,contactoemergencia,telemergencia
            FROM cat_beneficiario ORDER BY id_beneficiario DESC";

            $result = $this->conn->prepare($queryUser);
            $result->execute();
        
            return $result;
            
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    public function updateStatus($tipo, $id){
        $correcto = 1;
        $sqlCmd = "UPDATE cat_beneficiario SET activo=".$tipo." WHERE id_beneficiario=".$id;
        try {
            $sqlPrepare = $this->conn->prepare($sqlCmd);
            $sqlPrepare->execute();
            return $correcto;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }

    public function deleteReg($id){
        $correcto = 2;
        $sqlCmd = "DELETE FROM cat_beneficiario WHERE id_beneficiario=".$id;
        try {
            $sqlPrepare = $this->conn->prepare($sqlCmd);
            $sqlPrepare->execute();
            return $correcto;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }

    public function buscaBeneficiario(cBeneficiario $datos){
        // Busca si el Empleado ya existe en la tabla
        $resultado = 0;
        try {
            $sqlCmd = "SELECT id_beneficiario FROM cat_beneficiario WHERE nombre='".$datos->getNombre()."'";
            $sqlPrepare = $this->conn->prepare($sqlCmd);
            $sqlPrepare->execute();
            $resultado = $sqlPrepare->rowCount();
        }
        catch(\PDOException $e)
        {
            $resultado = "Error!: " . $e->getMessage();
        }
        return $resultado;
    }

    public function buscaCoincidencia_Edit(cBeneficiario $datos){
        // Busca si el Empleado repite en la tabla
        $resultado = 0;
        try{
            $sqlCmd = "SELECT id_beneficiario FROM cat_beneficiario WHERE (curp='".$datos->getCurp()."' OR rfc='".$datos->getRFC()."') AND id_beneficiario !=".$datos->getId_beneficiario();
            $sqlPrepare = $this->conn->prepare($sqlCmd);
            $sqlPrepare->execute();
            $resultado = $sqlPrepare->rowCount();
        }
        catch (\PDOException $e){
            $resultado = "Error: ". $e->getMessage();
        }
        return $resultado;
    }
    public function buscaCoincidencia(cBeneficiario $datos){
        // Busca si la el Empleado se repite en la tabla verificando CURP y RFC
        $resultado = 0;
        try{
            $sqlCmd = "SELECT id_beneficiario FROM cat_beneficiario WHERE curp='".$datos->getCurp()."' OR rfc='".$datos->getRFC()."'";
            $sqlPrepare = $this->conn->prepare($sqlCmd);
            $sqlPrepare->execute();
            $resultado = $sqlPrepare->rowCount();
        }
        catch (\PDOException $e){
            $resultado = "Error: ". $e->getMessage();
        }
        return $resultado;
    }
    public function agregarRegistro(cBeneficiario $datos){
        $resultado = 0;
        $baseDatos = $this->conn->conexion();
        $sqlCmd = "INSERT INTO cat_beneficiario(rfc, nombre, apepa, apema, curp, estudios, domiciio, tel, fecha, id_titulo, id_centrotrabajo, activo,
        sueldo, f_nacimiento, id_puesto, noempleado, email, cuenta_bancaria, segurosocial, estadocivil, id_banco, resguardo,
        celular,contactoemergencia,telemergencia
        ) VALUES (".
                    "UPPER('".$datos->getRFC()."')".
                    ",UPPER('".$datos->getNombre()."')".
                    ",UPPER('".$datos->getApepa()."')".
                    ",UPPER('".$datos->getApema()."')".
                    ",UPPER('".$datos->getCurp()."')".
                    ",UPPER('".$datos->getEstudios()."')".
                    ",'".$datos->getDomiciiio()."'".
                    ",'".$datos->getTel()."'".
                    ",'".$datos->getFecha()."'".
                    ",'".$datos->getId_titulo()."'".
                    ",'".$datos->getId_centrotrabajo()."'".
                    ",'".$datos->getActivo()."'".
                    ",'".$datos->getSueldo()."'".
                    ",'".$datos->getF_nacimiento()."'".
                    ",'".$datos->getId_puesto()."'".
                    ",'".$datos->getNoempleado()."'".
                    ",'".$datos->getEmail()."'".
                    ",'".$datos->getCuenta_bancaria()."'".
                    ",'".$datos->getSegurosocial()."'".
                    ",'".$datos->getEstadocivil()."'".
                    ",'".$datos->getId_banco()."'".
                    ",'".$datos->getResguardo()."'".
                    ",'".$datos->getCelular()."'".
                    ",'".$datos->getContactoemergencia()."'".
                    ",'".$datos->getTelemergencia()."')";
        try {
            $sqlPrep = $this->conn->prepare($sqlCmd);
            $baseDatos->beginTransaction();
            $sqlPrep->execute();
            $resultado = $baseDatos->lastInsertId();
            $baseDatos->commit();
        }
        catch(\PDOException $e)
        {
            $baseDatos->rollBack();
            $resultado = "Error!: " . $e->getMessage();
        }
        return $resultado;
    }
    
    public function actualizaRegistro(cBeneficiario $datos){
        $resultado = 1;
        $conexionBd = $this->conn->conexion();
        $sqlCmd = "UPDATE cat_beneficiario". 
                        " SET rfc=UPPER('".$datos->getRFC()."')".
                        ",nombre=UPPER('".$datos->getNombre()."')".
                        ",apepa=UPPER('".$datos->getApepa()."')".
                        ",apema=UPPER('".$datos->getApema()."')".
                        ",curp=UPPER('".$datos->getCurp()."')".
                        ",estudios=UPPER('".$datos->getEstudios()."')".
                        ",domiciio=UPPER('".$datos->getDomiciiio()."')".
                        ",tel='".$datos->getTel()."'".
                        ",fecha='".$datos->getFecha()."'".
                        ",id_titulo='".$datos->getId_titulo()."'".
                        ",id_centrotrabajo='".$datos->getId_centrotrabajo()."'".
                        ",activo='".$datos->getActivo()."'".
                        ",sueldo='".$datos->getSueldo()."'".
                        ",f_nacimiento='".$datos->getF_nacimiento()."'".
                        ",id_puesto='".$datos->getId_puesto()."'".
                        ",noempleado='".$datos->getNoempleado()."'".
                        ",email='".$datos->getEmail()."'".
                        ",cuenta_bancaria='".$datos->getCuenta_bancaria()."'".
                        ",segurosocial='".$datos->getSegurosocial()."'".
                        ",estadocivil='".$datos->getEstadocivil()."'".
                        ",id_banco='".$datos->getId_banco()."'".
                        ",resguardo='".$datos->getResguardo()."'".
                        ",celular='".$datos->getCelular()."'".
                        ",contactoemergencia='".$datos->getContactoemergencia()."'".
                        ",telemergencia='".$datos->getTelemergencia()."'".
                        " WHERE id_beneficiario=".$datos->getId_beneficiario();
        try {
            $sqlPrepare = $this->conn->prepare($sqlCmd);
            $conexionBd->beginTransaction();
            $sqlPrepare->execute();
            $conexionBd->commit();
        }catch (\PDOException $e){
            $conexionBd->rollBack();
            $resultado =  "Error!: " . $e->getMessage();
        }
        return $resultado;
    }
    public function getTitulo(){
        try {
            $queryMP = "SELECT id_titulo, descripcion
                    FROM cat_titulo ";
             echo $queryMP;
            $result = $this->conn->prepare($queryMP);
            $result->execute();
           
            return $result;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    public function getBancos(){
        try {
            $queryMP = "SELECT id_banco, nombre
                    FROM cat_bancos ";
             echo $queryMP;
            $result = $this->conn->prepare($queryMP);
            $result->execute();
           
            return $result;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    public function getPuesto(){
        try {
            $queryMP = "SELECT id_puesto, desc_puesto
                    FROM cat_puesto ";
             echo $queryMP;
            $result = $this->conn->prepare($queryMP);
            $result->execute();
           
            return $result;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    public function agregarAF($id_usuario, $id){
        $correcto = 1;
        $sqlCmd = "UPDATE cat_activo_fijo SET matricula_usuario=".$id_usuario." WHERE id_activo=".$id;
        try {
            $sqlPrepare = $this->conn->prepare($sqlCmd);
            $sqlPrepare->execute();
            return $correcto;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    public function getAF(){
        $sqlCmd = "SELECT id_activo,matricula_activo FROM cat_activo_fijo WHERE matricula_usuario IS NULL OR matricula_usuario = 0";
        try {
            $sqlPrepare = $this->conn->prepare($sqlCmd);
            $sqlPrepare->execute();
            return $sqlPrepare;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    public function getAFbyid($id){
        $sqlCmd = "SELECT id_activo,matricula_activo FROM `cat_activo_fijo` WHERE matricula_usuario=".$id;
        try {
            $sqlPrepare = $this->conn->prepare($sqlCmd);
            $sqlPrepare->execute();
            return $sqlPrepare;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    
    public function getTitulobyid($id){
        $sqlCmd = "SELECT descripcion FROM cat_titulo WHERE id_titulo=".$id." LIMIT 1";
        try {
            $sqlPrepare = $this->conn->prepare($sqlCmd);
            $sqlPrepare->execute();
            return $sqlPrepare;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    public function getCTrabajo(){
        try {
            $queryMP = "SELECT id_centrotrabajo, area
                    FROM cat_centrotrabajo ";
             echo $queryMP;
            $result = $this->conn->prepare($queryMP);
            $result->execute();
           
            return $result;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    public function getPuestobyid($id){
        $sqlCmd = "SELECT desc_puesto FROM cat_puesto WHERE id_puesto=".$id." LIMIT 1";
        try {
            $sqlPrepare = $this->conn->prepare($sqlCmd);
            $sqlPrepare->execute();
            return $sqlPrepare;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    public function getBancobyid($id){
        $sqlCmd = "SELECT nombre FROM cat_bancos WHERE id_banco=".$id." LIMIT 1";
        try {
            $sqlPrepare = $this->conn->prepare($sqlCmd);
            $sqlPrepare->execute();
            return $sqlPrepare;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    public function getCTrabajobyid($id){
        $sqlCmd = "SELECT area FROM cat_centrotrabajo WHERE id_centrotrabajo=".$id." LIMIT 1";
        try {
            $sqlPrepare = $this->conn->prepare($sqlCmd);
            $sqlPrepare->execute();
            return $sqlPrepare;
        }
        catch(\PDOException $e)
        {
            return "Error!: " . $e->getMessage();
        }
    }
    
}
